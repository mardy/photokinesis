# Contributing to the project


## Non-code contributions

Contributions of every type are warmly welcome: whether it's a bug report, a
mention in some public forum, a message with some suggestions or just with a
“*thank you*” line, I cannot overestimate how important such support is to me.
Even a single word of appreciation is a formidable stimulus for me to continue
spending my free time on this project.

Of course, if you like the application and can afford it, a monetary donation
is an even greater stimulus: who knows, if I get many of them maybe one day I
can quit my day job and dedicate all my time on my favourite open source
projects? OK, this is probably daydreaming :-), but anyway, the ways to support
me financially are these:

* Bank account donation to Alberto Mardegan:
  ```
  IBAN: FI53 1820 3500 0221 81
  BIC: NDEAFIHH
  ```

* [Liberapay](https://liberapay.com/mardy)

* [Yandex Wallet](https://money.yandex.ru/to/410015419471966) (accepts credit cards as well)


## Code contributions

Resolutions to bug fixes are always welcome. New features are also welcome, but
please before jumping to the implementation of something, first file an issue
about it and wait for feedback: it may be that someone else is already working
on it, or maybe we have different ideas on how the feature should work.

Also, before accepting code changes, I need you to explicitly state that you
grant me permission to relicence your code under any licence considered as
*free* by the Free Software Foundation, present or future (you can get the
current list [here](https://www.gnu.org/licenses/license-list.html.en)).

