TEMPLATE = subdirs
SUBDIRS = \
    lib \
    plugins \
    src

plugins.depends = lib
src.depends = lib

!CONFIG(nomake_tests) {
    SUBDIRS += tests
    tests.depends = src plugins
}

include(common-config.pri)

icon.files = data/photokinesis.png

CONFIG(desktop) {
    icon.path = $${INSTALL_ICON_DIR}
    INSTALLS += icon
}

