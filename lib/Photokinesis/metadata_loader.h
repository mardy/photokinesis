/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_METADATA_LOADER_H
#define PHOTOKINESIS_METADATA_LOADER_H

#include "global.h"

#include <QObject>
#include <QScopedPointer>
#include <QString>

namespace Photokinesis {

class MetadataLoaderPrivate;
class MetadataLoader: public QObject
{
    Q_OBJECT
    Q_PROPERTY(int pendingRequests READ pendingRequests
               NOTIFY pendingRequestsChanged)
    Q_PROPERTY(int completedRequests READ completedRequests
               NOTIFY metadataLoaded)

public:
    MetadataLoader(QObject *parent = nullptr);
    ~MetadataLoader();

    void loadMetadata(const QString &file, int requestId);

    int pendingRequests() const;
    int completedRequests() const;

Q_SIGNALS:
    void metadataLoaded(int requestId,
                        const QString &title,
                        const QString &description,
                        const QStringList &tags);
    void pendingRequestsChanged();

private:
    QScopedPointer<MetadataLoaderPrivate> d_ptr;
    Q_DECLARE_PRIVATE(MetadataLoader)
};

} // namespace

#endif // PHOTOKINESIS_METADATA_LOADER_H
