/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "utils.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonValue>

using namespace Photokinesis;

QStringList Photokinesis::jsonValueToStringList(const QJsonValue &value)
{
    QStringList list;
    QJsonArray array = value.toArray();
    for (const QJsonValue &v: array) {
        list.append(v.toString());
    }
    return list;
}

QStringList Photokinesis::qvariantToStringList(const QVariant &value)
{
    QStringList list = value.toStringList();
    return list.isEmpty() ?
        jsonValueToStringList(value.toJsonValue()) : list;
}
