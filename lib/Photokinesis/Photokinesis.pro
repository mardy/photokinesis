include (../../common-config.pri)

TEMPLATE = lib
TARGET = Photokinesis

CONFIG += \
    link_pkgconfig \
    qt

QT += \
    concurrent \
    positioning \
    qml \
    quick

!defined(EXIV2_LIBS, var):packagesExist(exiv2) {
    PKGCONFIG += exiv2
} else {
    LIBS += $${EXIV2_LIBS} -lexiv2
    INCLUDEPATH += $${EXIV2_INCLUDEPATH}
}

QMAKE_CXXFLAGS += \
    -fvisibility=hidden

DEFINES += \
    BUILDING_LIBPHOTOKINESIS \
    PLUGIN_DIR=\\\"$${PLUGIN_DIR}\\\"

# Error on undefined symbols
QMAKE_LFLAGS += $$QMAKE_LFLAGS_NOUNDEF

public_headers += \
    abstract_uploader.h AbstractUploader \
    global.h \
    image_processor.h ImageProcessor \
    json_storage.h JsonStorage \
    metadata.h Metadata \
    metadata_loader.h \
    plugin_interface.h PluginInterface \
    plugin_manager.h PluginManager \
    signal_waiter.h SignalWaiter \
    ui_helpers.h UiHelpers \
    upload_model.h UploadModel \
    uploader_profile.h UploaderProfile

private_headers += \
    image_operations.h \
    utils.h

SOURCES += \
    abstract_uploader.cpp \
    image_operations.cpp \
    image_processor.cpp \
    json_storage.cpp \
    metadata.cpp \
    metadata_loader.cpp \
    plugin_manager.cpp \
    signal_waiter.cpp \
    ui_helpers.cpp \
    upload_model.cpp \
    uploader_profile.cpp \
    utils.cpp

HEADERS += \
    $${private_headers} \
    $${public_headers}

headers.files = $${public_headers}
headers.path  = $${INSTALL_PREFIX}/include/$${TARGET}
INSTALLS += headers

RESOURCES += \
    qml/photokinesis-ui.qrc \
    qml/ui_helpers.qrc

target.path = $${INSTALL_PREFIX}/lib
INSTALLS += target
