import QtQuick 2.0
import QtWebView 1.1

WebView {
    id: root

    property var cookies: []

    signal done()

    width: 600
    height: 400
    onLoadingChanged: {
        if (loadRequest.errorString) {
            console.error(loadRequest.errorString)
        }
        if (loadRequest.status == WebView.LoadSucceededStatus) {
            clearCookies(root.cookies)
        }
    }

    Timer {
        id: doneTimer
        interval: 10
        onTriggered: root.done()
    }

    function clearCookies(cookies) {
        for (var i = 0; i < cookies.length; i++) {
            var cookie = cookies[i]
            clearCookie(cookie)
        }
    }

    function clearCookie(cookie) {
        var time = new Date()
        time.setDate(time.getDate() - 1) // yesterday
        //time.setDate(time.getDate() + 1) // tmrw
        var script = "document.cookie = \"" + cookie[0] + "=; expires="
        + time.toLocaleString(Qt.locale("C"), "ddd, dd MMM yyyy HH:mm:ss t") + "; domain=" + cookie[1] + "; path = " + cookie[2] + "; secure\";"
        runJavaScript(script, function(result) {
            console.log("Set cookie: " + result)
            doneTimer.restart()
        })
    }
}
