import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

RowLayout {
    id: root
    Layout.fillWidth: true

    property var _size: uploader.maxSize
    property string _sizeText: qsTr("%1x%2").arg(_size.width).arg(_size.height)

    Label {
        Layout.fillWidth: true
        text: root.isMaxSizeSet() ?
            qsTr("Resizing down to: <i>%1</i>").arg(_sizeText) :
            qsTr("Resizing: no")
    }

    Button {
        text: qsTr("Set a max size…")
        onClicked: PopupUtils.open(Qt.resolvedUrl("SizeDialog.qml"), root, {
            "uploader": uploader,
        })
    }

    function isMaxSizeSet() {
        return _size.width >= 0 || _size.height >= 0
    }
}
