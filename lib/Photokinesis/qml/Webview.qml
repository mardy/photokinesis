import QtQuick 2.0
import QtWebView 1.1

WebView {
    id: root

    property var request: null

    anchors.fill: parent
    url: request ? request.url : ""
    onLoadingChanged: {
        var finalUrl = request.finalUrl.toString()
        if (loadRequest.url.toString().startsWith(finalUrl)) {
            root.request.setResult(loadRequest.url)
        }
        if (loadRequest.errorString) {
            console.error(loadRequest.errorString)
        }
    }
}
