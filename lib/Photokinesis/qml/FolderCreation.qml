import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

RowLayout {
    id: root
    Layout.fillWidth: true
    visible: true // TODO enable only if some item has a directory

    CheckBox {
        Layout.fillWidth: true
        text: qsTr("Re-create folder structure")
        tooltip: qsTr("When uploading entire folders, recreate the same folders")
        checked: uploader.replicatingFolders
        onClicked: uploader.replicatingFolders = checked
    }
}
