/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "abstract_uploader.h"

#include "image_processor.h"
#include "json_storage.h"

#include <QDebug>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QQmlComponent>
#include <QQuickItem>
#include <QSharedPointer>
#include <QtQml>

using namespace Photokinesis;

namespace Photokinesis {

static const QString keyAccountPrefix = QStringLiteral("account-");

class AbstractUploaderPrivate {
    Q_DECLARE_PUBLIC(AbstractUploader)

public:
    AbstractUploaderPrivate(AbstractUploader *q,
                            const UploaderProfile &profile);

    void ensureOptionComponents();
    void createOptionWidgets();

    void updateStoragePath();

private:
    UploaderProfile m_profile;
    QString m_name;
    QString m_displayName;
    QUrl m_icon;
    bool m_isEnabled;
    bool m_needsConfiguration;
    QString m_userId;
    QString m_userName;
    QString m_accountInfo;
    AbstractUploader::Status m_status;
    bool m_usesBrowser = false;
    QNetworkAccessManager *m_nam;
    QString m_errorMessage;
    QStringList m_supportedOptions;
    QJsonArray m_albums;
    QString m_defaultAlbumId;
    QSize m_maxSize;
    bool m_replicatingFolders;
    QQuickItem *m_optionsContainer;
    QStringList m_optionComponentNames;
    QList<QQmlComponent*> m_optionComponents;
    QUrl m_uploadsUrl;
    JsonStorage m_storage;
    AbstractUploader *q_ptr;
};

} // namespace

void AbstractUploaderPrivate::ensureOptionComponents()
{
    Q_Q(AbstractUploader);
    Q_ASSERT(m_optionsContainer);

    if (!m_optionComponents.isEmpty()) return;

    QQmlEngine *engine = qmlEngine(m_optionsContainer);

    for (const QString &fileName: m_optionComponentNames) {
        QQmlComponent *component = new QQmlComponent(engine, fileName, q);
        if (Q_UNLIKELY(component->isError())) {
            qWarning() << "Failed to create component" << fileName <<
                component->errors();
        } else {
            m_optionComponents.append(component);
        }
    }
}

void AbstractUploaderPrivate::createOptionWidgets()
{
    Q_Q(AbstractUploader);
    ensureOptionComponents();

    QQmlContext *context =
        new QQmlContext(QQmlEngine::contextForObject(m_optionsContainer), q);
    context->setContextProperty("uploader", q);

    for (QQmlComponent *component: m_optionComponents) {
        QObject *object = component->create(context);
        QQuickItem *item = qobject_cast<QQuickItem*>(object);
        item->setParentItem(m_optionsContainer);
        item->setParent(m_optionsContainer);
    }
}

AbstractUploaderPrivate::AbstractUploaderPrivate(
        AbstractUploader *q,
        const UploaderProfile &profile):
    m_profile(profile),
    m_isEnabled(true),
    m_needsConfiguration(false),
    m_status(AbstractUploader::NeedsAuthentication),
    m_nam(0),
    m_replicatingFolders(false),
    m_optionsContainer(0),
    q_ptr(q)
{
    m_name = profile.name();
    m_displayName = profile.displayName();
    m_icon = profile.icon();
    m_optionComponentNames = profile.optionComponentNames();
    m_supportedOptions = profile.supportedOptions();

    m_storage.setBase(QStandardPaths::AppConfigLocation);
    updateStoragePath();
}

void AbstractUploaderPrivate::updateStoragePath()
{
    m_storage.setFilePath(m_name + ".json");
}

AbstractUploader::AbstractUploader(const UploaderProfile &profile,
                                   QObject *parent):
    QObject(parent),
    d_ptr(new AbstractUploaderPrivate(this, profile))
{
}

AbstractUploader::~AbstractUploader()
{
}

void AbstractUploader::setName(const QString &name)
{
    Q_D(AbstractUploader);
    d->m_name = name;
    d->updateStoragePath();
}

QString AbstractUploader::name() const
{
    Q_D(const AbstractUploader);
    return d->m_name;
}

void AbstractUploader::setDisplayName(const QString &displayName)
{
    Q_D(AbstractUploader);
    d->m_displayName = displayName;
}

QString AbstractUploader::displayName() const
{
    Q_D(const AbstractUploader);
    return d->m_displayName;
}

void AbstractUploader::setIcon(const QUrl &icon)
{
    Q_D(AbstractUploader);
    d->m_icon = icon;
}

QUrl AbstractUploader::icon() const
{
    Q_D(const AbstractUploader);
    return d->m_icon;
}

QStringList AbstractUploader::supportedOptions() const
{
    Q_D(const AbstractUploader);
    return d->m_supportedOptions;
}

bool AbstractUploader::hasOption(const QString &name) const
{
    Q_D(const AbstractUploader);
    return d->m_supportedOptions.contains(name);
}

void AbstractUploader::setEnabled(bool isEnabled)
{
    Q_D(AbstractUploader);
    if (isEnabled != d->m_isEnabled) {
        d->m_isEnabled = isEnabled;
        Q_EMIT enabledChanged();
    }
}

bool AbstractUploader::isEnabled() const
{
    Q_D(const AbstractUploader);
    return d->m_isEnabled;
}

void AbstractUploader::setUserId(const QString &userId)
{
    Q_D(AbstractUploader);
    if (userId == d->m_userId) return;
    d->m_userId = userId;
    Q_EMIT userIdChanged();
    if (d->m_userName.isEmpty()) {
        Q_EMIT userNameChanged();
    }
}

QString AbstractUploader::userId() const
{
    Q_D(const AbstractUploader);
    return d->m_userId;
}

void AbstractUploader::setUserName(const QString &userName)
{
    Q_D(AbstractUploader);
    if (userName == d->m_userName) return;
    d->m_userName = userName;
    Q_EMIT userNameChanged();
}

QString AbstractUploader::userName() const
{
    Q_D(const AbstractUploader);
    return !d->m_userName.isEmpty() ? d->m_userName : d->m_userId;
}

void AbstractUploader::setAccountInfo(const QString &accountInfo)
{
    Q_D(AbstractUploader);
    d->m_accountInfo = accountInfo;
    Q_EMIT accountInfoChanged();
}

QString AbstractUploader::accountInfo() const
{
    Q_D(const AbstractUploader);
    return d->m_accountInfo;
}

void AbstractUploader::setStatus(Status status)
{
    Q_D(AbstractUploader);
    if (status != d->m_status) {
        d->m_status = status;
        Q_EMIT statusChanged();
    }
}

AbstractUploader::Status AbstractUploader::status() const
{
    Q_D(const AbstractUploader);
    return d->m_status;
}

void AbstractUploader::setUsesBrowser(bool usesBrowser)
{
    Q_D(AbstractUploader);
    d->m_usesBrowser = usesBrowser;
}

bool AbstractUploader::usesBrowser() const
{
    Q_D(const AbstractUploader);
    return d->m_usesBrowser;
}

void AbstractUploader::setNeedsConfiguration(bool needsConfiguration)
{
    Q_D(AbstractUploader);
    if (needsConfiguration != d->m_needsConfiguration) {
        d->m_needsConfiguration = needsConfiguration;
        Q_EMIT statusChanged();
    }
}

bool AbstractUploader::needsConfiguration() const
{
    Q_D(const AbstractUploader);
    return d->m_needsConfiguration;
}

void AbstractUploader::setUploadsUrl(const QUrl &uploadsUrl)
{
    Q_D(AbstractUploader);
    d->m_uploadsUrl = uploadsUrl;
    Q_EMIT uploadsUrlChanged();
}

QUrl AbstractUploader::uploadsUrl() const
{
    Q_D(const AbstractUploader);
    return d->m_uploadsUrl;
}

/*
 * Mandatory fields are "name" and "albumId"
 */
void AbstractUploader::setAlbums(const QJsonArray &albums)
{
    Q_D(AbstractUploader);
    d->m_albums = albums;
    Q_EMIT albumsChanged();
}

QJsonArray AbstractUploader::albums() const
{
    Q_D(const AbstractUploader);
    return d->m_albums;
}

void AbstractUploader::setDefaultAlbumId(const QString &id)
{
    Q_D(AbstractUploader);
    d->m_defaultAlbumId = id;
    Q_EMIT defaultAlbumIdChanged();
}

QString AbstractUploader::defaultAlbumId() const
{
    Q_D(const AbstractUploader);
    return d->m_defaultAlbumId;
}

void AbstractUploader::setMaxSize(const QSize &size)
{
    Q_D(AbstractUploader);
    d->m_maxSize = size;
    Q_EMIT maxSizeChanged();
}

QSize AbstractUploader::maxSize() const
{
    Q_D(const AbstractUploader);
    return d->m_maxSize;
}

void AbstractUploader::setReplicatingFolders(bool replicatingFolders)
{
    Q_D(AbstractUploader);
    d->m_replicatingFolders = replicatingFolders;
    Q_EMIT replicatingFoldersChanged();
}

bool AbstractUploader::replicatingFolders() const
{
    Q_D(const AbstractUploader);
    return d->m_replicatingFolders;
}

void AbstractUploader::setOptionsContainer(QQuickItem *container)
{
    Q_D(AbstractUploader);
    if (container == d->m_optionsContainer) return;

    d->m_optionsContainer = container;
    if (container) {
        d->createOptionWidgets();
    }
    Q_EMIT optionsContainerChanged();
}

QQuickItem *AbstractUploader::optionsContainer() const
{
    Q_D(const AbstractUploader);
    return d->m_optionsContainer;
}

void AbstractUploader::setOptionComponents(const QStringList &components)
{
    Q_D(AbstractUploader);

    // We don't allow changing components, for simplicity
    Q_ASSERT(d->m_optionComponentNames.isEmpty());

    d->m_optionComponentNames = components;
}

void AbstractUploader::setNetworkAccessManager(QNetworkAccessManager *nam)
{
    Q_D(AbstractUploader);
    d->m_nam = nam;
}

QNetworkAccessManager *AbstractUploader::networkAccessManager() const
{
    Q_D(const AbstractUploader);
    return d->m_nam;
}

void AbstractUploader::setErrorMessage(const QString &message)
{
    Q_D(AbstractUploader);
    d->m_errorMessage = message;
}

QString AbstractUploader::errorMessage() const
{
    Q_D(const AbstractUploader);
    return d->m_errorMessage;
}

void AbstractUploader::processImage(const QUrl &url,
                                    const QJsonObject &options)
{
    Q_D(const AbstractUploader);

    QFileInfo fileInfo(url.toLocalFile());
    QJsonObject uploadOptions(options);
    uploadOptions.insert("fileName", fileInfo.fileName());

    if (d->m_maxSize.width() < 0 && d->m_maxSize.height() < 0) {
        QMetaObject::invokeMethod(this, "startUpload",
                                  Qt::QueuedConnection,
                                  Q_ARG(QString, fileInfo.filePath()),
                                  Q_ARG(QJsonObject, uploadOptions));
        return;
    }

    ImageProcessor *processor = ImageProcessor::instance();
    ImageProcessor::Request request(fileInfo.filePath());
    request.setMaxSize(d->m_maxSize);

    int id = processor->addRequest(request);
    QObject::connect(processor, &ImageProcessor::finished,
                     this, [this,id,uploadOptions](int requestId,
                                                   const QString outputFile) {
        if (requestId != id) return;
        startUpload(outputFile, uploadOptions);
    });
    processor->start();
}

void AbstractUploader::logout()
{
    setUserId(QString());
    setAccountInfo(QString());
    setDefaultAlbumId(QString());
    setAlbums(QJsonArray());
    setStatus(AbstractUploader::NeedsAuthentication);
}

void AbstractUploader::authenticate(QQuickItem *parent)
{
    Q_ASSERT(parent);
    setStatus(AbstractUploader::Ready);
}

void AbstractUploader::uploadFile(const QUrl &url,
                                  const QJsonObject &options)
{
    processImage(url, options);
}

void AbstractUploader::changeAccount(QQuickItem *parent)
{
    QSharedPointer<QMetaObject::Connection> connection(new QMetaObject::Connection);
    *connection =
        QObject::connect(this, &AbstractUploader::statusChanged,
                         this, [this, parent, connection]() {
        if (status() == AbstractUploader::NeedsAuthentication) {
            QObject::disconnect(*connection);
            authenticate(parent);
        }
    });

    setStatus(AbstractUploader::Busy);
    logout();
}

QStringList AbstractUploader::knownUserIds() const
{
    Q_D(const AbstractUploader);
    QStringList ids;
    QStringList keys = d->m_storage.loadObject().keys();
    for (const QString &key: keys) {
        if (key.startsWith(keyAccountPrefix)) {
            ids.append(key.mid(keyAccountPrefix.length()));
        }
    }
    return ids;
}

void AbstractUploader::storeAccountSettings(const QJsonObject &settings)
{
    Q_D(AbstractUploader);
    QJsonObject allSettings(d->m_storage.loadObject());
    allSettings.insert(keyAccountPrefix + d->m_userId, settings);
    allSettings.insert("lastAccount", d->m_userId);
    d->m_storage.storeObject(allSettings);
}

QJsonObject AbstractUploader::loadAccountSettings() const
{
    Q_D(const AbstractUploader);
    QJsonObject allSettings(d->m_storage.loadObject());
    QString userId = d->m_userId.isEmpty() ?
        allSettings.value("lastAccount").toString() : d->m_userId;
    return allSettings.value(keyAccountPrefix + userId).toObject();
}

void AbstractUploader::storeSettings()
{
    const QJsonObject settings = getSettings();
    storeAccountSettings(settings);
}

void AbstractUploader::loadSettings()
{
    const QJsonObject settings = loadAccountSettings();
    setSettings(settings);
}

void AbstractUploader::setSettings(const QJsonObject &settings)
{
    setDefaultAlbumId(settings.value("defaultAlbumId").toString());
    QJsonValue sizeValue = settings.value("maxSize");
    if (sizeValue.isObject()) {
        QJsonObject sizeObject = sizeValue.toObject();
        setMaxSize(QSize(sizeObject.value("width").toInt(),
                         sizeObject.value("height").toInt()));
    }
    setReplicatingFolders(settings.value("replicatingFolders").toBool());
}

QJsonObject AbstractUploader::getSettings() const
{
    Q_D(const AbstractUploader);

    QJsonValue sizeValue;
    if (d->m_maxSize.width() >= 0 || d->m_maxSize.height() >= 0) {
        sizeValue = QJsonObject {
            { "width", d->m_maxSize.width() },
            { "height", d->m_maxSize.height() },
        };
    }

    QJsonObject settings {
        { "defaultAlbumId", defaultAlbumId() },
        { "maxSize", sizeValue },
        { "replicatingFolders", replicatingFolders() },
    };
    return settings;
}

QVariantMap AbstractUploader::checkSize(const QSize &size)
{
    /* In case the proposed size is not acceptable, implementations should
     * add an "error" field containing a message for the user, and a
     * "size" member with the closest acceptable size. */
    return QVariantMap {
        { "size", size },
    };
}
