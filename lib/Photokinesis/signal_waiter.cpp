/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "signal_waiter.h"

#include <QDebug>

using namespace Photokinesis;

namespace Photokinesis {

class SignalWaiterPrivate {
    Q_DECLARE_PUBLIC(SignalWaiter)

public:
    SignalWaiterPrivate(const char *signalName, SignalWaiter *q);

    void _q_oneFinished();

private:
    const char *m_signal;
    int m_remaining;
    SignalWaiter *q_ptr;
};

} // namespace

SignalWaiterPrivate::SignalWaiterPrivate(const char *signalName,
                                         SignalWaiter *q):
    m_signal(signalName),
    m_remaining(0),
    q_ptr(q)
{
}

void SignalWaiterPrivate::_q_oneFinished()
{
    Q_Q(SignalWaiter);
    m_remaining--;
    if (m_remaining == 0) {
        Q_EMIT q->finished();
    }
}

SignalWaiter::SignalWaiter(const char *signalName, QObject *parent):
    QObject(parent),
    d_ptr(new SignalWaiterPrivate(signalName, this))
{
}

SignalWaiter::~SignalWaiter()
{
}

void SignalWaiter::add(QObject *object, const char *signalName)
{
    Q_D(SignalWaiter);
    d->m_remaining++;
    QObject::connect(object, signalName ? signalName : d->m_signal,
                     this, SLOT(_q_oneFinished()));
}

#include "moc_signal_waiter.cpp"
