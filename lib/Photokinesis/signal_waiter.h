/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_SIGNAL_WAITER_H
#define PHOTOKINESIS_SIGNAL_WAITER_H

#include "global.h"

#include <QObject>
#include <QScopedPointer>

namespace Photokinesis {

class SignalWaiterPrivate;
class PK_EXPORT SignalWaiter: public QObject
{
    Q_OBJECT

public:
    SignalWaiter(const char *signalName = 0, QObject *parent = 0);
    ~SignalWaiter();

    void add(QObject *object, const char *signalName = 0);

Q_SIGNALS:
    void finished();

private:
    QScopedPointer<SignalWaiterPrivate> d_ptr;
    Q_DECLARE_PRIVATE(SignalWaiter)
    Q_PRIVATE_SLOT(d_func(), void _q_oneFinished())
};

} // namespace

#endif // PHOTOKINESIS_SIGNAL_WAITER_H
