/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_IMAGE_PROCESSOR_H
#define PHOTOKINESIS_IMAGE_PROCESSOR_H

#include "global.h"

#include <QObject>
#include <QScopedPointer>
#include <QSharedPointer>
#include <QSize>
#include <QString>

namespace Photokinesis {

class ImageProcessorPrivate;
class PK_EXPORT ImageProcessor: public QObject
{
    Q_OBJECT

    class RequestPrivate;
public:
    class Request {
    public:
        Request(const QString &filePath);
        QString filePath() const;

        void setMaxSize(const QSize &size);
        QSize maxSize() const;

    private:
        friend class ImageProcessor;
        friend class ImageProcessorPrivate;
        QSharedPointer<RequestPrivate> d;
    };

    ImageProcessor(QObject *parent = 0);
    ~ImageProcessor();

    static ImageProcessor *instance();

    int addRequest(const Request &request);

    /* Tells the ImageProcessor that the file is no longer needed
     * and can be deleted. Deletion will happen when all clients
     * whose request resulted in this file have released it. */
    void releaseFile(const QString &outputFile);

    void start();
    void stopAndClear();

Q_SIGNALS:
    void finished(int request, const QString outputFile);

private:
    Q_DECLARE_PRIVATE(ImageProcessor)
    QScopedPointer<ImageProcessorPrivate> d_ptr;
};

} // namespace

#endif // PHOTOKINESIS_IMAGE_PROCESSOR_H
