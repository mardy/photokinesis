/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_processor.h"

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QList>
#include <QMap>
#include <QTemporaryDir>
#include <QtConcurrent>
#include "image_operations.h"

using namespace Photokinesis;

namespace Photokinesis {

class ImageProcessor::RequestPrivate {
public:
    RequestPrivate(const QString &filePath): m_filePath(filePath) {}

    QString m_filePath;
    QSize m_maxSize;
    int m_id;
};

class ImageProcessorPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(ImageProcessor)

public:
    ImageProcessorPrivate(ImageProcessor *q);
    ~ImageProcessorPrivate();

    void execRequest(ImageProcessor::Request &request);

    void start();
    void stopAndClear();
    void processRemaining();

private:
    static ImageProcessor *m_instance;
    QTemporaryDir m_tmpDir;
    int m_nextRequestId;
    QMap<int,ImageProcessor::Request> m_requests;
    QList<ImageProcessor::Request> m_activeRequests;
    QFutureWatcher<void> m_watcher;
    ImageProcessor *q_ptr;
};

ImageProcessor *ImageProcessorPrivate::m_instance = nullptr;

} // namespace

ImageProcessor::Request::Request(const QString &filePath):
    d(new ImageProcessor::RequestPrivate(filePath))
{
}

QString ImageProcessor::Request::filePath() const
{
    return d->m_filePath;
}

void ImageProcessor::Request::setMaxSize(const QSize &size)
{
    d->m_maxSize = size;
}

QSize ImageProcessor::Request::maxSize() const
{
    return d->m_maxSize;
}

ImageProcessorPrivate::ImageProcessorPrivate(ImageProcessor *q):
    QObject(q),
    m_nextRequestId(0),
    q_ptr(q)
{
    m_instance = q;
    QObject::connect(&m_watcher, &QFutureWatcher<void>::finished,
                     this, &ImageProcessorPrivate::processRemaining);
}

ImageProcessorPrivate::~ImageProcessorPrivate()
{
    stopAndClear();
    m_instance = nullptr;
}

void ImageProcessorPrivate::execRequest(ImageProcessor::Request &request)
{
    Q_Q(ImageProcessor);

    QFileInfo fileInfo(request.filePath());
    QString destFile =
        m_tmpDir.filePath(QString("%1-%2").
                          arg(request.d->m_id).
                          arg(fileInfo.fileName()));
    bool ok = ImageOperations::ensureMaxSize(request.filePath(),
                                             destFile,
                                             request.maxSize());

    Q_EMIT q->finished(request.d->m_id, ok ? destFile : QString());
}

void ImageProcessorPrivate::start()
{
    m_activeRequests = m_requests.values();
    if (m_activeRequests.isEmpty()) return;
    m_requests.clear();

    QFuture<void> future =
        QtConcurrent::map(m_activeRequests,
                          [this](ImageProcessor::Request &request) {
            execRequest(request);
        });
    m_watcher.setFuture(future);
}

void ImageProcessorPrivate::stopAndClear()
{
    m_watcher.cancel();
    m_watcher.waitForFinished();
    m_requests.clear();
}

void ImageProcessorPrivate::processRemaining()
{
    m_activeRequests.clear();
    start();
}

ImageProcessor::ImageProcessor(QObject *parent):
    QObject(parent),
    d_ptr(new ImageProcessorPrivate(this))
{
}

ImageProcessor::~ImageProcessor()
{
}

ImageProcessor *ImageProcessor::instance()
{
    if (!ImageProcessorPrivate::m_instance) {
        ImageProcessorPrivate::m_instance = new ImageProcessor;
    }
    return ImageProcessorPrivate::m_instance;
}

int ImageProcessor::addRequest(const Request &request)
{
    Q_D(ImageProcessor);
    int requestId = ++d->m_nextRequestId;
    const auto i = d->m_requests.insert(requestId, request);
    Request &req = i.value();
    req.d->m_id = requestId;
    return requestId;
}

void ImageProcessor::releaseFile(const QString &outputFile)
{
    /* Given that at the moment we don't support sharing output files among
     * different clients, we can safely delete it
     */
    QFile::remove(outputFile);
}

void ImageProcessor::start()
{
    Q_D(ImageProcessor);
    if (!d->m_activeRequests.isEmpty()) {
        qDebug() << "ImageProcessor is busy";
        return;
    }
    d->start();
}

void ImageProcessor::stopAndClear()
{
    Q_D(ImageProcessor);
    d->stopAndClear();
}

#include "image_processor.moc"
