/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader_profile.h"

#include <QJsonArray>
#include <QJsonValue>
#include <QObject>

using namespace Photokinesis;

namespace Photokinesis {

static const QString keyPluginName = QStringLiteral("plugin");
static const QString keyName = QStringLiteral("name");
static const QString keyDisplayName = QStringLiteral("displayName");
static const QString keyIcon = QStringLiteral("icon");
static const QString keyOptionComponents = QStringLiteral("optionComponents");
static const QString keySupportedOptions = QStringLiteral("supportedOptions");

} // namespace

UploaderProfile::UploaderProfile(const QJsonObject &object):
    QJsonObject(object)
{
}

UploaderProfile::UploaderProfile(const QString &pluginName,
                                 const QJsonObject &object):
    QJsonObject(object)
{
    insert(keyPluginName, pluginName);
}

UploaderProfile::~UploaderProfile()
{
}

QString UploaderProfile::pluginName() const
{
    return value(keyPluginName).toString();
}

QString UploaderProfile::name() const
{
    return value(keyName).toString();
}

QString UploaderProfile::name(const QJsonObject &metadata)
{
    return metadata.value(keyName).toString();
}

QString UploaderProfile::displayName() const
{
    QString displayName = value(keyDisplayName).toString();
    return QObject::tr(displayName.toUtf8().constData());
}

QUrl UploaderProfile::icon() const
{
    return QUrl(value(keyIcon).toString());
}

QStringList UploaderProfile::optionComponentNames() const
{
    QStringList ret;
    QJsonArray jsonComponents = value(keyOptionComponents).toArray();
    for (const QJsonValue &v: jsonComponents) {
        ret.append(v.toString());
    }
    return ret;
}

QStringList UploaderProfile::supportedOptions() const
{
    QStringList ret;
    QJsonArray jsonOptions = value(keySupportedOptions).toArray();
    for (const QJsonValue &v: jsonOptions) {
        ret.append(v.toString());
    }
    return ret;
}
