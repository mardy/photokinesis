/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "upload_model.h"

#include <QDebug>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonValue>
#include <QJSValue>
#include <QMetaEnum>
#include <QVector>
#include "metadata_loader.h"
#include "utils.h"

using namespace Photokinesis;

namespace Photokinesis {

static const int optionRoleStart = UploadModel::TitleRole;
static const int optionRolesCount =
    UploadModel::TagsRole - optionRoleStart + 1;

typedef QVector<UploadModel::UploadStatus> UploadStatuses;

static QVariant jsonToVariant(const QJsonValue &v, int role)
{
    if (v.isNull()) return QVariant();

    switch (role) {
    case UploadModel::TitleRole:
    case UploadModel::DescriptionRole:
        return v.toString();
    case UploadModel::TagsRole:
        return jsonValueToStringList(v);
    case UploadModel::UploadStatusRole:
        {
            QMetaEnum meta =
                QMetaEnum::fromType<UploadModel::UploadStatus>();
            int value = meta.keyToValue(v.toString().toUtf8().constData());
            return UploadModel::UploadStatus(value);
        }
    default:
        qWarning() << "Unhandled role" << role;
    }
    return QVariant();
}

class UploaderOptions
{
public:
    void setUploadersCount(int uploadersCount) {
        m_options.resize((uploadersCount + 1) * optionRolesCount);
    }
    int columnCount() const { return m_options.size() / optionRolesCount; }
    int uploadersCount() const { return columnCount() - 1; }

    int roleToIndex(int role) const { return role - optionRoleStart; }
    QVector<QVariant> options(int role) const {
        int columnCount = this->columnCount();
        return m_options.mid(roleToIndex(role) * columnCount,
                             columnCount);
    }

    void setOption(int role, int column, const QVariant &value) {
        m_options[column * optionRolesCount + roleToIndex(role)] = value;
    }
    QVariant option(int role, int column) const {
        return m_options[column * optionRolesCount + roleToIndex(role)];
    }

private:
    QVector<QVariant> m_options;
};

class Upload
{
public:
    Upload(const QUrl &url,
           const QUrl &baseUrl,
           int m_uploadersCount);

    QUrl url() const { return QUrl::fromLocalFile(filePath()); }
    QString filePath() const { return m_fileInfo.filePath(); }
    QString fileName() const { return m_fileInfo.fileName(); }
    QString photoId() const { return m_photoId; }

    void setSubDirectory(const QString &path) { m_subDirectory = path; }
    QString subDirectory() const { return m_subDirectory; }

    void setTitle(const QString &title) {
        m_options.setOption(UploadModel::TitleRole, 0, title);
    }
    QString title() const {
        return m_options.option(UploadModel::TitleRole, 0).toString();
    }

    void setDescription(const QString &description) {
        m_options.setOption(UploadModel::DescriptionRole, 0, description);
    }
    QString description() const {
        return m_options.option(UploadModel::DescriptionRole, 0).toString();
    }

    void addTag(const QString &tag) {
        QStringList tags = this->tags();
        if (!tags.contains(tag)) { tags.append(tag); setTags(tags); }
    }
    void removeTag(const QString &tag) {
        QStringList tags = this->tags();
        if (!tags.contains(tag)) { tags.removeOne(tag); setTags(tags); }
    }
    void setTags(const QStringList &tags) {
        m_options.setOption(UploadModel::TagsRole, 0, tags);
    }
    QStringList tags() const {
        return m_options.option(UploadModel::TagsRole, 0).toStringList();
    }

    void setPhotoId(const QString &id) { m_photoId = id; }
    void setStatus(int uploaderIndex, UploadModel::UploadStatus status) {
        m_statuses[uploaderIndex] = status;
    }
    UploadModel::UploadStatus status(int column) const {
        UploadModel::UploadStatus value = rawStatus(column);
        if (value > UploadModel::Uploading &&
            value < UploadModel::Uploaded) {
            value = UploadModel::Uploading;
        }
        return value;
    }
    int progress(int column) const {
        int value = rawStatus(column);
        return qMax(qMin(value, 100), 0);
    }

    void setUploadersCount(int count) {
        m_statuses = UploadStatuses(count, UploadModel::Ready);
        m_options.setUploadersCount(count);
    }

    void setOption(int role, int column, const QVariant &value) {
        m_options.setOption(role, column, value);
    }
    QVariant option(int role, int column) const {
        return m_options.option(role, column);
    }

    QJsonObject config(const QHash<int, QByteArray> &roleNames,
                       int column) const {
        QJsonObject ret;
        for (int i = 0; i < optionRolesCount; i++) {
            int role = i + optionRoleStart;
            QVariant value = option(role, column);
            if (!value.isValid() && column > 0) {
                value = option(role, 0);
            }
            if (!value.isValid()) continue;
            ret.insert(roleNames[role], QJsonValue::fromVariant(value));
        }

        // Add more useful data
        ret.insert("subDirectory", subDirectory());
        return ret;
    }

private:
    UploadModel::UploadStatus rawStatus(int column) const {
        return column == 0 ? UploadModel::Ready : m_statuses[column - 1];
    }

private:
    QFileInfo m_fileInfo;
    QString m_subDirectory;
    QString m_photoId;
    UploadStatuses m_statuses;
    UploaderOptions m_options;
};

class UploadModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(UploadModel)

public:
    UploadModelPrivate(UploadModel *q);
    ~UploadModelPrivate();

    void onMetadataLoaded(int requestId,
                          const QString &title,
                          const QString &description,
                          const QStringList &tags);
    inline void emitDataChanged(const QModelIndex &topLeft,
                                const QModelIndex &bottomRight);

private:
    int m_uploadersCount;
    bool m_isLoadingSession;
    MetadataLoader m_metadataLoader;
    QHash<int, QByteArray> m_roles;
    QList<Upload> m_uploads;
    UploadModel *q_ptr;
};

} // namespace

Upload::Upload(const QUrl &url, const QUrl &baseUrl,
               int uploadersCount):
    m_fileInfo(url.toLocalFile()),
    m_statuses(UploadStatuses(uploadersCount, UploadModel::Ready))
{
    if (!baseUrl.isEmpty()) {
        QString basePath = baseUrl.toLocalFile();
        QString filePath = m_fileInfo.path();
        if (filePath.startsWith(basePath)) {
            int offset = basePath.length();
            if (filePath[offset] == '/') offset++;
            m_subDirectory = filePath.mid(offset);
        }
    }

    m_options.setUploadersCount(uploadersCount);
}

UploadModelPrivate::UploadModelPrivate(UploadModel *q):
    QObject(q),
    m_uploadersCount(0),
    m_isLoadingSession(false),
    q_ptr(q)
{
    m_roles[UploadModel::UrlRole] = "url";
    m_roles[UploadModel::FileNameRole] = "fileName";
    m_roles[UploadModel::ProgressRole] = "progress";
    m_roles[UploadModel::UploadStatusRole] = "uploadStatus";
    m_roles[UploadModel::TitleRole] = "title";
    m_roles[UploadModel::DescriptionRole] = "description";
    m_roles[UploadModel::TagsRole] = "tags";
    m_roles[UploadModel::ConfigRole] = "config";
    m_roles[UploadModel::SubDirectoryRole] = "subDirectory";

    qRegisterMetaTypeStreamOperators<UploadModel::UploadStatus>();
    qRegisterMetaTypeStreamOperators<UploadModel::Roles>();

    QObject::connect(&m_metadataLoader, &MetadataLoader::metadataLoaded,
                     this, &UploadModelPrivate::onMetadataLoaded);
}

UploadModelPrivate::~UploadModelPrivate()
{
}

void UploadModelPrivate::onMetadataLoaded(int requestId,
                                          const QString &title,
                                          const QString &description,
                                          const QStringList &tags)
{
    Upload &upload = m_uploads[requestId];
    upload.setTitle(title);
    upload.setDescription(description);
    upload.setTags(tags);
}

void UploadModelPrivate::emitDataChanged(const QModelIndex &topLeft,
                                         const QModelIndex &bottomRight)
{
    Q_Q(UploadModel);
    /* if the change occurs on the first column, then we must notify all
     * columns of the change */
    if (topLeft.column() == 0) {
        QModelIndex br = bottomRight.sibling(bottomRight.row(),
                                             m_uploadersCount);
        Q_EMIT q->dataChanged(topLeft, br);
    } else {
        Q_EMIT q->dataChanged(topLeft, bottomRight);
    }
}

UploadModel::UploadModel(QObject *parent):
    QAbstractItemModel(parent),
    d_ptr(new UploadModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsRemoved(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));
    QObject::connect(this, SIGNAL(rowsInserted(const QModelIndex&,int,int)),
                     this, SIGNAL(countChanged()));

    QObject::connect(this, &UploadModel::countChanged,
                     this, &UploadModel::configChanged);
    QObject::connect(this, &QAbstractItemModel::dataChanged,
                     this, &UploadModel::configChanged);
}

UploadModel::~UploadModel()
{
}

void UploadModel::setUploadersCount(int uploadersCount)
{
    Q_D(UploadModel);
    if (d->m_uploadersCount == uploadersCount) return;

    for (Upload &upload: d->m_uploads) {
        upload.setUploadersCount(uploadersCount);
    }

    d->m_uploadersCount = uploadersCount;
    Q_EMIT uploadersCountChanged();
}

int UploadModel::uploadersCount() const
{
    Q_D(const UploadModel);
    return d->m_uploadersCount;
}

void UploadModel::setConfig(const QJsonObject &config)
{
    // The model must be empty
    Q_ASSERT(rowCount() == 0);

    setUploadersCount(config["uploadersCount"].toInt());
    QJsonArray rows = config["rows"].toArray();
    int i_row = 0;
    for (const QJsonValue &v: rows) {
        QJsonArray row = v.toArray();
        bool firstColumn = true;
        int i_col = 0;
        for (const QJsonValue &v: row) {
            QJsonObject cellData = v.toObject();
            if (firstColumn) {
                firstColumn = false;
                addPhoto(QUrl(cellData["url"].toString()));
            }

            QModelIndex index = this->index(i_row, i_col);
            for (auto i = cellData.constBegin();
                 i != cellData.constEnd();
                 i++) {
                if (i.key() == "url") continue; // already handled

                int role = roleNames().key(i.key().toLatin1(), -1);
                setData(index, jsonToVariant(i.value(), role), role);
            }
            i_col++;
        }
        i_row++;
    }
}

QJsonObject UploadModel::config() const
{
    Q_D(const UploadModel);

    QJsonObject conf {
        { "uploadersCount", uploadersCount() },
    };

    static const Roles globalRoles[] = {
        UrlRole,
        TitleRole,
        DescriptionRole,
        TagsRole,
    };
    static const Roles uploaderRoles[] = {
        UploadStatusRole,
        TitleRole,
        DescriptionRole,
        TagsRole,
    };

    QJsonArray rows;
    for (int row = 0; row < rowCount(); row++) {
        QJsonArray rowData;
        // handle the first column separately
        {
            QModelIndex index = this->index(row, 0);
            QJsonObject cellData;
            for (size_t i = 0; i < sizeof(globalRoles)/sizeof(Roles); i++) {
                Roles role = globalRoles[i];
                QJsonValue value = QJsonValue::fromVariant(index.data(role));
                cellData.insert(d->m_roles[role], value);
            }
            rowData.append(cellData);
        }

        for (int col = 1; col < columnCount(); col++) {
            QModelIndex index = this->index(row, col);
            QJsonObject cellData;
            for (size_t i = 0; i < sizeof(uploaderRoles)/sizeof(Roles); i++) {
                Roles role = uploaderRoles[i];
                QJsonValue value = QJsonValue::fromVariant(index.data(role));
                // skip null values
                if (!value.isNull()) {
                    cellData.insert(d->m_roles[role], value);
                }
            }
            rowData.append(cellData);
        }
        rows.append(rowData);
    }
    conf.insert("rows", rows);
    return conf;
}

void UploadModel::setIsLoadingSession(bool isLoadingSession)
{
    Q_D(UploadModel);
    d->m_isLoadingSession = isLoadingSession;
}

bool UploadModel::isLoadingSession() const
{
    Q_D(const UploadModel);
    return d->m_isLoadingSession;
}

void UploadModel::addPhoto(const QUrl &url)
{
    addPhotos({ url });
}

void UploadModel::addPhotos(const QList<QUrl> &urls, const QUrl &baseUrl)
{
    Q_D(UploadModel);

    if (urls.isEmpty()) return;

    QModelIndex parent;
    int row = d->m_uploads.count();
    beginInsertRows(parent, row, row + urls.count() - 1);
    for (const QUrl &url: urls) {
        int requestId = row++;
        if (!d->m_isLoadingSession) {
            d->m_metadataLoader.loadMetadata(url.toLocalFile(), requestId);
        }
        d->m_uploads.append(Upload(url, baseUrl,
                                   d->m_uploadersCount));
    }
    endInsertRows();
}

void UploadModel::clear()
{
    Q_D(UploadModel);

    beginResetModel();
    d->m_uploads.clear();
    endResetModel();
}

QVariantMap UploadModel::get(int row, int column) const
{
    Q_D(const UploadModel);
    QVariantMap ret;
    for (auto i = d->m_roles.constBegin(); i != d->m_roles.constEnd(); i++) {
        ret.insert(i.value(), data(index(row, column), i.key()));
    }
    return ret;
}

QVariant UploadModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

QVariant UploadModel::get(int row, int column, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, column), role);
}

bool UploadModel::set(int row, int column, const QString &roleName,
                      const QJsonValue &value)
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return setData(index(row, column), value, role);
}

void UploadModel::setTitle(int row, const QString &title)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count()) return;
    Upload &u = d->m_uploads[row];
    u.setTitle(title);
    QModelIndex modelIndex = index(row, 0);
    d->emitDataChanged(modelIndex, modelIndex);
}

void UploadModel::setDescription(int row, const QString &description)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count()) return;
    Upload &u = d->m_uploads[row];
    u.setDescription(description);
    QModelIndex modelIndex = index(row, 0);
    d->emitDataChanged(modelIndex, modelIndex);
}

void UploadModel::addTag(int row, const QString &tag)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count() || tag.isEmpty()) return;
    Upload &u = d->m_uploads[row];
    u.addTag(tag.trimmed());
    QModelIndex modelIndex = index(row, 0);
    d->emitDataChanged(modelIndex, modelIndex);
}

void UploadModel::removeTag(int row, const QString &tag)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count()) return;
    Upload &u = d->m_uploads[row];
    u.removeTag(tag);
    QModelIndex modelIndex = index(row, 0);
    d->emitDataChanged(modelIndex, modelIndex);
}

void UploadModel::setProgress(int row, int uploaderIndex, int progress)
{
    setStatus(row, uploaderIndex, static_cast<UploadStatus>(progress));
}

void UploadModel::setStatus(int row, int uploaderIndex, UploadStatus status)
{
    Q_D(UploadModel);

    if (row < 0 || row >= d->m_uploads.count()) return;
    Upload &u = d->m_uploads[row];
    u.setStatus(uploaderIndex, status);

    QModelIndex modelIndex = index(row, uploaderIndex + 1);
    d->emitDataChanged(modelIndex, modelIndex);
}

void UploadModel::resetFailures()
{
    Q_D(UploadModel);

    int itemCount = d->m_uploads.count();
    for (int row = 0; row < itemCount; row++) {
        Upload &u = d->m_uploads[row];
        for (int i = 0; i < d->m_uploadersCount; i++) {
            if (u.status(i + 1) == Failed) {
                setStatus(row, i, Ready);
            }
        }
    }
}

int UploadModel::columnCount(const QModelIndex &parent) const
{
    Q_D(const UploadModel);
    return parent.isValid() ? 0 : (d->m_uploadersCount + 1);
}

int UploadModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const UploadModel);
    Q_UNUSED(parent);
    return d->m_uploads.count();
}

QModelIndex UploadModel::index(int row, int column,
                               const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return createIndex(row, column);
}

QModelIndex UploadModel::parent(const QModelIndex &index) const
{
    Q_UNUSED(index);
    return QModelIndex();
}

bool UploadModel::setData(const QModelIndex &index, const QVariant &value,
                          int role)
{
    Q_D(UploadModel);

    int row = index.row();
    if (Q_UNLIKELY(row < 0 || row >= d->m_uploads.count())) return false;

    int column = index.column();
    if (Q_UNLIKELY(column < 0 || column > d->m_uploadersCount)) return false;

    Upload &u = d->m_uploads[row];
    switch (role) {
    case ProgressRole:
    case UploadStatusRole:
    case UploadStatusRawRole:
        u.setStatus(column - 1, static_cast<UploadStatus>(value.toInt()));
        break;
    case TitleRole:
    case DescriptionRole:
        u.setOption(role, column,
                    !value.isValid() || value.type() == QVariant::String ?
                    value : value.toString());
        break;
    case TagsRole:
        u.setOption(role, column,
                    !value.isValid() || value.type() == QVariant::StringList ?
                    value : qvariantToStringList(value));
        break;
    case SubDirectoryRole:
        u.setSubDirectory(value.toString());
        break;
    default:
        qWarning() << "Can't set data for role" << role;
        return false;
    }
    d->emitDataChanged(index, index);
    return true;
}

QVariant UploadModel::data(const QModelIndex &index, int role) const
{
    Q_D(const UploadModel);

    int row = index.row();
    if (row < 0 || row >= d->m_uploads.count()) return QVariant();

    int column = index.column();
    if (Q_UNLIKELY(column < 0 || column > d->m_uploadersCount)) {
        return QVariant();
    }

    const Upload &u = d->m_uploads[row];
    switch (role) {
    case UrlRole:
        return u.url();
    case FileNameRole:
        return u.fileName();
    case ProgressRole:
        return u.progress(column);
    case UploadStatusRole:
        return QVariant::fromValue(u.status(column));
    case UploadStatusRawRole:
        return int8_t(u.status(column));
    case TitleRole:
    case DescriptionRole:
    case TagsRole:
        return u.option(role, column);
    case ConfigRole:
        return u.config(d->m_roles, column);
    case SubDirectoryRole:
        return u.subDirectory();
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> UploadModel::roleNames() const
{
    Q_D(const UploadModel);
    return d->m_roles;
}

#include "upload_model.moc"
