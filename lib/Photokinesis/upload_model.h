/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_UPLOAD_MODEL_H
#define PHOTOKINESIS_UPLOAD_MODEL_H

#include "global.h"

#include <QAbstractItemModel>
#include <QDataStream>
#include <QJsonObject>
#include <QList>
#include <QUrl>
#include <QVariant>

namespace Photokinesis {

class UploadModelPrivate;
class PK_EXPORT UploadModel: public QAbstractItemModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)
    Q_PROPERTY(int uploadersCount READ uploadersCount WRITE setUploadersCount \
               NOTIFY uploadersCountChanged)
    Q_PROPERTY(QJsonObject config READ config WRITE setConfig \
               NOTIFY configChanged)

public:
    enum Roles {
        UrlRole = Qt::UserRole + 1,
        FileNameRole,
        ProgressRole,
        UploadStatusRole,
        TitleRole,
        DescriptionRole,
        TagsRole,
        ConfigRole,
        SubDirectoryRole,
        // For internal use only:
        UploadStatusRawRole,
    };
    Q_ENUM(Roles)

    enum UploadStatus {
        Ready = -1,
        Uploading, /* Values 0 - 100 are used to report progress */
        Uploaded = 101,
        Failed = 102,
    };
    Q_ENUM(UploadStatus)

    UploadModel(QObject *parent = 0);
    ~UploadModel();

    void setUploadersCount(int uploadersCount);
    int uploadersCount() const;

    void setConfig(const QJsonObject &config);
    QJsonObject config() const;

    void setIsLoadingSession(bool isLoadingSession);
    bool isLoadingSession() const;

    Q_INVOKABLE void addPhoto(const QUrl &url);
    Q_INVOKABLE void addPhotos(const QList<QUrl> &urls,
                               const QUrl &baseUrl = QUrl());
    Q_INVOKABLE void clear();

    Q_INVOKABLE QVariantMap get(int row, int column = 0) const;
    Q_INVOKABLE QVariant get(int row, const QString &role) const;
    Q_INVOKABLE QVariant get(int row, int column, const QString &role) const;
    Q_INVOKABLE bool set(int row, int column, const QString &role,
                         const QJsonValue &value);

    Q_INVOKABLE void setTitle(int row, const QString &title);
    Q_INVOKABLE void setDescription(int row, const QString &description);
    Q_INVOKABLE void addTag(int row, const QString &tag);
    Q_INVOKABLE void removeTag(int row, const QString &tag);

    Q_INVOKABLE void setProgress(int row, int uploaderIndex, int progress);
    Q_INVOKABLE void setStatus(int row, int uploaderIndex,
                               UploadStatus status);
    Q_INVOKABLE void resetFailures();

    int columnCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex index(int row, int column = 0,
                      const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QModelIndex parent(const QModelIndex &index) const Q_DECL_OVERRIDE;
    bool setData(const QModelIndex &index, const QVariant &value,
                 int role) Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();
    void uploadersCountChanged();
    void configChanged();

private:
    QScopedPointer<UploadModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(UploadModel)
};

inline QDataStream &operator<<(QDataStream &out, UploadModel::UploadStatus status)
{
    return out << int8_t(status);
}

inline QDataStream &operator>>(QDataStream &in, UploadModel::UploadStatus &status)
{
    int8_t rawStatus;
    QDataStream &ret = in >> rawStatus;
    status = UploadModel::UploadStatus(rawStatus);
    return ret;
}

inline QDataStream &operator<<(QDataStream &out, UploadModel::Roles role)
{
    return out << int16_t(role);
}

inline QDataStream &operator>>(QDataStream &in, UploadModel::Roles &role)
{
    int16_t intRole;
    QDataStream &ret = in >> intRole;
    role = UploadModel::Roles(intRole);
    return ret;
}

} // namespace

#endif // PHOTOKINESIS_UPLOAD_MODEL_H
