/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plugin_manager.h"

#include <QCoreApplication>
#include <QDebug>
#include <QDir>
#include <QHash>
#include <QJsonObject>
#include <QLibrary>
#include <QPluginLoader>
#include <QStandardPaths>
#include "abstract_uploader.h"
#include "plugin_interface.h"
#include "uploader_profile.h"

using namespace Photokinesis;

namespace Photokinesis {

static const QString keyMetaData = QStringLiteral("MetaData");
static const QString keyProfiles = QStringLiteral("profiles");

struct Plugin {
    PluginInterface *interface;
    QJsonObject metaData;
};

class PluginManagerPrivate
{
    Q_DECLARE_PUBLIC(PluginManager)

public:
    PluginManagerPrivate(PluginManager *q);
    ~PluginManagerPrivate();

    void loadAll();
    void readProfiles();

private:
    static PluginManager *m_instance;
    QHash<QString,Plugin> m_plugins;
    QHash<QString,UploaderProfile> m_profiles;
    PluginManager *q_ptr;
};

PluginManager *PluginManagerPrivate::m_instance = 0;

} // namespace

PluginManagerPrivate::PluginManagerPrivate(PluginManager *q):
    q_ptr(q)
{
    QCoreApplication::addLibraryPath(PLUGIN_DIR);
    loadAll();
    readProfiles();
}

PluginManagerPrivate::~PluginManagerPrivate()
{
    m_instance = 0;
}

void PluginManagerPrivate::loadAll()
{
    /* First, load all static plugins */

    QVector<QStaticPlugin> staticPlugins = QPluginLoader::staticPlugins();
    for (const QStaticPlugin &plugin: staticPlugins) {
        PluginInterface *interface =
            qobject_cast<PluginInterface *>(plugin.instance());
        if (Q_UNLIKELY(!interface)) continue;

        const QJsonObject metaData =
            plugin.metaData().value(keyMetaData).toObject();
        m_plugins.insert(UploaderProfile::name(metaData),
                         Plugin{ interface, metaData });
    }

    /* Then, find dynamic plugins */

    QString prefix = PLUGIN_DIR[0] == '/' ?
        "" : (QCoreApplication::applicationDirPath() + "/");
    QDir pluginDir(prefix + PLUGIN_DIR);
    QStringList entries = pluginDir.entryList(QDir::NoDotAndDotDot | QDir::Files);
    for (const QString &fileName: entries) {
        if (!QLibrary::isLibrary(fileName)) continue;
        QPluginLoader loader(pluginDir.filePath(fileName));
        QObject *instance = loader.instance();
        if (Q_UNLIKELY(!instance)) {
            qWarning() << "Could not load plugin" << fileName << loader.errorString();
            continue;
        }
        PluginInterface *interface =
            qobject_cast<PluginInterface *>(instance);
        if (Q_UNLIKELY(!interface)) continue;

        const QJsonObject metaData =
            loader.metaData().value(keyMetaData).toObject();
        m_plugins.insert(UploaderProfile::name(metaData),
                         Plugin{ interface, metaData });
    }
}

void PluginManagerPrivate::readProfiles()
{
    for (auto i = m_plugins.constBegin(); i != m_plugins.constEnd(); i++) {
        const QString &pluginName = i.key();
        const Plugin &plugin = i.value();
        const QJsonArray profiles =
            plugin.metaData.value(keyProfiles).toArray();
        bool hasProfiles = false;
        for (const QJsonValue &p: profiles) {
            UploaderProfile profile(pluginName, p.toObject());
            m_profiles.insert(profile.name(), profile);
            hasProfiles = true;
        }

        if (!hasProfiles) {
            UploaderProfile profile(pluginName, plugin.metaData);
            m_profiles.insert(profile.name(), profile);
        }
    }
}

PluginManager::PluginManager(QObject *parent):
    QObject(parent),
    d_ptr(new PluginManagerPrivate(this))
{
}

PluginManager::~PluginManager()
{
}

PluginManager *PluginManager::instance()
{
    if (!PluginManagerPrivate::m_instance) {
        PluginManagerPrivate::m_instance = new PluginManager;
    }
    return PluginManagerPrivate::m_instance;
}

QList<UploaderProfile> PluginManager::uploaderProfiles() const
{
    Q_D(const PluginManager);

    return d->m_profiles.values();
}

AbstractUploader *PluginManager::uploader(const UploaderProfile &profile)
{
    Q_D(PluginManager);

    const Plugin &plugin = d->m_plugins.value(profile.pluginName());

    return plugin.interface->createUploader(profile, this);
}
