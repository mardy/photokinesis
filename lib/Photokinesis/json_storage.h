/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_JSON_STORAGE_H
#define PHOTOKINESIS_JSON_STORAGE_H

#include "global.h"

#include <QJsonObject>
#include <QObject>
#include <QQmlParserStatus>
#include <QStandardPaths>

namespace Photokinesis {

class JsonStoragePrivate;
class PK_EXPORT JsonStorage: public QObject, public QQmlParserStatus
{
    Q_OBJECT
    Q_INTERFACES(QQmlParserStatus)
    Q_PROPERTY(QStandardPaths::StandardLocation base READ base WRITE setBase \
               NOTIFY baseChanged)
    Q_PROPERTY(QString filePath READ filePath WRITE setFilePath \
               NOTIFY filePathChanged)

public:
    enum StandardLocation {
        DesktopLocation = QStandardPaths::DesktopLocation,
        DocumentsLocation = QStandardPaths::DocumentsLocation,
        MusicLocation = QStandardPaths::MusicLocation,
        MoviesLocation = QStandardPaths::MoviesLocation,
        PicturesLocation = QStandardPaths::PicturesLocation,
        TempLocation = QStandardPaths::TempLocation,
        HomeLocation = QStandardPaths::HomeLocation,
        DataLocation = QStandardPaths::DataLocation,
        CacheLocation = QStandardPaths::CacheLocation,
        RuntimeLocation = QStandardPaths::RuntimeLocation,
        ConfigLocation = QStandardPaths::ConfigLocation,
        AppDataLocation = QStandardPaths::AppDataLocation,
        AppConfigLocation = QStandardPaths::AppConfigLocation,
    };
    Q_ENUM(StandardLocation)

    JsonStorage(QObject *parent = 0);
    ~JsonStorage();

    void setBase(QStandardPaths::StandardLocation base);
    QStandardPaths::StandardLocation base() const;

    void setFilePath(const QString &filePath);
    QString filePath() const;

    Q_INVOKABLE void storeObject(const QJsonObject &object);
    Q_INVOKABLE QJsonObject loadObject() const;

    void classBegin() Q_DECL_OVERRIDE;
    void componentComplete() Q_DECL_OVERRIDE;

Q_SIGNALS:
    void baseChanged();
    void filePathChanged();

private:
    QScopedPointer<JsonStoragePrivate> d_ptr;
    Q_DECLARE_PRIVATE(JsonStorage)
};

} // namespace

#endif // PHOTOKINESIS_JSON_STORAGE_H
