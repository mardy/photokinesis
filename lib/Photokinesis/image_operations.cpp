/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_operations.h"

#include <QDebug>
#include <QImageReader>
#include <climits>

using namespace Photokinesis;

bool ImageOperations::ensureMaxSize(const QString &filePathIn,
                                    const QString &filePathOut,
                                    const QSize &size)
{
    QImageReader reader(filePathIn);
    QImage image;

    QSize maxSize(size);
    if (maxSize.height() <= 0) maxSize.setHeight(INT_MAX);
    if (maxSize.width() <= 0) maxSize.setWidth(INT_MAX);

    QSize sourceSize = reader.size();
    if (!sourceSize.isValid()) {
        // We are forced to read the image
        image = reader.read();
        sourceSize = image.size();
    }

    QSize boundedSize = sourceSize.boundedTo(maxSize);
    QSize newSize = sourceSize.scaled(boundedSize, Qt::KeepAspectRatio);

    if (image.isNull()) {
        if (newSize != sourceSize) {
            reader.setScaledSize(newSize);
        }
        image = reader.read();
    } else if (newSize != sourceSize) {
        image = image.scaled(newSize, Qt::KeepAspectRatio,
                             Qt::SmoothTransformation);
    }

    return image.save(filePathOut);
}
