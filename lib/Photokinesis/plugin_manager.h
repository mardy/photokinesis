/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_PLUGIN_MANAGER_H
#define PHOTOKINESIS_PLUGIN_MANAGER_H

#include "global.h"
#include "uploader_profile.h"

#include <QList>
#include <QObject>
#include <QScopedPointer>

namespace Photokinesis {

class AbstractUploader;

class PluginManagerPrivate;
class PK_EXPORT PluginManager: public QObject
{
    Q_OBJECT

public:
    static PluginManager *instance();
    ~PluginManager();

    QList<UploaderProfile> uploaderProfiles() const;

    AbstractUploader *uploader(const UploaderProfile &profile);

protected:
    PluginManager(QObject *parent = 0);

private:
    QScopedPointer<PluginManagerPrivate> d_ptr;
    Q_DECLARE_PRIVATE(PluginManager)
};

} // namespace

#endif // PHOTOKINESIS_PLUGIN_MANAGER_H
