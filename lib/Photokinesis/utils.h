/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_UTILS_H
#define PHOTOKINESIS_UTILS_H

#include "global.h"

#include <QStringList>
#include <QVariant>

namespace Photokinesis {

QStringList PK_EXPORT jsonValueToStringList(const QJsonValue &value);
QStringList PK_EXPORT qvariantToStringList(const QVariant &value);

} // namespace

#endif // PHOTOKINESIS_UTILS_H
