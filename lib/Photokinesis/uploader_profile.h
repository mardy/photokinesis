/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_UPLOADER_PROFILE_H
#define PHOTOKINESIS_UPLOADER_PROFILE_H

#include "global.h"

#include <QJsonObject>
#include <QString>
#include <QUrl>

namespace Photokinesis {

class PK_EXPORT UploaderProfile: public QJsonObject
{
public:
    UploaderProfile(): QJsonObject() {}
    UploaderProfile(const QJsonObject &object);
    UploaderProfile(const QString &pluginName,
                    const QJsonObject &object);
    ~UploaderProfile();

    static QString name(const QJsonObject &metadata);

    QString pluginName() const;
    QString name() const;
    QString displayName() const;
    QUrl icon() const;
    QStringList optionComponentNames() const;
    QStringList supportedOptions() const;
};

} // namespace

#endif // PHOTOKINESIS_UPLOADER_PROFILE_H
