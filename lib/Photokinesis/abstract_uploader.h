/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_ABSTRACT_UPLOADER_H
#define PHOTOKINESIS_ABSTRACT_UPLOADER_H

#include "global.h"
#include "uploader_profile.h"

#include <QJsonArray>
#include <QObject>
#include <QScopedPointer>
#include <QSize>
#include <QUrl>
#include <QVariantMap>

class QJsonObject;
class QNetworkAccessManager;
class QQuickItem;

namespace Photokinesis {

class AbstractUploaderPrivate;
class PK_EXPORT AbstractUploader: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name CONSTANT)
    Q_PROPERTY(QString displayName READ displayName CONSTANT)
    Q_PROPERTY(QUrl icon READ icon CONSTANT)
    Q_PROPERTY(QStringList supportedOptions READ supportedOptions CONSTANT)
    Q_PROPERTY(bool enabled READ isEnabled WRITE setEnabled NOTIFY enabledChanged)
    Q_PROPERTY(QString userId READ userId WRITE setUserId \
               NOTIFY userIdChanged)
    Q_PROPERTY(QString userName READ userName WRITE setUserName \
               NOTIFY userNameChanged)
    Q_PROPERTY(QString accountInfo READ accountInfo NOTIFY accountInfoChanged)
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(bool usesBrowser READ usesBrowser CONSTANT)
    Q_PROPERTY(QJsonArray albums READ albums NOTIFY albumsChanged)
    Q_PROPERTY(QString defaultAlbumId READ defaultAlbumId \
               WRITE setDefaultAlbumId NOTIFY defaultAlbumIdChanged)
    Q_PROPERTY(bool needsConfiguration READ needsConfiguration
               WRITE setNeedsConfiguration NOTIFY statusChanged)
    /* This is the size decided by the user */
    Q_PROPERTY(QSize maxSize READ maxSize WRITE setMaxSize \
               NOTIFY maxSizeChanged)
    Q_PROPERTY(bool replicatingFolders READ replicatingFolders
               WRITE setReplicatingFolders NOTIFY replicatingFoldersChanged)
    Q_PROPERTY(QQuickItem *optionsContainer READ optionsContainer \
               WRITE setOptionsContainer NOTIFY optionsContainerChanged)
    Q_PROPERTY(QString errorMessage READ errorMessage NOTIFY statusChanged)
    Q_PROPERTY(QUrl uploadsUrl READ uploadsUrl NOTIFY uploadsUrlChanged)
    Q_PROPERTY(QStringList knownUserIds READ knownUserIds CONSTANT)

public:
    enum Status {
        NeedsAuthentication = 0,
        Authenticating,
        RetrievingAccountInfo,
        Ready,
        Busy,
    };
    Q_ENUM(Status)

    AbstractUploader(const UploaderProfile &profile, QObject *parent = 0);
    ~AbstractUploader();

    QString name() const;
    QString displayName() const;
    QUrl icon() const;
    QString accountInfo() const;
    Status status() const;
    bool usesBrowser() const;
    QUrl uploadsUrl() const;
    QStringList knownUserIds() const;

    void setUserId(const QString &userId);
    QString userId() const;

    void setUserName(const QString &userName);
    QString userName() const;

    QStringList supportedOptions() const;
    Q_INVOKABLE bool hasOption(const QString &name) const;

    void setEnabled(bool enabled);
    bool isEnabled() const;

    QJsonArray albums() const;

    void setDefaultAlbumId(const QString &id);
    QString defaultAlbumId() const;

    bool needsConfiguration() const;

    void setMaxSize(const QSize &size);
    QSize maxSize() const;

    void setReplicatingFolders(bool replicatingFolders);
    bool replicatingFolders() const;

    void setOptionsContainer(QQuickItem *container);
    QQuickItem *optionsContainer() const;

    void setNetworkAccessManager(QNetworkAccessManager *nam);
    QNetworkAccessManager *networkAccessManager() const;

    virtual void processImage(const QUrl &url,
                              const QJsonObject &options);
    virtual void logout();

    Q_INVOKABLE virtual void authenticate(QQuickItem *parent);
    Q_INVOKABLE virtual void startUpload(const QString &filePath,
                                         const QJsonObject &options) = 0;
    Q_INVOKABLE void uploadFile(const QUrl &url,
                                const QJsonObject &options);
    Q_INVOKABLE void changeAccount(QQuickItem *parent);

    Q_INVOKABLE virtual QVariantMap checkSize(const QSize &size);

    QString errorMessage() const;

Q_SIGNALS:
    void enabledChanged();
    void userIdChanged();
    void userNameChanged();
    void accountInfoChanged();

    void progress(int progress);
    void statusChanged();
    void albumsChanged();
    void defaultAlbumIdChanged();
    void maxSizeChanged();
    void replicatingFoldersChanged();
    void optionsContainerChanged();
    void uploadsUrlChanged();
    void done(bool ok);

protected:
    void setName(const QString &name);
    void setDisplayName(const QString &displayName);
    void setIcon(const QUrl &icon);
    void setAccountInfo(const QString &accountInfo);
    void setErrorMessage(const QString &message);
    void setStatus(Status status);
    void setUsesBrowser(bool usesBrowser);
    void setNeedsConfiguration(bool needsConfiguration);
    void setAlbums(const QJsonArray &albums);
    void setOptionComponents(const QStringList &components);
    void setUploadsUrl(const QUrl &uploadsUrl);

    void storeAccountSettings(const QJsonObject &settings);
    QJsonObject loadAccountSettings() const;
    Q_INVOKABLE void storeSettings();
    Q_INVOKABLE void loadSettings();

    virtual void setSettings(const QJsonObject &settings);
    virtual QJsonObject getSettings() const;

private:
    QScopedPointer<AbstractUploaderPrivate> d_ptr;
    Q_DECLARE_PRIVATE(AbstractUploader)
};

} // namespace

#endif // PHOTOKINESIS_ABSTRACT_UPLOADER_H
