/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ui_helpers.h"

#include <QDir>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickItem>
#include <QStandardPaths>

using namespace Photokinesis;

namespace Photokinesis {

class UiHelpersPrivate {
public:
    UiHelpersPrivate();
    virtual ~UiHelpersPrivate();

    static UiHelpersPrivate *instance();

private:
    friend class UiHelpers;
    QQmlComponent *m_webviewComponent;
    QQmlComponent *m_cookieDeleterComponent;
    QScopedPointer<QQmlEngine> m_engine;
    static UiHelpersPrivate *m_instance;
};

UiHelpersPrivate *UiHelpersPrivate::m_instance = 0;

} // namespace

UiHelpersPrivate::UiHelpersPrivate():
    m_webviewComponent(0),
    m_cookieDeleterComponent(0)
{
}

UiHelpersPrivate::~UiHelpersPrivate()
{
    m_instance = 0;

    delete m_webviewComponent;
}

UiHelpersPrivate *UiHelpersPrivate::instance()
{
    if (!m_instance) {
        m_instance = new UiHelpersPrivate;
    }
    return m_instance;
}

QQuickItem *UiHelpers::createWebview(QQuickItem *parent,
                                     QObject *authenticator)
{
    UiHelpersPrivate *d = UiHelpersPrivate::instance();

    Q_ASSERT(parent);
    QQmlContext *context = QQmlEngine::contextForObject(parent);
    QQmlEngine *engine = context->engine();
    if (!d->m_webviewComponent) {
        d->m_webviewComponent =
            new QQmlComponent(engine,
                              QUrl("qrc:/UiHelpers/WebviewLoader.qml"));
        if (Q_UNLIKELY(d->m_webviewComponent->isError())) {
            qCritical() << "Component errors:" << d->m_webviewComponent->errors();
        }
    }

    QObject *object = d->m_webviewComponent->create(context);
    Q_ASSERT(object);
    object->setProperty("authenticator", QVariant::fromValue(authenticator));
    QQuickItem *item = qobject_cast<QQuickItem*>(object);
    Q_ASSERT(item);
    item->setParentItem(parent);
    item->setParent(parent);
    QObject::connect(item, &QQuickItem::parentChanged,
                     item, &QObject::deleteLater);
    return item;
}

QObject *UiHelpers::clearCookies(QObject *parent,
                                 const QUrl &homePage,
                                 const QJsonArray &cookies)
{
    UiHelpersPrivate *d = UiHelpersPrivate::instance();

    Q_ASSERT(parent);

    if (!d->m_cookieDeleterComponent) {
        d->m_engine.reset(new QQmlEngine());
        d->m_cookieDeleterComponent =
            new QQmlComponent(d->m_engine.data(),
                              QUrl("qrc:/UiHelpers/CookieDeleter.qml"),
                              d->m_engine.data());
    }

    QObject *object = d->m_cookieDeleterComponent->create();
    Q_ASSERT(object);
    object->setParent(parent);
    object->setProperty("url", homePage);
    object->setProperty("cookies", cookies);
    QObject::connect(object, SIGNAL(done()), object, SLOT(deleteLater()));
    return object;
}
