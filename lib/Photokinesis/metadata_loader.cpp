/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "metadata_loader.h"

#include "metadata.h"

#include <QDebug>
#include <QThread>
#include <QVector>

using namespace Photokinesis;

namespace Photokinesis {

struct Request {
    QString filePath;
    int id;
};

/* Worker thread */

class Worker: public QThread
{
    Q_OBJECT

public:
    ~Worker();

    void processRequests(const QVector<Request> &requests);

Q_SIGNALS:
    void metadataLoaded(int requestId,
                        QString title,
                        QString description,
                        QStringList tags);

protected:
    void run() override;

private:
    void processRequest(const Request &request);

private:
    Imaginario::Metadata m_metadata;
    QVector<Request> m_requests;
};

class MetadataLoaderPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(MetadataLoader)

public:
    MetadataLoaderPrivate(MetadataLoader *q);

    void processRequest(const Request &request);
    void processRequests();
    void onProcessingFinished();

    Q_INVOKABLE void sendRequests();
    void queueSendRequests();

private:
    QVector<Request> m_activeRequests;
    QVector<Request> m_queuedRequests;
    Worker m_worker;
    bool m_sendRequestsQueued;
    int m_pendingRequests;
    int m_completedRequests;
    MetadataLoader *q_ptr;
};

} // namespace

Worker::~Worker()
{
    requestInterruption();
    wait(5000);
}

void Worker::processRequests(const QVector<Request> &requests)
{
    m_requests = requests;
    start();
}

void Worker::processRequest(const Request &request)
{
    Imaginario::Metadata::ImportData metadata;
    m_metadata.readImportData(request.filePath, metadata);

    Q_EMIT metadataLoaded(request.id,
                          metadata.title,
                          metadata.description,
                          metadata.tags);
}

void Worker::run()
{
    for (const Request &request: qAsConst(m_requests)) {
        if (QThread::currentThread()->isInterruptionRequested()) {
            break;
        }
        processRequest(request);
    }
}

MetadataLoaderPrivate::MetadataLoaderPrivate(MetadataLoader *q):
    m_sendRequestsQueued(false),
    m_pendingRequests(0),
    m_completedRequests(0),
    q_ptr(q)
{
    QObject::connect(&m_worker, &QThread::finished,
                     this, &MetadataLoaderPrivate::onProcessingFinished);
    QObject::connect(&m_worker, &Worker::metadataLoaded,
                     this, [this](int requestId,
                                  QString title,
                                  QString description,
                                  QStringList tags) {
        Q_Q(MetadataLoader);
        m_completedRequests++;
        q->metadataLoaded(requestId, title, description, tags);
    });
}

void MetadataLoaderPrivate::onProcessingFinished()
{
    m_activeRequests.clear();
    sendRequests();
}

void MetadataLoaderPrivate::sendRequests()
{
    if (m_queuedRequests.isEmpty()) {
        /* Nothing to do */
        m_sendRequestsQueued = false;
        return;
    }

    m_worker.processRequests(m_queuedRequests);
    m_queuedRequests.clear();
}

void MetadataLoaderPrivate::queueSendRequests()
{
    if (!m_sendRequestsQueued) {
        /* Only queue the request if there are no running operations, because
         * otherwise the sendRequests() method will anyway be called when they
         * finish. */
        if (!m_worker.isRunning()) {
            QMetaObject::invokeMethod(this, "sendRequests",
                                      Qt::QueuedConnection);
        }
        m_sendRequestsQueued = true;
    }
}

MetadataLoader::MetadataLoader(QObject *parent):
    QObject(parent),
    d_ptr(new MetadataLoaderPrivate(this))
{
}

MetadataLoader::~MetadataLoader()
{
}

void MetadataLoader::loadMetadata(const QString &file, int requestId)
{
    Q_D(MetadataLoader);

    d->m_queuedRequests.append(Request { file, requestId });
    d->queueSendRequests();

    d->m_pendingRequests++;
    Q_EMIT pendingRequestsChanged();
}

int MetadataLoader::pendingRequests() const
{
    Q_D(const MetadataLoader);
    return d->m_pendingRequests;
}

int MetadataLoader::completedRequests() const
{
    Q_D(const MetadataLoader);
    return d->m_completedRequests;
}

#include "metadata_loader.moc"
