/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_image_operations.h"

#include <QFile>
#include <QThread>

using namespace Photokinesis;

static ImageOperationsMocked *m_instance = nullptr;

ImageOperationsMocked::ImageOperationsMocked():
    QObject()
{
    m_instance = this;
}

ImageOperationsMocked::~ImageOperationsMocked()
{
    m_instance = nullptr;
}

ImageOperationsMocked *ImageOperationsMocked::instance()
{
    if (!m_instance) {
        m_instance = new ImageOperationsMocked();
    }
    return m_instance;
}

bool ImageOperations::ensureMaxSize(const QString &filePathIn,
                                    const QString &filePathOut,
                                    const QSize &maxSize)
{
    ImageOperationsMocked *mocked = ImageOperationsMocked::instance();
    QThread::msleep(20);

    {
        /* Create the output file, write something into it */
        QFile file(filePathOut);
        if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
            return false;

        file.write(filePathIn.toUtf8() + '\n');
        QString size =
            QString("%1x%2\n").arg(maxSize.width()).arg(maxSize.height());
        file.write(size.toUtf8());
    }

    QMetaObject::invokeMethod(mocked, "ensureMaxSizeCalled",
                              Qt::QueuedConnection,
                              Q_ARG(QString, filePathIn),
                              Q_ARG(QString, filePathOut),
                              Q_ARG(QSize, maxSize));
    return true;
}
