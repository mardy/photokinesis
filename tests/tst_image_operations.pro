TARGET = tst_image_operations

include(tests.pri)

QT += \
    gui

SOURCES += \
    $${PHOTOKINESIS_SRC_DIR}/image_operations.cpp \
    tst_image_operations.cpp

HEADERS += \
    $${PHOTOKINESIS_SRC_DIR}/image_operations.h

INCLUDEPATH += \
    $${PHOTOKINESIS_SRC_DIR}

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
