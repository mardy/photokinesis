/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_IMAGE_OPERATIONS_H
#define FAKE_IMAGE_OPERATIONS_H

#include "image_operations.h"

#include <QObject>

namespace Photokinesis {

class ImageOperationsMocked: public QObject
{
    Q_OBJECT

public:
    ImageOperationsMocked();
    ~ImageOperationsMocked();

    static ImageOperationsMocked *instance();

Q_SIGNALS:
    void ensureMaxSizeCalled(const QString filePathIn,
                             const QString filePathOut,
                             const QSize maxSize);

private:
    friend class ImageOperations;
};

} // namespace

#endif // FAKE_IMAGE_OPERATIONS_H
