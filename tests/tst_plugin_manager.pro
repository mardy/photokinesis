TARGET = tst_plugin_manager

include(tests.pri)

LIBS += \
    -lStaticPlugin
QMAKE_LIBDIR += \
    $$OUT_PWD/tst_plugin_manager_data

SRC_DIR = $${TOP_SRC_DIR}/lib/Photokinesis
INCLUDEPATH += $${SRC_DIR}

SOURCES += \
    $${SRC_DIR}/plugin_manager.cpp \
    $${SRC_DIR}/uploader_profile.cpp \
    tst_plugin_manager.cpp

HEADERS += \
    $${SRC_DIR}/plugin_manager.h \
    $${SRC_DIR}/uploader_profile.h

DEFINES += \
    PLUGIN_DIR=\\\"tst_plugin_manager_data\\\"
