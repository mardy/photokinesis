/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_json_serializer.h"

#include <QDebug>
#include <QIODevice>

using namespace Photokinesis;

static JsonSerializerPrivate *m_privateInstance = 0;

JsonSerializerPrivate::JsonSerializerPrivate():
    m_dumpReply(true)
{
    if (m_privateInstance) {
        qWarning() << Q_FUNC_INFO << "instance alread created!";
    }
    m_privateInstance = this;

    qRegisterMetaType<QIODevice*>();
}

JsonSerializerPrivate::~JsonSerializerPrivate()
{
    m_privateInstance = nullptr;
}

JsonSerializerPrivate *JsonSerializerPrivate::instance()
{
    if (!m_privateInstance) {
        new JsonSerializerPrivate;
    }
    return m_privateInstance;
}

bool JsonSerializer::dump(QIODevice *device, const QJsonObject &json)
{
    JsonSerializerPrivate *d = JsonSerializerPrivate::instance();
    Q_EMIT d->dumpObjectCalled(device, json);
    return d->m_dumpReply;
}

QJsonObject JsonSerializer::loadObject(QIODevice *device)
{
    JsonSerializerPrivate *d = JsonSerializerPrivate::instance();
    Q_EMIT d->loadObjectCalled(device);
    return d->m_loadObjectReply;
}

bool JsonSerializer::dump(QIODevice *device, const QJsonArray &json)
{
    JsonSerializerPrivate *d = JsonSerializerPrivate::instance();
    Q_EMIT d->dumpArrayCalled(device, json);
    return d->m_dumpReply;
}

QJsonArray JsonSerializer::loadArray(QIODevice *device)
{
    JsonSerializerPrivate *d = JsonSerializerPrivate::instance();
    Q_EMIT d->loadArrayCalled(device);
    return d->m_loadArrayReply;
}
