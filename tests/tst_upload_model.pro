TARGET = tst_upload_model

include(tests.pri)

QT += \
    qml

SOURCES += \
    $${TOP_SRC_DIR}/lib/Photokinesis/upload_model.cpp \
    $${TOP_SRC_DIR}/lib/Photokinesis/utils.cpp \
    fake_metadata_loader.cpp \
    tst_upload_model.cpp

HEADERS += \
    $${TOP_SRC_DIR}/lib/Photokinesis/metadata_loader.h \
    $${TOP_SRC_DIR}/lib/Photokinesis/upload_model.h \
    $${TOP_SRC_DIR}/lib/Photokinesis/utils.h \
    fake_metadata_loader.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
