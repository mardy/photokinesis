TARGET = tst_uploader_model

include(tests.pri)

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}
INCLUDEPATH += $${TOP_SRC_DIR}/lib/Photokinesis

SOURCES += \
    $${SRC_DIR}/uploader_model.cpp \
    tst_uploader_model.cpp

HEADERS += \
    $${SRC_DIR}/uploader_model.h
