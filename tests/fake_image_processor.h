/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_IMAGE_PROCESSOR_H
#define FAKE_IMAGE_PROCESSOR_H

#include "Photokinesis/image_processor.h"

#include <QMap>
#include <QObject>

namespace Photokinesis {

typedef ImageProcessorPrivate ImageProcessorMocked;
class ImageProcessorPrivate: public QObject
{
    Q_OBJECT

public:
    ImageProcessorPrivate(ImageProcessor *q_ptr);
    ~ImageProcessorPrivate();

    static ImageProcessorPrivate *mocked(ImageProcessor *o) {
        return o->d_ptr.data();
    }

    const QMap<int,ImageProcessor::Request> &requests() const {
        return m_requests;
    }

    void emitFinished(int request, const QString &outputFile);

Q_SIGNALS:
    void startCalled();
    void stopAndClearCalled();

private:
    Q_DECLARE_PUBLIC(ImageProcessor)
    int m_nextRequestId;
    QMap<int,ImageProcessor::Request> m_requests;
    static ImageProcessor *m_instance;
    ImageProcessor *q_ptr;
};

} // namespace

#endif // FAKE_IMAGE_PROCESSOR_H
