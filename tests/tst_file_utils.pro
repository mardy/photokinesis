TARGET = tst_file_utils

include(tests.pri)

QT += \
    concurrent \
    qml

SOURCES += \
    $${SRC_DIR}/file_utils.cpp \
    tst_file_utils.cpp

HEADERS += \
    $${SRC_DIR}/file_utils.h
