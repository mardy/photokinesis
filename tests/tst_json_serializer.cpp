/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "json_serializer.h"

#include <QBuffer>
#include <QDebug>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Photokinesis;

class JsonSerializerTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testDumpAndLoadObject_data();
    void testDumpAndLoadObject();
    void testDumpAndLoadArray_data();
    void testDumpAndLoadArray();
};

void JsonSerializerTest::testDumpAndLoadObject_data()
{
    QTest::addColumn<QJsonObject>("expectedJson");

    QTest::newRow("empty") <<
        QJsonObject {};

    QTest::newRow("simple") <<
        QJsonObject {
            { "key", "value" },
            { "number", 5213 },
            { "boolean", true },
        };
}

void JsonSerializerTest::testDumpAndLoadObject()
{
    QFETCH(QJsonObject, expectedJson);

    QBuffer buffer;

    {
        JsonSerializer serializer;
        QVERIFY(buffer.open(QIODevice::WriteOnly));
        QVERIFY(serializer.dump(&buffer, expectedJson));
        buffer.close();
    }

    QJsonObject json;
    {
        JsonSerializer serializer;
        QVERIFY(buffer.open(QIODevice::ReadOnly));
        json = serializer.loadObject(&buffer);
    }

    QCOMPARE(json, expectedJson);
}

void JsonSerializerTest::testDumpAndLoadArray_data()
{
    QTest::addColumn<QJsonArray>("expectedJson");

    QTest::newRow("empty") <<
        QJsonArray {};

    QTest::newRow("array of strings") <<
        QJsonArray { "one", "two", "three" };

    QTest::newRow("array of objects") <<
        QJsonArray {
            QJsonObject {
                { "key", "value" },
                { "number", 5213 },
                { "boolean", true },
            },
            QJsonObject {
                { "second", "object" },
                { "with two", "keys" },
            },
        };
}

void JsonSerializerTest::testDumpAndLoadArray()
{
    QFETCH(QJsonArray, expectedJson);

    QBuffer buffer;

    {
        JsonSerializer serializer;
        QVERIFY(buffer.open(QIODevice::WriteOnly));
        QVERIFY(serializer.dump(&buffer, expectedJson));
        buffer.close();
    }

    QJsonArray json;
    {
        JsonSerializer serializer;
        QVERIFY(buffer.open(QIODevice::ReadOnly));
        json = serializer.loadArray(&buffer);
    }

    QCOMPARE(json, expectedJson);
}

QTEST_GUILESS_MAIN(JsonSerializerTest)

#include "tst_json_serializer.moc"
