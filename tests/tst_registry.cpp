/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "registry.h"

#include <QCoreApplication>
#include <QDebug>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <QUrlQuery>
#include "fake_network.h"

using namespace Mardy;

class RegistryTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testStart_data();
    void testStart();
};

void RegistryTest::testStart_data()
{
    QTest::addColumn<QString>("serverReply");
    QTest::addColumn<Registry::Status>("expectedStatus");
    QTest::addColumn<QString>("expectedMessage");
    QTest::addColumn<bool>("expectedIsAllowed");

    QTest::newRow("invalid json") <<
        "You bet\n" <<
        Registry::Failed <<
        "" <<
        true;

    QTest::newRow("error") <<
        "{\n"
        "  \"status\": \"error\",\n"
        "  \"message\": \"Something happened\",\n"
        "  \"allowed\": true\n"
        "}" <<
        Registry::Failed <<
        "Something happened" <<
        true;

    QTest::newRow("ok, no msg") <<
        "{\n"
        "  \"status\": \"ok\",\n"
        "  \"message\": \"\",\n"
        "  \"allowed\": true\n"
        "}" <<
        Registry::Succeeded <<
        "" <<
        true;

    QTest::newRow("ok, with msg") <<
        "{\n"
        "  \"status\": \"ok\",\n"
        "  \"message\": \"Thanks for your support\",\n"
        "  \"allowed\": true\n"
        "}" <<
        Registry::Succeeded <<
        "Thanks for your support" <<
        true;

    QTest::newRow("ok, not allowed") <<
        "{\n"
        "  \"status\": \"ok\",\n"
        "  \"message\": \"Not allowed\",\n"
        "  \"allowed\": false\n"
        "}" <<
        Registry::Succeeded <<
        "Not allowed" <<
        false;
}

void RegistryTest::testStart()
{
    QFETCH(QString, serverReply);
    QFETCH(Registry::Status, expectedStatus);
    QFETCH(QString, expectedMessage);
    QFETCH(bool, expectedIsAllowed);

    QCoreApplication::setApplicationVersion("1.0beta1");
    FakeNamFactory *namFactory = new FakeNamFactory();
    QSignalSpy requestCreated(namFactory, &FakeNamFactory::requestCreated);

    QQmlEngine engine;
    engine.setNetworkAccessManagerFactory(namFactory);
    qmlRegisterType<Registry>("MyTest", 1, 0, "Registry");
    QQmlComponent component(&engine);
    component.setData("import MyTest 1.0\n"
                      "Registry {\n"
                      "}",
                      QUrl());
    QScopedPointer<QObject> object(component.create());
    QVERIFY(object);

    Registry *registry = qobject_cast<Registry*>(object.data());
    QVERIFY(registry != 0);
    QSignalSpy finished(registry, &Registry::finished);

    QCOMPARE(registry->status(), Registry::Idle);

    registry->start();
    QCOMPARE(registry->status(), Registry::Busy);

    QTRY_COMPARE(requestCreated.count(), 1);
    FakeReply *reply = requestCreated.at(0).at(0).value<FakeReply*>();
    QVERIFY(reply);

    QUrlQuery query(reply->url());
    QCOMPARE(query.queryItemValue("appName"), QString("tst_registry"));
    QCOMPARE(query.queryItemValue("appVersion"), QString("1.0beta1"));
    reply->setData(serverReply.toUtf8());
    QVERIFY(finished.wait());
    QCOMPARE(finished.count(), 1);

    QCOMPARE(registry->message(), expectedMessage);
    QCOMPARE(registry->isAllowed(), expectedIsAllowed);
    QCOMPARE(registry->status(), expectedStatus);
}

QTEST_GUILESS_MAIN(RegistryTest)

#include "tst_registry.moc"
