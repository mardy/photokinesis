/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_image_processor.h"

#include <QDebug>
#include <QFileInfo>
#include <QList>
#include <QTemporaryDir>

using namespace Photokinesis;

namespace Photokinesis {

class ImageProcessor::RequestPrivate {
public:
    RequestPrivate(const QString &filePath): m_filePath(filePath) {}

    QString m_filePath;
    QSize m_maxSize;
    int m_id;
};

ImageProcessor *ImageProcessorPrivate::m_instance = nullptr;

} // namespace

ImageProcessor::Request::Request(const QString &filePath):
    d(new ImageProcessor::RequestPrivate(filePath))
{
}

QString ImageProcessor::Request::filePath() const
{
    return d->m_filePath;
}

void ImageProcessor::Request::setMaxSize(const QSize &size)
{
    d->m_maxSize = size;
}

QSize ImageProcessor::Request::maxSize() const
{
    return d->m_maxSize;
}

ImageProcessorPrivate::ImageProcessorPrivate(ImageProcessor *q):
    QObject(q),
    m_nextRequestId(0),
    q_ptr(q)
{
    m_instance = q;
}

ImageProcessorPrivate::~ImageProcessorPrivate()
{
    m_instance = nullptr;
}

void ImageProcessorPrivate::emitFinished(int request,
                                         const QString &outputFile)
{
    Q_Q(ImageProcessor);
    QMetaObject::invokeMethod(q, "finished",
                              Qt::QueuedConnection,
                              Q_ARG(int, request),
                              Q_ARG(QString, outputFile));
}

ImageProcessor::ImageProcessor(QObject *parent):
    QObject(parent),
    d_ptr(new ImageProcessorPrivate(this))
{
}

ImageProcessor::~ImageProcessor()
{
}

ImageProcessor *ImageProcessor::instance()
{
    if (!ImageProcessorPrivate::m_instance) {
        ImageProcessorPrivate::m_instance = new ImageProcessor;
    }
    return ImageProcessorPrivate::m_instance;
}

int ImageProcessor::addRequest(const Request &request)
{
    Q_D(ImageProcessor);
    int requestId = ++d->m_nextRequestId;
    const auto i = d->m_requests.insert(requestId, request);
    Request &req = i.value();
    req.d->m_id = requestId;
    return requestId;
}

void ImageProcessor::start()
{
    Q_D(ImageProcessor);
    Q_EMIT d->startCalled();
}

void ImageProcessor::stopAndClear()
{
    Q_D(ImageProcessor);
    Q_EMIT d->stopAndClearCalled();
}
