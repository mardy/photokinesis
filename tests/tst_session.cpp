/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "session.h"

#include "fake_json_serializer.h"
#include "fake_upload_model_session.h"
#include "test_utils.h"

#include <QByteArray>
#include <QDebug>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <QTest>

using namespace Photokinesis;

class SessionTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testStoreSession_data();
    void testStoreSession();
    void testLoadLastSession_data();
    void testLoadLastSession();
    void testSessionIsFinished_data();
    void testSessionIsFinished();
};

void SessionTest::testStoreSession_data()
{
    QTest::addColumn<bool>("uploadModelSessionReply");
    QTest::addColumn<bool>("jsonSerializerReply");
    QTest::addColumn<bool>("expectedSessionReply");

    QTest::newRow("success") <<
        true << true << true;
    QTest::newRow("model failure") <<
        false << true << false;
    QTest::newRow("json failure") <<
        true << false << false;
}

void SessionTest::testStoreSession()
{
    QFETCH(bool, uploadModelSessionReply);
    QFETCH(bool, jsonSerializerReply);
    QFETCH(bool, expectedSessionReply);

    MockJsonSerializer jsonSerializer;
    jsonSerializer.setDumpReply(jsonSerializerReply);
    QSignalSpy dumpArrayCalled(&jsonSerializer,
                               &MockJsonSerializer::dumpArrayCalled);

    MockUploadModelSession uploadModelSession;
    uploadModelSession.setDumpSessionReply(uploadModelSessionReply);
    QSignalSpy dumpSessionCalled(&uploadModelSession,
                                 &MockUploadModelSession::dumpSessionCalled);

    QJsonArray uploaders {
        QJsonObject {{ "name", "uploader1" }},
    };
    QObject model;

    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());

    Session session;
    bool sessionReply = session.storeSession(uploaders, &model);
    QCOMPARE(sessionReply, expectedSessionReply);

    if (expectedSessionReply == true) {
        QCOMPARE(dumpArrayCalled.count(), 1);
        QCOMPARE(dumpArrayCalled.at(0).at(1).toJsonArray(), uploaders);

        QStringList expectedUploaderNames { "uploader1", };
        QCOMPARE(dumpSessionCalled.count(), 1);
        QStringList uploaderNames =
            dumpSessionCalled.at(0).at(1).toStringList();
        QCOMPARE(uploaderNames, expectedUploaderNames);
    }
}

void SessionTest::testLoadLastSession_data()
{
    QTest::addColumn<bool>("uploadModelSessionReply");
    QTest::addColumn<QJsonArray>("jsonSerializerReply");
    QTest::addColumn<QJsonObject>("expectedSessionReply");

    QTest::newRow("success") <<
        true <<
        QJsonArray {
            QJsonObject {{ "name", "uploader1" }},
        } <<
        QJsonObject {
            { "uploaders",
                QJsonArray {
                    QJsonObject {{ "name", "uploader1" }},
                }
            },
        };
    QTest::newRow("model failure") <<
        false <<
        QJsonArray {
            QJsonObject {{ "name", "uploader1" }},
        } <<
        QJsonObject {};
    QTest::newRow("json failure") <<
        true <<
        QJsonArray {} <<
        QJsonObject {};
}

void SessionTest::testLoadLastSession()
{
    QFETCH(bool, uploadModelSessionReply);
    QFETCH(QJsonArray, jsonSerializerReply);
    QFETCH(QJsonObject, expectedSessionReply);

    MockJsonSerializer jsonSerializer;
    jsonSerializer.setLoadArrayReply(jsonSerializerReply);
    QSignalSpy loadArrayCalled(&jsonSerializer,
                               &MockJsonSerializer::loadArrayCalled);

    MockUploadModelSession uploadModelSession;
    uploadModelSession.setLoadSessionReply(uploadModelSessionReply);
    QSignalSpy loadSessionCalled(&uploadModelSession,
                                 &MockUploadModelSession::loadSessionCalled);

    QStringList uploaders { "uploader1" };
    QObject model;

    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());

    /* We must create a file for the session; the contents are not
     * important, because all the reader objects are mocked. */
    QString sessionFileDir =
        QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    QVERIFY(QDir::root().mkpath(sessionFileDir));

    {
        QFile sessionFile(sessionFileDir + "/lastsession.bin");
        QVERIFY(sessionFile.open(QIODevice::WriteOnly));
        sessionFile.write("Hi!");
    }

    Session session;
    QJsonObject sessionReply = session.loadLastSession(uploaders, &model);
    QCOMPARE(sessionReply, expectedSessionReply);

    if (!expectedSessionReply.isEmpty()) {
        QCOMPARE(loadArrayCalled.count(), 1);

        QStringList expectedUploaderNames { "uploader1", };
        QCOMPARE(loadSessionCalled.count(), 1);
        QStringList uploaderNames =
            loadSessionCalled.at(0).at(1).toStringList();
        QCOMPARE(uploaderNames, expectedUploaderNames);
    }
}

void SessionTest::testSessionIsFinished_data()
{
    QTest::addColumn<QJsonArray>("jsonSerializerReply");
    QTest::addColumn<bool>("uploadModelSessionReply");
    QTest::addColumn<bool>("expectedSessionReply");

    QTest::newRow("success") <<
        QJsonArray { QJsonObject {{ "name", "uploader1" }}, } <<
        true << true;
    QTest::newRow("model not finished") <<
        QJsonArray { QJsonObject {{ "name", "uploader1" }}, } <<
        false << false;
    QTest::newRow("no uploaders") <<
        QJsonArray {} <<
        false << true;
}

void SessionTest::testSessionIsFinished()
{
    QFETCH(bool, uploadModelSessionReply);
    QFETCH(bool, expectedSessionReply);
    QFETCH(QJsonArray, jsonSerializerReply);

    MockJsonSerializer jsonSerializer;
    jsonSerializer.setLoadArrayReply(jsonSerializerReply);
    QSignalSpy loadArrayCalled(&jsonSerializer,
                               &MockJsonSerializer::loadArrayCalled);

    MockUploadModelSession uploadModelSession;
    uploadModelSession.setSessionIsFinishedReply(uploadModelSessionReply);
    QSignalSpy sessionIsFinishedCalled(&uploadModelSession,
        &MockUploadModelSession::sessionIsFinishedCalled);

    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());

    /* We must create a file for the session; the contents are not
     * important, because all the reader objects are mocked. */
    QString sessionFileDir =
        QStandardPaths::writableLocation(QStandardPaths::CacheLocation);
    QVERIFY(QDir::root().mkpath(sessionFileDir));

    {
        QFile sessionFile(sessionFileDir + "/lastsession.bin");
        QVERIFY(sessionFile.open(QIODevice::WriteOnly));
        sessionFile.write("Hi!");
    }

    Session session;
    bool sessionReply = session.sessionIsFinished();
    QCOMPARE(sessionReply, expectedSessionReply);

    QCOMPARE(loadArrayCalled.count(), 1);
    QCOMPARE(sessionIsFinishedCalled.count(),
             jsonSerializerReply.isEmpty() ? 0 : 1);
}

QTEST_GUILESS_MAIN(SessionTest)

#include "tst_session.moc"
