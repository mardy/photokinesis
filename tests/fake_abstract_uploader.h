/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef FAKE_ABSTRACT_UPLOADER_H
#define FAKE_ABSTRACT_UPLOADER_H

#include "Photokinesis/abstract_uploader.h"

#include <QJsonObject>
#include <QObject>

namespace Photokinesis {

typedef AbstractUploaderPrivate AbstractUploaderMocked;
class AbstractUploaderPrivate: public QObject
{
    Q_OBJECT

public:
    AbstractUploaderPrivate(AbstractUploader *q_ptr,
                            const UploaderProfile &profile);
    ~AbstractUploaderPrivate();

    static AbstractUploaderPrivate *mocked(AbstractUploader *o) {
        return o->d_ptr.data();
    }

private:
    Q_DECLARE_PUBLIC(AbstractUploader)
    UploaderProfile m_profile;
    QNetworkAccessManager *m_nam;
    QString m_errorMessage;
    QString m_name;
    QString m_displayName;
    QUrl m_icon;
    QStringList m_supportedOptions;
    bool m_isEnabled;
    QString m_userId;
    QString m_userName;
    QString m_accountInfo;
    AbstractUploader::Status m_status;
    QUrl m_uploadsUrl;
    QJsonArray m_albums;
    QString m_defaultAlbumId;
    QSize m_maxSize;
    AbstractUploader *q_ptr;
};

} // namespace

#endif // FAKE_ABSTRACT_UPLOADER_H
