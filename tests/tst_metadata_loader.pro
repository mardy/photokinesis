TARGET = tst_metadata_loader

include(tests.pri)

QT += \
    positioning

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib/Photokinesis

SOURCES += \
    $${TOP_SRC_DIR}/lib/Photokinesis/metadata_loader.cpp \
    fake_metadata.cpp \
    tst_metadata_loader.cpp

HEADERS += \
    $${TOP_SRC_DIR}/lib/Photokinesis/metadata.h \
    $${TOP_SRC_DIR}/lib/Photokinesis/metadata_loader.h \
    fake_metadata.h
