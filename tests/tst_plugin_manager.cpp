/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "plugin_manager.h"

#include <QByteArrayList>
#include <QDebug>
#include <QJsonDocument>
#include <QScopedPointer>
#include <QSet>
#include <QTest>
#include <QtPlugin>

using namespace Photokinesis;

namespace QTest {
template<>
char *toString(const QSet<UploaderProfile> &pset)
{
    QList<UploaderProfile> plist = pset.toList();
    QByteArrayList ba;
    for (const UploaderProfile &p: plist) {
        QJsonDocument doc(p);
        ba.append(doc.toJson(QJsonDocument::Compact));
    }
    return qstrdup(ba.join('|').data());
}
} // QTest namespace

inline uint qHash(const UploaderProfile &p, uint seed = 0)
{
    QJsonDocument doc(p);
    return ::qHash(doc.toJson(QJsonDocument::Compact), seed);
}

class PluginManagerTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testInstance();
    void testPlugins();
};

void PluginManagerTest::testInstance()
{
    QScopedPointer<PluginManager> manager(PluginManager::instance());

    QVERIFY(!manager.isNull());
}

void PluginManagerTest::testPlugins()
{
    QScopedPointer<PluginManager> manager(PluginManager::instance());

    QSet<UploaderProfile> expectedProfiles {
        QJsonObject {
            { "description", "A wonderful plugin" },
            { "name", "profile one" },
            { "plugin", "dynamic_plugin" },
        },
        QJsonObject {
            { "description", "A horrible plugin" },
            { "name", "profile two" },
            { "plugin", "dynamic_plugin" },
        },
        QJsonObject {
            { "description", "A lovely plugin" },
            { "name", "static_plugin" },
            { "plugin", "static_plugin" },
        },
    };
    QCOMPARE(manager->uploaderProfiles().toSet(), expectedProfiles);
}

Q_IMPORT_PLUGIN(StaticPlugin)

QTEST_GUILESS_MAIN(PluginManagerTest)

#include "tst_plugin_manager.moc"
