TARGET = tst_session

include(tests.pri)

QT += qml

LIBS += \
    -lPhotokinesis
QMAKE_LIBDIR += \
    $${TOP_BUILD_DIR}/lib/Photokinesis
QMAKE_RPATHDIR = $${QMAKE_LIBDIR}

SOURCES += \
    $${SRC_DIR}/session.cpp \
    fake_json_serializer.cpp \
    fake_upload_model_session.cpp \
    tst_session.cpp

HEADERS += \
    $${SRC_DIR}/json_serializer.h \
    $${SRC_DIR}/session.h \
    $${SRC_DIR}/upload_model_session.h \
    fake_json_serializer.h \
    fake_upload_model_session.h
