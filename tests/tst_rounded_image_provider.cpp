/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rounded_image_provider.h"

#include <QDir>
#include <QSignalSpy>
#include <QSize>
#include <QTest>

using namespace Photokinesis;

class RoundedImageProviderTest: public QObject
{
    Q_OBJECT

public:
    RoundedImageProviderTest();

private Q_SLOTS:
    void testRequestImage_data();
    void testRequestImage();

private:
    QDir m_dataDir;
};

RoundedImageProviderTest::RoundedImageProviderTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void RoundedImageProviderTest::testRequestImage_data()
{
    QTest::addColumn<QString>("id");
    QTest::addColumn<QList<QPoint>>("transparentPoints");
    QTest::addColumn<QList<QPoint>>("opaquePoints");

    QTest::newRow("null radius") <<
        QString("0/") +
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QList<QPoint>{} <<
        QList<QPoint>{ {0, 0} };

    QTest::newRow("6 px") <<
        QString("6/") +
        QUrl::fromLocalFile(m_dataDir.filePath("image5.jpg")).toString() <<
        QList<QPoint>{ {0, 0}, {0, 1}, {1, 0} } <<
        QList<QPoint>{ {4, 4} };
}

void RoundedImageProviderTest::testRequestImage()
{
    QFETCH(QString, id);
    QFETCH(QList<QPoint>, transparentPoints);
    QFETCH(QList<QPoint>, opaquePoints);

    RoundedImageProvider imageProvider;
    QSize size;
    QImage image = imageProvider.requestImage(id, &size, QSize());

    for (const QPoint &p: opaquePoints) {
        QCOMPARE(qAlpha(image.pixel(p)), 255);
    }

    for (const QPoint &p: transparentPoints) {
        QCOMPARE(qAlpha(image.pixel(p)), 0);
    }
}

QTEST_GUILESS_MAIN(RoundedImageProviderTest)

#include "tst_rounded_image_provider.moc"
