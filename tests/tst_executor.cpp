/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "executor.h"
#include <Photokinesis/UploadModel>
#include <QDebug>
#include <QRegularExpression>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Photokinesis;

struct Update {
    int row;
    int uploader;
    int status;
    int expectedRow;
    int expectedUploader;
};
Q_DECLARE_METATYPE(Update)

typedef QVector<Update> UpdateSequence;

class ExecutorTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void cleanup();
    void testScheduling_data();
    void testScheduling();
    void testScheduling1xUp_data();
    void testScheduling1xUp();
    void testPauseOrResume();

private:
    Update m_currentUpdate;
    QList<QVariantList> m_receivedSlotReady;

};

void ExecutorTest::cleanup()
{
    if (QTest::currentTestFailed()) {
        qDebug() << "Current update:";
        qDebug() << "  Row" << m_currentUpdate.row <<
            "  Uploader" << m_currentUpdate.uploader <<
            "  Status" << m_currentUpdate.status <<
            "  Expected row" << m_currentUpdate.expectedRow <<
            "  Expected uploader" << m_currentUpdate.expectedUploader;
        qDebug() << "Received slotReady signals:" << m_receivedSlotReady;
    }
}

void ExecutorTest::testScheduling_data()
{
    QTest::addColumn<QVariantList>("activeUploaders");
    QTest::addColumn<int>("maxJobs");
    QTest::addColumn<int>("totalRows");
    QTest::addColumn<UpdateSequence>("updateSequence");

    QTest::newRow("single uploader") <<
        QVariantList { 0 } <<
        2 <<
        3 <<
        UpdateSequence {
            { -1, -1, -1, 0, 0 },
            { -1, -1, -1, 1, 0 },
            { 1, 0, 33, -1, -1 },
            { 0, 0, 50, -1, -1 },
            { 1, 0, 66, -1, -1 },
            { 0, 0, 101, 2, 0 },
            { 1, 0, 99, -1, -1 },
            { 2, 0, 102, -1, -1 },
            { 1, 0, 101, -1, -1 },
        };

    QTest::newRow("two uploaders") <<
        QVariantList { 0, 1 } <<
        2 <<
        2 <<
        UpdateSequence {
            { -1, -1, -1, 0, 0 },
            { -1, -1, -1, 0, 1 },
            { 0, 1, 33, -1, -1 },
            { 0, 0, 50, -1, -1 },
            { 0, 0, 66, -1, -1 },
            { 0, 1, 101, 1, 0 },
            { 1, 0, 99, -1, -1 },
            { 0, 0, 102, 1, 1 },
            { 1, 1, 12, -1, -1 },
            { 1, 0, 101, -1, -1 },
            { 1, 1, 101, -1, -1 },
        };
}

void ExecutorTest::testScheduling()
{
    QFETCH(QVariantList, activeUploaders);
    QFETCH(int, maxJobs);
    QFETCH(int, totalRows);
    QFETCH(UpdateSequence, updateSequence);

    int uploadersCount = activeUploaders.last().toInt() + 1;
    UploadModel model;
    for (int i = 0; i < totalRows; i++) {
        model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    }
    model.setUploadersCount(uploadersCount);

    Executor executor;
    executor.setActiveUploaders(activeUploaders);
    executor.setMaxJobs(maxJobs);
    executor.setMaxJobsPerUploader(10);
    executor.setModel(&model);
    QSignalSpy slotReady(&executor, SIGNAL(slotReady(int,int)));
    QSignalSpy finished(&executor, SIGNAL(finished()));

    executor.start();

    for (const auto &update: updateSequence) {
        m_currentUpdate = update;
        QCOMPARE(finished.count(), 0);

        if (update.row >= 0) {
            model.setProgress(update.row, update.uploader, update.status);
        }

        m_receivedSlotReady = slotReady;
        if (update.expectedRow >= 0) {
            QVERIFY(slotReady.count() > 0);
            QCOMPARE(slotReady.at(0).at(0).toInt(), update.expectedRow);
            QCOMPARE(slotReady.at(0).at(1).toInt(), update.expectedUploader);
            slotReady.removeFirst();
        } else {
            QCOMPARE(slotReady.count(), 0);
        }
    }

    QCOMPARE(finished.count(), 1);
}

void ExecutorTest::testScheduling1xUp_data()
{
    QTest::addColumn<QVariantList>("activeUploaders");
    QTest::addColumn<int>("maxJobs");
    QTest::addColumn<int>("totalRows");
    QTest::addColumn<UpdateSequence>("updateSequence");

    QTest::newRow("single uploader") <<
        QVariantList { 0 } <<
        2 <<
        3 <<
        UpdateSequence {
            { -1, -1, -1, 0, 0 },
            { 0, 0, 50, -1, -1 },
            { 0, 0, 101, 1, 0 },
            { 1, 0, 33, -1, -1 },
            { 1, 0, 66, -1, -1 },
            { 1, 0, 99, -1, -1 },
            { 2, 0, 102, -1, -1 },
            { 1, 0, 101, -1, -1 },
        };

    QTest::newRow("two uploaders") <<
        QVariantList { 0, 1 } <<
        2 <<
        2 <<
        UpdateSequence {
            { -1, -1, -1, 0, 0 },
            { -1, -1, -1, 0, 1 },
            { 0, 1, 33, -1, -1 },
            { 0, 0, 50, -1, -1 },
            { 0, 0, 66, -1, -1 },
            { 0, 1, 101, 1, 1 },
            { 0, 0, 102, 1, 0 },
            { 1, 1, 12, -1, -1 },
            { 1, 0, 101, -1, -1 },
            { 1, 1, 101, -1, -1 },
        };

    QTest::newRow("many uploaders, only one selected") <<
        QVariantList { 3 } <<
        2 <<
        2 <<
        UpdateSequence {
            { -1, -1, -1, 0, 0 },
            { 0, 3, 33, -1, -1 },
            { 0, 0, 50, -1, -1 },
            { 0, 0, 66, -1, -1 },
            { 0, 3, 101, 1, 0 },
            { 1, 3, 12, -1, -1 },
            { 1, 0, 101, -1, -1 },
            { 1, 3, 101, -1, -1 },
        };

    QTest::newRow("many uploaders, two selected") <<
        QVariantList { 0, 2 } <<
        2 <<
        2 <<
        UpdateSequence {
            { -1, -1, -1, 0, 0 },
            { -1, -1, -1, 0, 1 },
            { 0, 2, 33, -1, -1 },
            { 0, 0, 50, -1, -1 },
            { 0, 2, 101, 1, 1 },
            { 1, 2, 12, -1, -1 },
            { 0, 0, 101, 1, 0 },
            { 1, 0, 54, -1, -1 },
            { 1, 2, 62, -1, -1 },
            { 1, 2, 101, -1, -1 },
            { 1, 0, 101, -1, -1 },
        };
}

void ExecutorTest::testScheduling1xUp()
{
    QFETCH(QVariantList, activeUploaders);
    QFETCH(int, maxJobs);
    QFETCH(int, totalRows);
    QFETCH(UpdateSequence, updateSequence);

    int uploadersCount = activeUploaders.last().toInt() + 1;
    UploadModel model;
    for (int i = 0; i < totalRows; i++) {
        model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    }
    model.setUploadersCount(uploadersCount);

    Executor executor;
    executor.setActiveUploaders(activeUploaders);
    executor.setMaxJobs(maxJobs);
    executor.setModel(&model);
    QSignalSpy slotReady(&executor, SIGNAL(slotReady(int,int)));
    QSignalSpy finished(&executor, SIGNAL(finished()));

    executor.start();

    for (const auto &update: updateSequence) {
        m_currentUpdate = update;
        QCOMPARE(finished.count(), 0);

        if (update.row >= 0) {
            model.setProgress(update.row, update.uploader, update.status);
        }

        m_receivedSlotReady = slotReady;
        if (update.expectedRow >= 0) {
            QVERIFY(slotReady.count() > 0);
            QCOMPARE(slotReady.at(0).at(0).toInt(), update.expectedRow);
            QCOMPARE(slotReady.at(0).at(1).toInt(), update.expectedUploader);
            slotReady.removeFirst();
        } else {
            QCOMPARE(slotReady.count(), 0);
        }
    }

    QCOMPARE(finished.count(), 1);
}

void ExecutorTest::testPauseOrResume()
{
    QTest::ignoreMessage(QtWarningMsg,
                         QRegularExpression("Cannot pause or resume in state.*"
                                            "Idle"));

    QVariantList activeUploaders { 0, 1 };
    int totalRows = 2;
    int maxJobs = 2;

    int uploadersCount = activeUploaders.last().toInt() + 1;
    UploadModel model;
    for (int i = 0; i < totalRows; i++) {
        model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    }
    model.setUploadersCount(uploadersCount);

    Executor executor;
    executor.setActiveUploaders(activeUploaders);
    executor.setMaxJobs(maxJobs);
    executor.setModel(&model);
    QSignalSpy slotReady(&executor, SIGNAL(slotReady(int,int)));
    QSignalSpy finished(&executor, SIGNAL(finished()));

    QCOMPARE(executor.status(), Executor::Idle);

    // expect a failure
    executor.pauseOrResume();

    executor.start();

    QCOMPARE(slotReady.count(), 2);
    model.setProgress(0, 0, 101);
    QCOMPARE(slotReady.count(), 3);
    slotReady.clear();
    model.setProgress(0, 1, 50);
    model.setProgress(1, 0, 10);
    executor.pauseOrResume();
    QCOMPARE(executor.status(), Executor::Pausing);
    model.setProgress(0, 1, 101);
    QCOMPARE(executor.status(), Executor::Pausing);
    model.setProgress(1, 0, 101);
    QCOMPARE(executor.status(), Executor::Paused);
    QCOMPARE(slotReady.count(), 0);

    executor.pauseOrResume();
    QCOMPARE(slotReady.count(), 1);
    QCOMPARE(executor.status(), Executor::Busy);
    model.setProgress(1, 1, 101);

    QCOMPARE(executor.status(), Executor::Finished);
    QCOMPARE(finished.count(), 1);
}

QTEST_GUILESS_MAIN(ExecutorTest)

#include "tst_executor.moc"
