/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "upload_model_session.h"

#include "Photokinesis/upload_model.h"

#include <QBuffer>
#include <QDebug>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Photokinesis;

struct Cell {
    int row;
    int col;
    UploadModel::Roles role;
    QVariant value;
};
Q_DECLARE_METATYPE(Cell)

class UploadModelSessionTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testVirtualDestructor();
    void testDumpAndLoad_data();
    void testDumpAndLoad();
    void testInvalidLoad_data();
    void testInvalidLoad();
    void testSessionFinished_data();
    void testSessionFinished();
    void testLoadingSessionFlag();
};

void UploadModelSessionTest::testVirtualDestructor()
{
    UploadModelSession *session = new UploadModelSession();
    delete session;
}

void UploadModelSessionTest::testDumpAndLoad_data()
{
    QTest::addColumn<int>("uploadersCount");
    QTest::addColumn<QStringList>("selectedUploaders");
    QTest::addColumn<QStringList>("urls");
    QTest::addColumn<QVector<Cell>>("values");

    QTest::newRow("2x3, all selected") <<
        3 <<
        QStringList { "First", "Second", "Third", } <<
        QStringList {
            "file:///one.jpg", "file:///two.jpg",
        } <<
        QVector<Cell> {
            { 0, 0, UploadModel::TitleRole, "The title" },
            { 0, 2, UploadModel::TitleRole, "Title for second uploader" },
            { 0, 2, UploadModel::TagsRole, QStringList {
                    "Tags", "for", "second", "uploader"
                }
            },
            { 0, 1, UploadModel::UploadStatusRawRole, UploadModel::Uploading },
            { 0, 3, UploadModel::UploadStatusRawRole, UploadModel::Failed },
            { 1, 0, UploadModel::DescriptionRole, "Cat and dogs" },
            { 1, 0, UploadModel::SubDirectoryRole, "some/dir" },
            { 1, 0, UploadModel::TagsRole, QStringList { "nice", "photo" }},
            { 1, 1, UploadModel::DescriptionRole, "Alternate description" },
            { 1, 2, UploadModel::UploadStatusRawRole, UploadModel::Uploaded },
        };
}

void UploadModelSessionTest::testDumpAndLoad()
{
    QFETCH(int, uploadersCount);
    QFETCH(QStringList, selectedUploaders);
    QFETCH(QStringList, urls);
    QFETCH(QVector<Cell>, values);

    UploadModel model;
    model.setUploadersCount(uploadersCount);
    for (const QString &url: urls) {
        model.addPhoto(QUrl(url));
    }
    for (const Cell &cell: values) {
        QModelIndex index = model.index(cell.row, cell.col);
        model.setData(index, cell.value, cell.role);
    }

    UploadModelSession session;
    session.setUploadModel(&model);

    QBuffer buffer;
    QVERIFY(buffer.open(QIODevice::WriteOnly));
    QVERIFY(session.dumpSession(&buffer, selectedUploaders));
    buffer.close();

    UploadModel modelCopy;
    modelCopy.setUploadersCount(uploadersCount);
    UploadModelSession sessionLoad;
    sessionLoad.setUploadModel(&modelCopy);
    QVERIFY(buffer.open(QIODevice::ReadOnly));
    QVERIFY(sessionLoad.loadSession(&buffer, selectedUploaders));

    QStringList actualUrls;
    for (int row = 0; row < modelCopy.rowCount(); row++) {
        QModelIndex index = modelCopy.index(row, 0);
        actualUrls.append(index.data(UploadModel::UrlRole).toString());
    }
    QCOMPARE(actualUrls, urls);

    for (const Cell &cell: values) {
        QModelIndex index = modelCopy.index(cell.row, cell.col);
        QVariant actualValue = index.data(cell.role);
        QCOMPARE(actualValue, cell.value);
    }
}

void UploadModelSessionTest::testInvalidLoad_data()
{
    QTest::addColumn<QByteArray>("dumpContents");

    QTest::newRow("empty") <<
        QByteArray();

    QTest::newRow("negative rowcount") <<
        QByteArrayLiteral(
            "\x00\x00\x00\x00" // m_incompleteUploadsCount, QVector
            "\xff\xff\xff\xff" // rowcount
            );

    QTest::newRow("excessive rowcount") <<
        QByteArrayLiteral(
            "\x00\x00\x00\x00" // m_incompleteUploadsCount, QVector
            "\x0f\xff\xff\xff" // rowcount
            );

    QTest::newRow("No roles") <<
        QByteArrayLiteral(
            "\x00\x00\x00\x00" // m_incompleteUploadsCount, QVector
            "\x00\x00\x00\x03" // rowcount
            "\x00\x00\x00\x00" // roles, QVector
            );

    QTest::newRow("First role is not URL") <<
        QByteArrayLiteral(
            "\x00\x00\x00\x00" // m_incompleteUploadsCount, QVector
            "\x00\x00\x00\x03" // rowcount
            "\x00\x00\x00\x01" // roles, QVector
            "\x00\x00" // role (int16_t)
            );
}

void UploadModelSessionTest::testInvalidLoad()
{
    QFETCH(QByteArray, dumpContents);

    QBuffer buffer(&dumpContents);
    QVERIFY(buffer.open(QIODevice::ReadOnly));

    UploadModel model;
    model.setUploadersCount(3);
    UploadModelSession session;
    session.setUploadModel(&model);

    QStringList selectedUploaders { "First", "Second", "Third", };

    QVERIFY(!session.loadSession(&buffer, selectedUploaders));
}

void UploadModelSessionTest::testSessionFinished_data()
{
    QTest::addColumn<QStringList>("urls");
    QTest::addColumn<QVector<Cell>>("values");
    QTest::addColumn<bool>("expectedIsFinished");

    QTest::newRow("All done") <<
        QStringList {
            "file:///one.jpg", "file:///two.jpg",
        } <<
        QVector<Cell> {
            { 0, 0, UploadModel::TitleRole, "title one" },
            { 1, 0, UploadModel::TitleRole, "title two" },
            { 0, 1, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 0, 2, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 1, 1, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 1, 2, UploadModel::UploadStatusRole, UploadModel::Uploaded },
        } <<
        true;

    QTest::newRow("Some still uploading") <<
        QStringList {
            "file:///one.jpg", "file:///two.jpg",
        } <<
        QVector<Cell> {
            { 0, 0, UploadModel::TitleRole, "title one" },
            { 1, 0, UploadModel::TitleRole, "title two" },
            { 0, 1, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 0, 2, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 1, 1, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 1, 2, UploadModel::UploadStatusRole, UploadModel::Uploading },
        } <<
        false;

    QTest::newRow("Some failed") <<
        QStringList {
            "file:///one.jpg", "file:///two.jpg",
        } <<
        QVector<Cell> {
            { 0, 0, UploadModel::TitleRole, "title one" },
            { 1, 0, UploadModel::TitleRole, "title two" },
            { 0, 1, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 0, 2, UploadModel::UploadStatusRole, UploadModel::Failed },
            { 1, 1, UploadModel::UploadStatusRole, UploadModel::Uploaded },
            { 1, 2, UploadModel::UploadStatusRole, UploadModel::Uploaded },
        } <<
        false;
}

void UploadModelSessionTest::testSessionFinished()
{
    QFETCH(QStringList, urls);
    QFETCH(QVector<Cell>, values);
    QFETCH(bool, expectedIsFinished);

    UploadModel model;
    model.setUploadersCount(2);
    for (const QString &url: urls) {
        model.addPhoto(QUrl(url));
    }
    for (const Cell &cell: values) {
        QModelIndex index = model.index(cell.row, cell.col);
        model.setData(index, cell.value, cell.role);
    }

    UploadModelSession session;
    session.setUploadModel(&model);
    QStringList selectedUploaders { "First", "Second", };

    QBuffer buffer;
    QVERIFY(buffer.open(QIODevice::WriteOnly));
    QVERIFY(session.dumpSession(&buffer, selectedUploaders));
    buffer.close();

    UploadModelSession sessionLoad;
    QVERIFY(buffer.open(QIODevice::ReadOnly));
    QCOMPARE(sessionLoad.sessionIsFinished(&buffer, selectedUploaders),
             expectedIsFinished);
}

void UploadModelSessionTest::testLoadingSessionFlag()
{
    // This is the container for our session data
    QBuffer buffer;

    QStringList selectedUploaders { "First" };

    // Prepare the session
    {
        UploadModel model;
        model.setUploadersCount(1);

        QString fileUrl("file:///one.jpg");
        model.addPhoto(QUrl(fileUrl));

        UploadModelSession session;
        session.setUploadModel(&model);

        QVERIFY(buffer.open(QIODevice::WriteOnly));
        QVERIFY(session.dumpSession(&buffer, selectedUploaders));
        buffer.close();
    }

    // Prepare a new model, on which we test the metadata flag
    UploadModel model;
    bool rowsInsertedCalled = false;
    bool wasLoadingSession = false;
    QObject::connect(&model, &QAbstractItemModel::rowsInserted,
                     [&]() {
        rowsInsertedCalled = true;
        wasLoadingSession = model.isLoadingSession();
    });
    model.setUploadersCount(1);

    UploadModelSession sessionLoad;
    sessionLoad.setUploadModel(&model);

    QVERIFY(!model.isLoadingSession());
    QVERIFY(buffer.open(QIODevice::ReadOnly));
    QVERIFY(sessionLoad.loadSession(&buffer, selectedUploaders));
    QVERIFY(!model.isLoadingSession());

    QVERIFY(rowsInsertedCalled);
    QVERIFY(wasLoadingSession);
}

QTEST_GUILESS_MAIN(UploadModelSessionTest)

#include "tst_upload_model_session.moc"
