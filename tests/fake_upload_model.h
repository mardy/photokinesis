/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_FAKE_UPLOAD_MODEL_H
#define PHOTOKINESIS_FAKE_UPLOAD_MODEL_H

#include "Photokinesis/upload_model.h"

#include <QHash>
#include <QObject>
#include <QScopedPointer>
#include <QString>

namespace Photokinesis {

class UploadModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(UploadModel)

    typedef QHash<int,QVariant> CellData;
    typedef QVector<CellData> RowData;

public:
    UploadModelPrivate(UploadModel *q);
    virtual ~UploadModelPrivate();

    static UploadModelPrivate *mockFor(UploadModel *o) { return o->d_ptr.data(); }

private:
    int m_uploadersCount;
    bool m_isLoadingSession;
    QVector<RowData> m_rows;
    QHash<int, QByteArray> m_roles;
    UploadModel *q_ptr;
};

} // namespace

#endif // PHOTOKINESIS_FAKE_UPLOAD_MODEL_H
