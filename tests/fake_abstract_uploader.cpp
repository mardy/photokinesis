/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_abstract_uploader.h"

#include <QDebug>

using namespace Photokinesis;

static AbstractUploaderMocked *m_instance = nullptr;

static const QString keyMetaData = QStringLiteral("MetaData");
static const QString keyName = QStringLiteral("name");

AbstractUploaderPrivate::AbstractUploaderPrivate(
        AbstractUploader *q,
        const UploaderProfile &profile):
    QObject(q),
    m_profile(profile),
    q_ptr(q)
{
    m_instance = this;
}

AbstractUploaderPrivate::~AbstractUploaderPrivate()
{
    m_instance = nullptr;
}

AbstractUploader::AbstractUploader(const UploaderProfile &profile,
                                   QObject *parent):
    QObject(parent),
    d_ptr(new AbstractUploaderPrivate(this, profile))
{
}

AbstractUploader::~AbstractUploader()
{
}

void AbstractUploader::setName(const QString &name)
{
    Q_D(AbstractUploader);
    d->m_name = name;
}

QString AbstractUploader::name() const
{
    Q_D(const AbstractUploader);
    return d->m_name;
}

void AbstractUploader::setDisplayName(const QString &displayName)
{
    Q_D(AbstractUploader);
    d->m_displayName = displayName;
}

QString AbstractUploader::displayName() const
{
    Q_D(const AbstractUploader);
    return d->m_displayName;
}

void AbstractUploader::setIcon(const QUrl &icon)
{
    Q_D(AbstractUploader);
    d->m_icon = icon;
}

QUrl AbstractUploader::icon() const
{
    Q_D(const AbstractUploader);
    return d->m_icon;
}

QStringList AbstractUploader::supportedOptions() const
{
    Q_D(const AbstractUploader);
    return d->m_supportedOptions;
}

bool AbstractUploader::hasOption(const QString &name) const
{
    Q_D(const AbstractUploader);
    return d->m_supportedOptions.contains(name);
}

void AbstractUploader::setEnabled(bool isEnabled)
{
    Q_D(AbstractUploader);
    if (isEnabled != d->m_isEnabled) {
        d->m_isEnabled = isEnabled;
        Q_EMIT enabledChanged();
    }
}

bool AbstractUploader::isEnabled() const
{
    Q_D(const AbstractUploader);
    return d->m_isEnabled;
}

void AbstractUploader::setUserId(const QString &userId)
{
    Q_D(AbstractUploader);
    if (userId == d->m_userId) return;
    d->m_userId = userId;
    Q_EMIT userIdChanged();
    if (d->m_userName.isEmpty()) {
        Q_EMIT userNameChanged();
    }
}

QString AbstractUploader::userId() const
{
    Q_D(const AbstractUploader);
    return d->m_userId;
}

void AbstractUploader::setUserName(const QString &userName)
{
    Q_D(AbstractUploader);
    if (userName == d->m_userName) return;
    d->m_userName = userName;
    Q_EMIT userNameChanged();
}

QString AbstractUploader::userName() const
{
    Q_D(const AbstractUploader);
    return !d->m_userName.isEmpty() ? d->m_userName : d->m_userId;
}

void AbstractUploader::setAccountInfo(const QString &accountInfo)
{
    Q_D(AbstractUploader);
    d->m_accountInfo = accountInfo;
    Q_EMIT accountInfoChanged();
}

QString AbstractUploader::accountInfo() const
{
    Q_D(const AbstractUploader);
    return d->m_accountInfo;
}

void AbstractUploader::setStatus(Status status)
{
    Q_D(AbstractUploader);
    if (status != d->m_status) {
        d->m_status = status;
        Q_EMIT statusChanged();
    }
}

AbstractUploader::Status AbstractUploader::status() const
{
    Q_D(const AbstractUploader);
    return d->m_status;
}

void AbstractUploader::setUploadsUrl(const QUrl &uploadsUrl)
{
    Q_D(AbstractUploader);
    d->m_uploadsUrl = uploadsUrl;
    Q_EMIT uploadsUrlChanged();
}

QUrl AbstractUploader::uploadsUrl() const
{
    Q_D(const AbstractUploader);
    return d->m_uploadsUrl;
}

void AbstractUploader::setAlbums(const QJsonArray &albums)
{
    Q_D(AbstractUploader);
    d->m_albums = albums;
    Q_EMIT albumsChanged();
}

QJsonArray AbstractUploader::albums() const
{
    Q_D(const AbstractUploader);
    return d->m_albums;
}

void AbstractUploader::setDefaultAlbumId(const QString &id)
{
    Q_D(AbstractUploader);
    d->m_defaultAlbumId = id;
    Q_EMIT defaultAlbumIdChanged();
}

QString AbstractUploader::defaultAlbumId() const
{
    Q_D(const AbstractUploader);
    return d->m_defaultAlbumId;
}

void AbstractUploader::setMaxSize(const QSize &size)
{
    Q_D(AbstractUploader);
    d->m_maxSize = size;
    Q_EMIT maxSizeChanged();
}

QSize AbstractUploader::maxSize() const
{
    Q_D(const AbstractUploader);
    return d->m_maxSize;
}

void AbstractUploader::setOptionsContainer(QQuickItem *container)
{
    Q_UNUSED(container);
}

QQuickItem *AbstractUploader::optionsContainer() const
{
    return nullptr;
}

void AbstractUploader::setOptionComponents(const QStringList &components)
{
    Q_UNUSED(components);
}

void AbstractUploader::setNetworkAccessManager(QNetworkAccessManager *nam)
{
    Q_D(AbstractUploader);
    d->m_nam = nam;
}

QNetworkAccessManager *AbstractUploader::networkAccessManager() const
{
    Q_D(const AbstractUploader);
    return d->m_nam;
}

void AbstractUploader::setErrorMessage(const QString &message)
{
    Q_D(AbstractUploader);
    d->m_errorMessage = message;
}

QString AbstractUploader::errorMessage() const
{
    Q_D(const AbstractUploader);
    return d->m_errorMessage;
}

void AbstractUploader::processImage(const QUrl &url,
                                    const QJsonObject &options)
{
    Q_UNUSED(url);
    Q_UNUSED(options);
}

void AbstractUploader::logout()
{
}

void AbstractUploader::authenticate(QQuickItem *parent)
{
    Q_UNUSED(parent);
}

void AbstractUploader::uploadFile(const QUrl &url,
                                  const QJsonObject &options)
{
    Q_UNUSED(url);
    Q_UNUSED(options);
}

void AbstractUploader::changeAccount(QQuickItem *parent)
{
    Q_UNUSED(parent);
}

QStringList AbstractUploader::knownUserIds() const
{
    return QStringList();
}

void AbstractUploader::storeAccountSettings(const QJsonObject &settings)
{
    Q_UNUSED(settings);
}

QJsonObject AbstractUploader::loadAccountSettings() const
{
    return QJsonObject();
}

void AbstractUploader::storeSettings()
{
}

void AbstractUploader::loadSettings()
{
}

void AbstractUploader::setSettings(const QJsonObject &settings)
{
    Q_UNUSED(settings);
}

QJsonObject AbstractUploader::getSettings() const
{
    return QJsonObject();
}

QVariantMap AbstractUploader::checkSize(const QSize &size)
{
    /* In case the proposed size is not acceptable, implementations should
     * add an "error" field containing a message for the user, and a
     * "size" member with the closest acceptable size. */
    return QVariantMap {
        { "size", size },
    };
}
