/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Photokinesis/AbstractUploader>
#include <Photokinesis/UiHelpers>
#include <QDebug>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QQuickItem>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <QtWebView>

using namespace Photokinesis;

class FakeAuthenticator;

class ActionRequest: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUrl url MEMBER m_url CONSTANT)
    Q_PROPERTY(QUrl finalUrl MEMBER m_finalUrl CONSTANT)

public:
    ActionRequest(FakeAuthenticator *auth,
                  const QUrl &url, const QUrl &finalUrl);

    Q_INVOKABLE void setResult(const QUrl &url);
    Q_INVOKABLE void reject();

private:
    QUrl m_url;
    QUrl m_finalUrl;
    FakeAuthenticator *m_authenticator;
};

class FakeAuthenticator: public QObject
{
    Q_OBJECT
public:
    void requestAuthentication(const QUrl &url,
                               const QUrl &finalUrl = QUrl()) {
        ActionRequest *request = new ActionRequest(this, url, finalUrl);
        Q_EMIT actionRequested(request);
    }
    void emitFinished() { Q_EMIT finished(); }

    void endAction(const QUrl &url) { Q_EMIT actionFinished(url); }

Q_SIGNALS:
    void actionRequested(ActionRequest *request);
    void actionFinished(const QUrl &url);
    void finished();
};

ActionRequest::ActionRequest(FakeAuthenticator *auth,
                             const QUrl &url, const QUrl &finalUrl):
    QObject(auth), m_url(url), m_finalUrl(finalUrl),
    m_authenticator(auth)
{
}

void ActionRequest::setResult(const QUrl &url) {
    m_authenticator->endAction(url);
}
void ActionRequest::reject() {
    m_authenticator->endAction(QUrl());
}

class UiHelpersTest: public QObject
{
    Q_OBJECT

public:
    UiHelpersTest();

private Q_SLOTS:
    void testCreateWebview();
};

UiHelpersTest::UiHelpersTest()
{
    QtWebView::initialize();
}

void UiHelpersTest::testCreateWebview()
{
    QQmlEngine engine;
    QQmlComponent component(&engine);
    component.setData("import QtQuick 2.0\n"
                      "\n"
                      "Rectangle {\n"
                      "    width: 300; height: 200\n"
                      "}\n",
                      QUrl());

    QObject *object = component.create();
    QVERIFY(object);
    QQuickItem *parentItem = qobject_cast<QQuickItem *>(object);

    FakeAuthenticator authenticator;
    QSignalSpy actionFinished(&authenticator,
                              SIGNAL(actionFinished(const QUrl&)));

    QQuickItem *item = UiHelpers::createWebview(parentItem, &authenticator);
    QVERIFY(item);
    QCOMPARE(item->property("wantsVisibility").toBool(), false);

    QCOMPARE(item->parent(), parentItem);
    QCOMPARE(item->parentItem(), parentItem);

    authenticator.requestAuthentication(QUrl("about:blank"),
                                        QUrl("about:blank"));

    actionFinished.wait();
    QCOMPARE(actionFinished.at(0).at(0).toString(), QString("about:blank"));
    QCOMPARE(item->property("wantsVisibility").toBool(), true);

    authenticator.emitFinished();
    QCOMPARE(item->property("wantsVisibility").toBool(), false);

    delete object;
}

QTEST_MAIN(UiHelpersTest)

#include "tst_ui_helpers.moc"
