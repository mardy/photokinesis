/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_FAKE_UPLOAD_MODEL_SESSION_H
#define PHOTOKINESIS_FAKE_UPLOAD_MODEL_SESSION_H

#include "upload_model_session.h"

#include <QHash>
#include <QObject>
#include <QString>

namespace Photokinesis {

class UploadModelSessionPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(UploadModelSession)

public:
    UploadModelSessionPrivate();
    ~UploadModelSessionPrivate();

    static UploadModelSessionPrivate *mockFor(UploadModelSession *o) {
        return o->d_ptr.data();
    }

    void setDumpSessionReply(bool success) { m_dumpSessionReply = success; }
    void setLoadSessionReply(bool success) { m_loadSessionReply = success; }
    void setSessionIsFinishedReply(bool finished) {
        m_sessionIsFinishedReply = finished;
    }

    UploadModel *uploadModel() const { return m_model; }

Q_SIGNALS:
    void instanceCreated(UploadModelSession *instance);
    void dumpSessionCalled(QIODevice *device,
                           const QStringList &uploaders);
    void loadSessionCalled(QIODevice *device,
                           const QStringList &uploaders);
    void sessionIsFinishedCalled(QIODevice *device,
                                 const QStringList &selectedUploaders);

private:
    UploadModelSessionPrivate(UploadModelSession *q);

private:
    UploadModel *m_model;
    bool m_dumpSessionReply;
    bool m_loadSessionReply;
    bool m_sessionIsFinishedReply;
    bool m_mustDelete;
    UploadModelSession *q_ptr;
};

using MockUploadModelSession = UploadModelSessionPrivate;

} // namespace

#endif // PHOTOKINESIS_FAKE_UPLOAD_MODEL_SESSION_H
