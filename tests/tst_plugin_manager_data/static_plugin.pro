TARGET=StaticPlugin
include(plugin.pri)
CONFIG += \
    static

DEFINES += \
    METADATA_FILE=\\\"static_plugin.json\\\" \
    UPLOADER_POINTER=0x8000
