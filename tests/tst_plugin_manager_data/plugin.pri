include(../../common-config.pri)
TEMPLATE = lib
CONFIG += \
    plugin

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib
LIB += -lPhotokinesis
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/Photokinesis

DEFINES += \
    PLUGIN_CLASS_NAME="$${TARGET}"

OBJECTS_DIR = $${TARGET}.obj
MOC_DIR = $${TARGET}.moc

HEADERS += \
    plugin.h
