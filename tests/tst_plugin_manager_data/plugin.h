/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Photokinesis/PluginInterface>

#include <QObject>

namespace Photokinesis {

class AbstractUploader;

} // namespace

class PLUGIN_CLASS_NAME: public QObject, public Photokinesis::PluginInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "it.mardy.Photokinesis.PluginInterface" FILE METADATA_FILE)
    Q_INTERFACES(Photokinesis::PluginInterface)

public:
    Photokinesis::AbstractUploader *createUploader(
            const Photokinesis::UploaderProfile &profile,
            QObject *parent = 0) Q_DECL_OVERRIDE {
        Q_UNUSED(profile);
        Q_UNUSED(parent);
        return reinterpret_cast<Photokinesis::AbstractUploader*>(UPLOADER_POINTER);
    }
};
