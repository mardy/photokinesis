/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_TEST_UTILS_H
#define PHOTOKINESIS_TEST_UTILS_H

#include <QByteArray>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTest>
#include <QVariantMap>

namespace QTest {

template<>
char *toString(const QJsonObject &p)
{
    QJsonDocument doc(p);
    QByteArray ba = doc.toJson(QJsonDocument::Compact);
    return qstrdup(ba.data());
}

template<>
char *toString(const QJsonArray &p)
{
    QJsonDocument doc(p);
    QByteArray ba = doc.toJson(QJsonDocument::Compact);
    return qstrdup(ba.data());
}

template<>
char *toString(const QVariantMap &p)
{
    return toString(QJsonObject::fromVariantMap(p));
}

} // QTest namespace

#endif // PHOTOKINESIS_TEST_UTILS_H
