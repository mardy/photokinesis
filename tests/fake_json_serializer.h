/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_FAKE_JSON_SERIALIZER_H
#define PHOTOKINESIS_FAKE_JSON_SERIALIZER_H

#include "json_serializer.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QObject>

namespace Photokinesis {

class JsonSerializerPrivate: public QObject
{
    Q_OBJECT
    friend class JsonSerializer;

public:
    JsonSerializerPrivate();
    ~JsonSerializerPrivate();

    void setDumpReply(bool success) { m_dumpReply = success; }
    void setLoadObjectReply(const QJsonObject &json) {
        m_loadObjectReply = json;
    }
    void setLoadArrayReply(const QJsonArray &json) {
        m_loadArrayReply = json;
    }

Q_SIGNALS:
    void dumpObjectCalled(QIODevice *device, const QJsonObject &json);
    void loadObjectCalled(QIODevice *device);
    void dumpArrayCalled(QIODevice *device, const QJsonArray &json);
    void loadArrayCalled(QIODevice *device);

private:
    static JsonSerializerPrivate *instance();

private:
    bool m_dumpReply;
    QJsonObject m_loadObjectReply;
    QJsonArray m_loadArrayReply;
};

using MockJsonSerializer = JsonSerializerPrivate;

} // namespace

#endif // PHOTOKINESIS_FAKE_JSON_SERIALIZER_H
