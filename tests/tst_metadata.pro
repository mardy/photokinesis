TARGET = tst_metadata

include(tests.pri)

CONFIG -= exceptions_off

QT += \
    positioning

CONFIG += link_pkgconfig

packagesExist(exiv2) {
    PKGCONFIG += exiv2
} else {
    LIBS += $${EXIV2_LIBS} -lexiv2
    INCLUDEPATH += $${EXIV2_INCLUDEPATH}
}

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib/Photokinesis

SOURCES += \
    $${TOP_SRC_DIR}/lib/Photokinesis/metadata.cpp \
    tst_metadata.cpp

HEADERS += \
    $${TOP_SRC_DIR}/lib/Photokinesis/metadata.h

ITEMS_DIR = $${TOP_SRC_DIR}/tests/data
DEFINES += \
    ITEMS_DIR=\\\"$${ITEMS_DIR}\\\"
