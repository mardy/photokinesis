/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_upload_model.h"

#include <QDebug>

using namespace Photokinesis;

static UploadModelPrivate *m_privateInstance = 0;

UploadModelPrivate::UploadModelPrivate(UploadModel *q):
    QObject(),
    m_uploadersCount(0),
    m_isLoadingSession(false),
    q_ptr(q)
{
    m_privateInstance = this;

    m_roles[UploadModel::UrlRole] = "url";
    m_roles[UploadModel::FileNameRole] = "fileName";
    m_roles[UploadModel::ProgressRole] = "progress";
    m_roles[UploadModel::UploadStatusRole] = "uploadStatus";
    m_roles[UploadModel::TitleRole] = "title";
    m_roles[UploadModel::DescriptionRole] = "description";
    m_roles[UploadModel::TagsRole] = "tags";
    m_roles[UploadModel::ConfigRole] = "config";
    m_roles[UploadModel::SubDirectoryRole] = "subDirectory";
}

UploadModelPrivate::~UploadModelPrivate()
{
    m_privateInstance = nullptr;
}

UploadModel::UploadModel(QObject *parent):
    QAbstractItemModel(parent),
    d_ptr(new UploadModelPrivate(this))
{
}

UploadModel::~UploadModel()
{
}

void UploadModel::setUploadersCount(int uploadersCount)
{
    Q_D(UploadModel);
    d->m_uploadersCount = uploadersCount;
    Q_EMIT uploadersCountChanged();
}

int UploadModel::uploadersCount() const
{
    Q_D(const UploadModel);
    return d->m_uploadersCount;
}

void UploadModel::setConfig(const QJsonObject &config)
{
    Q_UNUSED(config);
    qWarning() << Q_FUNC_INFO << "not implemented";
}

QJsonObject UploadModel::config() const
{
    qWarning() << Q_FUNC_INFO << "not implemented";
    return QJsonObject();
}

void UploadModel::setIsLoadingSession(bool isLoadingSession)
{
    Q_D(UploadModel);
    d->m_isLoadingSession = isLoadingSession;
}

bool UploadModel::isLoadingSession() const
{
    Q_D(const UploadModel);
    return d->m_isLoadingSession;
}

void UploadModel::addPhoto(const QUrl &url)
{
    Q_D(UploadModel);
    int row = rowCount();
    beginInsertRows(QModelIndex(), row, row);
    d->m_rows.append(UploadModelPrivate::RowData(d->m_uploadersCount + 1));
    UploadModelPrivate::CellData &cell = d->m_rows.last().first();
    cell[UploadModel::UrlRole] = url;
    endInsertRows();
}

void UploadModel::addPhotos(const QList<QUrl> &urls,
                            const QUrl &baseUrl)
{
    Q_UNUSED(urls);
    Q_UNUSED(baseUrl);
    qWarning() << Q_FUNC_INFO << "not implemented";
}

void UploadModel::clear()
{
    Q_D(UploadModel);
    beginResetModel();
    d->m_rows.clear();
    endResetModel();
}

QVariantMap UploadModel::get(int row, int column) const
{
    Q_D(const UploadModel);
    QVariantMap ret;
    for (auto i = d->m_roles.constBegin(); i != d->m_roles.constEnd(); i++) {
        ret.insert(i.value(), data(index(row, column), i.key()));
    }
    return ret;
}

QVariant UploadModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

QVariant UploadModel::get(int row, int column, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, column), role);
}

bool UploadModel::set(int row, int column, const QString &roleName,
                      const QJsonValue &value)
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return setData(index(row, column), value, role);
}

void UploadModel::setTitle(int row, const QString &title)
{
    setData(index(row, 0), title, UploadModel::TitleRole);
}

void UploadModel::setDescription(int row, const QString &description)
{
    setData(index(row, 0), description, UploadModel::DescriptionRole);
}

void UploadModel::addTag(int row, const QString &tag)
{
    Q_UNUSED(row);
    Q_UNUSED(tag);
    qWarning() << Q_FUNC_INFO << "not implemented";
}

void UploadModel::removeTag(int row, const QString &tag)
{
    Q_UNUSED(row);
    Q_UNUSED(tag);
    qWarning() << Q_FUNC_INFO << "not implemented";
}

void UploadModel::setProgress(int row, int uploaderIndex, int progress)
{
    setStatus(row, uploaderIndex, static_cast<UploadStatus>(progress));
}

void UploadModel::setStatus(int row, int uploaderIndex, UploadStatus status)
{
    setData(index(row, uploaderIndex + 1), status, UploadModel::UploadStatusRole);
}

void UploadModel::resetFailures()
{
    qWarning() << Q_FUNC_INFO << "not implemented";
}

int UploadModel::columnCount(const QModelIndex &parent) const
{
    Q_D(const UploadModel);
    return parent.isValid() ? 0 : (d->m_uploadersCount + 1);
}

int UploadModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const UploadModel);
    return parent.isValid() ? 0 : d->m_rows.count();
}

QModelIndex UploadModel::index(int row, int column, const QModelIndex &) const
{
    return createIndex(row, column);
}

QModelIndex UploadModel::parent(const QModelIndex &) const
{
    return QModelIndex();
}

bool UploadModel::setData(const QModelIndex &index, const QVariant &value,
                          int role)
{
    Q_D(UploadModel);

    int row = index.row();
    if (row < 0 || row >= rowCount()) {
        qWarning() << "Invalid row access" << row;
        return false;
    }

    int column = index.column();
    if (column < 0 || column > d->m_uploadersCount) {
        qWarning() << "Invalid column access" << column;
        return false;
    }

    UploadModelPrivate::CellData &cell = d->m_rows[row][column];
    cell[role] = value;
    Q_EMIT dataChanged(index, index);
    return true;
}

QVariant UploadModel::data(const QModelIndex &index, int role) const
{
    Q_D(const UploadModel);

    int row = index.row();
    if (row < 0 || row >= rowCount()) {
        qWarning() << "Invalid row access" << row;
        return false;
    }

    int column = index.column();
    if (column < 0 || column > d->m_uploadersCount) {
        qWarning() << "Invalid column access" << column;
        return false;
    }

    const UploadModelPrivate::CellData &cell = d->m_rows[row][column];
    return cell.value(role);
}

QHash<int, QByteArray> UploadModel::roleNames() const
{
    Q_D(const UploadModel);
    return d->m_roles;
}
