/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_upload_model_session.h"

#include <QDebug>

using namespace Photokinesis;

static UploadModelSessionPrivate *m_privateInstance = 0;

UploadModelSessionPrivate::UploadModelSessionPrivate():
    UploadModelSessionPrivate(nullptr)
{
}

UploadModelSessionPrivate::UploadModelSessionPrivate(UploadModelSession *q):
    m_model(nullptr),
    m_dumpSessionReply(true),
    m_loadSessionReply(true),
    m_sessionIsFinishedReply(true),
    m_mustDelete(true),
    q_ptr(q)
{
    m_privateInstance = this;

    qRegisterMetaType<QIODevice*>();
}

UploadModelSessionPrivate::~UploadModelSessionPrivate()
{
    m_privateInstance = nullptr;
}

UploadModelSession::UploadModelSession()
{
    if (m_privateInstance) {
        m_privateInstance->q_ptr = this;
        d_ptr.reset(m_privateInstance);
        d_ptr->m_mustDelete = false;
        Q_EMIT d_ptr->instanceCreated(this);
    } else {
        d_ptr.reset(new UploadModelSessionPrivate(this));
    }
}

UploadModelSession::~UploadModelSession()
{
    if (!d_ptr->m_mustDelete) {
        // Steal the d_ptr, so that it won't be deleted
        d_ptr.take();
    }
}

void UploadModelSession::setUploadModel(UploadModel *model)
{
    Q_D(UploadModelSession);
    d->m_model = model;
}

bool UploadModelSession::dumpSession(QIODevice *device,
                                     const QStringList &uploaders) const
{
    Q_D(const UploadModelSession);
    Q_EMIT const_cast<UploadModelSessionPrivate*>(d)->
        dumpSessionCalled(device, uploaders);
    return d->m_dumpSessionReply;
}

bool UploadModelSession::loadSession(QIODevice *device,
                                     const QStringList &uploaders)
{
    Q_D(UploadModelSession);
    Q_EMIT d->loadSessionCalled(device, uploaders);
    return d->m_loadSessionReply;
}

bool UploadModelSession::sessionIsFinished(
    QIODevice *device,
    const QStringList &selectedUploaders)
{
    Q_D(const UploadModelSession);
    Q_EMIT const_cast<UploadModelSessionPrivate*>(d)->
        sessionIsFinishedCalled(device, selectedUploaders);
    return d->m_sessionIsFinishedReply;
}
