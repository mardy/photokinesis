/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Photokinesis/UploadModel"

#include "fake_metadata_loader.h"
#include "test_utils.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QPair>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include <QVector>

using namespace Photokinesis;

typedef QVector<UploadModel::UploadStatus> UploadStatuses;
typedef QVector<int> Progresses;
typedef QVector<QPair<int,int>> StatusSequence;
struct Cell {
    int row;
    int col;
    UploadModel::Roles role;
    QVariant value;
};
Q_DECLARE_METATYPE(Cell)

namespace QTest {

template<>
char *toString(const UploadStatuses &p)
{
    QByteArrayList statuses;
    for (const UploadModel::UploadStatus status: p) {
        statuses.append(QByteArray::number(int(status)));
    }
    QByteArray ba = statuses.join(' ');
    return qstrdup(ba.data());
}

template<>
char *toString(const QVector<UploadStatuses> &p)
{
    QByteArrayList rows;
    for (const UploadStatuses statuses: p) {
        char *str = toString(statuses);
        rows.append(QByteArray(str));
        delete[] str;
    }
    QByteArray ba = rows.join(" - ");
    return qstrdup(ba.data());
}

} // namespace

class UploadModelTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testStatuses_data();
    void testStatuses();
    void testResetFailures_data();
    void testResetFailures();
    void testProgress_data();
    void testProgress();
    void testOptions();
    void testBaseOptions();
    void testSetData_data();
    void testSetData();
    void testUpdateAllRow_data();
    void testUpdateAllRow();
    void testConfig_data();
    void testConfig();
    void testBaseUrl_data();
    void testBaseUrl();
    void testLoadMetadata_data();
    void testLoadMetadata();
    void testSubDirectoryRole();
};

void UploadModelTest::testStatuses_data()
{
    QTest::addColumn<int>("uploadersCount");
    QTest::addColumn<StatusSequence>("statusSequence");
    QTest::addColumn<UploadStatuses>("expectedStatuses");

    QTest::newRow("no uploaders") <<
        0 <<
        StatusSequence {} <<
        UploadStatuses {};

    QTest::newRow("no updates") <<
        3 <<
        StatusSequence {} <<
        UploadStatuses {
            UploadModel::Ready,
            UploadModel::Ready,
            UploadModel::Ready,
        };

    QTest::newRow("complete") <<
        4 <<
        StatusSequence {
            { 0, UploadModel::Uploading },
            { 2, UploadModel::Uploading },
            { 1, UploadModel::Uploading },
            { 3, UploadModel::Uploading },
            { 0, 57 },
            { 1, UploadModel::Failed },
            { 2, 45 },
            { 3, 12 },
            { 0, UploadModel::Uploaded },
            { 2, UploadModel::Uploaded },
            { 3, 33 },
        } <<
        UploadStatuses {
            UploadModel::Uploaded,
            UploadModel::Failed,
            UploadModel::Uploaded,
            UploadModel::Uploading,
        };
}

void UploadModelTest::testStatuses()
{
    QFETCH(int, uploadersCount);
    QFETCH(StatusSequence, statusSequence);
    QFETCH(UploadStatuses, expectedStatuses);

    UploadModel model;
    model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    QCOMPARE(model.rowCount(), 1);

    model.setUploadersCount(uploadersCount);

    for (const auto &update: statusSequence) {
        model.setProgress(0, update.first, update.second);
    }

    UploadStatuses statuses;
    for (int i = 0; i < uploadersCount; i++) {
        QModelIndex index = model.index(0, i + 1);
        statuses.append(index.data(UploadModel::UploadStatusRole).
                        value<UploadModel::UploadStatus>());
    }
    QCOMPARE(statuses, expectedStatuses);
}

void UploadModelTest::testResetFailures_data()
{
    QTest::addColumn<QVector<UploadStatuses>>("initialStatuses");
    QTest::addColumn<QVector<UploadStatuses>>("expectedStatuses");

    QTest::newRow("empty") <<
        QVector<UploadStatuses> {} <<
        QVector<UploadStatuses> {};

    QTest::newRow("no failures") <<
        QVector<UploadStatuses> {
            { UploadModel::Ready, UploadModel::Ready },
            { UploadModel::Uploading, UploadModel::Uploaded },
        } <<
        QVector<UploadStatuses> {
            { UploadModel::Ready, UploadModel::Ready },
            { UploadModel::Uploading, UploadModel::Uploaded },
        };

    QTest::newRow("some failures") <<
        QVector<UploadStatuses> {
            { UploadModel::Failed, UploadModel::Ready },
            { UploadModel::Uploading, UploadModel::Uploaded },
            { UploadModel::Failed, UploadModel::Ready },
            { UploadModel::Ready, UploadModel::Failed },
        } <<
        QVector<UploadStatuses> {
            { UploadModel::Ready, UploadModel::Ready },
            { UploadModel::Uploading, UploadModel::Uploaded },
            { UploadModel::Ready, UploadModel::Ready },
            { UploadModel::Ready, UploadModel::Ready },
        };
}

void UploadModelTest::testResetFailures()
{
    QFETCH(QVector<UploadStatuses>, initialStatuses);
    QFETCH(QVector<UploadStatuses>, expectedStatuses);

    UploadModel model;
    for (int row = 0; row < initialStatuses.count(); row++) {
        model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    }
    QCOMPARE(model.rowCount(), initialStatuses.count());

    int uploadersCount = initialStatuses.isEmpty() ?
        0 : initialStatuses.first().count();
    model.setUploadersCount(uploadersCount);

    for (int row = 0; row < initialStatuses.count(); row++)
        for (int i = 0; i < uploadersCount; i++) {
            model.setStatus(row, i, initialStatuses[row][i]);
        }

    model.resetFailures();

    QVector<UploadStatuses> statuses;
    for (int row = 0; row < initialStatuses.count(); row++) {
        UploadStatuses statusRow;
        for (int i = 0; i < uploadersCount; i++) {
            QModelIndex index = model.index(row, i + 1);
            statusRow.append(index.data(UploadModel::UploadStatusRole).
                             value<UploadModel::UploadStatus>());
        }
        statuses.append(statusRow);
    }
    QCOMPARE(statuses, expectedStatuses);
}

void UploadModelTest::testProgress_data()
{
    QTest::addColumn<int>("uploadersCount");
    QTest::addColumn<StatusSequence>("statusSequence");
    QTest::addColumn<Progresses>("expectedProgresses");

    QTest::newRow("no uploaders") <<
        0 <<
        StatusSequence {} <<
        Progresses {};

    QTest::newRow("no updates") <<
        3 <<
        StatusSequence {} <<
        Progresses { 0, 0, 0 };

    QTest::newRow("complete") <<
        4 <<
        StatusSequence {
            { 0, UploadModel::Uploading },
            { 2, UploadModel::Uploading },
            { 1, UploadModel::Uploading },
            { 3, UploadModel::Uploading },
            { 0, 57 },
            { 1, UploadModel::Failed },
            { 2, 45 },
            { 3, 12 },
            { 0, UploadModel::Uploaded },
            { 2, UploadModel::Uploaded },
            { 3, 33 },
        } <<
        Progresses { 100, 100, 100, 33 };
}

void UploadModelTest::testProgress()
{
    QFETCH(int, uploadersCount);
    QFETCH(StatusSequence, statusSequence);
    QFETCH(Progresses, expectedProgresses);

    UploadModel model;
    model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    QCOMPARE(model.rowCount(), 1);

    model.setUploadersCount(uploadersCount);

    for (const auto &update: statusSequence) {
        model.setProgress(0, update.first, update.second);
    }

    Progresses progresses;
    for (int i = 0; i < uploadersCount; i++) {
        QModelIndex index = model.index(0, i + 1);
        progresses.append(index.data(UploadModel::ProgressRole).toInt());
    }
    QCOMPARE(progresses, expectedProgresses);
}

void UploadModelTest::testOptions()
{
    UploadModel model;
    QSignalSpy dataChanged(&model, &UploadModel::dataChanged);
    model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    QCOMPARE(model.rowCount(), 1);

    model.setUploadersCount(3);

    QModelIndex index = model.index(0, 3);
    QCOMPARE(index.data(UploadModel::TitleRole), QVariant());
    QCOMPARE(dataChanged.count(), 0);

    QString house("House");
    model.set(0, 3, "title", house);
    QCOMPARE(dataChanged.count(), 1);
    QModelIndex changedIndex = dataChanged.at(0).at(0).value<QModelIndex>();
    QCOMPARE(changedIndex, index);
    dataChanged.clear();
    QCOMPARE(index.data(UploadModel::TitleRole).toString(), house);
    QCOMPARE(index.data(UploadModel::DescriptionRole), QVariant());

    QStringList tags { "one", "two", "tree" };
    model.set(0, 2, "tags", QJsonArray::fromStringList(tags));
    QCOMPARE(dataChanged.count(), 1);
    index = model.index(0, 2);
    dataChanged.clear();
    QCOMPARE(index.data(UploadModel::TagsRole).toStringList(), tags);
    QCOMPARE(index.data(UploadModel::TitleRole), QVariant());
}

void UploadModelTest::testBaseOptions()
{
    UploadModel model;
    QSignalSpy dataChanged(&model, &UploadModel::dataChanged);
    model.addPhotos({ QUrl::fromLocalFile(ITEMS_DIR "/dir/image0.png") },
                    QUrl::fromLocalFile(ITEMS_DIR));
    QCOMPARE(model.rowCount(), 1);

    /* Metadata loading happens in the background; we need to wait until it
     * has completed */
    QTest::qWait(1);

    model.setUploadersCount(3);

    QModelIndex index = model.index(0, 0);
    QString house("House");
    model.setTitle(0, house);
    QCOMPARE(dataChanged.count(), 1);
    QModelIndex changedIndex = dataChanged.at(0).at(0).value<QModelIndex>();
    QCOMPARE(changedIndex, index);
    dataChanged.clear();
    QCOMPARE(index.data(UploadModel::TitleRole).toString(), house);
    QVERIFY(index.data(UploadModel::DescriptionRole).toString().isEmpty());

    QStringList tags { "one", "two", "tree" };
    for (const QString &tag: tags) {
        model.addTag(0, tag);
    }
    QCOMPARE(dataChanged.count(), 3);
    index = model.index(0, 0);
    dataChanged.clear();
    QCOMPARE(index.data(UploadModel::TagsRole).toStringList(), tags);

    QJsonObject expectedConfig =
        QJsonDocument::fromJson(R"({
                                  "description": "",
                                  "subDirectory": "dir",
                                  "tags": ["one","two","tree"],
                                  "title": "House"
                                })").object();
    QCOMPARE(index.data(UploadModel::ConfigRole).value<QJsonObject>(),
             expectedConfig);
}

void UploadModelTest::testSetData_data()
{
    QTest::addColumn<int>("column");
    QTest::addColumn<int>("role");
    QTest::addColumn<QVariant>("value");
    QTest::addColumn<bool>("expectedSuccess");

    QTest::newRow("unsupported role") <<
        2 <<
        int(UploadModel::UrlRole) <<
        QVariant(QUrl("http://www.example.com")) <<
        false;

    QTest::newRow("progress") <<
        1 <<
        int(UploadModel::ProgressRole) <<
        QVariant(34) <<
        true;

    QTest::newRow("title") <<
        2 <<
        int(UploadModel::TitleRole) <<
        QVariant("A house") <<
        true;
}

void UploadModelTest::testSetData()
{
    QFETCH(int, column);
    QFETCH(int, role);
    QFETCH(QVariant, value);
    QFETCH(bool, expectedSuccess);

    UploadModel model;
    model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    QCOMPARE(model.rowCount(), 1);

    model.setUploadersCount(3);

    QModelIndex index = model.index(0, column);
    bool ok = model.setData(index, value, role);
    QCOMPARE(ok, expectedSuccess);

    if (expectedSuccess) {
        QVariant writtenValue = index.data(role);
        QCOMPARE(writtenValue, value);
    }
}

void UploadModelTest::testUpdateAllRow_data()
{
    QTest::addColumn<int>("column");
    QTest::addColumn<int>("expectedColLeft");
    QTest::addColumn<int>("expectedColRight");

    QTest::newRow("other column") <<
        2 << 2 << 2;

    QTest::newRow("first column") <<
        0 << 0 << 3;
}

void UploadModelTest::testUpdateAllRow()
{
    QFETCH(int, column);
    QFETCH(int, expectedColLeft);
    QFETCH(int, expectedColRight);

    UploadModel model;
    QSignalSpy dataChanged(&model, &UploadModel::dataChanged);
    model.addPhoto(QUrl::fromLocalFile(ITEMS_DIR "/image0.png"));
    QCOMPARE(model.rowCount(), 1);

    model.setUploadersCount(3);

    QModelIndex index = model.index(0, column);
    model.setData(index, QString("hello"), UploadModel::TitleRole);

    QTRY_COMPARE(dataChanged.count(), 1);
    QModelIndex tl = dataChanged.at(0).at(0).value<QModelIndex>();
    QModelIndex br = dataChanged.at(0).at(1).value<QModelIndex>();

    QCOMPARE(tl.row(), 0);
    QCOMPARE(br.row(), 0);
    QCOMPARE(tl.column(), expectedColLeft);
    QCOMPARE(br.column(), expectedColRight);
}

void UploadModelTest::testConfig_data()
{
    QTest::addColumn<int>("uploadersCount");
    QTest::addColumn<QStringList>("urls");
    QTest::addColumn<QList<Cell>>("values");
    QTest::addColumn<QString>("expectedConfig");

    QTest::newRow("2x3") <<
        3 <<
        QStringList {
            "file:///one.jpg", "file:///two.jpg",
        } <<
        QList<Cell> {
            { 0, 0, UploadModel::TitleRole, "The title" },
            { 0, 2, UploadModel::TitleRole, "Title for second uploader" },
            { 0, 2, UploadModel::TagsRole, QStringList {
                    "Tags", "for", "second", "uploader"
                }
            },
            { 0, 1, UploadModel::ProgressRole, 45 },
            { 0, 3, UploadModel::UploadStatusRole, UploadModel::Failed },
            { 1, 0, UploadModel::DescriptionRole, "Cat and dogs" },
            { 1, 0, UploadModel::TagsRole, QStringList { "nice", "photo" }},
            { 1, 1, UploadModel::DescriptionRole, "Alternate description" },
            { 1, 2, UploadModel::UploadStatusRole, UploadModel::Uploaded },
        } <<
        R"({
            "uploadersCount": 3,
            "rows": [
                [
                    {
                        "url": "file:///one.jpg",
                        "title": "The title",
                        "description": "",
                        "tags": []
                    },
                    {
                        "uploadStatus": "Uploading"
                    },
                    {
                        "title": "Title for second uploader",
                        "tags": [ "Tags", "for", "second", "uploader" ],
                        "uploadStatus": "Ready"
                    },
                    {
                        "uploadStatus": "Failed"
                    }
                ],
                [
                    {
                        "url": "file:///two.jpg",
                        "title": "",
                        "description": "Cat and dogs",
                        "tags": [ "nice", "photo" ]
                    },
                    {
                        "description": "Alternate description",
                        "uploadStatus": "Ready"
                    },
                    {
                        "uploadStatus": "Uploaded"
                    },
                    {
                        "uploadStatus": "Ready"
                    }
                ]
            ]
        })";
}

void UploadModelTest::testConfig()
{
    QFETCH(int, uploadersCount);
    QFETCH(QStringList, urls);
    QFETCH(QList<Cell>, values);
    QFETCH(QString, expectedConfig);

    UploadModel model;
    model.setUploadersCount(uploadersCount);
    for (const QString &url: urls) {
        model.addPhoto(QUrl(url));
    }
    /* Metadata loading happens in the background; we need to wait until it
     * has completed */
    QTest::qWait(1);
    for (const Cell &cell: values) {
        QModelIndex index = model.index(cell.row, cell.col);
        model.setData(index, cell.value, cell.role);
    }

    QJsonObject config = model.config();
    QJsonDocument doc = QJsonDocument::fromJson(expectedConfig.toUtf8());
    QCOMPARE(config, doc.object());

    UploadModel modelCopy;
    modelCopy.setConfig(config);
    QCOMPARE(modelCopy.config(), doc.object());
}

void UploadModelTest::testBaseUrl_data()
{
    QTest::addColumn<QString>("baseUrl");
    QTest::addColumn<QStringList>("urls");
    QTest::addColumn<QStringList>("expectedSubDirectories");

    QTest::newRow("no base URL") <<
        QString() <<
        QStringList {
            "file:///home/me/one.jpg", "file:///home/me/two.jpg",
        } <<
        QStringList {
            QString(), QString(),
        };

    QTest::newRow("base URL") <<
        QString("file:///home") <<
        QStringList {
            "file:///home/me/one.jpg", "file:///home/me/two.jpg",
        } <<
        QStringList {
            QString("me"), QString("me"),
        };
}

void UploadModelTest::testBaseUrl()
{
    QFETCH(QString, baseUrl);
    QFETCH(QStringList, urls);
    QFETCH(QStringList, expectedSubDirectories);

    UploadModel model;
    QList<QUrl> fileUrls;
    for (const QString &url: urls) {
        fileUrls.append(QUrl(url));
    }
    model.addPhotos(fileUrls, QUrl(baseUrl));

    QStringList subDirectories;
    for (int i = 0; i < urls.count(); i++) {
        QModelIndex index = model.index(i, 0);
        subDirectories.append(index.data(UploadModel::SubDirectoryRole).
                              toString());
    }
    QCOMPARE(subDirectories, expectedSubDirectories);
}

void UploadModelTest::testLoadMetadata_data()
{
    QTest::addColumn<bool>("isLoadingSession");
    QTest::addColumn<bool>("loadMetadataCallExpected");

    QTest::newRow("normal execution") <<
        false << true;

    QTest::newRow("loading session") <<
        true << false;
}

void UploadModelTest::testLoadMetadata()
{
    QFETCH(bool, isLoadingSession);
    QFETCH(bool, loadMetadataCallExpected);

    MockMetadataLoader mockMetadataLoader;
    QSignalSpy loadMetadataCalled(&mockMetadataLoader,
                                  &MockMetadataLoader::loadMetadataCalled);

    UploadModel model;
    model.setIsLoadingSession(isLoadingSession);

    QString fileUrl("file:///one.jpg");
    model.addPhoto(fileUrl);

    QCOMPARE(loadMetadataCalled.count(), loadMetadataCallExpected ? 1 : 0);

    QCOMPARE(model.isLoadingSession(), isLoadingSession);
}

void UploadModelTest::testSubDirectoryRole()
{
    UploadModel model;

    QList<QUrl> fileUrls { QUrl("file:///home/me/dir/photo.jpg") };
    model.addPhotos(fileUrls, QUrl("file:///home"));
    QCOMPARE(model.rowCount(), 1);

    QModelIndex index = model.index(0, 0);
    QCOMPARE(model.data(index, UploadModel::SubDirectoryRole).toString(),
             QString("me/dir"));

    QString newSubDirectory("somewhere/else");
    model.setData(index, newSubDirectory, UploadModel::SubDirectoryRole);
    QCOMPARE(model.data(index, UploadModel::SubDirectoryRole).toString(),
             newSubDirectory);
}

QTEST_GUILESS_MAIN(UploadModelTest)

#include "tst_upload_model.moc"
