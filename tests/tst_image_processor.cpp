/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_processor.h"

#include <QDir>
#include <QFileInfo>
#include <QSignalSpy>
#include <QSize>
#include <QTemporaryDir>
#include <QTest>
#include <QThreadPool>
#include <algorithm>
#include "fake_image_operations.h"

using namespace Photokinesis;

/* We can't use QSignalSpy when signals are emitted from another thread */
struct FinishedSpy: public QList<QVariantList>
{
    FinishedSpy(ImageProcessor *object, QObject *context) {
        QObject::connect(object, &ImageProcessor::finished,
                         context, [this](int id, const QString fileNameOut) {
            append({ id, fileNameOut });
        });
    }
};

class ImageProcessorTest: public QObject
{
    Q_OBJECT

public:
    ImageProcessorTest();

private Q_SLOTS:
    void testInstance();
    void testResize();
    void testStopAndResume();
    void testRelease();
};

ImageProcessorTest::ImageProcessorTest()
{
}

void ImageProcessorTest::testInstance()
{
    ImageProcessor processor;

    QCOMPARE(ImageProcessor::instance(), &processor);
}

void ImageProcessorTest::testResize()
{
    ImageOperationsMocked mocked;
    QSignalSpy ensureMaxSizeCalled(&mocked,
                                   &ImageOperationsMocked::ensureMaxSizeCalled);

    QList<ImageProcessor::Request> requests;
    requests.append(ImageProcessor::Request("/path/to/file1.jpg"));
    requests.last().setMaxSize(QSize(200, 300));

    requests.append(ImageProcessor::Request("/path/to/file2.jpg"));
    requests.last().setMaxSize(QSize(-1, 300));

    ImageProcessor processor;
    FinishedSpy finished(&processor, this);

    QList<int> ids;
    QStringList inputNames;
    for (const ImageProcessor::Request &req: requests) {
        inputNames.append(req.filePath());
        int reqId = processor.addRequest(req);
        QVERIFY(reqId > 0);
        ids.append(reqId);
    }

    QTest::qWait(10);
    QCOMPARE(finished.count(), 0);

    processor.start();
    QTRY_COMPARE(finished.count(), 2);

    QThreadPool::globalInstance()->waitForDone(5000);
    QCOMPARE(finished.count(), 2);

    QMap<int,QString> outputNames;
    for (const auto signalData: finished) {
        outputNames.insert(signalData.at(0).toInt(),
                           signalData.at(1).toString());
    }
    QCOMPARE(outputNames.keys().toSet(), ids.toSet());

    QTest::qWait(10);
    QCOMPARE(ensureMaxSizeCalled.count(), requests.count());

    std::sort(ensureMaxSizeCalled.begin(),
              ensureMaxSizeCalled.end(),
              [inputNames](const QVariantList &a1, const QVariantList &a2) {
        return inputNames.indexOf(a1.at(0).toString()) <
               inputNames.indexOf(a2.at(0).toString());
    });

    int i = 0;
    for (const auto &req: requests) {
        QVariantList args = ensureMaxSizeCalled.at(i++);
        QCOMPARE(args.at(0).toString(), req.filePath());
        QCOMPARE(args.at(1).toString(), outputNames.value(i));
        QCOMPARE(args.at(2).toSize(), req.maxSize());
    }
}

void ImageProcessorTest::testStopAndResume()
{
    ImageOperationsMocked mocked;
    QSignalSpy ensureMaxSizeCalled(&mocked,
                                   &ImageOperationsMocked::ensureMaxSizeCalled);

    ImageProcessor processor;
    FinishedSpy finished(&processor, this);

    for (int i = 0; i < 100; i++) {
        ImageProcessor::Request req(QString("/path/file%1.jpg").arg(i));
        req.setMaxSize(QSize(10, 10));
        processor.addRequest(req);
    }

    processor.start();
    QTest::qWait(1);

    processor.stopAndClear();
    QTest::qWait(1);
    int finishedCount = finished.count();

    QTest::qWait(50);
    QCOMPARE(finished.count(), finishedCount);
    finished.clear();

    ImageProcessor::Request req("/new/request.jpg");
    req.setMaxSize(QSize(20, 20));
    int id = processor.addRequest(req);

    QTest::qWait(10);
    QCOMPARE(finished.count(), 0);

    processor.start();

    QTRY_COMPARE(finished.count(), 1);
    QCOMPARE(finished.at(0).at(0).toInt(), id);

    QThreadPool::globalInstance()->waitForDone(5000);
    QTest::qWait(10);
    QCOMPARE(finished.count(), 1);
}

void ImageProcessorTest::testRelease()
{
    const int num_requests = 3;

    ImageOperationsMocked mocked;

    ImageProcessor processor;
    FinishedSpy finished(&processor, this);

    QList<ImageProcessor::Request> requests;
    for (int i = 0; i < num_requests; i++) {
        ImageProcessor::Request req(QString("/path/to/file%1.jpg").arg(i));
        req.setMaxSize(QSize(10, 10));
        processor.addRequest(req);
    }

    processor.start();
    QTRY_COMPARE(finished.count(), num_requests);

    QStringList outputFiles;
    for (const auto signalData: finished) {
        QString outputFile = signalData.at(1).toString();
        outputFiles.append(outputFile);
        QVERIFY(QFileInfo::exists(outputFile));
    }

    while (!outputFiles.isEmpty()) {
        QString toBeDeleted = outputFiles.takeFirst();

        QVERIFY(QFileInfo::exists(toBeDeleted));
        processor.releaseFile(toBeDeleted);
        QVERIFY(!QFileInfo::exists(toBeDeleted));
    }
}

QTEST_GUILESS_MAIN(ImageProcessorTest)

#include "tst_image_processor.moc"
