TARGET = tst_upload_model_session

include(tests.pri)

SOURCES += \
    $${TOP_SRC_DIR}/src/upload_model_session.cpp \
    fake_upload_model.cpp \
    tst_upload_model_session.cpp

HEADERS += \
    $${TOP_SRC_DIR}/lib/Photokinesis/upload_model.h \
    $${TOP_SRC_DIR}/src/upload_model_session.h \
    fake_upload_model.h
