TARGET = tst_image_processor

include(tests.pri)

QT += \
    concurrent

SOURCES += \
    $${PHOTOKINESIS_SRC_DIR}/image_processor.cpp \
    tst_image_processor.cpp

HEADERS += \
    $${PHOTOKINESIS_SRC_DIR}/image_processor.h

INCLUDEPATH += \
    $${PHOTOKINESIS_SRC_DIR}
