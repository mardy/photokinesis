TARGET = tst_abstract_uploader

include(tests.pri)

QT += \
    network \
    qml \
    quick

HEADERS += \
    $${PHOTOKINESIS_SRC_DIR}/abstract_uploader.h \
    $${PHOTOKINESIS_SRC_DIR}/json_storage.h \
    $${PHOTOKINESIS_SRC_DIR}/uploader_profile.h \
    fake_image_processor.h

SOURCES += \
    $${PHOTOKINESIS_SRC_DIR}/abstract_uploader.cpp \
    $${PHOTOKINESIS_SRC_DIR}/json_storage.cpp \
    $${PHOTOKINESIS_SRC_DIR}/uploader_profile.cpp \
    tst_abstract_uploader.cpp
