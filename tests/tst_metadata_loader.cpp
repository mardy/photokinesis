/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Imaginario.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "metadata_loader.h"

#include "fake_metadata.h"

#include <QFileInfo>
#include <QHash>
#include <QSignalSpy>
#include <QTest>
#include <QVector>

using namespace Photokinesis;

struct MetadataResult {
    QString title;
    QString description;
    QStringList tags;

    bool operator==(const MetadataResult &other) const {
        return title == other.title &&
            description == other.description &&
            tags == other.tags;
    }
};

namespace QTest {

template<>
char *toString(const QHash<QString,MetadataResult> &p)
{
    QByteArray ba = "QHash(";
    for (auto i = p.begin(); i != p.end(); i++) {
        ba += '"';
        ba += i.key().toUtf8();
        ba += "\": { title:";
        const MetadataResult &r = i.value();
        ba += r.title.toUtf8();
        ba += ", description:";
        ba += r.description.toUtf8();
        ba += ", tags: [";
        ba += QStringList(r.tags).join(',');
        ba += "]}\n";
    }
    ba += ")";
    return qstrdup(ba.data());
}

} // QTest namespace

class MetadataLoaderTest: public QObject
{
    Q_OBJECT

public:
    using Requests = QVector<QHash<QString,MetadataResult>>;

private Q_SLOTS:
    void testVirtualDestructor();
    void testSingleIteration();
    void testMultiple_data();
    void testMultiple();
    void testQuitOnDestruction();
};

Q_DECLARE_METATYPE(MetadataLoaderTest::Requests)

void MetadataLoaderTest::testVirtualDestructor()
{
    MetadataLoader *loader = new MetadataLoader();
    delete loader;
}

void MetadataLoaderTest::testSingleIteration()
{
    MockMetadata mock;

    MetadataLoader loader;
    QSignalSpy metadataLoaded(&loader, &MetadataLoader::metadataLoaded);
    QSignalSpy pendingRequestsChanged(&loader, &MetadataLoader::pendingRequestsChanged);

    QHash<QString,MetadataResult> expectedResults {
        { "file1.jpg", { "title", "description", {"tag1", "tag2"}}},
        { "file2.jpg", { "title2", "description2", {"tag21", "tag22"}}},
        { "empty.jpg", { QString(), QString(), {}}},
        { "file4.jpg", { "title4", "description4", {"tag41", "tag42"}}},
        { "file5.jpg", { "title5", "description5", {"tag51"}}},
        { "file6.jpg", { "title6", "description6", {"tag61", "tag62"}}},
    };
    for (auto i =  expectedResults.constBegin();
         i != expectedResults.constEnd();
         i++) {
        Imaginario::Metadata::ImportData data;
        data.title = i.value().title;
        data.description = i.value().description;
        data.tags = i.value().tags;
        mock.setImportData(i.key(), data);
    }

    int requestId = 0;
    for (const QString &filePath: expectedResults.keys()) {
        loader.loadMetadata(filePath, requestId++);
    }

    QVERIFY(pendingRequestsChanged.count() > 0);
    QCOMPARE(loader.pendingRequests(), expectedResults.count());

    /* Loading will start when control returns to the main loop */
    QCOMPARE(metadataLoaded.count(), 0);

    QTest::qWait(1);
    QTRY_COMPARE(loader.completedRequests(), expectedResults.count());

    QHash<QString,MetadataResult> results;
    for (const QVariantList &args: metadataLoaded) {
        int requestId = args.at(0).toInt();
        QVERIFY(requestId < expectedResults.count());
        QString filePath = expectedResults.keys().at(requestId);
        results.insert(filePath, {
            args.at(1).toString(),
            args.at(2).toString(),
            args.at(3).toStringList(),
        });
    }

    QCOMPARE(results, expectedResults);
}

void MetadataLoaderTest::testMultiple_data()
{
    QTest::addColumn<Requests>("requests");
    QTest::addColumn<int>("maxSleep");

    QTest::newRow("two batches") <<
        Requests {
            {
                { "0.jpg", { "title", "description", {"tag1", "tag2"}}},
                { "1.jpg", { "t1", "d1", {"tag11", "tag12"}}},
                { "2.jpg", { "t2", "d2", {"tag21", "tag22"}}},
                { "3.jpg", { QString(), QString(), {}}},
                { "4.jpg", { "t4", "d4", {"tag41", "tag42"}}},
                { "5.jpg", { "t5", "d5", {"tag51"}}},
                { "6.jpg", { "t6", "d6", {"tag61", "tag62"}}},
            },
            {
                { "7.jpg", { "t7", "d7", {"tag1", "tag2"}}},
                { "8.jpg", { "t8", "d8", {"tag21", "tag22"}}},
                { "9.jpg", { "t9", "d9", { "tag91" }}},
                { "10.jpg", { "t10", "d10", {"tag101", "tag102"}}},
                { "11.jpg", { "t11", "d11", {"tag111"}}},
                { "12.jpg", { "t12", "d12", {"tag121", "tag122"}}},
            },
        } <<
        500;

    QTest::newRow("many batches") <<
        Requests {
            {
                { "0.jpg", { "title", "description", {"tag1", "tag2"}}},
            },
            {
                { "1.jpg", { "t1", "d1", {"tag11", "tag12"}}},
            },
            {
                { "2.jpg", { "t2", "d2", {"tag21", "tag22"}}},
            },
            {
                { "3.jpg", { QString(), QString(), {}}},
            },
            {
                { "4.jpg", { "t4", "d4", {"tag41", "tag42"}}},
            },
            {
                { "5.jpg", { "t5", "d5", {"tag51"}}},
            },
            {
                { "6.jpg", { "t6", "d6", {"tag61", "tag62"}}},
            },
            {
                { "7.jpg", { "t7", "d7", {"tag1", "tag2"}}},
            },
            {
                { "8.jpg", { "t8", "d8", {"tag21", "tag22"}}},
            },
            {
                { "9.jpg", { "t9", "d9", { "tag91" }}},
            },
            {
                { "10.jpg", { "t10", "d10", {"tag101", "tag102"}}},
            },
            {
                { "11.jpg", { "t11", "d11", {"tag111"}}},
            },
            {
                { "12.jpg", { "t12", "d12", {"tag121", "tag122"}}},
            },
        } <<
        0;
}

void MetadataLoaderTest::testMultiple()
{
    QFETCH(Requests, requests);
    QFETCH(int, maxSleep);

    MockMetadata mock;
    mock.setMaxSleep(maxSleep);

    MetadataLoader loader;
    QSignalSpy metadataLoaded(&loader, &MetadataLoader::metadataLoaded);
    QSignalSpy pendingRequestsChanged(&loader,
                                      &MetadataLoader::pendingRequestsChanged);

    /* Build expectedResults map */
    QHash<QString,MetadataResult> expectedResults;
    for (const QHash<QString,MetadataResult> request: requests) {
        expectedResults.unite(request);
    }

    /* Prepare mock data */
    for (auto i =  expectedResults.constBegin();
         i != expectedResults.constEnd();
         i++) {
        Imaginario::Metadata::ImportData data;
        data.title = i.value().title;
        data.description = i.value().description;
        data.tags = i.value().tags;
        mock.setImportData(i.key(), data);
    }

    /* Make the requests */
    for (const QHash<QString,MetadataResult> request: requests) {
        for (const QString &filePath: request.keys()) {
            QString requestId = QFileInfo(filePath).baseName();
            loader.loadMetadata(filePath, requestId.toInt());
        }

        /* Let the loader start processing these requests */
        QTest::qWait(1);
    }

    QCOMPARE(loader.pendingRequests(), expectedResults.count());
    QTRY_COMPARE(loader.completedRequests(), expectedResults.count());

    QHash<QString,MetadataResult> results;
    for (const QVariantList &args: metadataLoaded) {
        int requestId = args.at(0).toInt();
        QVERIFY(requestId < expectedResults.count());
        QString filePath = QString::number(requestId) + ".jpg";
        results.insert(filePath, {
            args.at(1).toString(),
            args.at(2).toString(),
            args.at(3).toStringList(),
        });
    }

    QCOMPARE(results, expectedResults);
}

void MetadataLoaderTest::testQuitOnDestruction()
{
    MockMetadata mock;
    mock.setMaxSleep(1000);
    QSignalSpy importDataCalled(&mock, &MockMetadata::importDataCalled);

    QScopedPointer<MetadataLoader> loader(new MetadataLoader);
    QSignalSpy metadataLoaded(loader.data(), &MetadataLoader::metadataLoaded);

    const int requestCount = 1000;
    for (int i = 0; i < requestCount; i++) {
        QString filePath = QString("file%1.jpg").arg(i);
        loader->loadMetadata(filePath, i);
    }

    QCOMPARE(loader->pendingRequests(), requestCount);

    /* Loading will start when control returns to the main loop */
    QCOMPARE(metadataLoaded.count(), 0);

    QTest::qWait(1);

    QVERIFY(metadataLoaded.wait());
    QVERIFY(importDataCalled.count() > 0);

    /* Destroy our loader; this will cause processing to stop. */
    loader.reset();
    importDataCalled.clear();

    QVERIFY(metadataLoaded.count() < requestCount);
    QTest::qWait(50);
    QCOMPARE(importDataCalled.count(), 0);
}

QTEST_GUILESS_MAIN(MetadataLoaderTest)

#include "tst_metadata_loader.moc"
