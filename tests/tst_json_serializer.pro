TARGET = tst_json_serializer

include(tests.pri)

SOURCES += \
    $${TOP_SRC_DIR}/src/json_serializer.cpp \
    tst_json_serializer.cpp

HEADERS += \
    $${TOP_SRC_DIR}/src/json_serializer.h
