/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Photokinesis/AbstractUploader"

#include <QDebug>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkAccessManager>
#include <QScopedPointer>
#include <QSet>
#include <QSignalSpy>
#include <QStringListModel>
#include <QTest>
#include "fake_image_processor.h"
#include "test_utils.h"

using namespace Photokinesis;

class DummyUploader: public AbstractUploader
{
    Q_OBJECT

public:
    DummyUploader(const QJsonObject &metadata = QJsonObject());
    void setData(const QString &name,
                 const QString &displayName,
                 const QUrl &icon);
    void setError(const QString &message);
    void setStatus(AbstractUploader::Status status) {
        AbstractUploader::setStatus(status);
    }
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;
    void authenticate(QQuickItem *parent) override;
    QJsonObject getSettings() const override;
    void logout() override;

    void setUserId(const QString &userId) {
        AbstractUploader::setUserId(userId);
    }
    void setAccountInfo(const QString &info) {
        AbstractUploader::setAccountInfo(info);
    }
    void setAlbums(const QJsonArray &albums) {
        AbstractUploader::setAlbums(albums);
    }
    void setUploadsUrl(const QUrl &url) {
        AbstractUploader::setUploadsUrl(url);
    }

    void setNeedsConfiguration(bool needsConfiguration) {
        AbstractUploader::setNeedsConfiguration(needsConfiguration);
    }

    void setUsesBrowser(bool usesBrowser) {
        AbstractUploader::setUsesBrowser(usesBrowser);
    }

    void storeSettings() { AbstractUploader::storeSettings(); }

    void storeAccountSettings(const QJsonObject &settings) {
        AbstractUploader::storeAccountSettings(settings);
    }
    QJsonObject loadAccountSettings() const {
        return AbstractUploader::loadAccountSettings();
    }

    void doLogout() { AbstractUploader::logout(); }

Q_SIGNALS:
    void startUploadCalled(const QString &filePath,
                           const QJsonObject &options);
    void authenticateCalled(void *parent);
    void getSettingsCalled() const;
    void logoutCalled();
};

DummyUploader::DummyUploader(const QJsonObject &metadata):
    AbstractUploader(metadata)
{
}

void DummyUploader::setData(const QString &name,
                            const QString &displayName,
                            const QUrl &icon)
{
    setName(name);
    setDisplayName(displayName);
    setIcon(icon);
}

void DummyUploader::setError(const QString &message)
{
    setErrorMessage(message);
}

void DummyUploader::startUpload(const QString &filePath,
                                const QJsonObject &options)
{
    Q_EMIT startUploadCalled(filePath, options);
    Q_EMIT done(true);
}

void DummyUploader::authenticate(QQuickItem *parent)
{
    Q_EMIT authenticateCalled(parent);
    AbstractUploader::authenticate(parent);
}

QJsonObject DummyUploader::getSettings() const
{
    Q_EMIT getSettingsCalled();
    return AbstractUploader::getSettings();
}

void DummyUploader::logout()
{
    Q_EMIT logoutCalled();
}

class AbstractUploaderTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testConstProperties_data();
    void testConstProperties();
    void testMetadata_data();
    void testMetadata();
    void testEnabled();
    void testStatus();
    void testNam();
    void testErrorMessage();
    void testAccountInfo();
    void testUploadsUrl();
    void testAlbums();
    void testChangeAccount();
    void testAccountSettings();
    void testStoreSettings();
    void testUserName();
    void testUploadFile_data();
    void testUploadFile();
    void testReplicatingFolders();
    void testNeedsConfiguation();
    void testUsesBrowser();
};

void AbstractUploaderTest::testConstProperties_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("displayName");
    QTest::addColumn<QUrl>("icon");

    QTest::newRow("all nulls") <<
        QString() <<
        QString() <<
        QUrl();

    QTest::newRow("valid") <<
        "uglyName" <<
        "Pretty name" <<
        QUrl("qrc:some-icon.png");
}

void AbstractUploaderTest::testConstProperties()
{
    QFETCH(QString, name);
    QFETCH(QString, displayName);
    QFETCH(QUrl, icon);

    DummyUploader uploader;
    uploader.setData(name, displayName, icon);

    QCOMPARE(uploader.property("name").toString(), name);
    QCOMPARE(uploader.property("displayName").toString(), displayName);
    QCOMPARE(uploader.property("icon").toUrl(), icon);
}

void AbstractUploaderTest::testMetadata_data()
{
    QTest::addColumn<QString>("json");
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("displayName");
    QTest::addColumn<QUrl>("icon");
    QTest::addColumn<QStringList>("supportedOptions");

    QTest::newRow("empty") <<
        "{}" <<
        QString() <<
        QString() <<
        QUrl() <<
        QStringList();

    QTest::newRow("valid") <<
        "{\n"
        "  \"name\": \"uglyName\",\n"
        "  \"displayName\": \"Pretty name\",\n"
        "  \"icon\": \"qrc:some-icon.png\",\n"
        "  \"supportedOptions\": [\"title\",\"tags\"]\n"
        "}" <<
        "uglyName" <<
        "Pretty name" <<
        QUrl("qrc:some-icon.png") <<
        QStringList { "title", "tags" };
}

void AbstractUploaderTest::testMetadata()
{
    QFETCH(QString, json);
    QFETCH(QString, name);
    QFETCH(QString, displayName);
    QFETCH(QUrl, icon);
    QFETCH(QStringList, supportedOptions);

    QJsonObject metadata =
        QJsonDocument::fromJson(json.toUtf8()).object();
    DummyUploader uploader(metadata);

    QCOMPARE(uploader.property("name").toString(), name);
    QCOMPARE(uploader.property("displayName").toString(), displayName);
    QCOMPARE(uploader.property("icon").toUrl(), icon);
    QCOMPARE(uploader.property("supportedOptions").toStringList(),
             supportedOptions);
}

void AbstractUploaderTest::testEnabled()
{
    DummyUploader uploader;

    QCOMPARE(uploader.isEnabled(), true);
    QCOMPARE(uploader.property("enabled").toBool(), true);

    QSignalSpy enabledChanged(&uploader, SIGNAL(enabledChanged()));

    uploader.setEnabled(true);
    QCOMPARE(enabledChanged.count(), 0);

    uploader.setEnabled(false);
    QCOMPARE(enabledChanged.count(), 1);

    QCOMPARE(uploader.isEnabled(), false);
}

void AbstractUploaderTest::testStatus()
{
    DummyUploader uploader;

    QCOMPARE(uploader.status(), AbstractUploader::NeedsAuthentication);

    QSignalSpy statusChanged(&uploader, SIGNAL(statusChanged()));

    uploader.setStatus(AbstractUploader::Authenticating);
    QCOMPARE(statusChanged.count(), 1);
    QCOMPARE(uploader.status(), AbstractUploader::Authenticating);
    statusChanged.clear();

    uploader.setStatus(AbstractUploader::Ready);
    QCOMPARE(statusChanged.count(), 1);
    QCOMPARE(uploader.status(), AbstractUploader::Ready);
}

void AbstractUploaderTest::testNam()
{
    DummyUploader uploader;

    QVERIFY(!uploader.networkAccessManager());

    QNetworkAccessManager nam;
    uploader.setNetworkAccessManager(&nam);
    QCOMPARE(uploader.networkAccessManager(), &nam);
}

void AbstractUploaderTest::testErrorMessage()
{
    DummyUploader uploader;

    QVERIFY(uploader.errorMessage().isNull());

    uploader.setError("Something bad happened!");
    QCOMPARE(uploader.errorMessage(), QString("Something bad happened!"));
}

void AbstractUploaderTest::testAccountInfo()
{
    DummyUploader uploader;
    QSignalSpy accountInfoChanged(&uploader, SIGNAL(accountInfoChanged()));

    QVERIFY(uploader.userId().isNull());
    QVERIFY(uploader.accountInfo().isNull());

    uploader.setUserId("me");
    uploader.setAccountInfo("Total photos: 3");
    QCOMPARE(accountInfoChanged.count(), 1);
    QCOMPARE(uploader.userId(), QString("me"));
    QCOMPARE(uploader.accountInfo(), QString("Total photos: 3"));
}

void AbstractUploaderTest::testUploadsUrl()
{
    DummyUploader uploader;
    QSignalSpy uploadsUrlChanged(&uploader, SIGNAL(uploadsUrlChanged()));

    QVERIFY(uploader.uploadsUrl().isEmpty());

    QUrl url("https://mysite.com/path");
    uploader.setUploadsUrl(url);
    QCOMPARE(uploadsUrlChanged.count(), 1);
    QCOMPARE(uploader.uploadsUrl(), url);
}

void AbstractUploaderTest::testAlbums()
{
    DummyUploader uploader;
    QSignalSpy albumsChanged(&uploader, SIGNAL(albumsChanged()));

    QVERIFY(uploader.albums().isEmpty());

    QJsonArray albums {
        QJsonObject {
            { "albumId", "1" },
            { "name", "My photos" },
        },
        QJsonObject {
            { "albumId", "2" },
            { "name", "Others" },
        },
    };
    uploader.setAlbums(albums);
    QCOMPARE(albumsChanged.count(), 1);
    QCOMPARE(uploader.albums(), albums);

    QSignalSpy defaultAlbumIdChanged(&uploader,
                                     SIGNAL(defaultAlbumIdChanged()));
    QVERIFY(uploader.defaultAlbumId().isEmpty());
    QString id("2");
    uploader.setProperty("defaultAlbumId", id);
    QCOMPARE(defaultAlbumIdChanged.count(), 1);
    QCOMPARE(uploader.defaultAlbumId(), id);
}

void AbstractUploaderTest::testChangeAccount()
{
    DummyUploader uploader;
    QSignalSpy statusChanged(&uploader, &AbstractUploader::statusChanged);
    QSignalSpy accountInfoChanged(&uploader,
                                  &AbstractUploader::accountInfoChanged);
    QSignalSpy authenticateCalled(&uploader,
                                  &DummyUploader::authenticateCalled);
    QSignalSpy logoutCalled(&uploader, &DummyUploader::logoutCalled);

    QCOMPARE(uploader.status(), AbstractUploader::NeedsAuthentication);

    QQuickItem *parent = reinterpret_cast<QQuickItem*>(0x123400);
    uploader.authenticate(parent);
    QCOMPARE(uploader.status(), AbstractUploader::Ready);
    QCOMPARE(accountInfoChanged.count(), 0);
    QCOMPARE(authenticateCalled.count(), 1);
    QCOMPARE(authenticateCalled.at(0).at(0).value<void*>(), (void*)parent);
    authenticateCalled.clear();
    statusChanged.clear();

    uploader.changeAccount(parent);
    QCOMPARE(logoutCalled.count(), 1);

    uploader.doLogout();
    QCOMPARE(accountInfoChanged.count(), 1);
    QCOMPARE(authenticateCalled.count(), 1);
    QCOMPARE(authenticateCalled.at(0).at(0).value<void*>(), (void*)parent);
    QCOMPARE(statusChanged.count(), 3);
    QCOMPARE(uploader.status(), AbstractUploader::Ready);
    accountInfoChanged.clear();
    statusChanged.clear();
    authenticateCalled.clear();
    logoutCalled.clear();

    /* call it again, to verify that signals have been properly disconnected */
    uploader.changeAccount(parent);
    QCOMPARE(logoutCalled.count(), 1);

    uploader.doLogout();
    QCOMPARE(accountInfoChanged.count(), 1);
    QCOMPARE(authenticateCalled.count(), 1);
    QCOMPARE(authenticateCalled.at(0).at(0).value<void*>(), (void*)parent);
    QCOMPARE(statusChanged.count(), 3);
    QCOMPARE(uploader.status(), AbstractUploader::Ready);
}

void AbstractUploaderTest::testAccountSettings()
{
    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());
    qputenv("XDG_CONFIG_HOME", tmpDir.path().toUtf8());

    QJsonObject obj1 = QJsonDocument::fromJson(R"({
        "key": "value",
        "array": [ "one", "two", "three" ]
    })").object();

    QJsonObject obj2 = QJsonDocument::fromJson(R"({
        "boolean": true,
        "array": [ 1, 2, 3 ]
    })").object();

    /* Load empty */
    {
        DummyUploader uploader;
        uploader.setData("dummy", "Dummy", QUrl());
        QVERIFY(uploader.loadAccountSettings().isEmpty());
        QCOMPARE(uploader.knownUserIds(), QStringList {});
    }

    /* Store data for a couple of accounts */
    {
        DummyUploader uploader;
        uploader.setData("dummy", "Dummy", QUrl());
        uploader.setUserId("tom");
        uploader.storeAccountSettings(obj1);
        QSet<QString> expectedIds { "tom" };
        QCOMPARE(uploader.knownUserIds().toSet(), expectedIds);
    }

    {
        DummyUploader uploader;
        uploader.setData("dummy", "Dummy", QUrl());
        uploader.setUserId("jack");
        uploader.storeAccountSettings(obj2);
        QSet<QString> expectedIds { "tom", "jack" };
        QCOMPARE(uploader.knownUserIds().toSet(), expectedIds);
    }

    QVERIFY(QFile::exists(tmpDir.path() + "/" +
                          QCoreApplication::applicationName() +
                          "/dummy.json"));

    /* Load without setting a user ID */
    {
        DummyUploader uploader;
        uploader.setData("dummy", "Dummy", QUrl());
        QCOMPARE(uploader.loadAccountSettings(), obj2);
    }

    /* Load a specific ID */
    {
        DummyUploader uploader;
        uploader.setData("dummy", "Dummy", QUrl());
        uploader.setUserId("tom");
        QCOMPARE(uploader.loadAccountSettings(), obj1);
    }
}

void AbstractUploaderTest::testStoreSettings()
{
    DummyUploader uploader;
    QSignalSpy getSettingsCalled(&uploader, &DummyUploader::getSettingsCalled);

    uploader.storeSettings();
    QCOMPARE(getSettingsCalled.count(), 1);
}

void AbstractUploaderTest::testUserName()
{
    DummyUploader uploader;
    QSignalSpy userNameChanged(&uploader, &AbstractUploader::userNameChanged);

    QVERIFY(uploader.userName().isEmpty());

    uploader.setUserId("someId");
    QCOMPARE(uploader.userName(), QString("someId"));
    QCOMPARE(userNameChanged.count(), 1);
    userNameChanged.clear();

    uploader.setUserName("other name");
    QCOMPARE(uploader.userName(), QString("other name"));
    QCOMPARE(userNameChanged.count(), 1);
    userNameChanged.clear();

    uploader.setUserId("someId2");
    QCOMPARE(uploader.userName(), QString("other name"));
    QCOMPARE(userNameChanged.count(), 0);
}

void AbstractUploaderTest::testUploadFile_data()
{
    QTest::addColumn<QSize>("maxSize");
    QTest::addColumn<bool>("expectImageProcessorInvoked");

    QTest::newRow("no processing") <<
        QSize() <<
        false;

    QTest::newRow("resize") <<
        QSize(-1, 50) <<
        true;
}

void AbstractUploaderTest::testUploadFile()
{
    QFETCH(QSize, maxSize);
    QFETCH(bool, expectImageProcessorInvoked);

    QUrl url("file:///tmp/image.jpg");
    QString filePath("/tmp/image.jpg");
    QJsonObject options {
        { "one", 1 },
        { "true", true },
    };

    ImageProcessorMocked *mockedImageProcessor =
        ImageProcessorMocked::mocked(ImageProcessor::instance());
    QSignalSpy startCalled(mockedImageProcessor,
                           &ImageProcessorMocked::startCalled);
    DummyUploader uploader;
    QSignalSpy done(&uploader, &AbstractUploader::done);
    QSignalSpy startUploadCalled(&uploader,
                                 &DummyUploader::startUploadCalled);
    uploader.setMaxSize(maxSize);
    uploader.uploadFile(url, options);

    QTest::qWait(5);
    QCOMPARE(startCalled.count(), expectImageProcessorInvoked ? 1 : 0);
    const auto requests = mockedImageProcessor->requests();
    QCOMPARE(requests.isEmpty(), expectImageProcessorInvoked ? false : true);
    if (expectImageProcessorInvoked) {
        QCOMPARE(requests.first().maxSize(), maxSize);
        QCOMPARE(requests.first().filePath(), filePath);

        QCOMPARE(done.count(), 0);

        int requestId = requests.keys().first();
        /* emit the signal for the wrong request Id, to be sure that the
         * AbstractUploader is checking it: */
        mockedImageProcessor->emitFinished(requestId + 3, filePath);
        QTest::qWait(5);
        QCOMPARE(done.count(), 0);

        // Now emit the right one
        mockedImageProcessor->emitFinished(requestId, filePath);
        QTest::qWait(5);
    }

    QCOMPARE(startUploadCalled.count(), 1);
    QJsonObject actualOptions = startUploadCalled.at(0).at(1).toJsonObject();
    QJsonObject expectedOptions(options);
    expectedOptions.insert("fileName", "image.jpg");
    QCOMPARE(actualOptions, expectedOptions);
    QCOMPARE(done.count(), 1);
}

void AbstractUploaderTest::testReplicatingFolders()
{
    DummyUploader uploader;
    QSignalSpy changed(&uploader,
                       &AbstractUploader::replicatingFoldersChanged);

    QCOMPARE(uploader.replicatingFolders(), false);

    uploader.setReplicatingFolders(true);
    QCOMPARE(uploader.replicatingFolders(), true);
    QCOMPARE(changed.count(), 1);
}

void AbstractUploaderTest::testNeedsConfiguation()
{
    DummyUploader uploader;
    QSignalSpy changed(&uploader, &AbstractUploader::statusChanged);

    QCOMPARE(uploader.needsConfiguration(), false);

    uploader.setNeedsConfiguration(true);
    QCOMPARE(uploader.needsConfiguration(), true);
    QCOMPARE(changed.count(), 1);
}

void AbstractUploaderTest::testUsesBrowser()
{
    DummyUploader uploader;

    QCOMPARE(uploader.usesBrowser(), false);

    uploader.setUsesBrowser(true);
    QCOMPARE(uploader.usesBrowser(), true);
}

QTEST_GUILESS_MAIN(AbstractUploaderTest)

#include "tst_abstract_uploader.moc"
