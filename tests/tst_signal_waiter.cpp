/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <Photokinesis/SignalWaiter>
#include <QDebug>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>

using namespace Photokinesis;

class Emitter: public QObject
{
    Q_OBJECT

public:
    void emitSignal1() { Q_EMIT signal1(); }
    void emitSignal2() { Q_EMIT signal2(); }
    void emitSignal3() { Q_EMIT signal3(); }

Q_SIGNALS:
    void signal1();
    void signal2();
    void signal3();
};

class SignalWaiterTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testSingleObject();
    void testSameSignal();
};

void SignalWaiterTest::testSingleObject()
{
    SignalWaiter waiter;
    QSignalSpy finished(&waiter, SIGNAL(finished()));

    Emitter emitter;
    waiter.add(&emitter, SIGNAL(signal1()));
    waiter.add(&emitter, SIGNAL(signal2()));
    waiter.add(&emitter, SIGNAL(signal3()));

    emitter.emitSignal2();
    emitter.emitSignal3();
    QCOMPARE(finished.count(), 0);
    emitter.emitSignal1();
    QCOMPARE(finished.count(), 1);
}

void SignalWaiterTest::testSameSignal()
{
    SignalWaiter waiter(SIGNAL(signal2()));
    QSignalSpy finished(&waiter, SIGNAL(finished()));

    Emitter emitter1;
    Emitter emitter2;
    Emitter emitter3;
    waiter.add(&emitter1);
    waiter.add(&emitter2);
    waiter.add(&emitter3);

    emitter1.emitSignal2();
    emitter1.emitSignal3();
    emitter3.emitSignal2();
    emitter3.emitSignal1();
    QCOMPARE(finished.count(), 0);
    emitter2.emitSignal2();
    QCOMPARE(finished.count(), 1);
}

QTEST_GUILESS_MAIN(SignalWaiterTest)

#include "tst_signal_waiter.moc"
