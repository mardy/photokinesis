/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_FAKE_METADATA_LOADER_H
#define PHOTOKINESIS_FAKE_METADATA_LOADER_H

#include "Photokinesis/metadata_loader.h"

#include <QHash>
#include <QObject>
#include <QScopedPointer>
#include <QString>
#include <QStringList>

namespace Photokinesis {

class MetadataLoaderPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(MetadataLoader)

public:
    struct Metadata {
        QString title;
        QString description;
        QStringList tags;
    };
    MetadataLoaderPrivate();
    ~MetadataLoaderPrivate();

    static MetadataLoaderPrivate *mockFor(MetadataLoader *o) {
        return o->d_ptr.data();
    }

    void setMetadata(const QString &file, const Metadata &metadata) {
        m_metadata[file] = metadata;
    }

Q_SIGNALS:
    void instanceCreated(MetadataLoader *instance);
    void loadMetadataCalled(const QString &file, int requestId);

private:
    MetadataLoaderPrivate(MetadataLoader *q);

private:
    int m_pendingRequests;
    int m_completedRequests;
    QHash<QString,Metadata> m_metadata;
    bool m_mustDelete;
    MetadataLoader *q_ptr;
};

using MockMetadataLoader = MetadataLoaderPrivate;

} // namespace

#endif // PHOTOKINESIS_FAKE_METADATA_LOADER_H
