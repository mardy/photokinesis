/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Photokinesis/JsonStorage"

#include <QByteArray>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QScopedPointer>
#include <QTemporaryDir>
#include <QTest>

using namespace Photokinesis;

class JsonStorageTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testProperties();
    void testFailedWrite_data();
    void testFailedWrite();
    void testFailedLoad();
    void testStore();
};

void JsonStorageTest::testProperties()
{
    JsonStorage storage;

    QCOMPARE(storage.base(), QStandardPaths::HomeLocation);
    storage.setBase(QStandardPaths::DataLocation);
    QCOMPARE(storage.base(), QStandardPaths::DataLocation);

    QVERIFY(storage.filePath().isEmpty());
    storage.setFilePath("dir/file.json");
    QCOMPARE(storage.filePath(), QString("dir/file.json"));
}

void JsonStorageTest::testFailedWrite_data()
{
    QTest::addColumn<QString>("base");
    QTest::addColumn<QString>("filePath");
    QTest::addColumn<QString>("expectedWarning");

    QTest::newRow("failed dir") <<
        "/non-existing-dir" <<
        "sample.json" <<
        "Could not create cache dir!";

    QTest::newRow("failed write") <<
        "/etc" <<
        "hosts" <<
        "Could not create storage file \"/etc/hosts\"";
}

void JsonStorageTest::testFailedWrite()
{
    QFETCH(QString, base);
    QFETCH(QString, filePath);
    QFETCH(QString, expectedWarning);

    /* If we can create any arbitrary files under the filesystem root,
     * then this test will fail. */
    if (QDir::root().mkpath("json-storage-test")) {
        QSKIP("This test does not work in a container");
    }

    QTest::ignoreMessage(QtWarningMsg, expectedWarning.toUtf8().constData());

    qputenv("HOME", base.toUtf8());

    QByteArray json = R"({
        "key": "value",
        "array": [ "one", "two", "three" ]
    })";
    QJsonObject obj = QJsonDocument::fromJson(json).object();

    JsonStorage storage;
    storage.setFilePath(filePath);
    storage.storeObject(obj);
}

void JsonStorageTest::testFailedLoad()
{
    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());
    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());

    JsonStorage storage;
    storage.setBase(QStandardPaths::CacheLocation);
    storage.setFilePath("missing.json");

    QJsonObject obj = storage.loadObject();
    QVERIFY(obj.isEmpty());
}

void JsonStorageTest::testStore()
{
    QTemporaryDir tmpDir;
    QVERIFY(tmpDir.isValid());

    qputenv("XDG_CACHE_HOME", tmpDir.path().toUtf8());

    QByteArray json = R"({
        "key": "value",
        "array": [ "one", "two", "three" ]
    })";
    QJsonDocument doc = QJsonDocument::fromJson(json);
    QJsonObject expectedObject = doc.object();

    {
        JsonStorage storage;
        storage.setBase(QStandardPaths::CacheLocation);
        storage.setFilePath("sample.json");
        storage.storeObject(expectedObject);
    }

    {
        JsonStorage storage;
        storage.setBase(QStandardPaths::CacheLocation);
        storage.setFilePath("sample.json");
        QCOMPARE(storage.loadObject(), expectedObject);
    }
}

QTEST_GUILESS_MAIN(JsonStorageTest)

#include "tst_json_storage.moc"
