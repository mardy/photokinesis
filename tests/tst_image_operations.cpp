/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_operations.h"

#include <QDir>
#include <QSignalSpy>
#include <QSize>
#include <QTemporaryDir>
#include <QTest>

using namespace Photokinesis;

class ImageOperationsTest: public QObject
{
    Q_OBJECT

public:
    ImageOperationsTest();

private Q_SLOTS:
    void testEnsureMaxSize_data();
    void testEnsureMaxSize();

private:
    QDir m_dataDir;
};

ImageOperationsTest::ImageOperationsTest():
    m_dataDir(QString(ITEMS_DIR "/"))
{
}

void ImageOperationsTest::testEnsureMaxSize_data()
{
    QTest::addColumn<QString>("fileName");
    QTest::addColumn<QSize>("maxSize");
    QTest::addColumn<QSize>("expectedSize");

    QTest::newRow("invalid size") <<
        "image5.jpg" <<
        QSize() <<
        QSize(32, 32);

    QTest::newRow("bigger size") <<
        "image5.jpg" <<
        QSize(40, 50) <<
        QSize(32, 32);

    QTest::newRow("valid size") <<
        "image5.jpg" <<
        QSize(4, 1) <<
        QSize(1, 1);

    QTest::newRow("valid size 2") <<
        "image5.jpg" <<
        QSize(2, 5) <<
        QSize(2, 2);

    QTest::newRow("only width") <<
        "image5.jpg" <<
        QSize(2, -1) <<
        QSize(2, 2);

    QTest::newRow("only height") <<
        "image5.jpg" <<
        QSize(-1, 3) <<
        QSize(3, 3);
}

void ImageOperationsTest::testEnsureMaxSize()
{
    QFETCH(QString, fileName);
    QFETCH(QSize, maxSize);
    QFETCH(QSize, expectedSize);

    QTemporaryDir tmpDir;

    QString filePathIn = m_dataDir.filePath(fileName);
    QString filePathOut = tmpDir.filePath("output.jpg");
    bool ok =
        ImageOperations::ensureMaxSize(filePathIn, filePathOut, maxSize);
    QVERIFY(ok);

    QImage image(filePathOut);
    QCOMPARE(image.size(), expectedSize);
}

QTEST_GUILESS_MAIN(ImageOperationsTest)

#include "tst_image_operations.moc"
