TARGET = fake_objects
TEMPLATE = lib

include(../common-config.pri)

CONFIG += \
    c++11 \
    static

QT += \
    network \
    qml \
    testlib

DEFINES += BUILDING_LIBPHOTOKINESIS

SRC_DIR = $${TOP_SRC_DIR}/lib/Photokinesis

INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib \
    $${SRC_DIR}

SOURCES += \
    fake_abstract_uploader.cpp \
    fake_image_operations.cpp \
    fake_image_processor.cpp \
    fake_plugin_manager.cpp

HEADERS += \
    $${SRC_DIR}/abstract_uploader.h \
    $${SRC_DIR}/image_processor.h \
    $${SRC_DIR}/plugin_manager.h \
    fake_abstract_uploader.h \
    fake_image_operations.h \
    fake_image_processor.h \
    fake_network.h \
    fake_plugin_manager.h
