/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader_model.h"

#include <Photokinesis/AbstractUploader>
#include <QDebug>
#include <QJsonArray>
#include <QJsonObject>
#include <QScopedPointer>
#include <QSignalSpy>
#include <QTest>
#include "fake_plugin_manager.h"

using namespace Photokinesis;

class DummyUploader: public AbstractUploader
{
    Q_OBJECT

public:
    DummyUploader(): AbstractUploader(QJsonObject()) {}
    void setData(const QString &name,
                 const QString &displayName,
                 const QUrl &icon);
    void startUpload(const QString &, const QJsonObject &) override {}
};

void DummyUploader::setData(const QString &name,
                            const QString &displayName,
                            const QUrl &icon)
{
    setName(name);
    setDisplayName(displayName);
    setIcon(icon);
}

class UploaderModelTest: public QObject
{
    Q_OBJECT

private Q_SLOTS:
    void testProperties();
    void testRoles_data();
    void testRoles();
};

void UploaderModelTest::testProperties()
{
    {
        UploaderModel model;
        QCOMPARE(model.rowCount(), 0);
    }

    PluginManagerPrivate *managerMocked =
        PluginManagerPrivate::mocked(PluginManager::instance());
    managerMocked->setProfiles({
        { "first", UploaderProfile() },
        { "second", UploaderProfile() },
    });

    {
        UploaderModel model;
        QCOMPARE(model.rowCount(), 2);
        QCOMPARE(model.property("count").toInt(), 2);
    }
}

void UploaderModelTest::testRoles_data()
{
    QTest::addColumn<QString>("name");
    QTest::addColumn<QString>("displayName");
    QTest::addColumn<QUrl>("icon");
    QTest::addColumn<bool>("enabled");
    QTest::addColumn<QStringList>("supportedOptions");

    QTest::newRow("all nulls") <<
        QString() <<
        QString() <<
        QUrl() <<
        false <<
        QStringList {};

    QTest::newRow("valid") <<
        "uglyName" <<
        "Pretty name" <<
        QUrl("qrc:some-icon.png") <<
        true <<
        QStringList { "title", "tags" };
}

void UploaderModelTest::testRoles()
{
    QFETCH(QString, name);
    QFETCH(QString, displayName);
    QFETCH(QUrl, icon);
    QFETCH(bool, enabled);
    QFETCH(QStringList, supportedOptions);

    DummyUploader uploader;
    uploader.setEnabled(enabled);

    PluginManagerPrivate *managerMocked =
        PluginManagerPrivate::mocked(PluginManager::instance());
    managerMocked->setProfiles({
        { "dummy", UploaderProfile("dummy", {
                { "name", name },
                { "displayName", displayName },
                { "icon", icon.toString() },
                { "supportedOptions", QJsonArray::fromStringList(supportedOptions) },
            })
        },
    });
    managerMocked->setUploaders({
        { "dummy", &uploader },
    });

    UploaderModel model;
    QCOMPARE(model.rowCount(), 1);

    QCOMPARE(model.get(0, "name").toString(), name);
    QCOMPARE(model.get(0, "display").toString(), displayName);
    QCOMPARE(model.get(0, "icon").toUrl(), icon);
    QCOMPARE(model.get(0, "enabled").toBool(), enabled);
    QCOMPARE(model.get(0, "supportedOptions").toStringList(), supportedOptions);
    QCOMPARE(model.get(0, "uploader").value<AbstractUploader*>(), &uploader);

    QTest::ignoreMessage(QtWarningMsg, "Unknown role ID: -1");
    QVERIFY(!model.get(0, "invalid-role").isValid());
}

QTEST_GUILESS_MAIN(UploaderModelTest)

#include "tst_uploader_model.moc"
