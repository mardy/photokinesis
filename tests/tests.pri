include(../common-config.pri)

CONFIG += \
    c++11 \
    exceptions_off \
    no_keywords \
    qt

QT += \
    core \
    testlib

LIBS += -L. -lfake_objects
PRE_TARGETDEPS += libfake_objects.a

OBJECTS_DIR = $${TARGET}.obj
SRC_DIR = $${TOP_SRC_DIR}/src
PHOTOKINESIS_SRC_DIR = $${TOP_SRC_DIR}/lib/Photokinesis

INCLUDEPATH += \
    $${SRC_DIR} \
    $${TOP_SRC_DIR}/lib

HEADERS += \
    test_utils.h

check.commands = ./$${TARGET}
check.depends = $${TARGET}
QMAKE_EXTRA_TARGETS += check
