TARGET = tst_registry

include(tests.pri)

QT += \
    network \
    qml

SOURCES += \
    $${SRC_DIR}/registry.cpp \
    tst_registry.cpp

HEADERS += \
    $${SRC_DIR}/registry.h \
    fake_network.h

