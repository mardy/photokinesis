/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_metadata_loader.h"

#include <QDebug>

using namespace Photokinesis;

static MetadataLoaderPrivate *m_privateInstance = 0;

MetadataLoaderPrivate::MetadataLoaderPrivate():
    MetadataLoaderPrivate(nullptr)
{
}

MetadataLoaderPrivate::MetadataLoaderPrivate(MetadataLoader *q):
    QObject(),
    m_pendingRequests(0),
    m_completedRequests(0),
    m_mustDelete(true),
    q_ptr(q)
{
    m_privateInstance = this;
}

MetadataLoaderPrivate::~MetadataLoaderPrivate()
{
    m_privateInstance = nullptr;
}

MetadataLoader::MetadataLoader(QObject *parent):
    QObject(parent)
{
    if (m_privateInstance) {
        m_privateInstance->q_ptr = this;
        d_ptr.reset(m_privateInstance);
        d_ptr->m_mustDelete = false;
        Q_EMIT d_ptr->instanceCreated(this);
    } else {
        d_ptr.reset(new MetadataLoaderPrivate(this));
    }
}

MetadataLoader::~MetadataLoader()
{
    if (!d_ptr->m_mustDelete) {
        // Steal the d_ptr, so that it won't be deleted
        d_ptr.take();
    }
}

void MetadataLoader::loadMetadata(const QString &file, int requestId)
{
    Q_D(MetadataLoader);
    d->m_completedRequests++;
    const MetadataLoaderPrivate::Metadata md = d->m_metadata.value(file);
    Q_EMIT d->loadMetadataCalled(file, requestId);
    QMetaObject::invokeMethod(this, "metadataLoaded",
                              Qt::QueuedConnection,
                              Q_ARG(int, requestId),
                              Q_ARG(QString, md.title),
                              Q_ARG(QString, md.description),
                              Q_ARG(QStringList, md.tags));
}

int MetadataLoader::pendingRequests() const
{
    Q_D(const MetadataLoader);
    return d->m_pendingRequests;
}

int MetadataLoader::completedRequests() const
{
    Q_D(const MetadataLoader);
    return d->m_completedRequests;
}
