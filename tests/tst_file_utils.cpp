/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "file_utils.h"

#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QJSValueIterator>
#include <QHash>
#include <QQmlComponent>
#include <QQmlEngine>
#include <QScopedPointer>
#include <QSet>
#include <QSignalSpy>
#include <QTemporaryDir>
#include <QTest>

using namespace Mardy;

namespace QTest {

template<>
char *toString(const QHash<QString,QSet<QString>> &p)
{
    QByteArray ba = "QHash(";
    for (auto i = p.begin(); i != p.end(); i++) {
        ba += '"';
        ba += i.key().toUtf8();
        ba += "\": [";
        ba += QStringList(i.value().toList()).join(',');
        ba += "]\n";
    }
    ba += ")";
    return qstrdup(ba.data());
}

} // QTest namespace


class FileUtilsTest: public QObject
{
    Q_OBJECT

public:
    FileUtilsTest();

    bool createFile(const QString &filePath);
    QString stripPrefix(const QString &filePath, const QString &prefix);

private Q_SLOTS:
    void testFindFiles_data();
    void testFindFiles();
    void testFindFilesAsync();

    void testFindFilesMulti_data();
    void testFindFilesMulti();

private:
    typedef QHash<QString,QSet<QString>> DirectoryContents;
};

FileUtilsTest::FileUtilsTest()
{
}

bool FileUtilsTest::createFile(const QString &filePath)
{
    QDir dir = QFileInfo(filePath).absoluteDir();
    dir.mkpath(".");

    QFile file(filePath);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
        return false;
    file.write("Hi!");
    return true;
}

QString FileUtilsTest::stripPrefix(const QString &filePath, const QString &prefix)
{
    if (!filePath.startsWith(prefix)) return filePath;

    int offset = prefix.length();
    if (filePath[offset] == '/') offset++;
    return filePath.mid(offset);
}

void FileUtilsTest::testFindFiles_data()
{
    QTest::addColumn<bool>("recursive");
    QTest::addColumn<QStringList>("expectedFiles");

    QTest::newRow("non recursive") <<
        false <<
        QStringList { "a.jpg", "b.jpg", "c.jpg", "z.jpg" };

    QTest::newRow("recursive") <<
        true <<
        QStringList { "a.jpg", "b.jpg", "c.jpg",
            "subdir/1.jpg",
            "subdir/2.jpg",
            "subdir/3.jpg",
            "z.jpg"
        };
}

void FileUtilsTest::testFindFiles()
{
    QFETCH(bool, recursive);
    QFETCH(QStringList, expectedFiles);

    QTemporaryDir tmpDir;
    QDir dest(tmpDir.path());

    QString fileName = dest.filePath("a.jpg");
    QVERIFY(createFile(fileName));
    QFile::copy(fileName, dest.filePath("b.jpg"));
    QFile::copy(fileName, dest.filePath("c.jpg"));
    QFile::copy(fileName, dest.filePath("z.jpg"));
    dest.mkdir("subdir");
    QFile::copy(fileName, dest.filePath("subdir/1.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/2.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/3.jpg"));

    FileUtils utils;
    auto result =
        utils.findFiles(QUrl::fromLocalFile(tmpDir.path()), recursive);

    QStringList files;
    for (const QUrl &url: result) {
        files.append(dest.relativeFilePath(url.toLocalFile()));
    }

    QCOMPARE(files, expectedFiles);
}

void FileUtilsTest::testFindFilesAsync()
{
    QTemporaryDir tmpDir;
    QDir dest(tmpDir.path());

    QString fileName = dest.filePath("a.jpg");
    QVERIFY(createFile(fileName));
    QFile::copy(fileName, dest.filePath("b.jpg"));
    QFile::copy(fileName, dest.filePath("c.jpg"));
    QFile::copy(fileName, dest.filePath("z.jpg"));
    dest.mkdir("subdir");
    QFile::copy(fileName, dest.filePath("subdir/1.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/2.jpg"));
    QFile::copy(fileName, dest.filePath("subdir/3.jpg"));

    QStringList expectedFiles {
        "a.jpg", "b.jpg", "c.jpg",
        "subdir/1.jpg",
        "subdir/2.jpg",
        "subdir/3.jpg",
        "z.jpg",
    };

    QQmlEngine engine;
    qmlRegisterType<FileUtils>("MyTest", 1, 0, "FileUtils");
    QQmlComponent component(&engine);
    component.setData(R"(
        import MyTest 1.0
        FileUtils {
          id: root
          signal done(var files)
          function run(folder) {
            findFiles(folder, true, function(files) {
              root.done(files)
            })
          }
        })",
        QUrl());
    QScopedPointer<QObject> object(component.create());
    QVERIFY(object != 0);

    QSignalSpy done(object.data(), SIGNAL(done(QVariant)));

    QVariant folder(QUrl::fromLocalFile(tmpDir.path()));
    bool ok = QMetaObject::invokeMethod(object.data(), "run",
                                        Q_ARG(QVariant, folder));
    QVERIFY(ok);

    QTRY_COMPARE(done.count(), 1);
    QStringList files;
    QList<QUrl> urlList = done.at(0).at(0).value<QList<QUrl>>();
    for (const QUrl &url: urlList) {
        files.append(dest.relativeFilePath(url.toLocalFile()));
    }
    QCOMPARE(files, expectedFiles);

    QTest::qWait(10); // Wait for QFuture destruction
}

void FileUtilsTest::testFindFilesMulti_data()
{
    QTest::addColumn<QStringList>("inputFiles");
    QTest::addColumn<DirectoryContents>("expectedFiles");

    QTest::newRow("all regular") <<
        QStringList { "a.jpg", "b.jpg", "c.jpg", "dir1/f.jpg" } <<
        DirectoryContents {
            { "", { "a.jpg", "b.jpg", "c.jpg", "dir1/f.jpg" }},
        };

    QTest::newRow("recursive") <<
        QStringList { "dir1", "dir2" } <<
        DirectoryContents {
            { "dir1",
                {
                    "dir1/subdir1/a.jpg",
                    "dir1/subdir1/d.jpg",
                    "dir1/subdir2/e.jpg",
                    "dir1/f.jpg",
                }
            },
            { "dir2",
                {
                    "dir2/subdir3/c.jpg",
                    "dir2/subdir3/g.jpg",
                }
            },
        };

    QTest::newRow("mixed") <<
        QStringList { "a.jpg", "b.jpg", "dir1/subdir1" } <<
        DirectoryContents {
            { "dir1/subdir1",
                {
                    "dir1/subdir1/a.jpg",
                    "dir1/subdir1/d.jpg",
                }
            },
            { "", { "a.jpg", "b.jpg", } },
        };
}

void FileUtilsTest::testFindFilesMulti()
{
    QFETCH(QStringList, inputFiles);
    QFETCH(DirectoryContents, expectedFiles);

    QTemporaryDir tmpDir;
    QDir dest(tmpDir.path());

    const QStringList filesToCreate = {
        "a.jpg",
        "b.jpg",
        "c.jpg",
        "dir1/subdir1/a.jpg",
        "dir1/subdir1/d.jpg",
        "dir1/subdir2/e.jpg",
        "dir1/f.jpg",
        "dir2/subdir3/c.jpg",
        "dir2/subdir3/g.jpg",
    };
    for (const QString &file: filesToCreate) {
        QVERIFY(createFile(dest.filePath(file)));
    }

    QQmlEngine engine;
    qmlRegisterType<FileUtils>("MyTest", 1, 0, "FileUtils");
    QQmlComponent component(&engine);
    component.setData(R"(
        import MyTest 1.0

        FileUtils {
          id: root

          property var foundFiles: ({})

          signal done()

          function run(input) {
            findFiles(input, true, function(files, baseDir, hasMore) {
              foundFiles[baseDir] = files
              if (!hasMore) {
                root.done()
              }
            })
          }
        })",
        QUrl());
    QScopedPointer<QObject> object(component.create());
    QVERIFY(object != 0);

    QSignalSpy done(object.data(), SIGNAL(done()));

    QList<QUrl> inputUrls;
    for (const QString &inputFile: inputFiles) {
        inputUrls.append(QUrl::fromLocalFile(dest.filePath(inputFile)));
    }
    QVariant inputValue = QVariant::fromValue(inputUrls);
    bool ok = QMetaObject::invokeMethod(object.data(), "run",
                                        Q_ARG(QVariant, inputValue));
    QVERIFY(ok);

    QTRY_COMPARE(done.count(), 1);

    /* Inspect the "foundFiles" property, and construct an object which we
     * could compare with "expectedFiles" */
    DirectoryContents actualFiles;
    QJSValue foundFilesValue = object->property("foundFiles").value<QJSValue>();
    QJSValueIterator valueIterator(foundFilesValue);
    while (valueIterator.hasNext()) {
        valueIterator.next();
        QVariant fileListVariant = valueIterator.value().toVariant();
        QVERIFY(fileListVariant.isValid());
        QVERIFY(fileListVariant.canConvert<QList<QUrl>>());

        QString baseDir = stripPrefix(QUrl(valueIterator.name()).path(), tmpDir.path());
        const QList<QUrl> fileUrls = fileListVariant.value<QList<QUrl>>();
        QSet<QString> filePaths;
        for (const QUrl &fileUrl: fileUrls) {
            filePaths.insert(stripPrefix(fileUrl.path(), tmpDir.path()));
        }
        actualFiles.insert(baseDir, filePaths);
    }

    QCOMPARE(actualFiles, expectedFiles);
    QTest::qWait(10); // Wait for QFuture destruction
}

QTEST_GUILESS_MAIN(FileUtilsTest)

#include "tst_file_utils.moc"
