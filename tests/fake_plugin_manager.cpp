/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "fake_plugin_manager.h"

#include <QDebug>

using namespace Photokinesis;

static PluginManager *m_instance = 0;

PluginManager::PluginManager(QObject *parent):
    QObject(parent),
    d_ptr(new PluginManagerPrivate())
{
}

PluginManager::~PluginManager()
{
    m_instance = 0;
}

PluginManager *PluginManager::instance()
{
    if (!m_instance) {
        m_instance = new PluginManager;
    }
    return m_instance;
}

QList<UploaderProfile> PluginManager::uploaderProfiles() const
{
    Q_D(const PluginManager);
    return d->m_profiles.values();
}

AbstractUploader *PluginManager::uploader(const UploaderProfile &profile)
{
    Q_D(PluginManager);
    return d->m_uploaders.value(profile.pluginName());
}
