TEMPLATE = subdirs
SUBDIRS = \
    tst_abstract_uploader.pro \
    tst_executor.pro \
    tst_file_utils.pro \
    tst_image_operations.pro \
    tst_image_processor.pro \
    tst_json_serializer.pro \
    tst_json_storage.pro \
    tst_metadata.pro \
    tst_metadata_loader.pro \
    tst_plugin_manager_subdirs.pro \
    tst_registry.pro \
    tst_rounded_image_provider.pro \
    tst_session.pro \
    tst_signal_waiter.pro \
    tst_ui_helpers.pro \
    tst_updater.pro \
    tst_upload_model.pro \
    tst_upload_model_session.pro \
    tst_uploader_model.pro
