# Photokinesis

## A cross-platform photo uploader

[![build status](https://gitlab.com/mardy/photokinesis/badges/master/build.svg)](https://gitlab.com/mardy/photokinesis/commits/master)
[![coverage report](https://gitlab.com/mardy/photokinesis/badges/master/coverage.svg)](https://gitlab.com/mardy/photokinesis/commits/master)

Photokinesis is the codename for [PhotoTeleport](http://phototeleport.com), an
open source, cross platform photo uploader written in Qt+QML.

For releases, news, FAQs and screenshots please see the public site
[phototeleport.com](http://phototeleport.com). For reporting bugs, please use
the [issues](https://gitlab.com/mardy/photokinesis/issues) section in this
website.


## How to build

In order to test Photokinesis (and hopefully to [contribute](CONTRIBUTING.md)
to its development ;-) ) please checkout the source code (including all
submodules) and follow these steps:

    # first, build libauthentication:
    cd libauthentication
    mkdir build && cd build
    qmake CONFIG+=nomake_examples PREFIX=/tmp/opt
    make install
    # build photokinesis:
    cd ../..
    mkdir build && cd build
    qmake CONFIG+=nomake_examples PREFIX=/tmp/opt
    make install

This will install Photokinesis in `/tmp/opt`, which will get cleared the next time
you reboot your computer; this is convenient if you don't want to risk messing
up your system just to try out the application.

In order to launch Photokinesis:
```
cd /tmp/opt/share
../bin/Photokinesis
```

Have fun! :-)

