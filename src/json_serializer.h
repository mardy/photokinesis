/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_JSON_SERIALIZER_H
#define PHOTOKINESIS_JSON_SERIALIZER_H

#include <QJsonArray>
#include <QJsonObject>

class QIODevice;

namespace Photokinesis {

class JsonSerializer
{
public:
    static bool dump(QIODevice *device, const QJsonObject &json);
    static QJsonObject loadObject(QIODevice *device);

    static bool dump(QIODevice *device, const QJsonArray &json);
    static QJsonArray loadArray(QIODevice *device);
};

} // namespace

#endif // PHOTOKINESIS_JSON_SERIALIZER_H
