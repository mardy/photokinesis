/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_UPLOADER_MODEL_H
#define PHOTOKINESIS_UPLOADER_MODEL_H

#include <QAbstractListModel>
#include <QVariant>

namespace Photokinesis {

class UploaderModelPrivate;
class UploaderModel: public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int count READ rowCount NOTIFY countChanged)

public:
    enum Roles {
        // Use Qt::DisplayName for user-facing name
        NameRole = Qt::UserRole + 1,
        IconRole,
        EnabledRole,
        SupportedOptionsRole,
        UploaderRole,
    };

    UploaderModel(QObject *parent = 0);
    ~UploaderModel();

    Q_INVOKABLE QVariantMap get(int row) const;
    Q_INVOKABLE QVariant get(int row, const QString &role) const;

    int rowCount(const QModelIndex &parent = QModelIndex()) const Q_DECL_OVERRIDE;
    QVariant data(const QModelIndex &index,
                  int role = Qt::DisplayRole) const Q_DECL_OVERRIDE;
    QHash<int, QByteArray> roleNames() const Q_DECL_OVERRIDE;

Q_SIGNALS:
    void countChanged();

private:
    QScopedPointer<UploaderModelPrivate> d_ptr;
    Q_DECLARE_PRIVATE(UploaderModel)
};

} // namespace

#endif // PHOTOKINESIS_UPLOADER_MODEL_H
