import QtQml 2.2
import QtQuick 2.7
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Column {
    id: root

    property alias label: labelWidget.text
    property var uploaders: null
    property var model: null
    property var rows: []
    property string name: ""

    property var _value0
    property bool _writingEnabled: true
    property int _hoveredUploader: -1
    property int _activeUploader: -1
    property int _shownUploader: _hoveredUploader >= 0 ? _hoveredUploader : (_activeUploader >= 0 ? _activeUploader : -1)

    default property alias component: loader.sourceComponent

    enabled: rows.length > 0

    Component.onCompleted: uploaderOptions.init()
    onRowsChanged: update()

    RowLayout {
        anchors { left: parent.left; right: parent.right }

        Label {
            id: labelWidget
            Layout.fillWidth: true
        }

        Label {
            font.pointSize: 10
            text: qsTr("Different value for:")
            visible: uploaderRow.visible
        }

        Row {
            id: uploaderRow
            visible: repeater.count > 1
            spacing: 4
            Repeater {
                id: repeater
                model: uploaderOptions.modelData
                delegate: ItemOptionUploader {
                    width: 24
                    height: 24
                    icon: modelData.icon
                    visible: modelData.supportsOption
                    hasValue: valueIsSet(modelData.value)
                    selected: root._activeUploader == index
                    onContainsMouseChanged: root._hoveredUploader = containsMouse ? index : -1
                    onClicked: {
                        root._activeUploader = selected ? -1 : index
                        loader.forceActiveFocus()
                    }
                }
            }
        }
    }

    Loader {
        id: loader
        property var displayValue
        anchors { left: parent.left; right: parent.right }
        focus: true
        onDisplayValueChanged: if (item) {
            root._writingEnabled = false
            item.value = cleanValue(displayValue)
            root._writingEnabled = true
        }
        onActiveFocusChanged: if (!activeFocus) {
            root._activeUploader = -1
        }
    }

    QtObject {
        id: uploaderOptions
        property var shownValue: root._shownUploader >= 0 ? get(root._shownUploader).value : undefined
        property var modelData: []
        property int count: modelData.length

        function init() {
            var tmp = []
            var uploaders = root.uploaders
            var supported = false
            for (var i = 0; i < uploaders.count; i++) {
                var uploaderData = uploaders.get(i)
                var data = {
                    "icon": uploaderData.icon,
                }

                var column = root.uploaders.column(i)
                var hasOption = uploaderData.uploader.hasOption(root.name)
                if (hasOption) supported = true
                data["supportsOption"] = hasOption
                data["value"] = computeValue(column)
                tmp.push(data)
            }
            modelData = tmp
            root.visible = supported
        }

        function update() {
            for (var i = 0; i < count; i++) {
                updateUploader(i)
            }
        }

        function updateUploader(i) {
            var column = root.uploaders.column(i)
            var value = computeValue(column)
            setProperty(i, "value", value)
        }

        function handleModelChanged() {
            root._activeUploader = -1
            clear()
            init()
        }

        function setProperty(row, name, value) {
            var tmp = modelData
            var rowData = tmp[row]
            rowData[name] = value
            modelData = tmp
        }

        function get(row) {
            return modelData[row]
        }

        function clear() {
            modelData = []
        }
    }

    Connections {
        target: root.uploaders
        onModelReset: updateModelTimer.restart()
        onRowsInserted: updateModelTimer.restart()
        onRowsRemoved: updateModelTimer.restart()
    }

    Timer {
        id: updateModelTimer
        interval: 200
        onTriggered: uploaderOptions.handleModelChanged()
    }

    Connections {
        target: loader.item
        enabled: root._writingEnabled
        onValueChanged: root.setValue(root._activeUploader == -1 ? 0 : root.uploaders.column(root._activeUploader), loader.item.value)
    }

    Binding {
        target: loader
        property: "displayValue"
        value: valueIsSet(uploaderOptions.shownValue) ? uploaderOptions.shownValue : _value0
    }

    function update() {
        root._activeUploader = -1
        root._value0 = computeValue(0)
        uploaderOptions.update()
    }

    function cleanValue(value) {
        var v = value
        if (v === null) {
            switch(typeof(loader.item.value)) {
            case "string": v = ""; break;
            }
        }
        return v
    }

    function setValue(column, value) {
        storeValue(column, value)
        if (column > 0) {
            var i = root.uploaders.uploaderIndex(column)
            uploaderOptions.updateUploader(i)
        } else {
            root._value0 = computeValue(0)
        }
    }
}
