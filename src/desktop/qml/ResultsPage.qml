import Photokinesis 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root

    property var items: null
    property var uploaders: null

    spacing: 12

    property int _failedTotal: 0
    property var _uploaderData: []

    Component.onCompleted: prepareData()
    onVisibleChanged: if (visible) prepareData()

    Label {
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        font.italic: true
        visible: root._failedTotal == 0
        text: qsTr("All items have been successfully uploaded.")
    }

    Label {
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        font.bold: true
        color: "red"
        visible: root._failedTotal > 0
        text: qsTr("Some items failed to upload. You can go back to the previous page to try uploading the failed items again.")
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        ListView {
            id: itemView

            spacing: 2
            model: root.uploaders
            delegate: UploaderResultsDelegate {
                width: itemView.width
                displayName: model.display
                icon: model.icon
                uploader: model.uploader
                uploads: root._uploaderData[index].uploads
                failures: root._uploaderData[index].failures
            }
        }
    }

    function prepareData() {
        var uploaderData = []
        var failedTotal = 0
        for (var u = 0; u < root.uploaders.count; u++) {
            var numUploads = 0
            var numFailures = 0
            var column = root.uploaders.column(u)
            for (var i = 0; i < root.items.count; i++) {
                var uploadStatus = root.items.get(i, column, "uploadStatus")
                if (uploadStatus == UploadModel.Uploaded) {
                    numUploads++
                } else if (uploadStatus == UploadModel.Failed) {
                    numFailures++
                }
            }
            uploaderData.push({
                "uploads": numUploads,
                "failures": numFailures,
            })
            failedTotal += numFailures
        }
        _uploaderData = uploaderData
        _failedTotal = failedTotal
    }
}
