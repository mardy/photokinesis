import QtQuick 2.2

ItemOption {
    id: root

    function computeValue(column) {
        var value = [];
        for (var i = 0; i < root.rows.length; i++) {
            var v = root.model.get(root.rows[i], column, root.name)
            value.push(v)
        }
        return value
    }

    function storeValue(column, value) {
        for (var i = 0; i < root.rows.length; i++) {
            root.model.set(root.rows[i], column, root.name, value[i])
        }
    }

    function valueIsSet(value) {
        if (!value) return false
        for (var i = 0; i < value.length; i++) {
            if (value[i] !== undefined) return true
        }
        return false
    }
}
