import Photokinesis 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import QtQuick.Window 2.2

ColumnLayout {
    id: root

    property var model: null

    property var attemptedAuths: []
    property bool done: false

    Label {
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        font.italic: true
        text: qsTr("Please wait until login is performed on all services and your account information is being retrieved.\nA window will be opened if your login details are needed.")
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true

        ListView {
            id: serviceView
            anchors { leftMargin: 2; rightMargin: 2 }
            spacing: 4
            model: root.model
            delegate: UploaderAuthDelegate {
                width: serviceView.width
                displayName: model.display
                icon: model.icon
                uploader: model.uploader
                othersAreAuthenticating: authenticationConnection.target != null &&
                                         authenticationConnection.target != uploader
                onAccountChangeRequested: authParent.changeAccount(model.uploader)
                onAuthenticationRequested: authParent.authenticate(model.uploader)
                onStateChanged: root.checkReadyness()
            }
        }
    }

    Window {
        id: authenticationWindow
        visible: authParent.authItem && authParent.authItem.wantsVisibility
        width: 680
        height: 480
        title: qsTr("%1 authentication")
            .arg(authParent.uploader ? authParent.uploader.displayName : "")
        flags: Qt.Dialog
        modality: Qt.ApplicationModal
        onClosing: closeTimer.restart()

        Timer {
            id: closeTimer
            interval: 200
            onTriggered: if (authParent.authItem) { authParent.authItem.cancel() }
        }

        Item {
            id: authParent
            anchors.fill: parent
            property Item authItem: children.length > 0 ? children[0] : null
            property var activeUploaders: []
            property var uploader: null

            Connections {
                target: authParent.authItem
                onFinished: {
                    authParent.authItem.parent = null
                    authParent.uploader = null
                }
            }

            function watchUploader(uploader) {
                authParent.uploader = uploader
                var tmp = activeUploaders
                tmp.push(uploader)
                activeUploaders = tmp
                function checkFinished() {
                    if (uploader.status == AbstractUploader.Ready ||
                        uploader.status == AbstractUploader.NeedsAuthentication) {
                        uploader.statusChanged.disconnect(checkFinished)
                        var i = activeUploaders.indexOf(uploader)
                        if (i >= 0) {
                            var tmp = activeUploaders
                            tmp.splice(i, 1)
                            activeUploaders = tmp
                        }
                    }
                }
                function checkAuthenticating() {
                    if (uploader.status == AbstractUploader.Authenticating) {
                        uploader.statusChanged.disconnect(checkAuthenticating)
                        uploader.statusChanged.connect(checkFinished)
                    }
                }
                uploader.statusChanged.connect(checkAuthenticating)
            }

            function authenticate(uploader) {
                watchUploader(uploader)
                uploader.authenticate(authParent)
            }

            function changeAccount(uploader) {
                watchUploader(uploader)
                uploader.changeAccount(authParent)
            }
        }
    }

    Connections {
        id: authenticationConnection
        ignoreUnknownSignals: true
        target: null
        onStatusChanged: if (target.status != AbstractUploader.Authenticating) {
            authenticationConnection.target = null
            if (!authenticateNext()) {
                authenticationConnection.enabled = false
            }
        }
    }

    Component.onCompleted: authenticateNext()
    onVisibleChanged: if (visible) {
        attemptedAuths = []
        authenticationConnection.enabled = true
        if (!authenticateNext()) {
            checkReadyness()
        }
    } else {
        storeSettings()
    }

    function authenticateNext() {
        var model = root.model
        for (var i = 0; i < model.count; i++) {
            var uploader = model.get(i, "uploader")
            if (uploader.status == AbstractUploader.NeedsAuthentication &&
                !authenticationConnection.target &&
                root.attemptedAuths.indexOf(uploader) < 0) {
                authenticationConnection.target = uploader
                root.attemptedAuths.push(uploader)
                authParent.authenticate(uploader)
                return true
            }
        }
        return false
    }

    function checkReadyness() {
        console.log("checking readiness")
        var model = root.model
        var allReady = true
        for (var i = 0; i < model.count; i++) {
            var uploader = model.get(i, "uploader")
            if (uploader.status != AbstractUploader.Ready ||
                uploader.needsConfiguration) {
                allReady = false
                break
            }
        }
        root.done = allReady
    }

    function storeSettings() {
        var model = root.model
        for (var i = 0; i < model.count; i++) {
            var uploader = model.get(i, "uploader")
            uploader.storeSettings()
        }
    }
}
