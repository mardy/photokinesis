import QtQuick 2.5
import QtQuick.Controls 1.4

Column {
    id: root

    property var rows: []
    property var tags: []

    onRowsChanged: updateTags()
    spacing: 2

    property int _targetHeight: implicitHeight
    on_TargetHeightChanged: if (_targetHeight > height) {
        heightAnimation.stop()
        height = _targetHeight
    } else {
        heightAnimationTimer.restart()
    }

    Timer {
        id: heightAnimationTimer
        interval: 2000
        onTriggered: heightAnimation.start()
    }
    NumberAnimation {
        id: heightAnimation
        target: root
        properties: "height"; to: root._targetHeight; easing.type: Easing.InOutQuad; duration: 300
    }

    TagView {
        id: flow
        anchors { left: parent.left; right: parent.right }
        editable: true
        tags: root.tags
        height: Math.max(24, implicitHeight)

        onTagClicked: flow.handleClick(index)

        function handleClick(i) {
            var t = root.tags[i]
            if (t.freq == 1) {
                removeTag(t.name)
            } else {
                addTag(t.name)
            }
        }

        function removeTag(tag) {
            var newRows = []
            for (var i = 0; i < root.rows.length; i++) {
                var tagRow = root.rows[i]
                var idx = tagRow.indexOf(tag)
                if (idx >= 0) {
                    tagRow = tagRow.slice()
                    tagRow.splice(idx, 1)
                }
                newRows.push(tagRow)
            }
            root.rows = newRows
        }

        function addTag(tag) {
            var newRows = []
            for (var i = 0; i < root.rows.length; i++) {
                var tagRow = root.rows[i].slice()
                var idx = tagRow.indexOf(tag)
                if (idx < 0) {
                    tagRow.push(tag)
                }
                newRows.push(tagRow)
            }
            root.rows = newRows
        }
    }

    TextField {
        id: tabField
        anchors { left: parent.left; right: parent.right }
        placeholderText: qsTr("Add tag")
        inputMethodHints: Qt.ImhNoAutoUppercase
        Keys.onReturnPressed: {
            flow.addTag(text)
            text = ""
        }
    }

    function updateTags() {
        var newTags = []
        var tagCounts = {}
        if (rows.length === undefined) return
        for (var i = 0; i < rows.length; i++) {
            var tagRow = rows[i]
            if (!tagRow) continue
            for (var j = 0; j < tagRow.length; j++) {
                var t = tagRow[j]
                if (t in tagCounts) {
                    tagCounts[t]++
                } else {
                    tagCounts[t] = 1
                }
            }
        }
        for (var t in tagCounts) {
            newTags.push({ "name": t, "freq": (tagCounts[t] / rows.length) })
        }
        root.tags = newTags
    }
}
