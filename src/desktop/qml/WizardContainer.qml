import QtQuick 2.5

FocusScope {
    id: root

    implicitWidth: 240
    implicitHeight: 150

    /*! The current tab index */
    property int currentIndex: 0

    /*! The current tab count */
    readonly property int count: __tabs.count

    /*!
        \qmlproperty Item TabView::contentItem
        \since QtQuick.Controls 1.3

        This property holds the content item of the tab view.

        Tabs declared as children of a TabView are automatically parented to the TabView's contentItem.
     */
    readonly property alias contentItem: stack

    /*! \internal */
    default property alias data: stack.data

    /*!
        \qmlmethod Tab TabView::addTab(string title, Component component)

        Adds a new tab with the given \a title and an optional \a component.

        Returns the newly added tab.
    */
    function addTab(title, component) {
        return insertTab(__tabs.count, title, component)
    }

    /*!
        \qmlmethod Tab TabView::insertTab(int index, string title, Component component)

        Inserts a new tab at \a index, with the given \a title and
        an optional \a component.

        Returns the newly added tab.
    */
    function insertTab(index, title, component) {
        var tab = tabcomp.createObject()
        tab.sourceComponent = component
        tab.title = title
        // insert at appropriate index first, then set the parent to
        // avoid onChildrenChanged appending it to the end of the list
        __tabs.insert(index, {tab: tab})
        tab.__inserted = true
        tab.parent = stack
        __didInsertIndex(index)
        __setOpacities()
        return tab
    }

    /*! \qmlmethod void TabView::removeTab(int index)
        Removes and destroys a tab at the given \a index. */
    function removeTab(index) {
        var tab = __tabs.get(index).tab
        __willRemoveIndex(index)
        __tabs.remove(index, 1)
        tab.sourceComponent = null
        tab.destroy()
        __setOpacities()
    }

    /*! \qmlmethod void TabView::moveTab(int from, int to)
        Moves a tab \a from index \a to another. */
    function moveTab(from, to) {
        __tabs.move(from, to, 1)

        if (currentIndex == from) {
            currentIndex = to
        } else {
            var start = Math.min(from, to)
            var end = Math.max(from, to)
            if (currentIndex >= start && currentIndex <= end) {
                if (from < to)
                    --currentIndex
                else
                    ++currentIndex
            }
        }
    }

    /*! \qmlmethod Tab TabView::getTab(int index)
        Returns the \l Tab item at \a index. */
    function getTab(index) {
        var data = __tabs.get(index)
        return data && data.tab
    }

    /*! \internal */
    property ListModel __tabs: ListModel { }

    onCurrentIndexChanged: __setOpacities()

    /*! \internal */
    function __willRemoveIndex(index) {
        // Make sure currentIndex will points to the same tab after the removal.
        // Also activate the next index if the current index is being removed,
        // except when it's both the current and last index.
        if (count > 1 && (currentIndex > index || currentIndex == count -1))
            --currentIndex
    }
    function __didInsertIndex(index) {
        // Make sure currentIndex points to the same tab as before the insertion.
        if (count > 1 && currentIndex >= index)
            currentIndex++
    }

    function __setOpacities() {
        for (var i = 0; i < __tabs.count; ++i) {
            var child = __tabs.get(i).tab
            child.visible = (i == currentIndex ? true : false)
        }
    }

    activeFocusOnTab: false

    Component {
        id: tabcomp
        WizardPage {}
    }

    Item {
        id: stack

        anchors.fill: parent

        property int frameWidth
        property bool completed: false

        Component.onCompleted: {
            addTabs(stack.children)
            completed = true
        }

        onChildrenChanged: {
            if (completed)
                stack.addTabs(stack.children)
        }

        function addTabs(tabs) {
            var tabAdded = false
            for (var i = 0 ; i < tabs.length ; ++i) {
                var tab = tabs[i]
                if (!tab.__inserted && tab.Accessible.role === Accessible.LayeredPane) {
                    tab.__inserted = true
                    // reparent tabs created dynamically by createObject(tabView)
                    tab.parent = stack
                    // a dynamically added tab should also get automatically removed when destructed
                    if (completed)
                        tab.Component.onDestruction.connect(stack.onDynamicTabDestroyed.bind(tab))
                    __tabs.append({tab: tab})
                    tabAdded = true
                }
            }
            if (tabAdded)
                __setOpacities()
        }

        function onDynamicTabDestroyed() {
            for (var i = 0; i < __tabs.count; ++i) {
                if (__tabs.get(i).tab === this) {
                    __willRemoveIndex(i)
                    __tabs.remove(i, 1)
                    __setOpacities()
                    break
                }
            }
        }
    }

    onChildrenChanged: stack.addTabs(root.children)
}
