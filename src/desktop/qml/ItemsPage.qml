import Mardy 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Item {
    id: root

    property alias model: uploadView.model
    property var sessionHandler: null
    property bool done: model.count > 0

    ColumnLayout {
        anchors.fill: parent
        visible: uploadView.count > 0

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn

            AdaptiveGridView {
                id: uploadView
                delegate: ImageDelegate {
                    width: uploadView.cellWidth
                    height: uploadView.cellHeight
                    source: model.url
                    caption: model.fileName
                    maxSize: uploadView.itemMaxSize
                }
            }
        }

        Label {
            Layout.fillWidth: true
            text: qsTr("Ready to upload %n elements", "", uploadView.count)
        }
    }

    DropArea {
        id: dropArea
        anchors.fill: parent
        keys: [ "text/uri-list" ]

        onDropped: FileUtils.findFiles(drop.urls, true,
                                       function(fileUrls, baseUrl) {
            root.model.addPhotos(fileUrls, baseUrl)
        })
    }

    Column {
        anchors.centerIn: parent
        width: parent.width * 3 / 4
        spacing: 12
        visible: uploadView.count == 0

        Label {
            id: dropLabel
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            wrapMode: Text.Wrap
            font.italic: true
            text: qsTr("Drag & drop the photos you wish to upload into this area")
        }

        Label {
            width: parent.width
            horizontalAlignment: Text.AlignHCenter
            text: qsTr("or")
            font.italic: true
            visible: sessionButton.visible
        }
        Button {
            id: sessionButton
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("Resume last session")
            visible: sessionHandler && sessionHandler.canResume
            onClicked: sessionHandler.resume()
        }
    }

    Registry {
        Component.onCompleted: start()
    }
}
