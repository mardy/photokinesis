import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.0
import QtQuick.Window 2.2

ColumnLayout {
    id: root

    property bool allowCancel: true
    property alias currentIndex: tabView.currentIndex
    default property alias data: tabView.data

    signal close()

    WizardContainer {
        id: tabView

        property bool alwaysAllowNext: Qt.application.arguments.indexOf("-next") >= 0
        property bool canBack: __history.length > 0
        property bool canNext: useProperty("done", true, false) || alwaysAllowNext
        property string nextButtonLabel: usePropertyIfTrue("nextButtonLabel", qsTr("Next"))
        property bool isLast: useProperty("isLast", currentIndex == count - 1)
        property Item currentItem: null

        Layout.fillWidth: true
        Layout.fillHeight: true
        implicitWidth: 600 * Screen.devicePixelRatio
        implicitHeight: 400 * Screen.devicePixelRatio

        property var __history: []

        onCurrentIndexChanged: updateCurrentItem()
        onCountChanged: updateCurrentItem()

        function useProperty(name, defaultValue, defaultUnloaded) {
            return currentItem ? (currentItem.hasOwnProperty(name) ? currentItem[name] : defaultValue) : defaultUnloaded
        }

        function usePropertyIfTrue(name, defaultValue, defaultUnloaded) {
            var value = useProperty(name, defaultValue, defaultUnloaded)
            return value ? value : defaultValue
        }

        function updateCurrentItem() {
            if (count > 0) {
                currentItem = getTab(currentIndex)
            } else {
                currentItem = null
            }
        }

        function jumpTo(index) {
            var t = __history
            __history.push(currentIndex)
            __history = t
            currentIndex = index
        }

        function back() {
            var t = __history
            currentIndex = t.pop()
            __history = t
        }

        function next() {
            var tab = getTab(currentIndex)
            if (tab.hasOwnProperty("onConfirmed")) {
                tab.onConfirmed()
            } else {
                if (isLast) {
                    root.close()
                } else {
                    jumpTo(currentIndex + 1)
                }
            }
        }
    }

    RowLayout {
        Layout.alignment: Qt.AlignRight

        Button {
            visible: root.allowCancel
            enabled: tabView.currentIndex == 0
            text: qsTr("Cancel")
            iconName: "cancel"
            onClicked: root.close()
        }

        Button {
            enabled: tabView.canBack
            text: qsTr("Previous")
            iconName: "back"
            onClicked: tabView.back()
        }

        Button {
            visible: !closeButton.visible
            enabled: tabView.canNext
            text: tabView.nextButtonLabel
            iconName: "next"
            onClicked: tabView.next()
        }

        Button {
            id: closeButton
            visible: tabView.isLast
            text: qsTr("Close")
            iconName: "dialog-ok"
            onClicked: root.close()
        }
    }

    function jumpTo(index) { tabView.jumpTo(index) }
    function removePage(index) { tabView.removeTab(index) }
    function insertPage(index, component) { tabView.insertTab(index, "", component) }
}
