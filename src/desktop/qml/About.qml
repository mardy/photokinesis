import Photokinesis.Ui 1.0
import QtQml 2.2
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Window 2.2

Popup {
    Rectangle {
        anchors.centerIn: parent
        width: Math.min(500 * Screen.devicePixelRatio, parent.width - 40)
        height: Math.min(400 * Screen.devicePixelRatio, parent.height - 30)
        color: palette.window
        radius: 20
        border { width: 1; color: palette.windowText }

        ScrollView {
            id: view
            anchors { fill: parent; margins: 20 }

            Column {
                width: view.width - 20
                spacing: 20

                Image {
                    anchors { left: parent.left; right: parent.right; margins: 40 }
                    source: "qrc:/icons/photokinesis_wide"
                    sourceSize.width: width
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    textFormat: Text.StyledText
                    text: qsTr("PhotoTeleport version %1<br/><font size=\"1\">Copyright ⓒ 2017-2022 mardy.it</font>").arg(Qt.application.version)
                    horizontalAlignment: Text.AlignHCenter
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    wrapMode: Text.WordWrap
                    text: qsTr("PhotoTeleport is free software, distributed under the <a href=\"https://www.gnu.org/licenses/gpl-3.0.en.html\">GPL-v3 license</a>. Among other things, this means that you can share it with your friends for free, or resell it for a fee.")
                    onLinkActivated: Qt.openUrlExternally(link)
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                    text: qsTr("If you got PhotoTeleport for free, please consider making a small donation to its author:<br/><ul><li>Paypal: <a href=\"https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=KQQGMR85AUVBE\">click here to donate a free amount</a></li><li><a href=\"https://money.yandex.ru/quickpay/shop-widget?writer=seller&targets=PhotoTeleport&targets-hint=&default-sum=&button-text=14&hint=&successURL=&quickpay=shop&account=410015419471966\">Yandex Money</a></li></ul>")
                    onLinkActivated: Qt.openUrlExternally(link)
                }

                Label {
                    anchors { left: parent.left; right: parent.right }
                    wrapMode: Text.WordWrap
                    textFormat: Text.StyledText
                    text: qsTr("FTP icon made by Freepik from Flaticon is licensed as CC 3.0 BY<br>SmugMug icon by <a href=\"http://uiconstock.com/\">uiconstock.com</a>")
                    font.pointSize: 8
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }
        }
    }

    SystemPalette { id: palette }
}
