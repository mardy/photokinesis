import Photokinesis 1.0
import SortFilterProxyModel 0.2
import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ApplicationWindow {
    id: root
    property var application: null

    visible: true

    contentItem {
        minimumWidth: 300; minimumHeight: 200
        implicitWidth: 800; implicitHeight: 600
    }

    menuBar: MenuBar {
        Menu {
            title: "Help"

            MenuItem {
                text: "Check for updates…"
                onTriggered: PopupUtils.open(Qt.resolvedUrl("UpdateDialog.qml"), root, {})
            }

            MenuSeparator {}

            MenuItem {
                text: "About"
                onTriggered: PopupUtils.open(Qt.resolvedUrl("About.qml"), root, {})
            }
        }
    }

    Wizard {
        id: wizard
        property int optionsPageIndex: -1
        property bool completed: false

        anchors { fill: parent; margins: 8 }
        allowCancel: false
        onClose: root.close()

        WizardPage {
            ItemsPage {
                model: uploadModel
                sessionHandler: sessionHandlerId
            }
        }

        WizardPage {
            UploadersPage {
                property bool _completed: false
                model: uploaderModel
                Component.onCompleted: {
                    selectedIndexes = selectedUploaders.selectedIndexes
                    _completed = true
                }
                onSelectedIndexesChanged: if (_completed) {
                    selectedUploaders.selectedIndexes = selectedIndexes
                }
            }
        }

        WizardPage {
            AuthenticationPage {
                model: selectedUploaders
            }
        }

        Component {
            id: itemOptionsPageComponent
            //WizardPage {
                ItemOptionsPage {
                    items: uploadModel
                    uploaders: selectedUploaders
                }
            //}
        }

        Component {
            id: itemOptionSummaryPageComponent
            //WizardPage {
                ItemOptionSummaryPage {
                    items: uploadModel
                    uploaders: selectedUploaders
                }
            //}
        }

        WizardPage {
            UploadPage {
                items: uploadModel
                uploaders: selectedUploaders
            }
        }

        WizardPage {
            ResultsPage {
                items: uploadModel
                uploaders: selectedUploaders
            }
        }

        Component.onCompleted: completed = true
        function setOptionPagesVisible(visible) {
            if (!completed) return
            console.log("Setting pages visible: " + visible)
            var currentlyVisibĺe = (optionsPageIndex >= 0)
            if (visible == currentlyVisibĺe) return
            if (!visible) {
                removePage(optionsPageIndex + 1)
                removePage(optionsPageIndex)
                optionsPageIndex = -1
            } else {
                optionsPageIndex = 3
                insertPage(optionsPageIndex, itemOptionsPageComponent)
                insertPage(optionsPageIndex + 1, itemOptionSummaryPageComponent)
            }
        }
    }

    Timer {
        interval: 50
        running: true
        onTriggered: {
            uploadModel.addPhotos(root.application.cmdLineItems)
            selectedUploaders.selectByName(root.application.cmdLineServices)
        }
    }

    UploadModel {
        id: uploadModel
        uploadersCount: uploaderModel.count
    }

    UploaderModel {
        id: uploaderModel
    }

    SortFilterProxyModel {
        id: selectedUploaders
        property var selectedIndexes: []
        property var supportedOptions: []
        sourceModel: uploaderModel
        filters: [
            IndexFilter {
                indexes: selectedUploaders.selectedIndexes
            }
        ]

        onCountChanged: buildOptions()

        function buildOptions() {
            var options = []
            for (var i = 0; i < count; i++) {
                var uploaderOptions = get(i, "supportedOptions")
                for (var j = 0; j < uploaderOptions.length; j++) {
                    var name = uploaderOptions[j]
                    if (options.indexOf(name) < 0) {
                        options.push(name)
                    }
                }
            }
            supportedOptions = options
            wizard.setOptionPagesVisible(supportedOptions.length > 0)
        }

        function hasOption(name) {
            return supportedOptions.indexOf(name) >= 0
        }

        function column(uploaderIndex) {
            return selectedIndexes[uploaderIndex] + 1
        }

        function uploaderIndex(column) {
            return selectedIndexes.indexOf(column - 1)
        }

        function selectByName(serviceNames) {
            var newSelection = []
            for (var i = 0; i < uploaderModel.count; i++) {
                var name = uploaderModel.get(i, "name")
                if (serviceNames.indexOf(name) >= 0) {
                    newSelection.push(i)
                }
            }
            selectedIndexes = newSelection
        }
    }

    SessionHandler {
        id: sessionHandlerId
        uploadModel: uploadModel
        uploaders: selectedUploaders
    }
}
