import QtQuick 2.2
import QtQuick.Controls 1.4

Item {
    id: root

    property alias source: thumbnail.source
    property string caption: ""
    property alias maxSize: thumbnail.maxSize

    Rectangle {
        anchors.fill: parent
        anchors.margins: 2
        color: "white"
        Thumbnail {
            id: thumbnail
            anchors.fill: parent
        }

        Text {
            anchors { bottom: parent.bottom; left: parent.left; right: parent.right }
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            elide: Text.ElideMiddle
            font.pixelSize: 12
            text: root.caption
            color: "white"

            Rectangle {
                anchors.fill: parent
                z: -1
                opacity: 0.3
                color: "black"
            }
        }

        Label {
            anchors.fill: parent
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
            text: qsTr("Loading...")
            visible: thumbnail.status == Image.Loading
        }
    }
}

