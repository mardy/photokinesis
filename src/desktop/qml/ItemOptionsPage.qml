import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root

    property var items: null
    property var uploaders: null

    Label {
        Layout.fillWidth: true
        wrapMode: Text.Wrap
        font.italic: true
        text: qsTr("Here you can set title, description and other options for each of the items being uploaded.")
    }

    SplitView {
        Layout.fillWidth: true
        Layout.fillHeight: true
        orientation: Qt.Vertical

        ScrollView {
            Layout.fillHeight: true
            verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn

            AdaptiveGridView {
                id: itemView

                property list<Action> actions: [
                    Action {
                        text: qsTr("Select all")
                        shortcut: StandardKey.SelectAll
                        onTriggered: itemView.selectAll()
                        enabled: itemView.selectedIndexes.length < itemView.count
                    },
                    Action {
                        text: qsTr("Select none")
                        shortcut: StandardKey.SelectNone
                        onTriggered: itemView.clearSelection()
                        enabled: itemView.selectedIndexes.length > 0
                    }
                ]
                property var selectedIndexes: []
                property int __previousIndex: -1
                property var __modifiers: 0

                focus: true
                keyNavigationEnabled: true
                model: root.items
                delegate: ImageDelegate {
                    width: itemView.cellWidth
                    height: itemView.cellHeight
                    source: model.url
                    caption: model.fileName
                    maxSize: itemView.itemMaxSize

                    Rectangle {
                        anchors.fill: parent
                        border { width: parent.activeFocus ? 1 : 0; color: "dimgrey" }
                        z: -1
                        color: itemView.isSelected(index) ? palette.highlight : "transparent"
                    }
                    MouseArea {
                        anchors.fill: parent
                        onClicked: itemView.select(index, mouse.modifiers)
                    }
                }

                Keys.onPressed: {
                    __modifiers = event.modifiers
                    if (count > 0) {
                        if (event.key == Qt.Key_Home) currentIndex = 0
                        else if (event.key == Qt.Key_End) currentIndex = count -1
                    }
                }
                Keys.onReleased: __modifiers = event.modifiers

                onCurrentIndexChanged: if (currentIndex != __previousIndex) {
                    select(currentIndex, __modifiers)
                }

                function isSelected(index) {
                    return selectedIndexes.indexOf(index) >= 0
                }

                function select(index, modifiers) {
                    if (index < 0 || index >= model.rowCount()) return
                    var s = selectedIndexes
                    if (modifiers & Qt.ShiftModifier) {
                        forCurrentToIndex(index, function(i) {
                            if (s.indexOf(i) < 0) s.push(i)
                        })
                    } else if (modifiers & Qt.ControlModifier) {
                        toggleSelection(index)
                    } else {
                        s = [ index ]
                    }
                    selectedIndexes = s
                    __previousIndex = index
                }

                function forCurrentToIndex(index, callback) {
                    if (index > __previousIndex) {
                        for (var i = __previousIndex; i <= index; i++) {
                            callback(i)
                        }
                    } else {
                        for (var i = index; i <= __previousIndex; i++) {
                            callback(i)
                        }
                    }
                }

                function toggleSelection(index) {
                    var i = selectedIndexes.indexOf(index)
                    var newList = selectedIndexes
                    if (i < 0) {
                        newList.push(index)
                    } else {
                        newList.splice(i, 1)
                    }
                    selectedIndexes = newList
                }

                function selectAll() {
                    var all = Array.apply(0, Array(count)).map(function(e, i) {
                        return i;
                    });
                    selectedIndexes = all
                }

                function clearSelection() {
                    selectedIndexes = []
                }
            }
        }

        ScrollView {
            id: optionsScrollView
            height: optionsView.implicitHeight
            horizontalScrollBarPolicy: Qt.ScrollBarAlwaysOff
            // Workaround for https://bugreports.qt.io/browse/QTBUG-64596
            activeFocusOnTab: false

            ItemOptionsPanel {
                id: optionsView

                width: optionsScrollView.flickableItem.width - 1
                itemModel: root.items
                uploadersModel: root.uploaders
                rows: itemView.selectedIndexes
                actions: itemView.actions
            }
        }
    }

    SystemPalette { id: palette }
}
