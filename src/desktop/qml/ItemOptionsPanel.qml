import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2
import Photokinesis 1.0

Item {
    id: root

    property var uploadersModel: null
    property var itemModel: null
    property var rows: []
    property var actions: []

    implicitHeight: columnLayout.height + 16
    implicitWidth: columnLayout.implicitWidth + 100

    ColumnLayout {
        id: columnLayout
        anchors { top: parent.top; left: parent.left; right: parent.right; margins: 8 }
        spacing: 12

        RowLayout {
            Layout.fillWidth: true

            Label {
                Layout.fillWidth: true
                wrapMode: Text.Wrap
                text: root.rows.length > 0 ?
                    qsTr("Operating on the %n selected item(s)", "", root.rows.length) :
                    qsTr("No item selected. Click on the photos above to select them and edit their metadata.")
            }

            Repeater {
                model: root.actions
                Button {
                    action: modelData
                }
            }
        }

        ItemOptionAggregate {
            Layout.fillWidth: true
            label: qsTr("Title:")
            model: itemModel; rows: root.rows; name: "title"
            uploaders: uploadersModel
            TextField {
                id: titleField
                property alias value: titleField.text
                placeholderText: qsTr("Enter title")
                verticalAlignment: Text.AlignVCenter
                focus: true
            }
        }

        ItemOptionAggregate {
            Layout.fillWidth: true
            label: qsTr("Description:")
            model: itemModel; rows: root.rows; name: "description"
            uploaders: uploadersModel
            TextArea {
                id: descriptionField
                property alias value: descriptionField.text
                implicitHeight: Math.min(Math.max(64, contentHeight), 128)
                wrapMode: Text.Wrap
            }
        }

        ItemOptionArray {
            Layout.fillWidth: true
            label: qsTr("Tags:")
            model: itemModel; rows: root.rows; name: "tags"
            uploaders: uploadersModel
            TagControl {
                id: tagsField
                property alias value: tagsField.rows
            }
        }
    }
}
