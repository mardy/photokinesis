import Photokinesis 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4

ProgressBar {
    id: root

    property var items: null
    property var uploaders: null

    minimumValue: 0
    maximumValue: items.count * uploaders.count

    Connections {
        target: root.items
        onDataChanged: root.update()
    }

    function update() {
        var progress = 0
        for (var u = 0; u < root.uploaders.count; u++) {
            var column = root.uploaders.column(u)
            for (var i = 0; i < root.items.count; i++) {
                var uploadStatus = root.items.get(i, column, "uploadStatus")
                if (uploadStatus == UploadModel.Uploaded ||
                    uploadStatus == UploadModel.Failed) {
                    progress += 1.0
                } else if (uploadStatus == UploadModel.Uploading) {
                    var p = root.items.get(i, column, "progress")
                    progress += (p / 100.0)
                }
            }
        }
        root.value = progress
    }
}
