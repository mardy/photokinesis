import QtQuick 2.2

ItemOption {
    id: root

    function computeValue(column) {
        var value;
        for (var i = 0; i < root.rows.length; i++) {
            var v = root.model.get(root.rows[i], column, root.name)
            if (i == 0) {
                value = v
            } else if (JSON.stringify(v) != JSON.stringify(value)) {
                return null
            }
        }
        return value
    }

    function storeValue(column, value) {
        for (var i = 0; i < root.rows.length; i++) {
            root.model.set(root.rows[i], column, root.name, value)
        }
    }

    function valueIsSet(value) {
        return value !== undefined
    }
}
