import ListProxyModel 0.1
import Photokinesis 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root

    property var items: null
    property var uploaders: null
    property bool done: false

    states: [
        State {
            name: "base"
        },
        State {
            name: "busy"
            when: executor.status == Executor.Busy
            PropertyChanges { target: progressRow; visible: true }
        },
        State {
            name: "pausing"
            when: executor.status == Executor.Pausing
            extend: "busy"
            PropertyChanges { target: progressLabel; text: qsTr("Pausing…") }
            PropertyChanges { target: pauseButton; enabled: false }
        },
        State {
            name: "paused"
            when: executor.status == Executor.Paused
            extend: "busy"
            PropertyChanges { target: progressLabel; text: qsTr("Paused") }
            PropertyChanges { target: pauseButton; text: qsTr("Resume") }
        },
        State {
            name: "finished"
            when: executor.status == Executor.Finished
            extend: "busy"
            PropertyChanges { target: progressLabel; text: qsTr("Upload completed") }
            PropertyChanges { target: progressBar; visible: false }
            PropertyChanges { target: pauseButton; visible: false }
        }
    ]
    onVisibleChanged: if (visible) resetFailures()
    Component.onCompleted: resetFailures()

    RowLayout {
        Layout.fillWidth: true

        Label {
            Layout.fillWidth: true
            wrapMode: Text.Wrap
            font.italic: true
            text: qsTr("The items are ready to be uploaded. Press the '%1' button to start the upload process.").arg(startButton.text)
        }

        Button {
            id: startButton
            text: qsTr("Start upload")
            enabled: executor.status == Executor.Idle
            onClicked: executor.start()
        }
    }

    ScrollView {
        Layout.fillWidth: true
        Layout.fillHeight: true
        verticalScrollBarPolicy: Qt.ScrollBarAlwaysOn

        AdaptiveGridView {
            id: itemView
            model: root.items
            delegate: ImageDelegate {
                width: itemView.cellWidth
                height: itemView.cellHeight
                source: model.url
                caption: model.fileName
                maxSize: itemView.itemMaxSize
                Component.onCompleted: {
                    for (var i = 0; i < outerInstantiator.count; i++) {
                        var inner = outerInstantiator.objectAt(i)
                        inner.objectAt(index).parent = flow
                    }
                }
                Flow {
                    id: flow
                    anchors { left: parent.left; right: parent.right; bottom: parent.bottom }
                    spacing: 8
                    layoutDirection: Qt.RightToLeft
                }
            }
        }
    }

    RowLayout {
        id: progressRow
        Layout.fillWidth: true
        visible: false
        Label {
            id: progressLabel
            Layout.minimumWidth: 150
            text: qsTr("Upload progress:")
        }
        UploadProgress {
            id: progressBar
            Layout.fillWidth: true
            items: root.items
            uploaders: root.uploaders
        }
        Button {
            id: pauseButton
            Layout.minimumWidth: 120
            text: qsTr("Pause")
            onClicked: executor.pauseOrResume()
        }
    }

    Instantiator {
        id: outerInstantiator
        model: root.uploaders
        delegate: QtObject {
            id: inner
            property var name: model.display
            property var icon: model.icon
            property int column: root.uploaders.column(index)
            property int row: -1

            property var filtered: ListProxyModel {
                sourceModel: root.items
                filterColumn: inner.column
            }

            property var internal: Instantiator {
                id: innerInstantiator
                model: inner.filtered
                delegate: UploaderStatusIcon {
                    width: 32
                    source: "image://rounded/15/" + inner.icon
                    progress: model.progress
                    uploadStatus: model.uploadStatus
                }
            }
            property var conn: Connections {
                target: model.uploader
                onProgress: {
                    root.items.set(inner.row, inner.column, "progress", progress)
                }
                onDone: {
                    var uploadStatus = ok ? UploadModel.Uploaded : UploadModel.Failed
                    root.items.set(inner.row, inner.column, "uploadStatus", uploadStatus)
                }
            }

            function objectAt(index) { return innerInstantiator.objectAt(index) }

            function startUpload(row) {
                var column = inner.column
                var url = root.items.get(row, column, "url")
                var config = root.items.get(row, column, "config")
                inner.row = row
                model.uploader.uploadFile(url, config)
            }
        }
    }

    Executor {
        id: executor
        model: root.items
        activeUploaders: root.uploaders.selectedIndexes
        onSlotReady: {
            var uploader = outerInstantiator.objectAt(uploaderIndex)
            uploader.startUpload(row)
        }
        onFinished: root.done = true
    }

    function resetFailures() {
        items.resetFailures()
    }
}
