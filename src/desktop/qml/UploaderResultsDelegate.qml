import Photokinesis 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

Rectangle {
    id: root

    property string displayName: ""
    property alias icon: image.source
    property var uploader: null
    property int uploads: 0
    property int failures: 0

    height: mainLayout.height
    color: palette.window
    border { color: palette.shadow; width: 2 }

    RowLayout {
        id: mainLayout
        anchors { left: parent.left; right: parent.right }

        ColumnLayout {
            Layout.minimumWidth: parent.width * 2 / 5
            Layout.fillHeight: true
            Layout.margins: 12

            Item {
                Layout.fillWidth: true
                height: 80
                Image {
                    id: image
                    anchors {
                        left: parent.left; top: parent.top; bottom: parent.bottom
                        margins: 8
                    }
                    width: height
                    sourceSize { width: width; height: height }
                    fillMode: Image.PreserveAspectFit
                    smooth: true
                }

                Text {
                    anchors {
                        left: image.right; right: parent.right
                        top: parent.top; bottom: parent.bottom
                        margins: 8
                    }
                    text: root.displayName
                    verticalAlignment: Text.AlignVCenter
                }
            }

            Text {
                Layout.fillWidth: true
                Layout.fillHeight: true
                text: qsTr("Logged in as %1").arg(uploader.userName)
            }
        }

        ColumnLayout {
            Layout.fillWidth: true
            Layout.fillHeight: true
            Layout.margins: 12

            Label {
                Layout.fillWidth: true
                text: qsTr("Uploaded %n item(s), ", "", root.uploads) +
                    qsTr("%n failure(s)", "", root.failures)
            }

            Label {
                Layout.fillWidth: true
                visible: uploader.uploadsUrl.toString().length > 0
                text: qsTr("Click <a href=\"%1\">here</a> to view the items in %2")
                    .arg(uploader.uploadsUrl).arg(root.displayName)
                onLinkActivated: Qt.openUrlExternally(link)
            }
        }
    }

    SystemPalette { id: palette }
}
