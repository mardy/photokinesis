import Photokinesis 1.0
import QtQml 2.2

Session {
    id: root

    property var uploadModel: null
    property var uploaders: null
    property bool canResume: !sessionIsFinished()

    property var _lastSessionData: null
    property var _timer: Timer {
        id: timer
        interval: 1000
        onTriggered: root.collectAndStore()
    }

    property var _conn: Connections {
        target: root.uploadModel
        onConfigChanged: timer.restart()
    }

    property var _conn2: Connections {
        target: root.uploaders
        onSelectedIndexesChanged: if (root.uploadModel.count > 0) {
            timer.restart()
        }
    }

    function resume() {
        var currentUploaders = getUploaderNames(root.uploaders.sourceModel)
        var data = loadLastSession(currentUploaders, uploadModel)
        setUploaders(data["uploaders"])
    }

    function collectAndStore() {
        storeSession(getUploaders(root.uploaders), uploadModel)
    }

    function getUploaderNames(uploaders) {
        var data = []

        for (var i = 0; i < uploaders.count; i++) {
            var name = uploaders.get(i, "name")
            data.push(name)
        }

        return data
    }

    function getUploaders(selectedUploaders) {
        var data = []

        var uploaders = selectedUploaders.sourceModel
        for (var i = 0; i < uploaders.count; i++) {
            var name = uploaders.get(i, "name")
            var selected = selectedUploaders.selectedIndexes.indexOf(i) >= 0
            data.push({
                "name": name,
                "selected": selected
            })
        }

        return data
    }

    function setUploaders(data) {
        var selectedIndexes = []
        for (var i = 0; i < data.length; i++) {
            var uploaderData = data[i]
            var index = uploaderData["index"] - 1
            if (uploaders.sourceModel.get(i, "name") != uploaderData.name) {
                console.warn("Uploader index mismatch, ignoring!")
                continue
            }
            if (uploaderData.selected) {
                selectedIndexes.push(i)
            }
        }
        uploaders.selectedIndexes = selectedIndexes
    }
}
