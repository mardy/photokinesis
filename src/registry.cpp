/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "registry.h"

#include <QCoreApplication>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QLocale>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QSysInfo>
#include <QUrl>
#include <QUrlQuery>
#include <QtQml>

using namespace Mardy;

namespace Mardy {

class RegistryPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Registry)

public:
    RegistryPrivate(Registry *q);

    void setStatus(Registry::Status status);

    void start();

private:
    Registry::Status m_status;
    QString m_message;
    bool m_isAllowed;
    Registry *q_ptr;
};

} // namespace

RegistryPrivate::RegistryPrivate(Registry *q):
    QObject(),
    m_status(Registry::Idle),
    m_isAllowed(true),
    q_ptr(q)
{
}

void RegistryPrivate::setStatus(Registry::Status status)
{
    Q_Q(Registry);
    if (status != m_status) {
        m_status = status;
        Q_EMIT q->statusChanged();
    }
}

void RegistryPrivate::start()
{
    Q_Q(Registry);
    setStatus(Registry::Busy);

    QQmlEngine *engine = qmlEngine(q);
    Q_ASSERT(engine);

    QNetworkAccessManager *nam = engine->networkAccessManager();
    Q_ASSERT(nam);

    QUrl url("https://www.mardy.it/app-registry.py");
    QUrlQuery query;
    query.addQueryItem("appName", QCoreApplication::applicationName());
    query.addQueryItem("appVersion", QCoreApplication::applicationVersion());
    query.addQueryItem("osType", QSysInfo::productType());
    query.addQueryItem("osVersion", QSysInfo::productVersion());
    query.addQueryItem("arch", QSysInfo::currentCpuArchitecture());
    query.addQueryItem("locale", QLocale::system().name());
    url.setQuery(query);
    QNetworkRequest req(url);

    QNetworkReply *reply = nam->get(req);
    QObject::connect(reply, &QNetworkReply::finished,
                     this, [this, q, reply]() {
        reply->deleteLater();

        QJsonDocument json = QJsonDocument::fromJson(reply->readAll());
        QJsonObject o = json.object();
        m_message = o["message"].toString();
        if (o["status"].toString() == "ok") {
            m_isAllowed = o["allowed"].toBool();
            setStatus(Registry::Succeeded);
        } else {
            setStatus(Registry::Failed);
        }
        Q_EMIT q->finished();
    });
}

Registry::Registry(QObject *parent):
    QObject(parent),
    d_ptr(new RegistryPrivate(this))
{
}

Registry::~Registry()
{
}

Registry::Status Registry::status() const
{
    Q_D(const Registry);
    return d->m_status;
}

QString Registry::message() const
{
    Q_D(const Registry);
    return d->m_message;
}

bool Registry::isAllowed() const
{
    Q_D(const Registry);
    return d->m_isAllowed;
}

void Registry::start()
{
    Q_D(Registry);
    d->start();
}

#include "registry.moc"
