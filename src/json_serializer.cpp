/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "json_serializer.h"

#include <QDataStream>
#include <QDebug>
#include <QIODevice>
#include <QVariantMap>

using namespace Photokinesis;

bool JsonSerializer::dump(QIODevice *device, const QJsonObject &json)
{
    QDataStream out(device);
    out.setVersion(QDataStream::Qt_5_6);
    out << json.toVariantMap();
    return out.status() == QDataStream::Ok;
}

QJsonObject JsonSerializer::loadObject(QIODevice *device)
{
    QDataStream in(device);
    in.setVersion(QDataStream::Qt_5_6);

    QVariantMap map;
    in >> map;
    return QJsonObject::fromVariantMap(map);
}

bool JsonSerializer::dump(QIODevice *device, const QJsonArray &json)
{
    QDataStream out(device);
    out.setVersion(QDataStream::Qt_5_6);
    out << json.toVariantList();
    return out.status() == QDataStream::Ok;
}

QJsonArray JsonSerializer::loadArray(QIODevice *device)
{
    QDataStream in(device);
    in.setVersion(QDataStream::Qt_5_6);

    QVariantList list;
    in >> list;
    return QJsonArray::fromVariantList(list);
}
