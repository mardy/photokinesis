/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "executor.h"

#include <Photokinesis/UploadModel>
#include <QDebug>

using namespace Photokinesis;

namespace Photokinesis {

class ExecutorPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Executor)

public:
    ExecutorPrivate(Executor *q);

    void setStatus(Executor::Status status);

    void start();
    void pauseOrResume();

    void resetUploading();
    void clearCompleted();
    void update();

    int column(int uploaderIndex) const {
        return m_activeUploaders[uploaderIndex] + 1;
    }

    int uploaderIndex(int column) const {
        return m_activeUploaders.indexOf(column - 1);
    }

private:
    UploadModel *m_model;
    QMetaObject::Connection m_dataChangedConnection;
    Executor::Status m_status;
    int m_maxJobs;
    int m_maxJobsPerUploader;
    QVector<int> m_activeUploaders;
    QVector<QPair<int,int>> m_currentUploads; // row and uploaderIndex
    Executor *q_ptr;
};

} // namespace

ExecutorPrivate::ExecutorPrivate(Executor *q):
    QObject(),
    m_model(0),
    m_status(Executor::Idle),
    m_maxJobs(2),
    m_maxJobsPerUploader(1),
    q_ptr(q)
{
}

void ExecutorPrivate::setStatus(Executor::Status status)
{
    Q_Q(Executor);
    if (status != m_status) {
        m_status = status;
        Q_EMIT q->statusChanged();
        if (status == Executor::Finished) {
            Q_EMIT q->finished();
        }
    }
}

void ExecutorPrivate::start()
{
    if (Q_UNLIKELY(!m_model)) return;
    if (Q_UNLIKELY(!m_currentUploads.isEmpty())) {
        qWarning() << "Executor busy";
        return;
    }

    if (Q_UNLIKELY(m_activeUploaders.isEmpty())) {
        qWarning() << "No uploaders available";
        return;
    }

    if (!m_dataChangedConnection) {
        m_dataChangedConnection =
            QObject::connect(m_model, &QAbstractItemModel::dataChanged,
                             this, &ExecutorPrivate::update);
    }
    setStatus(Executor::Busy);
    resetUploading();
    update();
}

void ExecutorPrivate::pauseOrResume()
{
    if (m_status == Executor::Paused) {
        setStatus(Executor::Busy);
        update();
    } else if (m_status == Executor::Busy) {
        setStatus(Executor::Pausing);
    } else {
        qWarning() << "Cannot pause or resume in state" << m_status;
    }
}

void ExecutorPrivate::resetUploading()
{
    int uploadersCount = m_activeUploaders.count();
    int totalRows = m_model->rowCount();
    for (int row = 0; row < totalRows; row++) {
        for (int i = 0; i < uploadersCount; i++) {
            QModelIndex index = m_model->index(row, column(i));
            QVariant vStatus = index.data(UploadModel::UploadStatusRole);
            UploadModel::UploadStatus status =
                vStatus.value<UploadModel::UploadStatus>();
            if (status == UploadModel::Uploading) {
                m_model->setData(index, UploadModel::Ready, UploadModel::UploadStatusRole);
            }
        }
    }
}

void ExecutorPrivate::clearCompleted()
{
    auto i = m_currentUploads.begin();
    while (i != m_currentUploads.end()) {
        QModelIndex index = m_model->index(i->first, column(i->second));
        QVariant vStatus =
            m_model->data(index, UploadModel::UploadStatusRole);
        UploadModel::UploadStatus status =
            vStatus.value<UploadModel::UploadStatus>();
        if (status >= UploadModel::Uploaded)
            i = m_currentUploads.erase(i);
        else
            i++;
    }
}

void ExecutorPrivate::update()
{
    Q_Q(Executor);

    int totalRows = m_model->rowCount();
    int startRow = m_currentUploads.isEmpty() ? 0 : m_currentUploads[0].first;

    clearCompleted();

    if (m_status == Executor::Finished) {
        setStatus(Executor::Idle);
        return;
    }
    if (m_status == Executor::Pausing) {
        if (m_currentUploads.isEmpty()) {
            setStatus(Executor::Paused);
        }
        return;
    }
    if (m_status == Executor::Paused || m_status == Executor::Idle) {
        return;
    }

    int availableJobs = m_maxJobs - m_currentUploads.count();
    if (availableJobs <= 0) return;

    /* First, count how many slots we have for each uploader */
    int uploadersCount = m_activeUploaders.count();
    QVector<int> availableJobsPerUploader(uploadersCount,
                                          m_maxJobsPerUploader);
    for (const QPair<int,int> &upload: m_currentUploads) {
        availableJobsPerUploader[upload.second]--;
    }

    for (int row = startRow; row < totalRows; row++) {
        for (int i = 0; i < uploadersCount; i++) {
            if (availableJobsPerUploader[i] <= 0) continue;

            QModelIndex index = m_model->index(row, column(i));
            QVariant vStatus = index.data(UploadModel::UploadStatusRole);
            UploadModel::UploadStatus status =
                vStatus.value<UploadModel::UploadStatus>();
            if (status == UploadModel::Ready) {
                m_currentUploads.append({ row, i });
                Q_EMIT q->slotReady(row, i);
                availableJobs--;
                availableJobsPerUploader[i]--;
                if (availableJobs <= 0) return;
            }
        }
    }

    /* We arrive here only if there are no more items in Ready state */
    if (m_currentUploads.isEmpty()) {
        setStatus(Executor::Finished);
    }
}

Executor::Executor(QObject *parent):
    QObject(parent),
    d_ptr(new ExecutorPrivate(this))
{
}

Executor::~Executor()
{
}

void Executor::setModel(UploadModel *model)
{
    Q_D(Executor);
    if (d->m_model == model) return;
    d->m_model = model;
    Q_EMIT modelChanged();
}

UploadModel *Executor::model() const
{
    Q_D(const Executor);
    return d->m_model;
}

Executor::Status Executor::status() const
{
    Q_D(const Executor);
    return d->m_status;
}

void Executor::setMaxJobs(int maxJobs)
{
    Q_D(Executor);
    if (d->m_maxJobs == maxJobs) return;
    d->m_maxJobs = maxJobs;
    Q_EMIT maxJobsChanged();
}

int Executor::maxJobs() const
{
    Q_D(const Executor);
    return d->m_maxJobs;
}

void Executor::setMaxJobsPerUploader(int maxJobsPerUploader)
{
    Q_D(Executor);
    if (d->m_maxJobsPerUploader == maxJobsPerUploader) return;
    d->m_maxJobsPerUploader = maxJobsPerUploader;
    Q_EMIT maxJobsPerUploaderChanged();
}

int Executor::maxJobsPerUploader() const
{
    Q_D(const Executor);
    return d->m_maxJobsPerUploader;
}

void Executor::setActiveUploaders(const QVariantList &activeUploaders)
{
    Q_D(Executor);
    QVector<int> asVector(activeUploaders.count());
    int i = 0;
    for (const QVariant &v: activeUploaders) {
        asVector[i++] = v.toInt();
    }
    if (d->m_activeUploaders == asVector) return;
    d->m_activeUploaders = asVector;
    Q_EMIT activeUploadersChanged();
}

QVariantList Executor::activeUploaders() const
{
    Q_D(const Executor);
    QVariantList asList;
    for (int i: d->m_activeUploaders) {
        asList.append(i);
    }
    return asList;
}

void Executor::start()
{
    Q_D(Executor);
    d->start();
}

void Executor::pauseOrResume()
{
    Q_D(Executor);
    d->pauseOrResume();
}

#include "executor.moc"
