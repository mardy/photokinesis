/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_UPLOAD_MODEL_SESSION_H
#define PHOTOKINESIS_UPLOAD_MODEL_SESSION_H

#include <QScopedPointer>
#include <QString>
#include <QStringList>

class QIODevice;

namespace Photokinesis {

class UploadModel;

class UploadModelSessionPrivate;
class UploadModelSession
{
public:
    UploadModelSession();
    virtual ~UploadModelSession();

    void setUploadModel(UploadModel *model);

    bool dumpSession(QIODevice *device, const QStringList &uploaders) const;
    bool loadSession(QIODevice *device, const QStringList &uploaders);
    bool sessionIsFinished(QIODevice *device,
                           const QStringList &selectedUploaders);

private:
    QScopedPointer<UploadModelSessionPrivate> d_ptr;
    Q_DECLARE_PRIVATE(UploadModelSession)
};

} // namespace

#endif // PHOTOKINESIS_UPLOAD_MODEL_SESSION_H
