include(../common-config.pri)

TARGET = Photokinesis

CONFIG += \
    link_pkgconfig \
    qt

QT += \
    concurrent \
    quick \
    webview

PKGCONFIG += \

LIBS += -lPhotokinesis
QMAKE_LIBDIR += $${TOP_BUILD_DIR}/lib/Photokinesis
!win32 {
    QMAKE_LFLAGS += '-Wl,-rpath,\'\$$ORIGIN/../lib\''
}
INCLUDEPATH += \
    $${TOP_SRC_DIR}/lib

SOURCES += \
    application.cpp \
    executor.cpp \
    file_utils.cpp \
    json_serializer.cpp \
    main.cpp \
    registry.cpp \
    rounded_image_provider.cpp \
    session.cpp \
    updater.cpp \
    upload_model_session.cpp \
    uploader_model.cpp

HEADERS += \
    application.h \
    executor.h \
    file_utils.h \
    json_serializer.h \
    registry.h \
    rounded_image_provider.h \
    session.h \
    updater.h \
    upload_model_session.h \
    uploader_model.h

include(SortFilterProxyModel/SortFilterProxyModel.pri)
include(ListProxyModel/ListProxyModel.pri)

DEFINES += \
    APPLICATION_DISPLAY_NAME=\\\"$${APPLICATION_DISPLAY_NAME}\\\" \
    APPLICATION_NAME=\\\"$${APPLICATION_NAME}\\\" \
    APPLICATION_VERSION=\\\"$${PROJECT_VERSION}\\\" \
    DATA_DIR=\\\"$${DATA_DIR}\\\"

target.path = $${INSTALL_BIN_DIR}
INSTALLS += target

CONFIG(desktop) {
    DEFINES += DESKTOP_BUILD

    QT += widgets # for styling

    QMAKE_SUBSTITUTES += $${TOP_SRC_DIR}/data/desktop/photokinesis.desktop.in
    desktop.files = $${TOP_BUILD_DIR}/data/desktop/photokinesis.desktop
    desktop.path = $${INSTALL_DESKTOP_DIR}
    INSTALLS += desktop

    RESOURCES += \
        $${TOP_SRC_DIR}/data/desktop/icons/icons-desktop.qrc \
        desktop/qml/ui.qrc
}

CONFIG(appimage) {
    DEFINES += APPIMAGE

    appimage_script.path = $${INSTALL_BIN_DIR}
    appimage_script.files = desktop-integration.sh
    INSTALLS += appimage_script
}

macx {
    TARGET = PhotoTeleport
    QMAKE_SUBSTITUTES += $${TOP_SRC_DIR}/data/PhotokinesisInfo.plist.in
    QMAKE_INFO_PLIST = $${TOP_BUILD_DIR}/data/PhotokinesisInfo.plist
    QT += svg # so that macdeployqt copies the svg plugin
}

win32 {
    RC_FILE = $${TOP_SRC_DIR}/data/windows/photokinesis.rc
}

RESOURCES += \
    $${TOP_SRC_DIR}/data/icons/icons.qrc
