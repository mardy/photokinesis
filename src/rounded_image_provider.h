/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_ROUNDED_IMAGE_PROVIDER_H
#define PHOTOKINESIS_ROUNDED_IMAGE_PROVIDER_H

#include <QQuickImageProvider>
#include <QScopedPointer>

namespace Photokinesis {

class RoundedImageProviderPrivate;
class RoundedImageProvider: public QQuickImageProvider
{
public:
    RoundedImageProvider();
    ~RoundedImageProvider();

    QImage requestImage(const QString &id,
                        QSize *size,
                        const QSize &requestedSize) override;

private:
    QScopedPointer<RoundedImageProviderPrivate> d_ptr;
    Q_DECLARE_PRIVATE(RoundedImageProvider)
};

} // namespace

#endif // PHOTOKINESIS_ROUNDED_IMAGE_PROVIDER_H
