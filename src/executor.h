/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_EXECUTOR_H
#define PHOTOKINESIS_EXECUTOR_H

#include <Photokinesis/UploadModel>
#include <QObject>

namespace Photokinesis {

class ExecutorPrivate;
class Executor: public QObject
{
    Q_OBJECT
    Q_PROPERTY(UploadModel *model READ model WRITE setModel \
               NOTIFY modelChanged)
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(int maxJobs READ maxJobs WRITE setMaxJobs \
               NOTIFY maxJobsChanged)
    Q_PROPERTY(int maxJobsPerUploader READ maxJobsPerUploader \
               WRITE setMaxJobsPerUploader NOTIFY maxJobsPerUploaderChanged)
    Q_PROPERTY(QVariantList activeUploaders READ activeUploaders \
               WRITE setActiveUploaders NOTIFY activeUploadersChanged)

public:
    enum Status {
        Idle = 0,
        Busy,
        Pausing,
        Paused,
        Finished,
    };
    Q_ENUM(Status)

    Executor(QObject *parent = 0);
    ~Executor();

    void setModel(UploadModel *model);
    UploadModel *model() const;

    Status status() const;

    void setMaxJobs(int maxJobs);
    int maxJobs() const;

    void setMaxJobsPerUploader(int maxJobsPerUploader);
    int maxJobsPerUploader() const;

    void setActiveUploaders(const QVariantList &activeUploaders);
    QVariantList activeUploaders() const;

    Q_INVOKABLE void start();
    Q_INVOKABLE void pauseOrResume();

Q_SIGNALS:
    void modelChanged();
    void statusChanged();
    void maxJobsChanged();
    void maxJobsPerUploaderChanged();
    void activeUploadersChanged();

    void slotReady(int row, int uploaderIndex);
    void finished();

private:
    QScopedPointer<ExecutorPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Executor)
};

} // namespace

#endif // PHOTOKINESIS_EXECUTOR_H
