/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MARDY_REGISTRY_H
#define MARDY_REGISTRY_H

#include <QObject>

namespace Mardy {

class RegistryPrivate;
class Registry: public QObject
{
    Q_OBJECT
    Q_PROPERTY(Status status READ status NOTIFY statusChanged)
    Q_PROPERTY(QString message READ message NOTIFY finished)
    Q_PROPERTY(bool allowed READ isAllowed NOTIFY finished)

public:
    enum Status {
        Idle = 0,
        Busy,
        Succeeded,
        Failed,
    };
    Q_ENUM(Status)

    Registry(QObject *parent = 0);
    ~Registry();

    Status status() const;

    QString message() const;
    bool isAllowed() const;

    Q_INVOKABLE void start();

Q_SIGNALS:
    void statusChanged();

    void finished();

private:
    QScopedPointer<RegistryPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Registry)
};

} // namespace

#endif // MARDY_REGISTRY_H
