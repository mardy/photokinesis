/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "file_utils.h"

#include <QAtomicInt>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QFutureWatcher>
#include <QJSEngine>
#include <QJSValue>
#include <QtConcurrent>
#include <functional>

using namespace Mardy;

namespace Mardy {

class FileUtilsPrivate {
    Q_DECLARE_PUBLIC(FileUtils)

public:
    FileUtilsPrivate(FileUtils *q);

    typedef std::function<void(const QList<QUrl> &urls)> FindFilesCallback;
    void findFiles(const QUrl &dirUrl, bool recursive,
                   const FindFilesCallback &callback);
    void findFiles(const QList<QUrl> &urls, bool recursive,
                   const QJSValue &callback);

    void reportFoundFiles(QAtomicInt *pendingOperations,
                          const QList<QUrl> &fileUrls,
                          const QUrl &baseUrl,
                          const QJSValue &callback);

private:
    FileUtils *q_ptr;
};

} // namespace

FileUtilsPrivate::FileUtilsPrivate(FileUtils *q):
    q_ptr(q)
{
}

void FileUtilsPrivate::findFiles(const QUrl &dirUrl, bool recursive,
                                 const FindFilesCallback &callback)
{
    Q_Q(FileUtils);
    auto *watcher = new QFutureWatcher<QList<QUrl>>(q);
    QObject::connect(watcher, &QFutureWatcher<QList<QUrl>>::finished,
                     q, [watcher,callback]() {
        QList<QUrl> files = watcher->result();
        callback(files);
        watcher->deleteLater();
    });
    watcher->setFuture(QtConcurrent::run(q, &FileUtils::findFiles,
                                         dirUrl, recursive));
}

void FileUtilsPrivate::reportFoundFiles(QAtomicInt *pendingOperations,
                                        const QList<QUrl> &fileUrls,
                                        const QUrl &baseUrl,
                                        const QJSValue &callback)
{
    Q_Q(FileUtils);

    bool hasMore = true;
    if (!pendingOperations->deref()) {
        delete pendingOperations;
        hasMore = false;
    }

    if (!fileUrls.isEmpty()) {
        QJSValue cbCopy(callback); // needed as callback is const
        QJSEngine *engine = qjsEngine(q);
        cbCopy.call(QJSValueList {
            engine->toScriptValue(fileUrls),
            engine->toScriptValue(baseUrl),
            hasMore,
        });
    }
}

void FileUtilsPrivate::findFiles(const QList<QUrl> &urls, bool recursive,
                                 const QJSValue &callback)
{
    QAtomicInt *pendingOperations = new QAtomicInt(1);

    /* Synchronously invoke the callback for all the regular files, and
     * invoke the threaded finder for all directories. */
    QList<QUrl> regularFileUrls;
    for (const QUrl &url: urls) {
        QFileInfo fileInfo(url.toLocalFile());
        if (fileInfo.isDir()) {
            using namespace std::placeholders;
            pendingOperations->ref();
            findFiles(url, recursive,
                      std::bind(&FileUtilsPrivate::reportFoundFiles, this,
                                pendingOperations, _1, url, callback));
        } else {
            regularFileUrls.append(url);
        }
    }

    reportFoundFiles(pendingOperations, regularFileUrls, QUrl(), callback);
}

FileUtils::FileUtils(QObject *parent):
    QObject(parent),
    d_ptr(new FileUtilsPrivate(this))
{
}

FileUtils::~FileUtils()
{
}

QList<QUrl> FileUtils::findFiles(const QUrl &dirUrl, bool recursive) const
{
    QList<QUrl> files;

    QDir dir(dirUrl.toLocalFile());
    if (Q_UNLIKELY(!dir.exists())) return files;

    auto list = dir.entryInfoList(QDir::NoDotAndDotDot |
                                  QDir::Dirs | QDir::Files,
                                  QDir::Name);
    for (const QFileInfo &info: list) {
        if (info.isDir()) {
            if (recursive) {
                files += findFiles(QUrl::fromLocalFile(info.filePath()), true);
            }
            continue;
        }

        files.append(QUrl::fromLocalFile(info.filePath()));
    }

    return files;
}

void FileUtils::findFiles(const QUrl &dirUrl, bool recursive,
                          const QJSValue &callback)
{
    Q_D(FileUtils);

    QJSValue baseDir = qjsEngine(this)->toScriptValue(dirUrl);
    d->findFiles(dirUrl, recursive,
                 [this,baseDir,callback](const QList<QUrl> &files) {
        QJSValue cbCopy(callback); // needed as callback is captured as const
        QJSEngine *engine = qjsEngine(this);
        cbCopy.call(QJSValueList { engine->toScriptValue(files), baseDir });
    });
}

void FileUtils::findFiles(const QList<QUrl> &urls, bool recursive,
                          const QJSValue &callback)
{
    Q_D(FileUtils);
    d->findFiles(urls, recursive, callback);
}
