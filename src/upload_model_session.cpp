/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "upload_model_session.h"

#include "Photokinesis/UploadModel"

#include <QByteArray>
#include <QDataStream>
#include <QDebug>
#include <QIODevice>

using namespace Photokinesis;

namespace Photokinesis {

typedef QVector<UploadModel::Roles> RolesVector;

class UploadModelSessionPrivate
{
    friend class UploadModelSession;

public:
    UploadModelSessionPrivate() = default;

    bool loadHeader(QDataStream &in);

    bool dumpItems(QDataStream &out) const;
    bool loadItems(QDataStream &in);

    bool dumpUploader(QDataStream &out, int uploaderIndex) const;
    bool loadUploader(QDataStream &in, int uploaderIndex);

private:
    UploadModel *m_model = nullptr;
    static const RolesVector m_globalRoles;
    static const RolesVector m_uploaderRoles;
    mutable QVector<int32_t> m_incompleteUploadsCount;
    QStringList m_uploaderIds;
};

} // namespace

/* This are the roles we are interested in, for the first column */
const RolesVector UploadModelSessionPrivate::m_globalRoles = {
    UploadModel::UrlRole,
    UploadModel::TitleRole,
    UploadModel::DescriptionRole,
    UploadModel::TagsRole,
    UploadModel::SubDirectoryRole,
};

/* This are the roles we are interested in, for the uploader columns */
const RolesVector UploadModelSessionPrivate::m_uploaderRoles = {
    UploadModel::UploadStatusRawRole,
    UploadModel::TitleRole,
    UploadModel::DescriptionRole,
    UploadModel::TagsRole,
};

bool UploadModelSessionPrivate::loadHeader(QDataStream &in)
{
    in.setVersion(QDataStream::Qt_5_6);

    in >> m_incompleteUploadsCount;

    int storedUploadersCount = m_incompleteUploadsCount.count();
    m_uploaderIds.clear();
    for (int i = 0; i < storedUploadersCount; i++) {
        QString uploaderId;
        in >> uploaderId;
        m_uploaderIds.append(uploaderId);
    }
    return true;
}

bool UploadModelSessionPrivate::dumpItems(QDataStream &out) const
{
    Q_ASSERT(m_model);

    out << int32_t(m_model->rowCount());

    out << m_globalRoles;

    for (int row = 0; row < m_model->rowCount(); row++) {
        QModelIndex index = m_model->index(row, 0);
        for (const UploadModel::Roles role: m_globalRoles) {
            out << index.data(role);
        }
    }

    return out.status() == QDataStream::Ok;
}

bool UploadModelSessionPrivate::loadItems(QDataStream &in)
{
    Q_ASSERT(m_model);

    int32_t rowCount;
    in >> rowCount;

    /* A small sanity check: */
    if (rowCount < 0 || rowCount > 100000) return false;

    RolesVector roles;
    in >> roles;

    if (roles.isEmpty() || roles[0] != UploadModel::UrlRole) {
        qWarning() << "URL role missing in session data";
        return false;
    }

    for (int row = 0; row < rowCount; row++) {
        QVariant value;
        in >> value;
        m_model->addPhoto(value.toUrl());

        QModelIndex index = m_model->index(row, 0);
        for (int i = 1; i < roles.count(); i++) {
            in >> value;
            m_model->setData(index, value, roles[i]);
        }
    }

    return in.status() == QDataStream::Ok;
}

bool UploadModelSessionPrivate::dumpUploader(QDataStream &out, int uploaderIndex) const
{
    Q_ASSERT(m_model);

    int incompleteUploads = 0;

    out << m_uploaderRoles;

    for (int row = 0; row < m_model->rowCount(); row++) {
        QModelIndex index = m_model->index(row, uploaderIndex + 1);
        for (const UploadModel::Roles role: m_uploaderRoles) {
            out << index.data(role);
        }

        /* Also count the incomplete uploads */
        UploadModel::UploadStatus status =
            index.data(UploadModel::UploadStatusRole).
            value<UploadModel::UploadStatus>();
        if (status != UploadModel::Uploaded) {
            incompleteUploads++;
        }
    }

    m_incompleteUploadsCount[uploaderIndex] = incompleteUploads;
    return out.status() == QDataStream::Ok;
}

bool UploadModelSessionPrivate::loadUploader(QDataStream &in,
                                             int uploaderIndex)
{
    Q_ASSERT(m_model);

    RolesVector roles;
    in >> roles;

    const int rowCount = m_model->rowCount();
    for (int row = 0; row < rowCount; row++) {
        QVariant value;

        QModelIndex index = m_model->index(row, uploaderIndex + 1);
        for (const UploadModel::Roles role: roles) {
            in >> value;
            if (uploaderIndex >= 0) {
                m_model->setData(index, value, role);
            }
        }
    }

    return in.status() == QDataStream::Ok;
}

UploadModelSession::UploadModelSession():
    d_ptr(new UploadModelSessionPrivate())
{
}

UploadModelSession::~UploadModelSession()
{
}

void UploadModelSession::setUploadModel(UploadModel *model)
{
    Q_D(UploadModelSession);
    d->m_model = model;
}

bool UploadModelSession::dumpSession(QIODevice *device,
                                     const QStringList &uploaders) const
{
    Q_D(const UploadModelSession);

    const int uploaderCount = uploaders.count();

    QDataStream out(device);
    out.setVersion(QDataStream::Qt_5_6);

    /* Write the number of the missing upload items for each uploader; we don't
     * have this information yet, so now we only reserve the size and remember
     * the offset within the stream */
    qint64 incompleteCountOffset = device->pos();
    d->m_incompleteUploadsCount.fill(0, uploaderCount);
    out << d->m_incompleteUploadsCount;

    for (const QString &uploaderId: uploaders) {
        out << uploaderId;
    }

    bool ok = d->dumpItems(out);
    if (Q_UNLIKELY(!ok)) return false;

    for (int uploaderIndex = 0;
         uploaderIndex < uploaderCount;
         uploaderIndex++) {
        ok = d->dumpUploader(out, uploaderIndex);
        if (Q_UNLIKELY(!ok)) return false;
    }

    /* The m_incompleteUploadsCount is now up-to-date. Go and write it */
    qint64 lastPos = device->pos();
    ok = device->seek(incompleteCountOffset);
    if (Q_UNLIKELY(!ok)) return false;
    out << d->m_incompleteUploadsCount;

    ok = device->seek(lastPos);
    if (Q_UNLIKELY(!ok)) return false;

    return out.status() == QDataStream::Ok;
}

bool UploadModelSession::loadSession(QIODevice *device,
                                     const QStringList &uploaders)
{
    Q_D(UploadModelSession);

    QDataStream in(device);

    bool ok = d->loadHeader(in);
    if (Q_UNLIKELY(!ok)) return false;

    d->m_model->setIsLoadingSession(true);
    ok = d->loadItems(in);
    d->m_model->setIsLoadingSession(false);
    if (Q_UNLIKELY(!ok)) return false;

    int storedUploadersCount = d->m_uploaderIds.count();

    /* This vector tells, for each uploader stored in the session, which is the
     * corresponding index in the new model. The value -1 means that the
     * uploader does not need to be loaded. */
    QVector<int> uploaderIndexes(storedUploadersCount, -1);
    const int uploaderCount = uploaders.count();
    for (int uploaderIndex = 0;
         uploaderIndex < uploaderCount;
         uploaderIndex++) {
        QString requestedUploaderId = uploaders[uploaderIndex];
        int oldIndex = d->m_uploaderIds.indexOf(requestedUploaderId);
        if (oldIndex >= 0) {
            uploaderIndexes[oldIndex] = uploaderIndex;
        }
    }

    for (int uploaderIndex: uploaderIndexes) {
        /* loadUploader() is smart enough to just skip the data if the
         * uploaderIndex is nagative. */
        ok = d->loadUploader(in, uploaderIndex);
        if (Q_UNLIKELY(!ok)) return false;
    }

    return in.status() == QDataStream::Ok;
}

bool UploadModelSession::sessionIsFinished(QIODevice *device,
                                           const QStringList &selectedUploaders)
{
    Q_D(UploadModelSession);
    Q_ASSERT(device);

    QDataStream in(device);
    if (!d->loadHeader(in)) return false;

    int totalIncompleteUploads = 0;
    int uploaderIndex = 0;
    for (int incompleteUploads: d->m_incompleteUploadsCount) {
        const QString &uploaderId = d->m_uploaderIds[uploaderIndex];
        if (selectedUploaders.contains(uploaderId)) {
            totalIncompleteUploads += incompleteUploads;
        }
        uploaderIndex++;
    }
    return totalIncompleteUploads == 0;
}
