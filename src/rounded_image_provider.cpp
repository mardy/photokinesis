/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "rounded_image_provider.h"

#include <QDebug>
#include <QImageReader>
#include <QPainter>
#include <QPainterPath>

using namespace Photokinesis;

namespace Photokinesis {

class RoundedImageProviderPrivate {};

} // namespace

RoundedImageProvider::RoundedImageProvider():
    QQuickImageProvider(QQmlImageProviderBase::Image)
{
}

RoundedImageProvider::~RoundedImageProvider()
{
}

QImage RoundedImageProvider::requestImage(const QString &id,
                                          QSize *size,
                                          const QSize &requestedSize)
{
    int slashPos = id.indexOf('/');
    int radius = id.leftRef(slashPos).toInt();
    QUrl uri(id.mid(slashPos + 1));
    QString filePath;
    if (uri.scheme() == "qrc") {
        filePath = QString(":") + uri.path();
    } else if (uri.isLocalFile()) {
        filePath = uri.toLocalFile();
    } else {
        filePath = uri.toString();
    }

    QImageReader reader(filePath);
    QSize imageSize = reader.size();
    /* scale when loading, if possible */
    if (imageSize.isValid() && requestedSize.isValid() &&
        (imageSize.width() > requestedSize.width() ||
         imageSize.height() > requestedSize.height())) {
        QSize scaledSize = imageSize.scaled(requestedSize,
                                            Qt::KeepAspectRatio);
        reader.setScaledSize(scaledSize);
    }

    *size = imageSize;
    QImage image = reader.read();

    QImage ret(image.size(), QImage::Format_ARGB32);
    ret.fill(Qt::transparent);

    QPainter p(&ret);
    p.setRenderHints(QPainter::Antialiasing);
    QPainterPath path;
    path.addRoundedRect(image.rect(), radius, radius);
    p.setClipPath(path);
    p.drawImage(image.rect(), image);

    return ret;
}
