/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader_model.h"

#include <QDebug>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/PluginManager>
#include <algorithm>

using namespace Photokinesis;

namespace Photokinesis {

struct Row {
    UploaderProfile profile;
    mutable AbstractUploader *uploader;

    AbstractUploader *ensureUploader() const;
};

class UploaderModelPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(UploaderModel)

public:
    UploaderModelPrivate(UploaderModel *q);
    ~UploaderModelPrivate();

private:
    QHash<int, QByteArray> m_roles;
    QVector<Row> m_uploaders;
    UploaderModel *q_ptr;
};

} // namespace

AbstractUploader *Row::ensureUploader() const
{
    if (!uploader) {
        qDebug() << "Creating uploader for" << profile.name();
        uploader = PluginManager::instance()->uploader(profile);
    }
    return uploader;
}

UploaderModelPrivate::UploaderModelPrivate(UploaderModel *q):
    QObject(q),
    q_ptr(q)
{
    m_roles[Qt::DisplayRole] = "display";
    m_roles[UploaderModel::NameRole] = "name";
    m_roles[UploaderModel::IconRole] = "icon";
    m_roles[UploaderModel::EnabledRole] = "enabled";
    m_roles[UploaderModel::SupportedOptionsRole] = "supportedOptions";
    m_roles[UploaderModel::UploaderRole] = "uploader";

    QList<UploaderProfile> profiles =
        PluginManager::instance()->uploaderProfiles();
    std::sort(profiles.begin(),
              profiles.end(),
              [](const UploaderProfile &p1, const UploaderProfile &p2) {
        return p1.displayName() < p2.displayName();
    });
    m_uploaders.reserve(profiles.count());
    for (const UploaderProfile &profile: profiles) {
        m_uploaders.append({ profile, nullptr });
    }
}

UploaderModelPrivate::~UploaderModelPrivate()
{
}

UploaderModel::UploaderModel(QObject *parent):
    QAbstractListModel(parent),
    d_ptr(new UploaderModelPrivate(this))
{
    QObject::connect(this, SIGNAL(modelReset()),
                     this, SIGNAL(countChanged()));
}

UploaderModel::~UploaderModel()
{
}

QVariantMap UploaderModel::get(int row) const
{
    Q_D(const UploaderModel);
    QVariantMap ret;
    for (auto i = d->m_roles.constBegin(); i != d->m_roles.constEnd(); i++) {
        ret.insert(i.value(), data(index(row, 0), i.key()));
    }
    return ret;
}

QVariant UploaderModel::get(int row, const QString &roleName) const
{
    int role = roleNames().key(roleName.toLatin1(), -1);
    return data(index(row, 0), role);
}

int UploaderModel::rowCount(const QModelIndex &parent) const
{
    Q_D(const UploaderModel);
    Q_UNUSED(parent);
    return d->m_uploaders.count();
}

QVariant UploaderModel::data(const QModelIndex &index, int role) const
{
    Q_D(const UploaderModel);

    int row = index.row();
    if (row < 0 || row >= d->m_uploaders.count()) return QVariant();

    const Row &r = d->m_uploaders[row];
    const UploaderProfile &profile = r.profile;
    switch (role) {
    case Qt::DisplayRole:
        return profile.displayName();
    case NameRole:
        return profile.name();
    case IconRole:
        return profile.icon();
    case EnabledRole:
        return r.ensureUploader()->isEnabled();
    case SupportedOptionsRole:
        return profile.supportedOptions();
    case UploaderRole:
        return QVariant::fromValue(r.ensureUploader());
    default:
        qWarning() << "Unknown role ID:" << role;
        return QVariant();
    }
}

QHash<int, QByteArray> UploaderModel::roleNames() const
{
    Q_D(const UploaderModel);
    return d->m_roles;
}

#include "uploader_model.moc"
