/*
 * Copyright (C) 2020-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MARDY_FILE_UTILS_H
#define MARDY_FILE_UTILS_H

#include <QList>
#include <QObject>
#include <QScopedPointer>
#include <QUrl>

class QJSValue;

namespace Mardy {

class FileUtilsPrivate;
class FileUtils: public QObject
{
    Q_OBJECT

public:
    FileUtils(QObject *parent = 0);
    ~FileUtils();

    Q_INVOKABLE QList<QUrl> findFiles(const QUrl &dirUrl,
                                      bool recursive) const;
    Q_INVOKABLE void findFiles(const QUrl &dirUrl, bool recursive,
                               const QJSValue &callback);
    Q_INVOKABLE void findFiles(const QList<QUrl> &urls, bool recursive,
                               const QJSValue &callback);

private:
    Q_DECLARE_PRIVATE(FileUtils)
    QScopedPointer<FileUtilsPrivate> d_ptr;
};

} // namespace

#endif // MARDY_FILE_UTILS_H
