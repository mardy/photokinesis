/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "session.h"

#include "Photokinesis/upload_model.h"
#include "json_serializer.h"
#include "upload_model_session.h"

#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QStandardPaths>

using namespace Photokinesis;

namespace Photokinesis {

class SessionPrivate
{
    friend class Session;

public:
    SessionPrivate();

private:
    QFileInfo m_sessionFile;
};

} // namespace

SessionPrivate::SessionPrivate()
{
    m_sessionFile =
        QStandardPaths::writableLocation(QStandardPaths::CacheLocation) +
        "/lastsession.bin";
}

Session::Session(QObject *parent):
    QObject(parent),
    d_ptr(new SessionPrivate())
{
}

Session::~Session()
{
}

bool Session::storeSession(const QJsonArray &uploaders, QObject *uploadModel)
{
    Q_D(Session);

    UploadModel *model = qobject_cast<UploadModel*>(uploadModel);

    if (Q_UNLIKELY(!QDir::root().mkpath(d->m_sessionFile.path()))) {
        qWarning() << "Could not create session dir!";
        return false;
    }

    QFile sessionFile(d->m_sessionFile.filePath());
    if (Q_UNLIKELY(!sessionFile.open(QIODevice::WriteOnly))) {
        qWarning() << "Couldn't open session file for writing" <<
            d->m_sessionFile.path();
        return false;
    }

    bool ok = JsonSerializer::dump(&sessionFile, uploaders);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not save uploaders";
        return false;
    }

    QStringList uploaderNames;
    for (const QJsonValue &uploader: uploaders) {
        uploaderNames.append(uploader.toObject().value("name").toString());
    }
    UploadModelSession modelSession;
    modelSession.setUploadModel(model);
    ok = modelSession.dumpSession(&sessionFile, uploaderNames);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not save UploadModel";
        return false;
    }

    return true;
}

QJsonObject Session::loadLastSession(const QStringList &uploaders,
                                     QObject *uploadModel)
{
    Q_D(Session);
    QJsonObject ret;

    UploadModel *model = qobject_cast<UploadModel*>(uploadModel);

    QFile sessionFile(d->m_sessionFile.filePath());
    if (Q_UNLIKELY(!sessionFile.open(QIODevice::ReadOnly))) {
        qWarning() << "Couldn't open session file for reading";
        return ret;
    }

    QJsonArray uploaderData = JsonSerializer::loadArray(&sessionFile);
    if (Q_UNLIKELY(uploaderData.isEmpty())) {
        qWarning() << "Could not load uploaders";
        return ret;
    }

    UploadModelSession modelSession;
    modelSession.setUploadModel(model);
    bool ok = modelSession.loadSession(&sessionFile, uploaders);
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not load UploadModel";
        return ret;
    }

    return QJsonObject {
        { "uploaders", uploaderData },
    };
}

bool Session::sessionIsFinished() const
{
    Q_D(const Session);

    QFile sessionFile(d->m_sessionFile.filePath());
    if (Q_UNLIKELY(!sessionFile.open(QIODevice::ReadOnly))) {
        qWarning() << "Couldn't open session file for reading";
        return true;
    }

    QJsonArray uploaderData = JsonSerializer::loadArray(&sessionFile);
    if (uploaderData.isEmpty()) return true;

    QStringList selectedUploaders;
    for (const QJsonValue &uploaderEntry: uploaderData) {
        const QJsonObject uploader = uploaderEntry.toObject();
        if (uploader["selected"].toBool())
            selectedUploaders.append(uploader["name"].toString());
    }

    UploadModelSession modelSession;
    return modelSession.sessionIsFinished(&sessionFile, selectedUploaders);
}
