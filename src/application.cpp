/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "application.h"

#include <QDebug>
#include <QFileInfo>
#include <QFileOpenEvent>
#include <QIcon>
#include <QProcess>
#include <QTextStream>
#include <QThreadPool>
#include <QTimer>
#include <QtWebView>
#include <cstdlib>

using namespace Photokinesis;

namespace Photokinesis {

class ApplicationPrivate: public QObject
{
    Q_OBJECT
    Q_DECLARE_PUBLIC(Application)

public:
    ApplicationPrivate(Application *q);

    void showHelp() const;
    void parseCmdLine();

private:
    QList<QUrl> m_items;
    QStringList m_services;
    Application *q_ptr;
};

} // namespace

ApplicationPrivate::ApplicationPrivate(Application *q):
    QObject(),
    q_ptr(q)
{
    parseCmdLine();
}

void ApplicationPrivate::showHelp() const
{
    Q_Q(const Application);

    QTextStream out(stdout);

    out << q->arguments().at(0) << "[SERVICES]... [FILE]...\n\n";
    out << "Where SERVICES is a list of zero or more such elements:\n";
    out << "  -s <service>\tSelect the given <service>\n";
    out.flush();
    std::exit(EXIT_SUCCESS);
}

void ApplicationPrivate::parseCmdLine()
{
    Q_Q(const Application);

    enum Status { Start, Service, Item, Items, };

    const QStringList args = q->arguments().mid(1);
    Status status = Start;
    for (const QString &arg: args) {
        if (status == Start) {
            if (arg[0] == '-') {
                switch (arg[1].toLatin1()) {
                case 's': status = Service; break;
                case 'i': status = Item; break;
                case '-': status = Items; break;
                case 'h': showHelp(); break;
                default:
                    qWarning() << "Unknown option" << arg;
                    status = Items;
                }
                continue;
            } else {
                status = Items;
            }
        }

        if (status == Service) {
            m_services.append(arg);
            status = Start;
        } else if (status == Item || status == Items) {
            QFileInfo fileInfo(arg);
            if (fileInfo.isFile()) {
                m_items.append(QUrl::fromLocalFile(arg));
            }
            if (status == Item) status = Start;
        }
    };
}

Application::Application(int &argc, char **argv):
#ifdef DESKTOP_BUILD
    QApplication(argc, argv),
#else
    QGuiApplication(argc, argv),
#endif
    d_ptr(new ApplicationPrivate(this))
{
    setApplicationDisplayName(APPLICATION_DISPLAY_NAME);
    setApplicationName(APPLICATION_NAME);
    setApplicationVersion(APPLICATION_VERSION);
    setOrganizationName(QString());
    setOrganizationDomain(APPLICATION_NAME);
    setWindowIcon(QIcon(":/icons/photokinesis"));

    /* for libauthentication */
    QtWebView::initialize();
    addLibraryPath(applicationDirPath() + "/../lib");

#ifdef APPIMAGE
    QTimer::singleShot(1000, this, [this]() {
        QProcess::execute(applicationDirPath() + "/desktop-integration.sh");
    });
#endif
}

Application::~Application()
{
    QThreadPool::globalInstance()->waitForDone(5000);
}

QList<QUrl> Application::cmdLineItems() const
{
    Q_D(const Application);
    return d->m_items;
}

QStringList Application::cmdLineServices() const
{
    Q_D(const Application);
    return d->m_services;
}

bool Application::event(QEvent *event)
{
    Q_D(Application);

    if (event->type() == QEvent::FileOpen) {
        QFileOpenEvent *openEvent = static_cast<QFileOpenEvent *>(event);
        d->m_items.append(openEvent->url());
        Q_EMIT cmdLineItemsChanged();
    }

    return QApplication::event(event);
}

#include "application.moc"
