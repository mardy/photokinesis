/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_APPLICATION_H
#define PHOTOKINESIS_APPLICATION_H

#ifdef DESKTOP_BUILD
#include <QApplication>
#else
#include <QGuiApplication>
#endif
#include <QList>
#include <QStringList>
#include <QUrl>

namespace Photokinesis {

class ApplicationPrivate;
class Application:
#ifdef DESKTOP_BUILD
    public QApplication
#else
    public QGuiApplication
#endif
{
    Q_OBJECT
    Q_PROPERTY(QList<QUrl> cmdLineItems READ cmdLineItems
               NOTIFY cmdLineItemsChanged)
    Q_PROPERTY(QStringList cmdLineServices READ cmdLineServices CONSTANT)

public:
    Application(int &argc, char **argv);
    ~Application();

    QList<QUrl> cmdLineItems() const;
    QStringList cmdLineServices() const;

Q_SIGNALS:
    void cmdLineItemsChanged();

protected:
    bool event(QEvent *event) override;

private:
    QScopedPointer<ApplicationPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Application)
};

} // namespace

#endif // PHOTOKINESIS_APPLICATION_H
