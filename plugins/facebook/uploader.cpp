/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/OAuth2>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QByteArray>
#include <QDebug>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

using namespace Photokinesis;

static const QString baseUrl = QStringLiteral("https://graph.facebook.com/");

namespace Photokinesis {

class FacebookReply: public QObject
{
    Q_OBJECT

public:
    FacebookReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            qDebug() << "raw reply:" << replyText;
            m_json = QJsonDocument::fromJson(replyText).object();
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &FacebookReply::finished);
    }

    QJsonObject response() const { return m_json; }

    bool isError() const { return m_json.contains("error"); }
    QString errorMessage() const {
        return m_json.value("error").toObject().value("message").toString();
    }
    bool needsAuthentication() const {
        return m_json.value("error").toObject().value("type").toString() ==
            "OAuthException";
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QScopedPointer<QNetworkReply> m_reply;
};

class FacebookUploader: public AbstractUploader
{
    Q_OBJECT
    Q_PROPERTY(QJsonArray groups MEMBER m_groups NOTIFY groupsChanged)
    Q_PROPERTY(QString groupId MEMBER m_groupId NOTIFY groupIdChanged)

public:
    FacebookUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;
    void setSettings(const QJsonObject &settings) override;
    QJsonObject getSettings() const override;

    QJsonObject findGroup(const QString &id) const;
    QJsonObject findAlbum(const QString &id) const;

    QUrl userEndpoint(const QString &method);
    Authentication::OAuth2 &oauth();
    FacebookReply *call(const QString &method,
                        const QVariantMap &params = QVariantMap());
    FacebookReply *callOnUser(const QString &method,
                              const QVariantMap &params = QVariantMap());
    FacebookReply *getProfileInfo();
    FacebookReply *getAlbums();
    FacebookReply *getGroups();

    void initialize();
    bool checkAuthentication(FacebookReply *reply);

    Q_INVOKABLE void createAlbum(const QJsonObject &creationData);

Q_SIGNALS:
    void groupsChanged();
    void groupIdChanged();

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth2 m_auth;
    Authentication::OAuth2 m_pageAuth;
    QQuickItem *m_parentItem;
    QJsonObject m_profileInfo;
    QJsonArray m_groups;
    QString m_groupId;
};

} // namespace

FacebookUploader::FacebookUploader(const UploaderProfile &profile,
                                   QObject *parent):
    AbstractUploader(profile, parent),
    m_parentItem(0)
{
    m_auth.setClientId("157629554830728");
    m_auth.setScopes({
        "user_photos",
        "pages_show_list", "publish_pages", "manage_pages",
    });
    m_auth.setAuthorizationUrl(QUrl("https://www.facebook.com/v2.10/dialog/oauth"));
    m_auth.setResponseType("token");
    m_auth.setCallbackUrl("https://web.facebook.com/connect/login_success.html");
    m_auth.setUserAgent("Photokinesis/1.0");

    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));

    QObject::connect(this, &FacebookUploader::groupIdChanged,
                     this, [this]() {
        const QJsonObject group = findGroup(m_groupId);
        if (group.isEmpty() && !m_groups.isEmpty()) {
            /* Reset the group ID to the first one */
            m_groupId = m_groups.first().toObject().value("id").toString();
            Q_EMIT groupIdChanged();
            return;
        }
        QByteArray token = group.value("access_token").toString().toUtf8();
        m_pageAuth.injectToken(token);
    });
    QObject::connect(this, &FacebookUploader::groupIdChanged,
                     this, &FacebookUploader::getAlbums);
}

void FacebookUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    UiHelpers::createWebview(parent, &m_auth);
    m_auth.process();
}

void FacebookUploader::logout()
{
    m_auth.logout();

    QJsonArray cookies {
        QJsonArray { "wd", ".facebook.com", "/" },
        QJsonArray { "AA003", ".atdmt.com", "/" },
        QJsonArray { "datr", ".facebook.com", "/" },
        QJsonArray { "sb", ".facebook.com", "/" },
        QJsonArray { "c_user", ".facebook.com", "/" },
        QJsonArray { "xs", ".facebook.com", "/" },
        QJsonArray { "fr", ".facebook.com", "/" },
        QJsonArray { "pl", ".facebook.com", "/" },
    };
    QObject *deleter =
        UiHelpers::clearCookies(this, QUrl("https://www.facebook.com"),
                                cookies);
    QObject::connect(deleter, &QObject::destroyed,
                     this, [this]() { AbstractUploader::logout(); });

    m_groupId.clear();
    Q_EMIT groupIdChanged();
    m_groups = {};
    Q_EMIT groupsChanged();
}

void FacebookUploader::startUpload(const QString &filePath,
                                   const QJsonObject &options)
{
    setStatus(AbstractUploader::Busy);

    QStringList texts {
        options["title"].toString(),
        options["description"].toString(),
    };

    QStringList tags;
    QJsonArray tagArray = options["tags"].toArray();
    for (const QJsonValue &v: tagArray) {
        tags.append('#' + v.toString());
    }
    if (!tags.isEmpty()) {
        texts.append(tags.join(' '));
    }
    QString caption = texts.join("\n\n");

    QFileInfo fileInfo(filePath);

    QVariantMap params {
        { "source", QVariantMap {
                { "filePath", fileInfo.filePath() },
                { "fileName", fileInfo.fileName() },
            }
        },
        { "caption", caption },
    };
    QUrl endpoint = defaultAlbumId().isEmpty() ?
        userEndpoint("photos") :
        QUrl(baseUrl + defaultAlbumId() + "/photos");
    qDebug() << "Posting to" << endpoint;
    QNetworkReply *reply = oauth().post(endpoint, params);
    QObject::connect(reply, &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &QNetworkReply::finished, this, [this, reply]() {
        reply->deleteLater();
        QByteArray replyText = reply->readAll();
        qDebug() << "raw reply:" << replyText;
        QJsonObject json = QJsonDocument::fromJson(replyText).object();
        if (json.contains("id")) {
            setUploadsUrl(QUrl(findAlbum(defaultAlbumId())["link"].toString()));
            Q_EMIT done(true);
        } else {
            QString message("Photo upload failed");
            if (json.contains("error")) {
                message = json["error"].toObject()["message"].toString();
            }
            setErrorMessage(message);
            Q_EMIT done(false);
        }
        setStatus(AbstractUploader::Ready);
    });
}

QJsonObject FacebookUploader::findGroup(const QString &id) const
{
    for (const QJsonValue &v: m_groups) {
        QJsonObject o = v.toObject();
        if (o.value("id").toString() == id) {
            return o;
        }
    }
    return QJsonObject();
}

QJsonObject FacebookUploader::findAlbum(const QString &id) const
{
    QJsonArray albums = this->albums();
    for (const QJsonValue &v: albums) {
        QJsonObject o = v.toObject();
        if (o.value("albumId").toString() == id) {
            return o;
        }
    }
    return QJsonObject();
}

QUrl FacebookUploader::userEndpoint(const QString &method)
{
    return QUrl(baseUrl +
                (m_groupId.isEmpty() ? "me" : m_groupId) + '/' +
                method);
}

Authentication::OAuth2 &FacebookUploader::oauth()
{
    return m_groupId.isEmpty() ? m_auth : m_pageAuth;
}

FacebookReply *FacebookUploader::call(const QString &method, const QVariantMap &params)
{
    QUrl url(baseUrl + method);
    return new FacebookReply(m_auth.get(url, params));
}

FacebookReply *FacebookUploader::callOnUser(const QString &method, const QVariantMap &params)
{
    QUrl url(userEndpoint(method));
    return new FacebookReply(m_auth.get(url, params));
}

FacebookReply *FacebookUploader::getProfileInfo()
{
    FacebookReply *reply = call(QStringLiteral("me"));
    QObject::connect(reply, &FacebookReply::finished, this, [this, reply]() {
        checkAuthentication(reply);
        m_profileInfo = reply->response();
    });
    return reply;
}

FacebookReply *FacebookUploader::getAlbums()
{
    QVariantMap params {
        { "fields", "id,name,link,can_upload" },
    };
    FacebookReply *reply = callOnUser(QStringLiteral("albums"), params);
    QObject::connect(reply, &FacebookReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums = reply->response().value("data").toArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            if (!s["can_upload"].toBool()) continue;
            QJsonObject a;
            a.insert("name", s["name"]);
            a.insert("albumId", s["id"].toVariant().toString());
            a.insert("link", s["link"]);
            albums.append(a);
        }

        setAlbums(albums);
    });
    return reply;
}

FacebookReply *FacebookUploader::getGroups()
{
    QVariantMap params;
    FacebookReply *reply = call(QStringLiteral("me/accounts"), params);
    QObject::connect(reply, &FacebookReply::finished, this, [this, reply]() {
        const QJsonArray netGroups = reply->response().value("data").toArray();
        QJsonArray groups;
        for (const QJsonValue &v: netGroups) {
            const QJsonObject s = v.toObject();
            groups.append(s);
        }

        setProperty("groups", groups);
    });
    return reply;
}

void FacebookUploader::initialize()
{
    setStatus(AbstractUploader::RetrievingAccountInfo);

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                    this, [this, waiter]() {
        waiter->deleteLater();
        if (status() == AbstractUploader::NeedsAuthentication) return;

        QString name = m_profileInfo.value("name").toString();
        setUserId(name);

        loadSettings();

        setAccountInfo(tr("%n album(s)", "", albums().count()));
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
    });

    waiter->add(getAlbums());
    waiter->add(getGroups());
    waiter->add(getProfileInfo());
}

void FacebookUploader::setSettings(const QJsonObject &settings)
{
    AbstractUploader::setSettings(settings);
    m_groupId = settings.value("groupId").toString();
    Q_EMIT groupIdChanged();
}

QJsonObject FacebookUploader::getSettings() const
{
    QJsonObject settings = AbstractUploader::getSettings();
    settings.insert("groupId", m_groupId);
    return settings;
}

bool FacebookUploader::checkAuthentication(FacebookReply *reply)
{
    if (reply->isError() && reply->needsAuthentication()) {
        qDebug() << "Needs authentication!";
        m_auth.logout();
        setErrorMessage(reply->errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
        return false;
    }
    return true;
}

void FacebookUploader::onAuthenticationFinished()
{
    if (!m_auth.hasError()) {
        initialize();
    } else {
        qWarning() << "Authentication error:" << m_auth.errorMessage();
        setErrorMessage(m_auth.errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

void FacebookUploader::createAlbum(const QJsonObject &creationData)
{
    QVariantMap params = creationData.toVariantMap();
    qDebug() << "create album:" << params;
    params.insert("fields", "name,link");
    QUrl endpoint = userEndpoint(QStringLiteral("albums"));
    QNetworkReply *reply = oauth().post(endpoint, params);
    QObject::connect(reply, &QNetworkReply::finished,
                     this, [this, reply]() {
        reply->deleteLater();

        QByteArray replyText = reply->readAll();
        qDebug() << "raw reply:" << replyText;
        const QJsonObject s = QJsonDocument::fromJson(replyText).object();
        QJsonArray albums = this->albums();
        QJsonObject a {
            { "name", s["name"] },
            { "albumId", s["id"] },
            { "link", s["link"] },
        };
        albums.append(a);

        setDefaultAlbumId(a.value("albumId").toString());
        setAlbums(albums);
    });
}

Photokinesis::AbstractUploader *
FacebookPlugin::createUploader(const UploaderProfile &profile,
                               QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new FacebookUploader(profile, parent);
}

#include "uploader.moc"
