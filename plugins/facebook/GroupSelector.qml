import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root
    Layout.fillWidth: true

    Label {
        text: qsTr("Publish destination (page):")
    }

    Label {
        text: qsTr("You don't have any pages accessible from your account.\nSince August 1st, 2018 Facebook allows application to only post to pages, not to a user's profile.")
        wrapMode: Text.WordWrap
        color: "red"
        visible: uploader.accountInfo && uploader.groups.length == 0
    }

    ComboBox {
        id: groupSelector
        Layout.fillWidth: true
        enabled: uploader.groups.length > 0
        model: uploader.groups
        textRole: "name"
    }

    Binding {
        target: uploader
        property: "groupId"
        value: uploader.groups.length > 0 ? uploader.groups[groupSelector.currentIndex].id : ""
    }
}
