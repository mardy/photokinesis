/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_FTP_BACKEND
#define PHOTOKINESIS_FTP_BACKEND

#include "ftp_interface.h"

#include <QHash>
#include <QString>
#include <QStringList>
#include <QtFtp/QFtp>
#include <functional>

namespace Photokinesis {

class FtpBackend: public FtpInterface
{
    Q_OBJECT

public:
    FtpBackend();
    ~FtpBackend();

    void login(const QString &host,
               uint16_t port,
               const QString &username,
               const QString &password,
               const QJsonObject &options) override;
    void cd(const QString &dirName) override;
    void mkdir(const QString &dirName) override;
    void upload(const QString &localFile, const QString &fileName) override;
    void logout() override;

private:
    void changePath(const QString &newPath,
                    const std::function<void(bool)> &callback);

private:
    QFtp m_ftp;
    QStringList m_subdirs;
    QStringList m_currentPathComponents;
    QHash<int,QStringList> m_pathComponentsPerCommand;
    QHash<int,QStringList> m_missingComponentsPerCommand;
    QMetaObject::Connection m_changingPathConnection;
    bool m_subdirsChangedQueued;
};

} // namespace

#endif // PHOTOKINESIS_FTP_BACKEND
