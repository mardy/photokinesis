import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

ColumnLayout {
    id: root

    property var authenticator: null
    property bool wantsVisibility: true
    property string errorMessage: ""

    signal finished()

    anchors { fill: parent; margins: 12 }

    RowLayout {
        Layout.fillWidth: true
        Label {
            text: authenticator.canChangeHost ?
                qsTr("Choose a saved site:") : qsTr("Choose a saved login:")
        }

        ComboBox {
            Layout.fillWidth: true
            enabled: authenticator.knownUserIds.length > 0
            model: enabled ? authenticator.knownUserIds : [ "No previous logins" ]
            onCurrentTextChanged: {
                authenticator.userId = currentText
                authenticator.loadSettings()
            }
        }
    }

    Label {
        Layout.fillHeight: true
        text: authenticator.canChangeHost ?
            qsTr("Or enter the connection details for a new FTP server:") :
            qsTr("Or enter the connection details for a new user:")
        font.italic: true
        verticalAlignment: Text.AlignBottom
        wrapMode: Text.WordWrap
    }

    GridLayout {
        Layout.fillWidth: true
        columns: 2
        Label {
            visible: authenticator.canChangeHost
            text: qsTr("Host address")
        }

        TextField {
            id: hostnameField
            Layout.fillWidth: true
            visible: authenticator.canChangeHost
            placeholderText: qsTr("Host name, or paste a full URL and press Enter")
            text: authenticator.host
            onAccepted: authenticator.tryParseUrl(text)
            onEditingFinished: authenticator.tryParseUrl(text)
        }

        Label {
            text: qsTr("Username")
        }

        TextField {
            id: usernameField
            Layout.fillWidth: true
            placeholderText: authenticator.canChangeHost ?
                qsTr("Your FTP username") : qsTr("Your %1 username").arg(authenticator.displayName)
            text: authenticator.username
        }

        Label {
            text: qsTr("Password")
        }

        TextField {
            id: passwordField
            Layout.fillWidth: true
            echoMode: TextInput.Password
            inputMethodHints: Qt.ImhHiddenText
            text: authenticator.password
            onAccepted: root.doLogin()
        }
    }

    Item {
        Layout.columnSpan: 2
        Layout.fillWidth: true
        Layout.fillHeight: true
        implicitHeight: loginButton.implicitHeight + 12
        Button {
            id: loginButton
            anchors {
                top: parent.top; topMargin: 8
                horizontalCenter: parent.horizontalCenter
            }
            text: qsTr("Login")
            onClicked: root.doLogin()
        }
    }

    function doLogin() {
        if (authenticator.canChangeHost) {
            authenticator.host = hostnameField.text
        }
        authenticator.username = usernameField.text
        authenticator.password = passwordField.text
        authenticator.login()
        root.finished()
    }

    function cancel() {
        root.errorMessage = qsTr("User canceled")
        root.finished()
    }
}
