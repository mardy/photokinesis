/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "backend_selector.h"

#include <QDebug>
#include <QJsonObject>
#include <QMetaObject>
#include "ftp_backend.h"
#include "scp_backend.h"

using namespace Photokinesis;

namespace Photokinesis {

class BackendSelectorPrivate {
    Q_DECLARE_PUBLIC(BackendSelector)

public:
    BackendSelectorPrivate(BackendSelector *q);

    void login(const QString &host,
               uint16_t port,
               const QString &username,
               const QString &password,
               const QJsonObject &options);
    void tryNextBackend(const QString &host,
                        uint16_t port,
                        const QString &username,
                        const QString &password,
                        const QJsonObject &options);

    FtpInterface *createBackend(BackendSelector::Backend backend) const;
    void useBackend(FtpInterface *backend);

private:
    QScopedPointer<FtpInterface> m_backend;
    BackendSelector::Backend m_preferredBackend;
    int m_numBackendTried;
    BackendSelector *q_ptr;
};

} // namespace

BackendSelectorPrivate::BackendSelectorPrivate(BackendSelector *q):
    m_preferredBackend(BackendSelector::BackendScp),
    m_numBackendTried(0),
    q_ptr(q)
{
}

void BackendSelectorPrivate::login(const QString &host,
                                   uint16_t port,
                                   const QString &username,
                                   const QString &password,
                                   const QJsonObject &options)
{
    qDebug() << "Logging in to" << host << port << "as" << username;
    m_numBackendTried = 0;
    tryNextBackend(host, port, username, password, options);
}

void BackendSelectorPrivate::tryNextBackend(const QString &host,
                                            uint16_t port,
                                            const QString &username,
                                            const QString &password,
                                            const QJsonObject &options)
{
    Q_Q(BackendSelector);

    if (m_numBackendTried == BackendSelector::BackendCount) {
        Q_EMIT q->stateChanged(FtpInterface::Unconnected);
        return;
    }

    int index = int(m_preferredBackend) + m_numBackendTried;
    if (index >= BackendSelector::BackendCount) {
        index -= BackendSelector::BackendCount;
    }
    BackendSelector::Backend nextBackend = BackendSelector::Backend(index);
    FtpInterface *backend = createBackend(nextBackend);
    m_numBackendTried++;

    qDebug() << "trying login with backend" << backend;
    QSharedPointer<QMetaObject::Connection> connection(new QMetaObject::Connection);
    *connection =
        QObject::connect(backend, &FtpInterface::stateChanged,
                         [=](FtpInterface::State state,
                             FtpInterface::Error error) {
            if (state == FtpInterface::Connected) {
                qDebug() << "Connected to backend" << backend;
                QObject::disconnect(*connection);
                useBackend(backend);
                Q_EMIT q->stateChanged(state);
            } else if (state == FtpInterface::Unconnected) {
                if (error == FtpInterface::AuthenticationError) {
                    Q_EMIT q->stateChanged(state, error);
                } else {
                    tryNextBackend(host, port, username, password, options);
                }
                backend->deleteLater();
            }
        });

    backend->login(host, port, username, password, options);
}

FtpInterface *
BackendSelectorPrivate::createBackend(BackendSelector::Backend backend) const
{
    switch (backend) {
    case BackendSelector::BackendScp: return new ScpBackend();
    case BackendSelector::BackendFtp: return new FtpBackend();
    default: return nullptr;
    }
}

void BackendSelectorPrivate::useBackend(FtpInterface *backend)
{
    Q_Q(BackendSelector);

    m_backend.reset(backend);
    QObject::connect(backend, &FtpInterface::subdirsChanged,
                     q, &FtpInterface::subdirsChanged);
    QObject::connect(backend, &FtpInterface::dirCreated,
                     q, &FtpInterface::dirCreated);
    QObject::connect(backend, &FtpInterface::progress,
                     q, &FtpInterface::progress);
    QObject::connect(backend, &FtpInterface::stateChanged,
                     q, &FtpInterface::stateChanged);
    QObject::connect(backend, &FtpInterface::finished,
                     q, &FtpInterface::finished);
}

BackendSelector::BackendSelector():
    d_ptr(new BackendSelectorPrivate(this))
{
}

BackendSelector::~BackendSelector()
{
}

void BackendSelector::setPreferredBackend(Backend backend)
{
    Q_D(BackendSelector);
    d->m_preferredBackend = backend;
}

void BackendSelector::login(const QString &host,
                            uint16_t port,
                            const QString &username,
                            const QString &password,
                            const QJsonObject &options)
{
    Q_D(BackendSelector);
    d->login(host, port, username, password, options);
}

void BackendSelector::mkdir(const QString &dirName)
{
    Q_D(BackendSelector);
    d->m_backend->mkdir(dirName);
}

void BackendSelector::upload(const QString &localFile, const QString &filePath)
{
    Q_D(BackendSelector);
    d->m_backend->upload(localFile, filePath);
}

void BackendSelector::logout()
{
    Q_D(BackendSelector);
    if (d->m_backend) {
        d->m_backend->logout();
        d->m_backend->deleteLater();
        d->m_backend.reset();
    }
}

void BackendSelector::cd(const QString &dirName)
{
    Q_D(BackendSelector);
    d->m_backend->cd(dirName);
}
