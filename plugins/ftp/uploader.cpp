/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Photokinesis/AbstractUploader>
#include <Photokinesis/Metadata>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickItem>
#include <QScopedPointer>
#include <QTemporaryDir>
#include "backend_selector.h"

using namespace Photokinesis;

namespace Photokinesis {

class FtpUploader: public AbstractUploader
{
    Q_OBJECT
    Q_PROPERTY(QString host MEMBER m_host NOTIFY hostChanged)
    Q_PROPERTY(bool canChangeHost MEMBER m_canChangeHost CONSTANT)
    Q_PROPERTY(QString username MEMBER m_username NOTIFY userNameChanged)
    Q_PROPERTY(QString password MEMBER m_password NOTIFY passwordChanged)
    Q_PROPERTY(QString path MEMBER m_path WRITE setPath NOTIFY pathChanged)
    Q_PROPERTY(QStringList subdirs READ subdirs NOTIFY subdirsChanged)

public:
    FtpUploader(const UploaderProfile &profile, QObject *parent = 0);

    QStringList subdirs() const { return m_subdirs; }
    void setPath(const QString &path);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;
    void setSettings(const QJsonObject &settings) override;
    QJsonObject getSettings() const override;

    QString errorToText(FtpInterface::Error code) const;
    void setFailed(const QString &errorMessage);

    Q_INVOKABLE void cd(const QString &string);
    Q_INVOKABLE void mkdir(const QString &string);
    Q_INVOKABLE void tryParseUrl(const QString &string);
    Q_INVOKABLE void login();

Q_SIGNALS:
    void hostChanged();
    void userNameChanged();
    void passwordChanged();
    void pathChanged();
    void subdirsChanged();
    void dirCreated(bool ok);

private:
    void createLoginComponent(QQuickItem *parent);

private:
    BackendSelector m_backend;
    bool m_canChangeHost;
    QString m_host;
    QString m_username;
    QString m_password;
    QJsonObject m_loginOptions;
    QString m_requestedPath;
    QString m_path;
    QString m_newDir;
    QStringList m_subdirs;
    Imaginario::Metadata m_metadata;
    QJsonObject m_exifMap;
    QTemporaryDir m_tmpDir;
    QString m_uploadedFile;
    QScopedPointer<QQmlComponent> m_loginComponent;
};

} // namespace

FtpUploader::FtpUploader(const UploaderProfile &profile,
                         QObject *parent):
    AbstractUploader(profile, parent),
    m_canChangeHost(true),
    m_path("/"),
    m_loginComponent(0)
{
    m_host = profile.value("host").toString();
    m_canChangeHost = m_host.isEmpty();
    m_requestedPath = profile.value("path").toString();
    setUploadsUrl(QUrl(profile.value("uploadsUrl").toString()));
    m_exifMap = profile.value("exifmap").toObject();
    m_loginOptions = profile.value("loginOptions").toObject();
    loadSettings();

    m_metadata.setEmbed(true);

    QObject::connect(&m_backend, &FtpInterface::progress,
                     [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });

    QObject::connect(&m_backend, &FtpInterface::stateChanged,
                     [this](FtpInterface::State state,
                            FtpInterface::Error error) {
        switch (state) {
        case FtpInterface::Unconnected:
            setErrorMessage(errorToText(error));
            setStatus(AbstractUploader::NeedsAuthentication); break;
        case FtpInterface::Connecting:
            setStatus(AbstractUploader::Authenticating); break;
        case FtpInterface::Connected:
            setStatus(AbstractUploader::Ready);
            setPath(m_requestedPath);
            break;
        default:
            qWarning() << "FtpInterface unhandled status" << state;
        }
    });

    QObject::connect(&m_backend, &FtpInterface::finished,
                     [this](const QString &errorMessage) {
        QFile::remove(m_uploadedFile);
        if (!errorMessage.isEmpty()) {
            qWarning() << "Got error" << errorMessage;
            setErrorMessage(errorMessage);
        }
        if (status() == AbstractUploader::Busy) {
            Q_EMIT done(errorMessage.isEmpty());
            setStatus(AbstractUploader::Ready);
        }
    });

    QObject::connect(&m_backend, &FtpInterface::dirCreated,
                     [this](bool ok) {
        if (ok) cd(m_newDir);
        Q_EMIT dirCreated(ok);
    });

    QObject::connect(&m_backend, &FtpInterface::subdirsChanged,
                     [this](const QStringList &subdirs) {
        m_subdirs = subdirs;
        Q_EMIT subdirsChanged();
    });

    QObject::connect(this, &FtpUploader::hostChanged,
                     [this]() { m_backend.logout(); });
    QObject::connect(this, &FtpUploader::pathChanged,
                     this, &FtpUploader::storeSettings);
}

void FtpUploader::setPath(const QString &path)
{
    QDir currentDir("/" + m_path);
    m_path = path;
    if (path.isEmpty() || path.endsWith('/')) {
        m_subdirs = QStringList { ".." };
        Q_EMIT subdirsChanged();
        m_backend.cd(currentDir.relativeFilePath("/" + path));
    }
    Q_EMIT pathChanged();
}

void FtpUploader::authenticate(QQuickItem *parent)
{
    const auto oldStatus = status();
    setStatus(AbstractUploader::Authenticating);
    if (m_host.isEmpty() ||
        (!m_canChangeHost && m_username.isEmpty()) ||
        (oldStatus == AbstractUploader::NeedsAuthentication &&
         !errorMessage().isEmpty())) {
        createLoginComponent(parent);
    } else {
        login();
    }
}

void FtpUploader::logout()
{
    m_backend.logout();
    m_host.clear();
    m_username.clear();
    m_password.clear();
    AbstractUploader::logout();
}

void FtpUploader::startUpload(const QString &filePath,
                              const QJsonObject &options)
{
    setStatus(AbstractUploader::Busy);

    QString fileName = options["fileName"].toString();

    m_uploadedFile = m_tmpDir.filePath(fileName);
    QFile::copy(filePath, m_uploadedFile);

    Imaginario::Metadata::Changes changes;
    if (m_exifMap.contains("title")) {
        changes.setFreeField(m_exifMap["title"].toString(),
                             options["title"].toString());
    }
    if (m_exifMap.contains("description")) {
        changes.setFreeField(m_exifMap["description"].toString(),
                             options["description"].toString());
    }
    if (m_exifMap.contains("tags")) {
        QJsonArray jsonTags = options["tags"].toArray();
        QStringList tags;
        for (const QJsonValue &value: jsonTags) {
            tags.append(value.toString());
        }
        changes.setFreeField(m_exifMap["tags"].toString(), tags);
    }
    m_metadata.writeChanges(m_uploadedFile, changes);

    QString subDirectory;
    if (replicatingFolders()) {
        subDirectory = options["subDirectory"].toString();
        if (!subDirectory.isEmpty() && !subDirectory.endsWith('/')) {
            subDirectory += '/';
        }
    }
    m_backend.upload(m_uploadedFile, subDirectory + fileName);
}

QString FtpUploader::errorToText(FtpInterface::Error code) const
{
    switch (code) {
    case FtpInterface::AuthenticationError:
        return tr("Wrong username or password");
    case FtpInterface::ConnectionRefused:
        return tr("Connection refused");
    case FtpInterface::ServerTimeout:
        return tr("The server did not reply");
    default:
        return tr("Connection error");
    }
}

void FtpUploader::setFailed(const QString &errorMessage)
{
    setErrorMessage(errorMessage);
    Q_EMIT done(false);
    setStatus(AbstractUploader::Ready);
}

void FtpUploader::setSettings(const QJsonObject &settings)
{
    AbstractUploader::setSettings(settings);

    QString host = settings.value("host").toString();
    if (!host.isEmpty() && m_host.isEmpty()) {
        m_host = host;
        Q_EMIT hostChanged();
    }

    m_username = settings.value("username").toString();
    Q_EMIT userNameChanged();

    m_password = settings.value("password").toString();
    Q_EMIT passwordChanged();

    const auto i = settings.find("path");
    if (i != settings.end()) {
        m_requestedPath = i.value().toString();
    }
}

QJsonObject FtpUploader::getSettings() const
{
    QJsonObject settings = AbstractUploader::getSettings();
    settings.insert("host", m_host);
    settings.insert("username", m_username);
    settings.insert("password", m_password);
    settings.insert("path", m_path);
    return settings;
}

void FtpUploader::cd(const QString &dir)
{
    setPath(QDir::cleanPath(m_path + dir) + "/");
}

void FtpUploader::mkdir(const QString &dir)
{
    m_newDir = dir;
    m_backend.mkdir(dir);
}

void FtpUploader::tryParseUrl(const QString &string)
{
    QUrl url = QUrl::fromUserInput(string);
    if (!url.isValid()) return;

    if (!url.host().isEmpty()) {
        m_host = url.host() +
            ((url.port() != -1) ? QString(":%1").arg(url.port()) : "");
        Q_EMIT hostChanged();
    }
    if (!url.userName().isEmpty()) {
        m_username = url.userName();
        Q_EMIT userNameChanged();
    }
    if (!url.password().isEmpty()) {
        m_password = url.password();
        Q_EMIT passwordChanged();
    }
    if (!url.path().isEmpty()) {
        m_path = url.path();
        Q_EMIT pathChanged();
    }

    if (url.scheme() == "ftp" || url.port() == 21) {
        m_backend.setPreferredBackend(BackendSelector::BackendFtp);
    } else {
        m_backend.setPreferredBackend(BackendSelector::BackendScp);
    }
}

void FtpUploader::login()
{
    QString host = m_host;
    int16_t port = 22;
    QUrl url = QUrl::fromUserInput(m_host);
    if (url.isValid()) {
        host = url.host();
        if (url.scheme() == "ftp") {
            m_backend.setPreferredBackend(BackendSelector::BackendFtp);
            port = url.port(21);
        } else {
            m_backend.setPreferredBackend(BackendSelector::BackendScp);
            port = url.port(22);
        }
    } else {
        qDebug() << "Host invalid, connecting anyway to" << host;
    }
    m_backend.login(host, port, m_username, m_password, m_loginOptions);

    if (m_canChangeHost) {
        setUserId(m_username + "@" + m_host);
    } else {
        setUserId(m_username);
    }
    storeSettings();
}

void FtpUploader::createLoginComponent(QQuickItem *parent)
{
    Q_ASSERT(parent);
    QQmlContext *context = QQmlEngine::contextForObject(parent);
    QQmlEngine *engine = context->engine();
    if (!m_loginComponent) {
        QUrl url("qrc:/ftp/LoginPanel.qml");
        m_loginComponent.reset(new QQmlComponent(engine, url));
    }

    QObject *object = m_loginComponent->create(context);
    Q_ASSERT(object);
    object->setProperty("authenticator", QVariant::fromValue(this));
    QQuickItem *item = qobject_cast<QQuickItem*>(object);
    Q_ASSERT(item);
    item->setParentItem(parent);
    item->setParent(parent);
    QObject::connect(item, &QQuickItem::parentChanged,
                     item, [this, item]() {
        item->deleteLater();
        QString message = item->property("errorMessage").toString();
        if (!message.isEmpty()) {
            setFailed(message);
            setStatus(AbstractUploader::NeedsAuthentication);
        }
    });
}

Photokinesis::AbstractUploader *
FtpPlugin::createUploader(const UploaderProfile &profile,
                          QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new FtpUploader(profile, parent);
}

#include "uploader.moc"
