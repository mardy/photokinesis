/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_FTP_INTERFACE
#define PHOTOKINESIS_FTP_INTERFACE

#include <QObject>
#include <QString>

class QJsonObject;

namespace Photokinesis {

class FtpInterface: public QObject
{
    Q_OBJECT

public:
    enum State { Unconnected, Connecting, Connected };
    enum Error {
        NoError = 0,
        AuthenticationError,
        ConnectionRefused,
        ServerTimeout,
    };

    virtual void login(const QString &host,
                       uint16_t port,
                       const QString &username,
                       const QString &password,
                       const QJsonObject &options) = 0;
    virtual void cd(const QString &dirName) = 0;
    virtual void mkdir(const QString &dirName) = 0;
    virtual void upload(const QString &localFile, const QString &filePath) = 0;
    virtual void logout() = 0;

Q_SIGNALS:
    void subdirsChanged(const QStringList &subdirs);
    void dirCreated(bool ok);
    void progress(qint64 sent, qint64 total);
    void stateChanged(State state, Error error = NoError);
    void finished(const QString &errorMessage);
};

} // namespace

#endif // PHOTOKINESIS_FTP_INTERFACE
