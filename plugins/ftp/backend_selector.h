/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_BACKEND_SELECTOR
#define PHOTOKINESIS_BACKEND_SELECTOR

#include "ftp_interface.h"

#include <QScopedPointer>

namespace Photokinesis {

class BackendSelectorPrivate;
class BackendSelector: public FtpInterface
{
    Q_OBJECT

public:
    enum Backend {
        BackendScp = 0,
        BackendFtp,
        BackendCount,
    };

    BackendSelector();
    ~BackendSelector();

    void setPreferredBackend(Backend backend);

    void login(const QString &host,
               uint16_t port,
               const QString &username,
               const QString &password,
               const QJsonObject &options) override;
    void cd(const QString &dirName) override;
    void mkdir(const QString &dirName) override;
    void upload(const QString &localFile, const QString &filePath) override;
    void logout() override;

private:
    Q_DECLARE_PRIVATE(BackendSelector);
    QScopedPointer<BackendSelectorPrivate> d_ptr;
};

} // namespace

#endif // PHOTOKINESIS_BACKEND_SELECTOR
