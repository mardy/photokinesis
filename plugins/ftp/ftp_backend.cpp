/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ftp_backend.h"

#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QJsonObject>
#include <QJsonValue>
#include <QTimer>

using namespace Photokinesis;

FtpBackend::FtpBackend():
    m_subdirsChangedQueued(false)
{
    QObject::connect(&m_ftp, &QFtp::dataTransferProgress,
                     this, &FtpInterface::progress);

    QObject::connect(&m_ftp, &QFtp::stateChanged,
                     [this](int) {
        State state = FtpInterface::Unconnected;
        switch (m_ftp.state()) {
        case QFtp::Unconnected:
            state = FtpInterface::Unconnected; break;
        case QFtp::HostLookup:
        case QFtp::Connecting:
        case QFtp::Connected:
            state = FtpInterface::Connecting; break;
        case QFtp::LoggedIn:
            state = FtpInterface::Connected; break;
        default:
            qWarning() << "QFtp unhandled status" << m_ftp.state();
        }
        Q_EMIT stateChanged(state);
    });

    QObject::connect(&m_ftp, &QFtp::commandFinished,
                     [this](int, bool error) {
        if (m_changingPathConnection) return;
        if (error) {
            Q_EMIT finished(m_ftp.errorString());
            if (m_ftp.currentCommand() == QFtp::Login) {
                Q_EMIT stateChanged(Unconnected);
            }
        }
        if (m_ftp.currentCommand() == QFtp::Put) {
            QString msg = error ?
                (QString("Upload error: ") + m_ftp.errorString()) :
                QString();
            Q_EMIT finished(msg);
            QIODevice *file = m_ftp.currentDevice();
            if (file) {
                file->deleteLater();
            }
        } else if (m_ftp.currentCommand() == QFtp::Mkdir) {
            Q_EMIT dirCreated(!error);
        }
    });

    QObject::connect(&m_ftp, &QFtp::listInfo,
                     [this](const QUrlInfo &urlInfo) {
        if (!urlInfo.isDir()) return;
        m_subdirs.append(urlInfo.name());
        if (!m_subdirsChangedQueued) {
            m_subdirsChangedQueued = true;
            QTimer::singleShot(0, this, [this]() {
                Q_EMIT subdirsChanged(m_subdirs);
            });
        }
    });
}

FtpBackend::~FtpBackend()
{
}

void FtpBackend::login(const QString &host,
                       uint16_t port,
                       const QString &username,
                       const QString &password,
                       const QJsonObject &options)
{
    qDebug() << "Connecting to" << host << port << options;
    m_ftp.connectToHost(host, port);
    const QString ftpAuth = options.value("ftpAuth").toString();
    if (!ftpAuth.isEmpty()) {
        qDebug() << "Using AUTH" << ftpAuth;
        m_ftp.auth(ftpAuth);
    }
    m_ftp.login(username, password);
}

void FtpBackend::mkdir(const QString &dirName)
{
    m_ftp.mkdir(dirName);
}

void FtpBackend::upload(const QString &localFile, const QString &filePath)
{
    QFileInfo fileInfo(filePath);
    changePath(fileInfo.path(), [this,localFile,fileInfo](bool success) {
        if (!success) {
            Q_EMIT finished(m_ftp.errorString());
            return;
        }

        QFile *file = new QFile(localFile, this);
        file->open(QIODevice::ReadOnly);
        m_ftp.put(file, fileInfo.fileName());
    });
}

void FtpBackend::logout()
{
    m_ftp.close();
}

void FtpBackend::cd(const QString &dirName)
{
    m_subdirs = QStringList { ".." };
    m_subdirsChangedQueued = false;
    m_ftp.cd(dirName);
    m_ftp.list();
    Q_EMIT subdirsChanged(m_subdirs);
}

void FtpBackend::changePath(const QString &newPath,
                            const std::function<void(bool)> &callback)
{
    qDebug() << Q_FUNC_INFO << newPath << "from" << m_currentPathComponents;

    QStringList newPathComponents =
        newPath.split('/', QString::SkipEmptyParts);
    newPathComponents.removeAll(".");
    int commonParts = 0;
    for (int i = 0; i < newPathComponents.count(); i++) {
        if (m_currentPathComponents.value(i) == newPathComponents.at(i)) {
            commonParts++;
        } else {
            break;
        }
    }

    int cdUpCount = m_currentPathComponents.count() - commonParts;
    int cdCount = newPathComponents.count() - commonParts;

    if (cdUpCount == 0 && cdCount == 0) {
        /* nothing to do */
        callback(true);
        return;
    }

    /* The m_pathComponentsPerCommand dictionary maps a command ID to the path
     * that will be the current one in case that command succeeds. */
    for (int i = 0; i < cdUpCount; i++) {
        int commandId = m_ftp.cd("..");
        m_pathComponentsPerCommand[commandId] =
            m_currentPathComponents.mid(0, cdUpCount - 1 - i);
    }

    for (int i = commonParts; i < newPathComponents.count(); i++) {
        int commandId = m_ftp.cd(newPathComponents[i]);
        m_missingComponentsPerCommand[commandId] = newPathComponents.mid(i);
        m_pathComponentsPerCommand[commandId] =
            newPathComponents.mid(0, i + 1);
    }

    m_changingPathConnection =
        QObject::connect(&m_ftp, &QFtp::commandFinished,
                         [this,newPathComponents, callback](int cmdId, bool error) {
        qDebug() << "command finished in func" << cmdId << "error:" << error;
        bool done = false;
        if (error) {
            if (m_missingComponentsPerCommand.contains(cmdId)) {
                const QStringList &missingComponents =
                    m_missingComponentsPerCommand.value(cmdId);
                QStringList reachedComponents = m_currentPathComponents;
                for (const QString &component: missingComponents) {
                    m_ftp.mkdir(component);
                    int commandId = m_ftp.cd(component);
                    reachedComponents.append(component);
                    m_pathComponentsPerCommand[commandId] = reachedComponents;
                }
                return;
            } else {
                qWarning() << "failed in a cdUp?";
                done = true;
            }
        } else {
            m_currentPathComponents = m_pathComponentsPerCommand.value(cmdId);
            qDebug() << "current path:" << m_currentPathComponents;
            done = m_currentPathComponents == newPathComponents;
        }
        if (done) {
            callback(!error);
            QObject::disconnect(m_changingPathConnection);
            m_changingPathConnection = QMetaObject::Connection();
            m_missingComponentsPerCommand.clear();
            m_pathComponentsPerCommand.clear();
        }
    });
}
