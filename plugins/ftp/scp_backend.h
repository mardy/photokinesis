/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PHOTOKINESIS_SCP_BACKEND
#define PHOTOKINESIS_SCP_BACKEND

#include "ftp_interface.h"

#include <QDir>
#include <QHash>
#include <QScopedPointer>
#include <QSet>
#include <QSharedPointer>
#include <functional>
#include "sftpchannel.h"
#include "sshconnection.h"

namespace Photokinesis {

class ScpBackend: public FtpInterface
{
    Q_OBJECT

public:
    ScpBackend();
    ~ScpBackend();

    void login(const QString &host,
               uint16_t port,
               const QString &username,
               const QString &password,
               const QJsonObject &options) override;
    void cd(const QString &dirName) override;
    void mkdir(const QString &dirName) override;
    void upload(const QString &localFile, const QString &fileName) override;
    void logout() override;

private:
    State state() const;
    void onConnected();
    typedef std::function<void(const QString &)> FinishedCb;
    typedef std::function<void(const QStringList &)> ListCb;
    typedef std::function<void(quint64, quint64)> ProgressCb;
    typedef std::function<void(const QString &error)> DirectoryCreatedCb;

    void onFinished(QSsh::SftpJobId, FinishedCb);
    void onListAvailable(QSsh::SftpJobId, ListCb);
    void onProgress(QSsh::SftpJobId, ProgressCb);
    void onDirectoryCreated(const QString &, const DirectoryCreatedCb &);

private:
    QScopedPointer<QSsh::SshConnection> m_connection;
    QSharedPointer<QSsh::SftpChannel> m_channel;
    QString m_currentDir;
    QStringList m_subdirs;
    QHash<QSsh::SftpJobId,FinishedCb> m_finishedCb;
    QHash<QSsh::SftpJobId,ListCb> m_listCb;
    QHash<QSsh::SftpJobId,ProgressCb> m_progressCb;
    QSet<QString> m_knownDirectories;
    QMetaObject::Connection m_changingPathConnection;
    QHash<QSsh::SftpJobId,QStringList> m_missingComponentsPerJob;
    QHash<QSsh::SftpJobId,QString> m_directoryPerJob;
};

} // namespace

#endif // PHOTOKINESIS_SCP_BACKEND
