/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/OAuth2>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

using namespace Photokinesis;

namespace Photokinesis {

static const QString baseUrl = QStringLiteral("https://www.deviantart.com/api/v1/oauth2/");

class DeviantartReply: public QObject
{
    Q_OBJECT

public:
    DeviantartReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            qDebug() << "raw reply:" << replyText;
            m_json = QJsonDocument::fromJson(replyText).object();
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &DeviantartReply::finished);
    }

    QJsonObject response() const { return m_json; }

    bool isError() const { return m_json.contains("error"); }
    QString errorMessage() const {
        return m_json.value("error_description").toString();
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QScopedPointer<QNetworkReply> m_reply;
};

class DeviantartUploader: public AbstractUploader
{
    Q_OBJECT

public:
    DeviantartUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;
    void setSettings(const QJsonObject &settings) override;
    QJsonObject getSettings() const override;

    void publish(qint64 itemId);
    DeviantartReply *getProfileInfo();
    DeviantartReply *getAlbums();

    void initialize();

    DeviantartReply *call(const QString &method,
                          const QVariantMap &params = QVariantMap());
    void setFailed(const QString &errorMessage);

    Q_INVOKABLE void createAlbum(const QJsonObject &creationData);

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth2 m_auth;
    QQuickItem *m_parentItem;
    QJsonObject m_profileInfo;
};

} // namespace

DeviantartUploader::DeviantartUploader(const UploaderProfile &profile,
                                       QObject *parent):
    AbstractUploader(profile, parent),
    m_parentItem(0)
{
    m_auth.setClientId("6488");
    m_auth.setClientSecret("bd01fedfec41a1260a729dd912ba62ab");
    m_auth.setScopes({ "stash", "browse", "publish", "gallery" });
    m_auth.setAuthorizationUrl(QUrl("https://www.deviantart.com/oauth2/authorize"));
    m_auth.setAccessTokenUrl(QUrl("https://www.deviantart.com/oauth2/token"));
    m_auth.setResponseType("code");
    m_auth.setCallbackUrl("https://www.mardy.it/oauth/teleport");
    m_auth.setUserAgent("Photokinesis/1.0");

    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));
}

void DeviantartUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    UiHelpers::createWebview(parent, &m_auth);
    m_auth.process();
}

void DeviantartUploader::logout()
{
    m_auth.logout();

    QJsonArray cookies {
        QJsonArray { "_gat", ".deviantart.com", "/" },
        QJsonArray { "__qca", ".deviantart.com", "/" },
        QJsonArray { "_pxvid", ".deviantart.com", "/" },
        QJsonArray { "userinfo", ".deviantart.com", "/" },
        QJsonArray { "_ga", ".deviantart.com", "/" },
        QJsonArray { "_gid", ".deviantart.com", "/" },
        QJsonArray { "_px", ".deviantart.com", "/" },
    };
    QObject *deleter =
        UiHelpers::clearCookies(this, QUrl("https://www.deviantart.com"),
                                cookies);
    QObject::connect(deleter, &QObject::destroyed,
                     this, [this]() { AbstractUploader::logout(); });
}

void DeviantartUploader::startUpload(const QString &filePath,
                                     const QJsonObject &options)
{
    QString title = options["title"].toString();

    QFileInfo fileInfo(filePath);

    QUrl uploadUrl("https://www.deviantart.com/api/v1/oauth2/stash/submit");
    QVariantMap params {
        { "image", QVariantMap {
                { "filePath", fileInfo.filePath() },
                { "fileName", fileInfo.fileName() },
            }
        },
        { "title", title },
    };
    QJsonArray tagArray = options["tags"].toArray();
    int i = 0;
    for (const QJsonValue &v: tagArray) {
        params.insert(QString("tags[%1]").arg(i++), v.toString());
    }
    QNetworkReply *networkReply = m_auth.post(uploadUrl, params);

    DeviantartReply *reply = new DeviantartReply(networkReply);
    QObject::connect(networkReply, &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &DeviantartReply::finished,
                     this, [this, reply]() {
        if (Q_UNLIKELY(reply->isError())) {
            setFailed(reply->errorMessage());
        } else {
            qint64 itemId(reply->response().value("itemid").toDouble());
            publish(itemId);
        }
    });
}

void DeviantartUploader::publish(qint64 itemId)
{
    QVariantMap params {
        { "itemid", itemId },
        { "agree_submission", true },
        { "agree_tos", true },
        { "is_mature", false },
    };

    if (!defaultAlbumId().isEmpty()) {
        params.insert("galleryids[0]", defaultAlbumId());
    }

    DeviantartReply *reply = call("stash/publish", params);
    QObject::connect(reply, &DeviantartReply::finished,
                     this, [this, reply]() {
        if (Q_UNLIKELY(reply->isError())) {
            setFailed(reply->errorMessage());
        } else {
            setUploadsUrl(QUrl("https://" + userId() + ".deviantart.com/"
                               "gallery/"));
            Q_EMIT done(true);
            setStatus(AbstractUploader::Ready);
        }
    });
}

DeviantartReply *DeviantartUploader::getProfileInfo()
{
    QVariantMap params {
        { "expand", "user.stats" },
    };
    DeviantartReply *reply = call(QStringLiteral("user/whoami"), params);
    QObject::connect(reply, &DeviantartReply::finished, this, [this, reply]() {
        m_profileInfo = reply->response();
    });
    return reply;
}

DeviantartReply *DeviantartUploader::getAlbums()
{
    QVariantMap params;
    DeviantartReply *reply = call(QStringLiteral("gallery/folders"), params);
    QObject::connect(reply, &DeviantartReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums =
            reply->response().value("results").toArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            QJsonObject a;
            a.insert("name", s["name"]);
            a.insert("albumId", s["folderid"].toString());
            albums.append(a);
        }

        if (defaultAlbumId().isEmpty() && !albums.isEmpty()) {
            setDefaultAlbumId(albums.first().toObject().value("albumId").toString());
        }
        setAlbums(albums);
    });
    return reply;
}

DeviantartReply *DeviantartUploader::call(const QString &method,
                                          const QVariantMap &params)
{
    QUrl url(baseUrl + method);
    return new DeviantartReply(m_auth.get(url, params));
}

void DeviantartUploader::setFailed(const QString &errorMessage)
{
    setErrorMessage(errorMessage);
    Q_EMIT done(false);
    setStatus(AbstractUploader::Ready);
}

void DeviantartUploader::initialize()
{
    loadSettings();

    setStatus(AbstractUploader::RetrievingAccountInfo);

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                     this, [this, waiter]() {
        setUserId(m_profileInfo.value("username").toString());
        QJsonObject stats = m_profileInfo.value("stats").toObject();
        setAccountInfo(tr("Watchers: %1\nFriends: %2").
                       arg(stats.value("watchers").toInt()).
                       arg(stats.value("friends").toInt()));
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
        waiter->deleteLater();
    });

    waiter->add(getProfileInfo());
    waiter->add(getAlbums());
}

void DeviantartUploader::setSettings(const QJsonObject &settings)
{
    AbstractUploader::setSettings(settings);
}

QJsonObject DeviantartUploader::getSettings() const
{
    QJsonObject settings = AbstractUploader::getSettings();
    return settings;
}

void DeviantartUploader::onAuthenticationFinished()
{
    if (!m_auth.hasError()) {
        initialize();
    } else {
        qWarning() << "Authentication error:" << m_auth.errorMessage();
        setErrorMessage(m_auth.errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

void DeviantartUploader::createAlbum(const QJsonObject &creationData)
{
    QVariantMap params {
        { "folder", creationData["title"].toString() },
    };
    m_auth.setAuthorizationTransmission(Authentication::OAuth2::ParametersInBody);
    QUrl endpoint(baseUrl + "gallery/folders/create");
    DeviantartReply *reply =
        new DeviantartReply(m_auth.post(endpoint, params));
    QObject::connect(reply, &DeviantartReply::finished,
                     this, [this, reply]() {
        m_auth.setAuthorizationTransmission(Authentication::OAuth2::ParametersInQuery);

        if (Q_UNLIKELY(reply->isError())) {
            qWarning() << "Got error" << reply->errorMessage();
            setErrorMessage(reply->errorMessage());
            return;
        }
        const QJsonObject s = reply->response();
        QJsonArray albums = this->albums();
        QJsonObject a {
            { "name", s["name"] },
            { "albumId", s["folderid"] },
        };
        albums.append(a);

        setDefaultAlbumId(a.value("albumId").toString());
        setAlbums(albums);
    });
}

Photokinesis::AbstractUploader *
DeviantartPlugin::createUploader(const UploaderProfile &profile,
                                 QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new DeviantartUploader(profile, parent);
}

#include "uploader.moc"
