import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var creationData: null

    title: qsTr("Create new album")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    GridLayout {
        id: internal
        columns: 2

        Label {
            text: qsTr("Album name:")
        }

        TextField {
            id: titleField
            Layout.fillWidth: true
        }
    }

    onAccepted: {
        // TODO: validate!
        var data = {
            "title": titleField.text,
        }
        root.creationData = data
    }
}
