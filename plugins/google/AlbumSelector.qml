import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

RowLayout {
    id: root
    Layout.fillWidth: true

    property var albums: computeAlbums()

    Label {
        text: qsTr("Destination album:")
    }
    ComboBox {
        id: comboBox
        Layout.fillWidth: true
        model: root.albums
        textRole: "name"
        onActivated: {
            var id = root.albums[index].albumId
            if (index < root.albums.length - 1) {
                uploader.defaultAlbumId = id
            } else {
                var item = PopupUtils.open(Qt.resolvedUrl("NewAlbumDialog.qml"), root)
                item.onCreationDataChanged.connect(function () {
                    uploader.createAlbum(item.creationData)
                })
                item.onRejected.connect(function () {
                    currentIndex = 0
                    uploader.defaultAlbumId = ""
                })
            }
        }

        onModelChanged: selectDefault()

        Connections {
            target: uploader
            onDefaultAlbumIdChanged: comboBox.selectDefault()
        }

        function selectDefault() {
            var defaultAlbumId = uploader.defaultAlbumId
            if (!defaultAlbumId) return

            for (var i = 0; i < model.length; i++) {
                if (model[i].albumId == defaultAlbumId) {
                    currentIndex = i
                    break
                }
            }
        }
    }

    function computeAlbums() {
        var albums = uploader.albums
        albums.unshift({
            "name": qsTr("No album, post to photostream"),
            "albumId": ""
        })
        albums.push({
            "name": qsTr("Add new album…"),
            "albumId": ""
        })
        return albums
    }
}
