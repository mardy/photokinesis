/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/LoopbackServer>
#include <Authentication/OAuth2>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <QByteArray>
#include <QDebug>
#include <QDesktopServices>
#include <QFileInfo>
#include <QHttpMultiPart>
#include <QHttpPart>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <QUrlQuery>

using namespace Photokinesis;

static const QString baseUrl =
    QStringLiteral("https://photoslibrary.googleapis.com/v1/");

namespace Photokinesis {

class GoogleReply: public QObject
{
    Q_OBJECT

public:
    GoogleReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            qDebug() << "raw reply:" << replyText;
            m_json = QJsonDocument::fromJson(replyText).object();
            if (m_json.isEmpty()) {
                m_errorMessage = replyText;
            }
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &GoogleReply::finished);
    }

    QNetworkReply *networkReply() const { return m_reply.data(); }
    QJsonObject response() const { return m_json; }

    bool isError() const { return !m_errorMessage.isEmpty(); }
    QString errorMessage() const { return m_errorMessage; }
    bool needsAuthentication() const {
        // TODO
        return false;
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QString m_errorMessage;
    QScopedPointer<QNetworkReply> m_reply;
};

class GoogleUploader: public AbstractUploader
{
    Q_OBJECT

public:
    GoogleUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;

    void onActionRequested(const Authentication::ActionRequest &req);

    void createMediaItem(const QByteArray &uploadToken,
                         const QString &description);
    QJsonObject findAlbum(const QString &id) const;

    GoogleReply *call(const QString &method,
                      const QVariantMap &params = QVariantMap());
    GoogleReply *callOAuth(const QString &method,
                           const QVariantMap &params = QVariantMap());
    GoogleReply *getProfileInfo();
    GoogleReply *getAlbums();

    void initialize();
    bool parseProfile(const QJsonObject &profile);
    bool checkAuthentication(GoogleReply *reply);

    Q_INVOKABLE void createAlbum(const QJsonObject &creationData);

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth2 m_auth;
    Authentication::LoopbackServer m_httpServer;
    Authentication::ActionRequest m_oauthRequest;
    QJsonObject m_profileInfo;
};

} // namespace

GoogleUploader::GoogleUploader(const UploaderProfile &profile,
                               QObject *parent):
    AbstractUploader(profile, parent)
{
    setUsesBrowser(true);

    QFile html(":html/authenticated.html");
    if (html.open(QIODevice::ReadOnly)) {
        m_httpServer.setServedHtml(html.readAll());
    }

    m_auth.setClientId("261797508896-q7cpfhe5ls5d88503hi7e7kgogv10jml.apps.googleusercontent.com");
    m_auth.setClientSecret("Zh-6ExXTSXvtUlZe6lFgXcHn");
    m_auth.setScopes({
        "https://www.googleapis.com/auth/photoslibrary",
        "https://www.googleapis.com/auth/userinfo.profile",
    });
    m_auth.setAuthorizationUrl(QUrl("https://accounts.google.com/o/oauth2/"
                                    "v2/auth?access_type=offline"));
    m_auth.setAccessTokenUrl(QUrl("https://www.googleapis.com/oauth2/v4/token"));
    m_auth.setResponseType("code");
    m_auth.setUserAgent("Photokinesis/1.0");
    m_auth.setProtocolTweaks(Authentication::OAuth2::ClientAuthInRequestBody);

    QObject::connect(&m_auth, &Authentication::OAuth2::actionRequested,
                     this, &GoogleUploader::onActionRequested);
    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));
}

void GoogleUploader::authenticate(QQuickItem *)
{
    setStatus(AbstractUploader::Authenticating);
    if (Q_UNLIKELY(!m_httpServer.listen())) {
        setErrorMessage("Couldn't start loopback server");
        setStatus(AbstractUploader::NeedsAuthentication);
        return;
    }

    QObject::connect(&m_httpServer, &Authentication::LoopbackServer::visited,
                     this, [this](const QUrl &url) {
        qDebug() << "Setting result url:" << url;
        m_oauthRequest.setResult(url);
        m_httpServer.disconnect(this);
    });

    m_auth.setCallbackUrl(m_httpServer.callbackUrl().toString());
    m_auth.process();
}

void GoogleUploader::logout()
{
    m_auth.logout();
    QUrl url = m_auth.authorizationUrl();
    QUrlQuery query(url);
    if (!query.hasQueryItem("prompt")) {
        query.addQueryItem("prompt", "select_account");
        url.setQuery(query);
        m_auth.setAuthorizationUrl(url);
    }
    AbstractUploader::logout();
}

void GoogleUploader::startUpload(const QString &filePath, const QJsonObject &options)
{
    setStatus(AbstractUploader::Busy);

    QString title = options["title"].toString();
    QString description = options["description"].toString();
    QStringList tags;
    QJsonArray tagArray = options["tags"].toArray();
    for (const QJsonValue &v: tagArray) {
        tags.append('#' + v.toString());
    }

    /* The only metadata really supported by Google Photos is a description,
     * up to 1000 characters long.
     */
    QString caption;
    if (!title.isEmpty()) {
        caption = title + "\n\n";
    }
    if (!description.isEmpty()) {
        caption += description + "\n\n";
    }
    if (!tags.isEmpty()) {
        caption += tags.join(' ');
    }

    QFileInfo fileInfo(filePath);

    QUrl endpoint(baseUrl + "uploads");
    QNetworkRequest req = m_auth.authorizedRequest("POST", endpoint);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/octet-stream");
    req.setRawHeader("X-Goog-Upload-File-Name", fileInfo.fileName().toUtf8());
    req.setRawHeader("X-Goog-Upload-Protocol", "raw");

    QFile *file = new QFile(fileInfo.filePath(), this);
    file->open(QIODevice::ReadOnly);

    qDebug() << "Uploading to" << endpoint;
    QNetworkAccessManager *nam = m_auth.networkAccessManager();
    QNetworkReply *reply = nam->post(req, file);
    file->setParent(reply);
    QObject::connect(reply, &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &QNetworkReply::finished,
                     this, [this, reply, caption]() {
        reply->deleteLater();
        QByteArray uploadToken = reply->readAll();
        if (uploadToken.isEmpty()) {
            qWarning() << "Didn't get an upload token";
            setErrorMessage("Photo upload failed");
            Q_EMIT done(false);
            setStatus(AbstractUploader::Ready);
            return;
        }

        createMediaItem(uploadToken, caption);
    });
}

void GoogleUploader::onActionRequested(const Authentication::ActionRequest &req)
{
    m_oauthRequest = req;
    bool ok = QDesktopServices::openUrl(req.url());
    if (Q_UNLIKELY(!ok)) {
        qWarning() << "Could not open URL:" << req.url();
        setErrorMessage("Could not open web browser");
        setStatus(AbstractUploader::NeedsAuthentication);
        return;
    }
}

void GoogleUploader::createMediaItem(const QByteArray &uploadToken,
                                     const QString &description)
{
    QJsonObject data {
        { "newMediaItems", QJsonArray {
                QJsonObject {
                    { "description", description },
                    { "simpleMediaItem", QJsonObject {
                            { "uploadToken", QString::fromUtf8(uploadToken) },
                        }
                    },
                },
            }
        },
    };
    if (!defaultAlbumId().isEmpty()) {
        qDebug() << "Adding albumId" << defaultAlbumId();
        data.insert(QStringLiteral("albumId"), defaultAlbumId());
    }

    QUrl url(baseUrl + "mediaItems:batchCreate");
    QNetworkRequest req = m_auth.authorizedRequest("POST", url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    req.setRawHeader("Accept", "application/json");
    QByteArray body = QJsonDocument(data).toJson(QJsonDocument::Compact);

    QNetworkReply *networkReply =
        m_auth.networkAccessManager()->post(req, body);
    GoogleReply *reply = new GoogleReply(networkReply);
    QObject::connect(reply, &GoogleReply::finished,
                     this, [this, reply, uploadToken]() {
        const QJsonObject s = reply->response();
        const QJsonArray results =
            s[QStringLiteral("newMediaItemResults")].toArray();

        QJsonObject result;
        for (const QJsonValue &v: results) {
            const QJsonObject r = v.toObject();
            if (r["uploadToken"].toString() == uploadToken) {
                result = r;
                break;
            }
            qWarning() << "Received result for other item:" << result;
        }

        const QJsonObject status = result["status"].toObject();
        if (status.isEmpty() || status["code"].toInt() != 0) {
            setErrorMessage(status["message"].toString());
            Q_EMIT done(false);
        } else {
            if (defaultAlbumId().isEmpty()) {
                setUploadsUrl(QUrl("https://photos.google.com"));
            } else {
                setUploadsUrl(QUrl(findAlbum(defaultAlbumId())["url"].toString()));
            }
            Q_EMIT done(true);
        }
        setStatus(AbstractUploader::Ready);
    });
}

QJsonObject GoogleUploader::findAlbum(const QString &id) const
{
    QJsonArray albums = this->albums();
    for (const QJsonValue &v: albums) {
        QJsonObject o = v.toObject();
        if (o.value("albumId").toString() == id) {
            return o;
        }
    }
    return QJsonObject();
}

GoogleReply *GoogleUploader::call(const QString &method, const QVariantMap &params)
{
    QUrl url(baseUrl + method);
    return new GoogleReply(m_auth.get(url, params));
}

GoogleReply *GoogleUploader::callOAuth(const QString &method,
                                       const QVariantMap &params)
{
    QUrl url("https://www.googleapis.com/oauth2/v3/" + method);
    return new GoogleReply(m_auth.get(url, params));
}

GoogleReply *GoogleUploader::getProfileInfo()
{
    GoogleReply *reply = callOAuth(QStringLiteral("userinfo"));
    QObject::connect(reply, &GoogleReply::finished, this, [this, reply]() {
        checkAuthentication(reply);
        m_profileInfo = reply->response();
    });
    return reply;
}

GoogleReply *GoogleUploader::getAlbums()
{
    QVariantMap params {
        { "pageSize", 50 },
    };
    GoogleReply *reply = call(QStringLiteral("albums"), params);
    QObject::connect(reply, &GoogleReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums = reply->response().value("albums").toArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            QJsonObject a;
            a.insert("name", s["title"].toString());
            a.insert("albumId", s["id"].toString());
            a.insert("url", s["productUrl"].toString());
            if (!s["isWriteable"].toBool()) {
                qDebug() << "Skipping non writable:" << s;
                continue;
            }
            albums.append(a);
        }

        if (defaultAlbumId().isEmpty() && !albums.isEmpty()) {
            setDefaultAlbumId(albums.first().toObject().value("albumId").toString());
        }
        setAlbums(albums);
    });
    return reply;
}

void GoogleUploader::initialize()
{
    setStatus(AbstractUploader::RetrievingAccountInfo);

    QByteArray rawIdToken =
        m_auth.extraFields().value("id_token").toString().toUtf8();
    QList<QByteArray> parts = rawIdToken.split('.');
    QByteArray idToken = parts.count() > 1 ?
        QByteArray::fromBase64(parts[1]) : QByteArray();

    m_profileInfo = QJsonDocument::fromJson(idToken).object();

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                    this, [this, waiter]() {
        waiter->deleteLater();
        if (status() == AbstractUploader::NeedsAuthentication) return;

        parseProfile(m_profileInfo);

        loadSettings();
        if (!defaultAlbumId().isEmpty()) {
            // check whether it's valid
            if (findAlbum(defaultAlbumId()).isEmpty()) {
                setDefaultAlbumId(QString());
            }
        }

        /* TODO: find something meaningful for accountInfo */
        setAccountInfo(QString());
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
    });

    waiter->add(getAlbums());
    if (!parseProfile(m_profileInfo)) {
        waiter->add(getProfileInfo());
    }
}

bool GoogleUploader::parseProfile(const QJsonObject &profile)
{
    QString sub = profile.value(QStringLiteral("sub")).toString();
    QString name = profile.value(QStringLiteral("name")).toString();

    setUserId(sub);
    if (!name.isEmpty()) {
        setUserName(name);
        return true;
    } else {
        setUserName(sub);
        return false;
    }
}

bool GoogleUploader::checkAuthentication(GoogleReply *reply)
{
    if (reply->isError() && reply->needsAuthentication()) {
        qDebug() << "Needs authentication!";
        m_auth.logout();
        setStatus(AbstractUploader::NeedsAuthentication);
        return false;
    }
    return true;
}

void GoogleUploader::onAuthenticationFinished()
{
    m_httpServer.close();
    if (!m_auth.hasError()) {
        initialize();
    } else {
        QString message = m_auth.errorMessage();
        qWarning() << "Authentication error:" << message;
        setErrorMessage(message.isEmpty() ?
                        "Internal authentication error" : message);
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

void GoogleUploader::createAlbum(const QJsonObject &creationData)
{
    QJsonObject data {
        { "album", creationData },
    };

    QUrl url(baseUrl + "albums");
    QNetworkRequest req = m_auth.authorizedRequest("POST", url);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    req.setRawHeader("Accept", "application/json");
    QByteArray body = QJsonDocument(data).toJson(QJsonDocument::Compact);

    QNetworkReply *networkReply =
        m_auth.networkAccessManager()->post(req, body);
    GoogleReply *reply = new GoogleReply(networkReply);
    QObject::connect(reply, &GoogleReply::finished,
                     this, [this, reply]() {
        const QJsonObject s = reply->response();
        QJsonArray albums = this->albums();
        QJsonObject a;
        a.insert("name", s["title"]);
        a.insert("albumId", s["id"].toString());
        a.insert("url", s["productUrl"].toString());
        albums.append(a);

        setDefaultAlbumId(a.value("albumId").toString());
        setAlbums(albums);
    });
}

Photokinesis::AbstractUploader *
GooglePlugin::createUploader(const UploaderProfile &profile,
                             QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new GoogleUploader(profile, parent);
}

#include "uploader.moc"
