/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/OAuth2>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QByteArray>
#include <QDebug>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

using namespace Photokinesis;

static const QString baseUrl = QStringLiteral("https://api.vk.com/method/");
static const QString defaultVersion = QStringLiteral("5.131");

namespace Photokinesis {

class VkReply: public QObject
{
    Q_OBJECT

public:
    VkReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            qDebug() << "raw reply:" << replyText;
            m_json = QJsonDocument::fromJson(replyText).object();
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &VkReply::finished);
    }

    QJsonObject response() const { return m_json.value("response").toObject(); }
    QJsonArray responseArray() const {
        return m_json.value("response").toArray();
    }

    bool isError() const { return m_json.contains("error"); }
    QString errorMessage() const {
        return m_json.value("error").toObject().value("error_msg").toString();
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QScopedPointer<QNetworkReply> m_reply;
};

class VkUploader: public AbstractUploader
{
    Q_OBJECT
    Q_PROPERTY(QJsonArray groups MEMBER m_groups NOTIFY groupsChanged)
    Q_PROPERTY(QString groupId MEMBER m_groupId NOTIFY groupIdChanged)

public:
    VkUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;
    void setSettings(const QJsonObject &settings) override;
    QJsonObject getSettings() const override;

    QJsonObject findAlbum(const QString &id) const;

    QString ownerId() const;
    VkReply *call(const QString &method,
                  const QVariantMap &params = QVariantMap());
    VkReply *getProfileInfo();
    VkReply *getAlbums();
    VkReply *getGroups();
    QNetworkReply *uploadPhoto(const QUrl &uploadUrl, const QString &filePath,
                               const QJsonObject &options);
    VkReply *postToWall(int ownerId, int photoId);
    bool checkUploadError(VkReply *reply);

    void initialize();

    Q_INVOKABLE void createAlbum(const QJsonObject &creationData);

Q_SIGNALS:
    void groupsChanged();
    void groupIdChanged();

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth2 m_auth;
    QQuickItem *m_parentItem;
    QJsonObject m_profileInfo;
    QJsonArray m_groups;
    QString m_groupId;
};

} // namespace

VkUploader::VkUploader(const UploaderProfile &profile,
                       QObject *parent):
    AbstractUploader(profile, parent),
    m_parentItem(0)
{
    m_auth.setClientId("6120893");
    m_auth.setScopes({ "8196" });
    m_auth.setAuthorizationUrl(QUrl("https://oauth.vk.com/authorize"));
    m_auth.setResponseType("token");
    m_auth.setCallbackUrl("https://oauth.vk.com/blank.html");
    m_auth.setUserAgent("Photokinesis/1.0");
    m_auth.setAuthorizationTransmission(Authentication::OAuth2::ParametersInQuery);

    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));

    QObject::connect(this, &VkUploader::groupIdChanged,
                     this, &VkUploader::getAlbums);
}

void VkUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    UiHelpers::createWebview(parent, &m_auth);
    m_auth.process();
}

void VkUploader::logout()
{
    m_auth.logout();

    QJsonArray cookies {
        QJsonArray { "remixlang", ".vk.com", "/" },
        QJsonArray { "remixlhk", ".vk.com", "/" },
        QJsonArray { "h", ".login.vk.com", "/" },
        QJsonArray { "s", ".login.vk.com", "/" },
        QJsonArray { "l", ".login.vk.com", "/" },
        QJsonArray { "p", ".login.vk.com", "/" },
        QJsonArray { "remixsid", ".vk.com", "/" },
    };
    QObject *deleter =
        UiHelpers::clearCookies(this, QUrl("https://www.vk.com"),
                                cookies);
    QObject::connect(deleter, &QObject::destroyed,
                     this, [this]() { AbstractUploader::logout(); });
}

QJsonObject VkUploader::findAlbum(const QString &id) const
{
    QJsonArray albums = this->albums();
    for (const QJsonValue &v: albums) {
        QJsonObject o = v.toObject();
        if (o.value("albumId").toString() == id) {
            return o;
        }
    }
    return QJsonObject();
}

QString VkUploader::ownerId() const {
    return m_groupId.isEmpty() ? userId() : ("-" + m_groupId);
}

void VkUploader::startUpload(const QString &filePath,
                             const QJsonObject &options)
{
    setStatus(AbstractUploader::Busy);

    QVariantMap params;
    QString getServerUrl(QStringLiteral("photos.getWallUploadServer"));

    QString albumId = defaultAlbumId();
    if (!albumId.isEmpty()) {
        params.insert("album_id", albumId);
        getServerUrl = QStringLiteral("photos.getUploadServer");
    }
    if (!m_groupId.isEmpty()) {
        params.insert("group_id", m_groupId);
    }
    VkReply *reply = call(getServerUrl, params);
    QObject::connect(reply, &VkReply::finished,
                     this, [this, reply, filePath, options]() {
        QJsonObject response = reply->response();
        uploadPhoto(QUrl(response.value("upload_url").toString()),
                    filePath, options);
    });
}

VkReply *VkUploader::call(const QString &method, const QVariantMap &params)
{
    QUrl url(baseUrl + method);
    QVariantMap versionedParams(params);
    if (!params.contains("v")) {
        versionedParams.insert("v", defaultVersion);
    }
    return new VkReply(m_auth.get(url, versionedParams));
}

VkReply *VkUploader::getProfileInfo()
{
    VkReply *reply = call(QStringLiteral("account.getProfileInfo"));
    QObject::connect(reply, &VkReply::finished, this, [this, reply]() {
        m_profileInfo = reply->response();
    });
    return reply;
}

VkReply *VkUploader::getAlbums()
{
    QVariantMap params;
    if (!m_groupId.isEmpty()) {
        params.insert("owner_id", "-" + m_groupId);
    }
    VkReply *reply = call(QStringLiteral("photos.getAlbums"), params);
    QObject::connect(reply, &VkReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums =
            reply->response().value("items").toArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            if (s.contains("can_upload") &&
                s["can_upload"].toInt() == 0) continue;
            QJsonObject a;
            a.insert("name", s["title"]);
            a.insert("albumId", s["id"].toVariant().toString());
            a.insert("count", s["size"]);
            albums.append(a);
        }

        setAlbums(albums);

        if (!defaultAlbumId().isEmpty()) {
            // check whether it's valid
            if (findAlbum(defaultAlbumId()).isEmpty()) {
                setDefaultAlbumId(QString());
            }
        }
    });
    return reply;
}

VkReply *VkUploader::getGroups()
{
    QVariantMap params {
        { "extended", "1" },
        { "fields", "can_post" },
        { "filter", "publics,moder" },
    };
    VkReply *reply = call(QStringLiteral("groups.get"), params);
    QObject::connect(reply, &VkReply::finished, this, [this, reply]() {
        const QJsonArray netGroups =
            reply->response().value("items").toArray();
        QJsonArray groups;
        for (const QJsonValue &v: netGroups) {
            const QJsonObject s = v.toObject();
            if (s["can_post"].toInt() == 0) continue;
            groups.append(s);
        }

        setProperty("groups", groups);
    });
    return reply;
}

QNetworkReply *VkUploader::uploadPhoto(const QUrl &uploadUrl,
                                       const QString &filePath,
                                       const QJsonObject &options)
{
    QString title = options["title"].toString();
    QString description = options["description"].toString();
    QStringList tags;
    QJsonArray tagArray = options["tags"].toArray();
    for (const QJsonValue &v: tagArray) {
        tags.append('#' + v.toString());
    }

    /* The only metadata really supported by VK is a caption. It can be up to
     * 2048 characters long (making it effectively a description), and tags are
     * highlighted. Therefore, we can safely merge all our metadata into this
     * field.
     */
    QString caption;
    if (!title.isEmpty()) {
        caption = title + "\n\n";
    }
    if (!description.isEmpty()) {
        caption += description + "\n\n";
    }
    if (!tags.isEmpty()) {
        caption += tags.join(' ');
    }

    QFileInfo fileInfo(filePath);

    QVariantMap params {
        { "file1", QVariantMap {
                { "filePath", fileInfo.filePath() },
                { "fileName", fileInfo.fileName() },
            }
        },
    };
    QNetworkReply *reply = m_auth.post(uploadUrl, params);
    QObject::connect(reply, &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &QNetworkReply::finished, this, [=]() {
        QByteArray replyText = reply->readAll();
        QJsonObject json = QJsonDocument::fromJson(replyText).object();
        QVariantMap params = json.toVariantMap();
        params.insert("caption", caption);
        if (!m_groupId.isEmpty()) {
            params.remove("user_id");
            params.insert("group_id", m_groupId);
        }
        QString saveUrl(QStringLiteral("photos.saveWallPhoto"));
        bool mustPostToWall = true;
        if (json.contains("aid")) {
            params.insert("album_id", json.value("aid").toInt());
            saveUrl = QStringLiteral("photos.save");
            mustPostToWall = false;
        }
        VkReply *reply = call(saveUrl, params);
        QObject::connect(reply, &VkReply::finished, this,
                         [this, reply, mustPostToWall]() {
            if (checkUploadError(reply)) return;

            if (mustPostToWall) {
                const QJsonObject netPhoto =
                    reply->responseArray().first().toObject();
                postToWall(netPhoto.value("owner_id").toInt(),
                           netPhoto.value("id").toInt());
            } else {
                setUploadsUrl(QUrl("https://vk.com/album" + ownerId()
                                   + "_" + defaultAlbumId()));
                Q_EMIT done(true);
                setStatus(AbstractUploader::Ready);
            }
        });
    });
    return reply;
}

VkReply *VkUploader::postToWall(int ownerId, int photoId)
{
    QString attachments = QString("photo%1_%2").arg(ownerId).arg(photoId);
    QVariantMap params {
        { "owner_id", this->ownerId() },
        { "from_group", m_groupId.isEmpty() ? false : true },
        { "attachments", attachments },
    };
    VkReply *reply = call("wall.post", params);
    QObject::connect(reply, &VkReply::finished, this, [this, reply]() {
        if (checkUploadError(reply)) return;

        setUploadsUrl(QUrl("https://vk.com/wall" + this->ownerId()));
        Q_EMIT done(true);
        setStatus(AbstractUploader::Ready);
    });
    return reply;
}

bool VkUploader::checkUploadError(VkReply *reply)
{
    if (reply->isError()) {
        setErrorMessage(reply->errorMessage());
        Q_EMIT done(false);
        setStatus(AbstractUploader::Ready);
        return true;
    } else {
        return false;
    }
}

void VkUploader::initialize()
{
    loadSettings();

    setStatus(AbstractUploader::RetrievingAccountInfo);

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                    this, [this, waiter]() {
        QString firstName = m_profileInfo.value("first_name").toString();
        QString lastName = m_profileInfo.value("last_name").toString();
        setUserName(firstName + " " + lastName);

        setAccountInfo(tr("%n album(s)", "", albums().count()));
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
        waiter->deleteLater();
    });

    waiter->add(getAlbums());
    waiter->add(getGroups());
    waiter->add(getProfileInfo());
}

void VkUploader::setSettings(const QJsonObject &settings)
{
    AbstractUploader::setSettings(settings);
    m_groupId = settings.value("groupId").toString();
    Q_EMIT groupIdChanged();
}

QJsonObject VkUploader::getSettings() const
{
    QJsonObject settings = AbstractUploader::getSettings();
    settings.insert("groupId", m_groupId);
    return settings;
}

void VkUploader::onAuthenticationFinished()
{
    if (!m_auth.hasError()) {
        setUserId(m_auth.extraFields().value("user_id").toString());
        initialize();
    } else {
        qWarning() << "Authentication error:" << m_auth.errorMessage();
        setErrorMessage(m_auth.errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

void VkUploader::createAlbum(const QJsonObject &creationData)
{
    QVariantMap params = creationData.toVariantMap();
    if (!m_groupId.isEmpty()) {
        params.insert("group_id", m_groupId);
    }
    VkReply *reply = call("photos.createAlbum?v=5.131", params);
    QObject::connect(reply, &VkReply::finished,
                     this, [this, reply]() {
        const QJsonObject s = reply->response();
        QJsonArray albums = this->albums();
        QJsonObject a;
        a.insert("name", s["title"]);
        a.insert("albumId", s["id"].toVariant().toString());
        a.insert("count", 0);
        albums.append(a);

        setDefaultAlbumId(a.value("albumId").toString());
        setAlbums(albums);
    });
}

Photokinesis::AbstractUploader *
VkPlugin::createUploader(const UploaderProfile &profile,
                         QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new VkUploader(profile, parent);
}

#include "uploader.moc"
