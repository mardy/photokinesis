import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var creationData: null

    title: qsTr("Create new album")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    GridLayout {
        id: internal
        columns: 2

        property var privacyModel: [
            { "name": qsTr("Nobody"), "value": "nobody" },
            { "name": qsTr("Everybody"), "value": "all" },
            { "name": qsTr("Friends only"), "value": "friends" },
            { "name": qsTr("Friends and friends of friends"), "value": "friends_of_friends" }
        ]

        Label {
            text: qsTr("Album name:")
        }

        TextField {
            id: titleField
            Layout.fillWidth: true
        }

        Label {
            text: qsTr("Description:")
        }

        TextArea {
            id: descriptionField
            Layout.fillWidth: true
        }

        Label {
            text: qsTr("View permissions:")
        }

        ComboBox {
            id: viewPrivacyField
            Layout.fillWidth: true
            model: internal.privacyModel
            textRole: "name"
        }

        Label {
            text: qsTr("Comment permissions:")
        }

        ComboBox {
            id: commentPrivacyField
            Layout.fillWidth: true
            model: internal.privacyModel
            textRole: "name"
        }
    }

    onAccepted: {
        // TODO: validate!
        var data = {
            "title": titleField.text,
            "description": descriptionField.text,
            "privacy_view": getPrivacy(viewPrivacyField),
            "privacy_comment": getPrivacy(commentPrivacyField)
        }
        root.creationData = data
    }

    function getPrivacy(field) {
        return field.model[field.currentIndex].value
    }
}
