/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/OAuth1>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QDebug>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNamedNodeMap>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>
#include <algorithm>

using namespace Photokinesis;

// https://bugreports.qt.io/browse/QTBUG-44944
inline void swap(QJsonValueRef v1, QJsonValueRef v2)
{
    QJsonValue temp(v1);
    v1 = QJsonValue(v2);
    v2 = temp;
}

namespace Photokinesis {

static const QString baseUrl = QStringLiteral("https://api.flickr.com/services/rest/?"
                                              "format=json&nojsoncallback=1");

class FlickrReply: public QObject
{
    Q_OBJECT

public:
    FlickrReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            qDebug() << "raw reply:" << replyText;
            QString contentType =
                m_reply->header(QNetworkRequest::ContentTypeHeader).toString();
            int statusCode =
                m_reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).
                toInt();

            if (contentType.startsWith("text/xml")) {
                m_json = xmlToJson(replyText);
            } else if (contentType.startsWith("text/html")) {
                /* synthetize an error */
                QString errorMessage =
                    QString("Got status code %1").arg(statusCode);
                m_json = QJsonObject {
                    { "stat", "error" },
                    { "errorMessage", errorMessage },
                };
            } else { // assume JSON
                m_json = QJsonDocument::fromJson(replyText).object();
            }
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &FlickrReply::finished);
    }

    QJsonObject xmlToJson(QDomElement &elem) {
        QJsonObject ret;
        const auto attrs = elem.attributes();
        for (int i = 0; i < attrs.length(); i++) {
            const QDomNode n = attrs.item(i);
            ret.insert(n.nodeName(), n.nodeValue());
        }
        for (QDomElement e = elem.firstChildElement();
             !e.isNull(); e = elem.nextSiblingElement()) {
            if (e.hasAttributes() || !e.firstChildElement().isNull()) {
                ret.insert(e.nodeName(), xmlToJson(e));
            } else {
                ret.insert(e.nodeName(), e.text());
            }
        }
        return ret;
    }

    QJsonObject xmlToJson(const QByteArray &data) {
        QDomDocument doc;
        doc.setContent(data);
        QDomElement root = doc.documentElement();
        return xmlToJson(root);
    }

    QJsonObject response() const { return m_json; }

    bool isError() const { return m_json["stat"].toString() != "ok"; }
    QString errorMessage() const {
        return m_json.value("message").toString();
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QScopedPointer<QNetworkReply> m_reply;
};

class FlickrUploader: public AbstractUploader
{
    Q_OBJECT
    Q_PROPERTY(bool isPublic MEMBER m_public NOTIFY isPublicChanged)
    Q_PROPERTY(bool isFamily MEMBER m_family NOTIFY isFamilyChanged)
    Q_PROPERTY(bool isFriends MEMBER m_friends NOTIFY isFriendsChanged)

public:
    FlickrUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;
    void setSettings(const QJsonObject &settings) override;
    QJsonObject getSettings() const override;

    void publish(const QString &itemId);
    FlickrReply *getProfileInfo();
    FlickrReply *getAlbums();

    void initialize();

    FlickrReply *call(const QVariantMap &params = QVariantMap());
    FlickrReply *post(const QVariantMap &params = QVariantMap());
    void setFailed(const QString &errorMessage);

    Q_INVOKABLE void createAlbum(const QJsonObject &creationData);

Q_SIGNALS:
    void isPublicChanged();
    void isFamilyChanged();
    void isFriendsChanged();

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth1 m_auth;
    QQuickItem *m_parentItem;
    QJsonObject m_profileInfo;
    QVariantMap m_destinationAlbum;
    bool m_public = false;
    bool m_family = false;
    bool m_friends = false;
};

} // namespace

FlickrUploader::FlickrUploader(const UploaderProfile &profile,
                               QObject *parent):
    AbstractUploader(profile, parent),
    m_parentItem(0)
{
    m_auth.setClientId("75ec98c409f0e991b9855c60791928ef");
    m_auth.setClientSecret("bea9b389c5782ce6");
    m_auth.setTemporaryCredentialsUrl(QUrl("https://secure.flickr.com/services/oauth/request_token"));
    m_auth.setAuthorizationUrl(QUrl("https://secure.flickr.com/services/oauth/authorize"
                                    "?perms=write"));
    m_auth.setTokenRequestUrl(QUrl("https://secure.flickr.com/services/oauth/access_token"));
    m_auth.setCallbackUrl("https://www.mardy.it/oauth/teleport");
    m_auth.setSignatureMethod("HMAC-SHA1");
    m_auth.setUserAgent("Photokinesis/1.0");
    m_auth.setAuthorizationTransmission(Authentication::OAuth1::ParametersInQuery);

    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));
}

void FlickrUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    UiHelpers::createWebview(parent, &m_auth);
    m_auth.process();
}

void FlickrUploader::logout()
{
    m_auth.logout();

    /* Unfortunately this doesn't work: the cookie_* cookies (and others) seem
     * to be getting overwritten.
     * See also https://bugreports.qt.io/browse/QTBUG-63158
     */
    QJsonArray cookies {
        QJsonArray { "flrbp", ".flickr.com", "/" },
        QJsonArray { "flrbs", ".flickr.com", "/" },
        QJsonArray { "flrbgrp", ".flickr.com", "/" },
        QJsonArray { "flrbgdrp", ".flickr.com", "/" },
        QJsonArray { "flrbgmrp", ".flickr.com", "/" },
        QJsonArray { "flrbcr", ".flickr.com", "/" },
        QJsonArray { "flrbrst", ".flickr.com", "/" },
        QJsonArray { "flrtags", ".flickr.com", "/" },
        QJsonArray { "flrbfd", ".flickr.com", "/" },
        QJsonArray { "flrbrp", ".flickr.com", "/" },
        QJsonArray { "flrb", ".flickr.com", "/" },
        QJsonArray { "liqpw", "secure.flickr.com", "/" },
        QJsonArray { "liqph", "secure.flickr.com", "/" },
        QJsonArray { "BX", ".flickr.com", "/" },
        QJsonArray { "xb", ".flickr.com", "/" },
        QJsonArray { "ffs", ".flickr.com", "/" },
        QJsonArray { "cookie_session", ".flickr.com", "/" },
        QJsonArray { "cookie_accid", ".flickr.com", "/" },
        QJsonArray { "cookie_epass", ".flickr.com", "/" },
        QJsonArray { "sa", ".flickr.com", "/" },
        QJsonArray { "localization", ".flickr.com", "/" },
        QJsonArray { "liqpw", "www.flickr.com", "/" },
        QJsonArray { "liqph", "www.flickr.com", "/" },
    };
    QObject *deleter =
        UiHelpers::clearCookies(this, QUrl("https://secure.flickr.com/"),
                                cookies);
    QObject::connect(deleter, &QObject::destroyed,
                     this, [this]() { AbstractUploader::logout(); });
}

void FlickrUploader::startUpload(const QString &filePath,
                                 const QJsonObject &options)
{
    QString title = options["title"].toString();
    QJsonArray tagArray = options["tags"].toArray();
    QStringList escapedTags;
    for (const QJsonValue &v: tagArray) {
        QString escaped(QUrl::toPercentEncoding(v.toString()));
        escaped.replace("\"", "\\\"");
        escapedTags.append(QString("\"%1\"").arg(escaped));
    }

    QFileInfo fileInfo(filePath);

    QUrl uploadUrl("https://up.flickr.com/services/upload/");
    QVariantMap params {
        { "photo", QVariantMap {
                { "filePath", fileInfo.filePath() },
                { "fileName", fileInfo.fileName() },
            }
        },
        { "is_public", m_public ? "1" : "0" },
        { "is_friend", m_friends ? "1" : "0" },
        { "is_family", m_family ? "1" : "0" },
        { "title", title },
        { "description", options["description"].toString() },
        { "tags", escapedTags.join(" ") },
    };
    m_auth.setAuthorizationTransmission(Authentication::OAuth1::ParametersInBody);
    QNetworkReply *networkReply = m_auth.post(uploadUrl, params);

    FlickrReply *reply = new FlickrReply(networkReply);
    QObject::connect(networkReply, &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &FlickrReply::finished,
                     this, [this, reply]() {
        if (Q_UNLIKELY(reply->isError())) {
            setFailed(reply->errorMessage());
        } else {
            setUploadsUrl(QUrl("https://www.flickr.com/photos/organize"));
            QString itemId(reply->response().value("photoid").toString());
            publish(itemId);
        }
    });
}

void FlickrUploader::publish(const QString &itemId)
{
    //m_auth.setAuthorizationTransmission(Authentication::OAuth1::ParametersInQuery);
    if (!m_destinationAlbum.isEmpty()) {
        // create album, set this photo as primary
        QVariantMap params = m_destinationAlbum;
        QString title = m_destinationAlbum["title"].toString();
        params.insert("method", "flickr.photosets.create");
        params.insert("primary_photo_id", itemId);
        m_destinationAlbum.clear();

        FlickrReply *reply = post(params);
        QObject::connect(reply, &FlickrReply::finished,
                         this, [this, reply, title]() {
            if (reply->isError()) {
                setErrorMessage(reply->errorMessage());
            } else {
                QString id = reply->response()["photoset"].toObject().
                    value("id").toString();
                QJsonArray albums = this->albums();
                for (auto i = albums.begin(); i != albums.end(); i++) {
                    QJsonObject a = i->toObject();
                    if (a["name"].toString() != title) continue;
                    a.insert("albumId", id);
                    *i = a;
                    setDefaultAlbumId(a.value("albumId").toString());
                    break;
                }
                setAlbums(albums);
            }

            Q_EMIT done(!reply->isError());
            setStatus(AbstractUploader::Ready);
        });
    } else if (!defaultAlbumId().isEmpty()) {
        QVariantMap params {
            { "method", "flickr.photosets.addPhoto" },
            { "photoset_id", defaultAlbumId() },
            { "photo_id", itemId },
        };
        FlickrReply *reply = post(params);
        QObject::connect(reply, &FlickrReply::finished,
                         this, [this, reply]() {
            if (reply->isError()) {
                setErrorMessage(reply->errorMessage());
            }
            Q_EMIT done(!reply->isError());
            setStatus(AbstractUploader::Ready);
        });
    } else {
        Q_EMIT done(true);
        setStatus(AbstractUploader::Ready);
    }
}

FlickrReply *FlickrUploader::getProfileInfo()
{
    QVariantMap params {
        { "method", "flickr.test.login" },
    };
    FlickrReply *reply = call(params);
    QObject::connect(reply, &FlickrReply::finished, this, [this, reply]() {
        if (!reply->isError()) {
            QJsonObject response = reply->response();
            m_profileInfo = response["user"].toObject();
        } else {
            qWarning() << Q_FUNC_INFO << reply->errorMessage();
        }
    });
    return reply;
}

FlickrReply *FlickrUploader::getAlbums()
{
    QVariantMap params {
        { "method", "flickr.photosets.getList" },
        { "api_key", m_auth.clientId() },
    };
    FlickrReply *reply = call(params);
    QObject::connect(reply, &FlickrReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums =
            reply->response()["photosets"].toObject()["photoset"].toArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            QJsonObject a;
            a.insert("name", s["title"].toObject()["_content"].toString());
            a.insert("albumId", s["id"].toString());
            albums.append(a);
        }

        std::sort(albums.begin(), albums.end(), [](const QJsonValue &a,
                                                   const QJsonValue &b) {
            return a.toObject().value("name").toString() <
                b.toObject().value("name").toString();
        });

        setAlbums(albums);
    });
    return reply;
}

FlickrReply *FlickrUploader::call(const QVariantMap &params)
{
    return new FlickrReply(m_auth.get(baseUrl, params));
}

FlickrReply *FlickrUploader::post(const QVariantMap &params)
{
    return new FlickrReply(m_auth.post(baseUrl, params));
}

void FlickrUploader::setFailed(const QString &errorMessage)
{
    setErrorMessage(errorMessage);
    Q_EMIT done(false);
    setStatus(AbstractUploader::Ready);
}

void FlickrUploader::initialize()
{
    loadSettings();

    setStatus(AbstractUploader::RetrievingAccountInfo);

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                     this, [this, waiter]() {
        setUserId(m_profileInfo["username"].toObject()["_content"].toString());
        setAccountInfo(tr("Photostream: <a href=\"https://www.flickr.com/photos/%1/\">%1</a>").
                       arg(m_profileInfo["path_alias"].toString()));
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
        waiter->deleteLater();
    });

    waiter->add(getProfileInfo());
    waiter->add(getAlbums());
}

void FlickrUploader::setSettings(const QJsonObject &settings)
{
    AbstractUploader::setSettings(settings);
    m_public = settings.value("isPublic").toBool();
    Q_EMIT isPublicChanged();
    m_friends = settings.value("isFriends").toBool();
    Q_EMIT isFriendsChanged();
    m_family = settings.value("isFamily").toBool();
    Q_EMIT isFamilyChanged();
}

QJsonObject FlickrUploader::getSettings() const
{
    QJsonObject settings = AbstractUploader::getSettings();
    settings.insert("isPublic", m_public);
    settings.insert("isFriends", m_friends);
    settings.insert("isFamily", m_family);
    return settings;
}

void FlickrUploader::onAuthenticationFinished()
{
    if (!m_auth.hasError()) {
        initialize();
    } else {
        qWarning() << "Authentication error:" << m_auth.errorMessage();
        setErrorMessage(m_auth.errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

void FlickrUploader::createAlbum(const QJsonObject &creationData)
{
    m_destinationAlbum = creationData.toVariantMap();

    QJsonArray albums = this->albums();
    QJsonObject a;
    a.insert("name", m_destinationAlbum["title"].toString());
    a.insert("albumId", "-1");
    a.insert("count", 0);
    albums.append(a);

    setDefaultAlbumId(a.value("albumId").toString());
    setAlbums(albums);
}

Photokinesis::AbstractUploader *
FlickrPlugin::createUploader(const UploaderProfile &profile,
                             QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new FlickrUploader(profile, parent);
}

#include "uploader.moc"
