import Photokinesis 1.0
import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var utils: null

    title: qsTr("Select folder")
    standardButtons: Qt.NoButton

    property var _initialPath: []
    property string _newFolderName: ""

    Component.onCompleted: _initialPath = uploader.folder

    ColumnLayout {
        id: internal
        anchors.fill: parent
        enabled: uploader.status != AbstractUploader.Busy

        Label {
            text: qsTr("Current folder:")
        }

        Rectangle {
            Layout.fillWidth: true
            color: "transparent"
            border { color: palette.shadow; width: 1 }
            implicitHeight: dirLabel.implicitHeight + 4
            implicitWidth: dirLabel.implicitWidth
            Label {
                id: dirLabel
                anchors { fill: parent; margins: 2 }
                text: utils.beautifyFolder(uploader.folder)
                horizontalAlignment: Text.AlignHCenter
                elide: Text.ElideLeft
                font { pixelSize: 16; italic: true }
            }
        }

        ScrollView {
            Layout.fillWidth: true
            Layout.fillHeight: true

            Rectangle {
                anchors.fill: parent
                color: palette.base
                border { color: palette.shadow; width: 1 }
            }

            ListView {
                anchors { fill: parent; margins: 2 }
                model: uploader.subfolders
                delegate: Label {
                    property string icon: "\uD83D\uDCC1"
                    width: parent.width
                    text: qsTr("%1 %2").arg(icon).arg(modelData.Name)
                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            var newFolder = uploader.folder
                            newFolder.push(modelData)
                            uploader.folder = newFolder
                        }
                    }
                }
                header: uploader.folder.length > 0 ? headerComponent : null
                footer: _newFolderName.length > 0 ? newFolderComponent : null
            }
        }

        RowLayout {
            Layout.fillWidth: true

            Button {
                enabled: !_newFolderName
                text: qsTr("New folder...")
                onClicked: _newFolderName = qsTr("New folder")
            }

            Item {
                Layout.fillWidth: true
                implicitWidth: 50
            }

            Button {
                text: qsTr("Cancel")
                iconName: "cancel"
                onClicked: {
                    uploader.folder = _initialPath
                    root.close()
                }
            }

            Button {
                text: qsTr("OK")
                iconName: "dialog-ok"
                onClicked: {
                    root.close()
                }
            }
        }
    }

    SystemPalette { id: palette; colorGroup: SystemPalette.Active } 

    Component {
        id: headerComponent
        Label {
            width: parent ? parent.width : 0
            text: qsTr("\u2190 <i>Go back</i>")
            textFormat: Text.StyledText
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    var newFolder = uploader.folder
                    newFolder.pop()
                    uploader.folder = newFolder
                }
            }
        }
    }

    Component {
        id: newFolderComponent

        TextField {
            width: parent ? parent.width : 0
            text: _newFolderName
            onAccepted: { uploader.createFolder(text); _newFolderName = "" }
            Keys.onEscapePressed: _newFolderName = ""
            Component.onCompleted: forceActiveFocus()
        }
    }
}
