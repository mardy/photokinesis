import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

GridLayout {
    id: root

    property var albums: computeAlbums()

    Layout.fillWidth: true
    columns: 2

    Label {
        Layout.topMargin: 8
        Layout.columnSpan: 2
        Layout.fillWidth: true
        text: qsTr("A gallery must be selected!")
        font { bold: true; pointSize: 10 }
        verticalAlignment: Text.AlignBottom
        color: "red"
        visible: !uploader.defaultAlbumId
    }

    Label {
        text: qsTr("Destination gallery:")
    }
    Button {
        Layout.fillWidth: true
        visible: !comboBox.visible
        text: qsTr("Create new gallery...")
        onClicked: root.showAlbumDialog()
    }
    ComboBox {
        id: comboBox
        Layout.fillWidth: true
        model: root.albums
        textRole: "name"
        visible: model.length > 1
        onActivated: {
            console.log("activated")
            var id = root.albums[index].albumId
            if (id) {
                uploader.defaultAlbumId = id
            } else {
                showAlbumDialog()
            }
        }
        onAccepted: console.log("accepted")

        onModelChanged: selectDefault()

        function selectDefault() {
            var defaultAlbumId = uploader.defaultAlbumId
            if (!defaultAlbumId) return

            for (var i = 0; i < model.length; i++) {
                if (model[i].albumId == defaultAlbumId) {
                    currentIndex = i
                    break
                }
            }
        }
    }

    function showAlbumDialog() {
        var item = PopupUtils.open(Qt.resolvedUrl("NewAlbumDialog.qml"), root)
        item.onCreationDataChanged.connect(function () {
            uploader.createAlbum(item.creationData)
        })
    }

    function computeAlbums() {
        var albums = uploader.albums
        albums.push({
            "name": qsTr("Create new gallery…"),
            "albumId": ""
        })
        return albums
    }
}
