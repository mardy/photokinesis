import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import QtQuick.Layouts 1.2

Dialog {
    id: root

    property var creationData: null

    title: qsTr("Create new album")
    standardButtons: StandardButton.Ok | StandardButton.Cancel

    GridLayout {
        id: internal
        columns: 2

        property var privacyModel: [
            { "name": qsTr("Public"), "value": "Public" },
            { "name": qsTr("Unlisted"), "value": "Unlisted" },
            { "name": qsTr("Private"), "value": "Private" },
        ]

        Label {
            text: qsTr("Album name:")
        }

        TextField {
            id: titleField
            Layout.fillWidth: true
            maximumLength: 50
        }

        Label {
            text: qsTr("Description:")
        }

        TextArea {
            id: descriptionField
            Layout.fillWidth: true
        }

        Label {
            text: qsTr("Privacy:")
        }

        ComboBox {
            id: privacyField
            Layout.fillWidth: true
            model: internal.privacyModel
            textRole: "name"
        }
    }

    onAccepted: {
        var data = {
            "Type": "Album",
            "Name": titleField.text,
            "Description": descriptionField.text,
            "Privacy": getPrivacy(privacyField),
        }
        root.creationData = data
    }

    function getPrivacy(field) {
        return field.model[field.currentIndex].value
    }
}
