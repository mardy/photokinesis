import Photokinesis.Ui 1.0
import QtQuick 2.2
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

GridLayout {
    id: root
    Layout.fillWidth: true
    columns: 3

    Label {
        Layout.minimumWidth: implicitWidth
        text: qsTr("Destination folder:")
    }

    Label {
        Layout.fillWidth: true
        text: root.beautifyFolder(uploader.folder)
        elide: Text.ElideLeft
        font { italic: true }
    }

    Button {
        text: qsTr("Change...")
        onClicked: PopupUtils.open(Qt.resolvedUrl("FolderDialog.qml"), root, {
            "utils": root,
        })
    }

    function beautifyFolder(folder) {
        var path = []
        for (var i = 0; i < folder.length; i++) {
            path.push(folder[i].Name)
        }
        return path.join(' / ')
    }
}
