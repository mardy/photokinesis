/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/OAuth1>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QDebug>
#include <QFile>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

using namespace Photokinesis;

namespace Photokinesis {

static const QString hostUrl = QStringLiteral("https://api.smugmug.com");
static const QString baseUrl = QStringLiteral("https://api.smugmug.com/api/v2");
static const QString uploadUrl = QStringLiteral("https://upload.smugmug.com/");

class SmugmugReply: public QObject
{
    Q_OBJECT

public:
    SmugmugReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_statusKey(reply->url().host() == QUrl(uploadUrl).host() ?
                    "stat" : "Message"),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            m_json = QJsonDocument::fromJson(replyText).object();
            if (m_json.isEmpty()) {
                qDebug() << "raw reply:" << replyText;
            } else {
                qDebug().noquote() << "json:" << QJsonDocument::fromJson(replyText).toJson();
            }
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &SmugmugReply::finished);
    }

    QJsonObject response() const {
        return m_json.value("Response").toObject();
    }

    bool isError() const {
        return m_json[m_statusKey].toString().toLower() != "ok";
    }
    QString errorMessage() const {
        return m_json.value(m_statusKey).toString();
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QString m_statusKey;
    QScopedPointer<QNetworkReply> m_reply;
};

class SmugmugUploader: public AbstractUploader
{
    Q_OBJECT
    Q_PROPERTY(QJsonArray folder MEMBER m_folder WRITE setFolder \
               NOTIFY folderChanged)
    Q_PROPERTY(QJsonArray subfolders MEMBER m_subfolders \
               NOTIFY subfoldersChanged)

public:
    SmugmugUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;
    void setSettings(const QJsonObject &settings) override;
    QJsonObject getSettings() const override;

    SmugmugReply *getProfileInfo();
    QJsonObject parseFolder(const QJsonObject &node) const;
    QJsonObject parseAlbum(const QJsonObject &node) const;
    SmugmugReply *getSubnodes();
    SmugmugReply *getRootNode();

    void setFolder(const QJsonArray &folder);
    QString folderId() const;

    QJsonObject findAlbum(const QString &id) const;

    void initialize();

    SmugmugReply *call(const QString &method,
                       const QVariantMap &params = QVariantMap());
    void setFailed(const QString &errorMessage);

    void createNode(const QJsonObject &data);

    Q_INVOKABLE void createAlbum(const QJsonObject &creationData);
    Q_INVOKABLE void createFolder(const QString &name);

Q_SIGNALS:
    void folderChanged();
    void subfoldersChanged();

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth1 m_auth;
    QJsonObject m_profileInfo;
    QJsonArray m_folder; // as array of components, starting with root
    QJsonArray m_subfolders;
};

} // namespace

SmugmugUploader::SmugmugUploader(const UploaderProfile &profile,
                                 QObject *parent):
    AbstractUploader(profile, parent)
{
    m_auth.setClientId("nW7TJVJ2M9pbxD2dmNWh3CPLRqfFxq8F");
    m_auth.setClientSecret("jZJvFT8CGCsMgkTdNMWSPGTc8gQzmZTpcrvPfz3gcZ2S46SctdgJhmcPmhHQCNRH");
    m_auth.setTemporaryCredentialsUrl(QUrl("https://api.smugmug.com/services/oauth/1.0a/getRequestToken"));
    m_auth.setAuthorizationUrl(QUrl("https://api.smugmug.com/services/oauth/1.0a/authorize"
                                    "?Access=Full&Permissions=Modify"));
    m_auth.setTokenRequestUrl(QUrl("https://api.smugmug.com/services/oauth/1.0a/getAccessToken"));
    m_auth.setCallbackUrl("https://www.mardy.it/oauth/teleport");
    m_auth.setSignatureMethod("HMAC-SHA1");
    m_auth.setUserAgent("Photokinesis/1.0");
    m_auth.setAuthorizationTransmission(Authentication::OAuth1::ParametersInQuery);

    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));
    QObject::connect(this, &AbstractUploader::albumsChanged,
                     [this]() {
        if (findAlbum(defaultAlbumId()).isEmpty()) {
            qDebug() << "clearing default album";
            setDefaultAlbumId(QString());
        }
    });
    QObject::connect(this, &AbstractUploader::defaultAlbumIdChanged,
                     [this]() {
        setErrorMessage(tr("A gallery must be selected!"));
        setNeedsConfiguration(defaultAlbumId().isEmpty());
    });
}

void SmugmugUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    UiHelpers::createWebview(parent, &m_auth);
    m_auth.process();
}

void SmugmugUploader::logout()
{
    m_auth.logout();

    /* Unfortunately this doesn't work: the cookie_* cookies (and others) seem
     * to be getting overwritten.
     * See also https://bugreports.qt.io/browse/QTBUG-63158
     */
    QJsonArray cookies {
        QJsonArray { "Sreferrer", ".smugmug.com", "/" },
        QJsonArray { "sp", ".smugmug.com", "/" },
        QJsonArray { "rememberKeepMeLoggedIn", ".smugmug.com", "/" },
        QJsonArray { "shy", ".smugmug.com", "/" },
        QJsonArray { "shm", ".smugmug.com", "/" },
        QJsonArray { "SMSESS", ".smugmug.com", "/" },
        QJsonArray { "od", ".smugmug.com", "/" },
        QJsonArray { "_sp_id", ".smugmug.com", "/" },
        QJsonArray { "_sp_ses", ".smugmug.com", "/" },
        QJsonArray { "sstrack", ".smugmug.com", "/" },
    };
    QObject *deleter =
        UiHelpers::clearCookies(this, QUrl("https://api.smugmug.com/"),
                                cookies);
    QObject::connect(deleter, &QObject::destroyed,
                     this, [this]() { AbstractUploader::logout(); });
}

void SmugmugUploader::startUpload(const QString &filePath,
                                  const QJsonObject &options)
{
    QFile *file = new QFile(filePath, this);
    if (Q_UNLIKELY(!file->open(QIODevice::ReadOnly))) {
        qDebug() << "Image file not found" << filePath;
        Q_EMIT done(false);
        setStatus(AbstractUploader::Ready);
        delete file;
        return;
    }

    QString title = options["title"].toString();
    QString description =
        options["description"].toString().replace('\n', "<br/>");
    QJsonArray tagArray = options["tags"].toArray();
    QByteArray tagHeader;
    QByteArrayList tags;
    for (const QJsonValue &v: tagArray) {
        QString escaped =
            QString("\"%1\"").arg(v.toString().replace("\"", "\\\""));
        tags.append(escaped.toUtf8());
    }
    tagHeader = tags.join("; ");

    QFileInfo fileInfo(filePath);

    QUrl url(uploadUrl);
    const QJsonObject defaultAlbum = findAlbum(defaultAlbumId());
    QByteArray albumUri(defaultAlbum["uploadUri"].toString().toUtf8());

    QVariantMap params;
    m_auth.setAuthorizationTransmission(Authentication::OAuth1::ParametersInHeader);
    QNetworkRequest req = m_auth.authorizedRequest("POST", url, params);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "image/jpeg");
    req.setRawHeader("X-Smug-AlbumUri", albumUri);
    req.setRawHeader("X-Smug-ResponseType", "JSON");
    req.setRawHeader("X-Smug-Version", "v2");
    req.setRawHeader("X-Smug-FileName", fileInfo.fileName().toUtf8());
    req.setRawHeader("X-Smug-Title", title.toUtf8());
    req.setRawHeader("X-Smug-Caption", description.toUtf8());
    req.setRawHeader("X-Smug-Keywords", tagHeader);

    QNetworkReply *networkReply =
        m_auth.networkAccessManager()->post(req, file);
    file->setParent(networkReply);

    QString albumWebUrl(defaultAlbum["url"].toString());

    SmugmugReply *reply = new SmugmugReply(networkReply);
    QObject::connect(networkReply, &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &SmugmugReply::finished,
                     this, [this, reply, albumWebUrl]() {
        if (Q_UNLIKELY(reply->isError())) {
            setFailed(reply->errorMessage());
        } else {
            setUploadsUrl(QUrl(albumWebUrl));
            Q_EMIT done(true);
            setStatus(AbstractUploader::Ready);
        }
    });
}

SmugmugReply *SmugmugUploader::getProfileInfo()
{
    QVariantMap params {
        { "headers", QVariantMap { { "Accept", "application/json" } } },
    };
    SmugmugReply *reply = call("!authuser", params);
    QObject::connect(reply, &SmugmugReply::finished, this, [this, reply]() {
        if (!reply->isError()) {
            m_profileInfo = reply->response().value("User").toObject();
        } else {
            qWarning() << Q_FUNC_INFO << reply->errorMessage();
        }
    });
    return reply;
}

QJsonObject SmugmugUploader::parseFolder(const QJsonObject &node) const
{
    static const QStringList wantedFields {
        "NodeID", "Name", "HasChildren", "IsRoot",
    };

    QJsonObject ret;
    for (const QString &field: wantedFields) {
        ret.insert(field, node[field]);
    }
    return ret;
}

QJsonObject SmugmugUploader::parseAlbum(const QJsonObject &node) const
{
    return QJsonObject {
        { "name", node["Name"] },
        { "albumId", node["NodeID"] },
        { "uploadUri", node["Uris"].toObject()["Album"].toObject()["Uri"] },
        { "url", node["WebUri"] },
    };
}

SmugmugReply *SmugmugUploader::getSubnodes()
{
    if (Q_UNLIKELY(m_folder.isEmpty())) {
        qWarning() << "No folder selected!";
        return nullptr;
    }

    setStatus(AbstractUploader::Busy);

    QString parentId = folderId();

    QVariantMap params {
        { "headers", QVariantMap { { "Accept", "application/json" } } },
    };
    SmugmugReply *reply = call("node/" + parentId + "!children", params);
    QObject::connect(reply, &SmugmugReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums = reply->response()["Node"].toArray();
        m_subfolders = QJsonArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            const QString type = s["Type"].toString();
            if (type == "Album") {
                albums.append(parseAlbum(s));
            } else if (type == "Folder") {
                m_subfolders.append(parseFolder(s));
            }
        }

        if (defaultAlbumId().isEmpty() && !albums.isEmpty()) {
            setDefaultAlbumId(albums.first().toObject().value("albumId").toString());
        }
        setAlbums(albums);
        Q_EMIT subfoldersChanged();
        setStatus(AbstractUploader::Ready);
    });
    return reply;
}

SmugmugReply *SmugmugUploader::getRootNode()
{
    QVariantMap params {
        { "headers", QVariantMap { { "Accept", "application/json" } } },
    };

    QString rootNodeUri =
        m_profileInfo["Uris"].toObject()["Node"].toObject()["Uri"].toString();
    SmugmugReply *reply =
        new SmugmugReply(m_auth.get(hostUrl + rootNodeUri, params));
    QObject::connect(reply, &SmugmugReply::finished, this, [this, reply]() {
        m_folder = QJsonArray();
        QJsonObject o = parseFolder(reply->response()["Node"].toObject());
        o.insert("Name", tr("Main folder"));
        m_folder.append(o);
        Q_EMIT folderChanged();
        setStatus(AbstractUploader::Ready);
        getSubnodes();
    });
    return reply;
}

void SmugmugUploader::setFolder(const QJsonArray &folder)
{
    if (m_folder == folder) return;

    m_folder = folder;
    Q_EMIT folderChanged();

    qDebug() << "folder is now" << m_folder;
    m_subfolders = QJsonArray();
    Q_EMIT subfoldersChanged();

    getSubnodes();
}

QString SmugmugUploader::folderId() const
{
    return m_folder.last().toObject()["NodeID"].toString();
}

QJsonObject SmugmugUploader::findAlbum(const QString &id) const
{
    QJsonArray albums = this->albums();
    for (const QJsonValue &v: albums) {
        QJsonObject o = v.toObject();
        if (o.value("albumId").toString() == id) {
            return o;
        }
    }
    return QJsonObject();
}

SmugmugReply *SmugmugUploader::call(const QString &method,
                                    const QVariantMap &params)
{
    return new SmugmugReply(m_auth.get(baseUrl + method, params));
}

void SmugmugUploader::setFailed(const QString &errorMessage)
{
    setErrorMessage(errorMessage);
    Q_EMIT done(false);
    setStatus(AbstractUploader::Ready);
}

void SmugmugUploader::initialize()
{
    loadSettings();

    setStatus(AbstractUploader::RetrievingAccountInfo);

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                     this, [this, waiter]() {
        setUserId(m_profileInfo["NickName"].toString());
        setUserName(m_profileInfo["Name"].toString());
        Q_EMIT accountInfoChanged();

        if (m_folder.isEmpty()) {
            getRootNode();
        }
        waiter->deleteLater();
    });

    waiter->add(getProfileInfo());
}

void SmugmugUploader::setSettings(const QJsonObject &settings)
{
    setFolder(settings.value("folder").toArray());
    AbstractUploader::setSettings(settings);
}

QJsonObject SmugmugUploader::getSettings() const
{
    QJsonObject settings = AbstractUploader::getSettings();
    settings.insert("folder", m_folder);
    return settings;
}

void SmugmugUploader::onAuthenticationFinished()
{
    if (!m_auth.hasError()) {
        initialize();
    } else {
        qWarning() << "Authentication error:" << m_auth.errorMessage();
        setErrorMessage(m_auth.errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

void SmugmugUploader::createNode(const QJsonObject &data)
{
    setStatus(AbstractUploader::Busy);

    QUrl url(baseUrl + "/node/" + folderId() + "!children");

    QVariantMap params;
    m_auth.setAuthorizationTransmission(Authentication::OAuth1::ParametersInHeader);
    QNetworkRequest req = m_auth.authorizedRequest("POST", url, params);
    req.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    req.setRawHeader("Accept", "application/json");
    QByteArray body = QJsonDocument(data).toJson(QJsonDocument::Compact);

    QNetworkReply *networkReply =
        m_auth.networkAccessManager()->post(req, body);
    SmugmugReply *reply = new SmugmugReply(networkReply);
    QObject::connect(reply, &SmugmugReply::finished,
                     this, [this, reply]() {
        const QJsonObject s = reply->response()["Node"].toObject();
        const QString type = s["Type"].toString();
        if (type == "Album") {
            QJsonArray albums = this->albums();
            const QJsonObject a = parseAlbum(s);
            albums.append(a);
            setDefaultAlbumId(a["albumId"].toString());
            setAlbums(albums);
        } else if (type == "Folder") {
            m_folder.append(parseFolder(s));
            Q_EMIT folderChanged();
            m_subfolders = QJsonArray();
            Q_EMIT subfoldersChanged();
            setAlbums(QJsonArray());
        }
        setStatus(AbstractUploader::Ready);
    });
}

void SmugmugUploader::createAlbum(const QJsonObject &creationData)
{
    createNode(creationData);
}

void SmugmugUploader::createFolder(const QString &name)
{
    createNode({
        { "Type", "Folder" },
        { "Name", name },
        { "Privacy", "Public" },
    });
}

Photokinesis::AbstractUploader *
SmugmugPlugin::createUploader(const UploaderProfile &profile,
                              QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new SmugmugUploader(profile, parent);
}

#include "uploader.moc"
