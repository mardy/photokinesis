TEMPLATE = subdirs
SUBDIRS = \
    deviantart \
    facebook \
    flickr \
    ftp \
    google \
    imgur \
    smugmug \
    vk

contains(EXTRA_PLUGINS, "instagram") {
    SUBDIRS += instagram
}
