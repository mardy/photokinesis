import QtQuick 2.5
import QtQuick.Controls 1.4
import QtQuick.Layouts 1.2

GridLayout {
    id: root

    property var authenticator: null
    property bool wantsVisibility: true
    property string errorMessage: ""

    signal finished()

    anchors {
        left: parent.left; right: parent.right; margins: 12
        verticalCenter: parent.verticalCenter
    }
    columns: 2

    Label {
        text: qsTr("Username")
    }

    TextField {
        id: usernameField
        Layout.fillWidth: true
        placeholderText: qsTr("Your Instagram username")
    }

    Label {
        text: qsTr("Password")
    }

    TextField {
        id: passwordField
        Layout.fillWidth: true
        echoMode: TextInput.Password
        inputMethodHints: Qt.ImhHiddenText
        onAccepted: root.doLogin()
    }

    Button {
        Layout.columnSpan: 2
        Layout.alignment: Qt.AlignRight
        text: qsTr("Login")
        onClicked: root.doLogin()
    }

    function doLogin() {
        authenticator.setUsername(usernameField.text)
        authenticator.setPassword(passwordField.text)
        authenticator.login()
        root.finished()
    }

    function cancel() {
        root.errorMessage = qsTr("User canceled")
        root.finished()
    }
}
