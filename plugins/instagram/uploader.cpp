/*
 * Copyright (C) 2017-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QDebug>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QQmlComponent>
#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickItem>
#include <QScopedPointer>

#include "QtInstagram"

using namespace Photokinesis;

namespace Photokinesis {

class InstagramUploader: public AbstractUploader
{
    Q_OBJECT

public:
    InstagramUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QUrl &url, const QJsonObject &options) override;

    void setFailed(const QString &errorMessage);

private:
    void createLoginComponent(QQuickItem *parent);
    QJsonObject parseReply(const QVariant &reply);

private:
    Instagram m_instagram;
    QJsonObject m_profileInfo;
    QScopedPointer<QQmlComponent> m_loginComponent;
};

} // namespace

InstagramUploader::InstagramUploader(const UploaderProfile &profile,
                                     QObject *parent):
    AbstractUploader(profile, parent),
    m_loginComponent(0)
{
    QObject::connect(&m_instagram, &Instagram::profileConnected,
                     [this](QVariant answer) {
        QJsonObject profile =
            parseReply(answer).value("logged_in_user").toObject();
        setUserId(profile.value("username").toString());
        setAccountInfo(profile.value("full_name").toString());
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
    });

    QObject::connect(&m_instagram, &Instagram::profileConnectedFail,
                     [this]() {
        qWarning() << "Authentication failed";
        setStatus(AbstractUploader::NeedsAuthentication);
    });

    QObject::connect(&m_instagram, &Instagram::error,
                     [this](QString message) {
        qWarning() << "Got error" << message;
        setErrorMessage(message);
        if (status() == AbstractUploader::Busy) {
            Q_EMIT done(false);
            setStatus(AbstractUploader::Ready);
        }
    });

    QObject::connect(&m_instagram, &Instagram::imageConfigureDataReady,
                     [this](QVariant answer) {
        qDebug() << "Image uploaded" << answer;
        QString status = parseReply(answer).value("status").toString();
        bool ok = (status == "ok");
        if (!ok) {
            setErrorMessage(parseReply(answer).value("message").toString());
        }
        Q_EMIT done(ok);
        setStatus(AbstractUploader::Ready);
    });
}

void InstagramUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    createLoginComponent(parent);
}

void InstagramUploader::logout()
{
    m_instagram.logout();
    AbstractUploader::logout();
}

void InstagramUploader::startUpload(const QUrl &url,
                                    const QJsonObject &options)
{
    QString title = options["title"].toString();
    QString description = options["description"].toString();
    QStringList tags;
    QJsonArray tagArray = options["tags"].toArray();
    for (const QJsonValue &v: tagArray) {
        tags.append('#' + v.toString());
    }

    QString caption;
    if (!title.isEmpty()) {
        caption = title + "\n\n";
    }
    if (!description.isEmpty()) {
        caption += description + "\n\n";
    }
    if (!tags.isEmpty()) {
        caption += tags.join(' ');
    }
    m_instagram.postImage(url.toLocalFile(), caption);
}

void InstagramUploader::setFailed(const QString &errorMessage)
{
    setErrorMessage(errorMessage);
    Q_EMIT done(false);
    setStatus(AbstractUploader::Ready);
}

void InstagramUploader::createLoginComponent(QQuickItem *parent)
{
    Q_ASSERT(parent);
    QQmlContext *context = QQmlEngine::contextForObject(parent);
    QQmlEngine *engine = context->engine();
    if (!m_loginComponent) {
        QUrl url("qrc:/instagram/LoginPanel.qml");
        m_loginComponent.reset(new QQmlComponent(engine, url));
    }

    QObject *object = m_loginComponent->create(context);
    Q_ASSERT(object);
    object->setProperty("authenticator", QVariant::fromValue(&m_instagram));
    QQuickItem *item = qobject_cast<QQuickItem*>(object);
    Q_ASSERT(item);
    item->setParentItem(parent);
    item->setParent(parent);
    QObject::connect(item, &QQuickItem::parentChanged,
                     item, [this, item]() {
        item->deleteLater();
        QString message = item->property("errorMessage").toString();
        if (!message.isEmpty()) {
            setFailed(message);
            setStatus(AbstractUploader::NeedsAuthentication);
        }
    });
}

QJsonObject InstagramUploader::parseReply(const QVariant &reply)
{
    QJsonDocument doc = QJsonDocument::fromJson(reply.toString().toUtf8());
    return doc.object();
}

Photokinesis::AbstractUploader *
InstagramPlugin::createUploader(const UploaderProfile &profile,
                                 QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new InstagramUploader(profile, parent);
}

#include "uploader.moc"
