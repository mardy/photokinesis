TARGET = instagram

include(../plugins.pri)

QT += quick

include(QtInstagram/QtInstagram.pri)

SOURCES += \
    uploader.cpp

HEADERS += \
    uploader.h
