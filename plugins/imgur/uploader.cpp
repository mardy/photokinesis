/*
 * Copyright (C) 2018-2022 Alberto Mardegan <mardy@users.sourceforge.net>
 *
 * This file is part of Photokinesis.
 *
 * Photokinesis is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Photokinesis is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Photokinesis.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "uploader.h"

#include <Authentication/OAuth2>
#include <Photokinesis/AbstractUploader>
#include <Photokinesis/SignalWaiter>
#include <Photokinesis/UiHelpers>
#include <QDebug>
#include <QDir>
#include <QFileInfo>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QNetworkReply>

using namespace Photokinesis;

namespace Photokinesis {

static const QString baseUrl = QStringLiteral("https://api.imgur.com/3/");

class ImgurReply: public QObject
{
    Q_OBJECT

public:
    ImgurReply(QNetworkReply *reply, QObject *parent = 0):
        QObject(parent),
        m_reply(reply)
    {
        QObject::connect(reply, &QNetworkReply::finished,
                         this, [this]() {
            QByteArray replyText = m_reply->readAll();
            qDebug() << "raw reply:" << replyText;
            m_json = QJsonDocument::fromJson(replyText).object();
            deleteLater();
        });
        QObject::connect(reply, &QNetworkReply::finished,
                         this, &ImgurReply::finished);
    }

    QJsonObject response() const { return m_json.value("data").toObject(); }
    QJsonArray responseArray() const { return m_json.value("data").toArray(); }

    bool isError() const { return !m_json.value("success").toBool(); }
    QString errorMessage() const {
        return m_json.value("data").toObject().value("error").toString();
    }

Q_SIGNALS:
    void finished();

private:
    QJsonObject m_json;
    QScopedPointer<QNetworkReply> m_reply;
};

class ImgurUploader: public AbstractUploader
{
    Q_OBJECT

public:
    ImgurUploader(const UploaderProfile &profile, QObject *parent = 0);

    void authenticate(QQuickItem *parent) override;
    void logout() override;
    void startUpload(const QString &filePath,
                     const QJsonObject &options) override;

    void publish(const QString &hash, const QString &title);
    ImgurReply *getProfileInfo();
    ImgurReply *getAlbums();

    void initialize();

    ImgurReply *call(const QString &method,
                     const QVariantMap &params = QVariantMap());
    void setFailed(const QString &errorMessage);

    Q_INVOKABLE void createAlbum(const QJsonObject &creationData);

private Q_SLOTS:
    void onAuthenticationFinished();

private:
    Authentication::OAuth2 m_auth;
    QJsonObject m_profileInfo;
};

} // namespace

ImgurUploader::ImgurUploader(const UploaderProfile &profile,
                             QObject *parent):
    AbstractUploader(profile, parent)
{
    m_auth.setClientId("608192b9722656c");
    m_auth.setClientSecret("ffef1e2942c47fb9180d4593ace249d9a51f1589");
    m_auth.setScopes({});
    m_auth.setAuthorizationUrl(QUrl("https://api.imgur.com/oauth2/authorize"));
    m_auth.setAccessTokenUrl(QUrl("https://api.imgur.com/oauth2/token"));
    m_auth.setResponseType("code");
    m_auth.setCallbackUrl("https://www.mardy.it/oauth/teleport");
    m_auth.setUserAgent("Photokinesis/1.0");

    QObject::connect(&m_auth, SIGNAL(finished()),
                     this, SLOT(onAuthenticationFinished()));
}

void ImgurUploader::authenticate(QQuickItem *parent)
{
    setStatus(AbstractUploader::Authenticating);
    UiHelpers::createWebview(parent, &m_auth);
    m_auth.process();
}

void ImgurUploader::logout()
{
    m_auth.logout();

    QJsonArray cookies {
        QJsonArray { "_gat", ".deviantart.com", "/" },
        QJsonArray { "__qca", ".deviantart.com", "/" },
        QJsonArray { "_pxvid", ".deviantart.com", "/" },
        QJsonArray { "userinfo", ".deviantart.com", "/" },
        QJsonArray { "_ga", ".deviantart.com", "/" },
        QJsonArray { "_gid", ".deviantart.com", "/" },
        QJsonArray { "_px", ".deviantart.com", "/" },
    };
    QObject *deleter =
        UiHelpers::clearCookies(this, QUrl("https://www.deviantart.com"),
                                cookies);
    QObject::connect(deleter, &QObject::destroyed,
                     this, [this]() { AbstractUploader::logout(); });
}

void ImgurUploader::startUpload(const QString &filePath,
                                const QJsonObject &options)
{
    setStatus(AbstractUploader::Busy);

    QString title = options["title"].toString();
    QString description = options["description"].toString();
    QStringList tags;
    QJsonArray tagArray = options["tags"].toArray();
    for (const QJsonValue &v: tagArray) {
        tags.append('#' + v.toString());
    }

    if (!tags.isEmpty()) {
        description += "\n\n";
        description += tags.join(' ');
    }

    QFileInfo fileInfo(filePath);

    QUrl uploadUrl(baseUrl + "image");
    QVariantMap params {
        { "image", QVariantMap {
                { "filePath", fileInfo.filePath() },
                { "fileName", fileInfo.fileName() },
            }
        },
        { "type", "file" },
        { "title", title },
        { "description", description },
    };
    if (!defaultAlbumId().isEmpty()) {
        params.insert("album", defaultAlbumId());
    }

    /* Title is mandatory when publishing, so prepare it here  by setting it to
     * the filename
     */
    if (title.isEmpty()) {
        title = fileInfo.fileName();
    }

    QNetworkReply *networkReply = m_auth.post(uploadUrl, params);
    ImgurReply *reply = new ImgurReply(networkReply);
    QObject::connect(networkReply, &QNetworkReply::uploadProgress,
                     this, [this](qint64 bytesSent, qint64 bytesTotal) {
        if (bytesTotal > 0) {
            Q_EMIT progress(bytesSent * 100 / bytesTotal);
        }
    });
    QObject::connect(reply, &ImgurReply::finished,
                     this, [this, title, reply]() {
        if (Q_UNLIKELY(reply->isError())) {
            setFailed(reply->errorMessage());
        } else {
            setUploadsUrl(QUrl(reply->response().value("link").toString()));
            publish(reply->response().value("id").toString(), title);
        }
    });
}

void ImgurUploader::publish(const QString &hash,
                            const QString &title)
{
    QVariantMap params {
        { "title", title },
        { "terms", 1 },
    };

    QUrl url(baseUrl + "gallery/image/" + hash);
    QNetworkReply *networkReply = m_auth.post(url, params);
    ImgurReply *reply = new ImgurReply(networkReply);
    QObject::connect(reply, &ImgurReply::finished,
                     this, [this, reply]() {
        if (Q_UNLIKELY(reply->isError())) {
            setFailed(reply->errorMessage());
        } else {
            Q_EMIT done(true);
            setStatus(AbstractUploader::Ready);
        }
    });
}

ImgurReply *ImgurUploader::getProfileInfo()
{
    ImgurReply *reply = call(QStringLiteral("account/me"));
    QObject::connect(reply, &ImgurReply::finished, this, [this, reply]() {
        m_profileInfo = reply->response();
    });
    return reply;
}

ImgurReply *ImgurUploader::getAlbums()
{
    ImgurReply *reply = call(QStringLiteral("account/me/albums"));
    QObject::connect(reply, &ImgurReply::finished, this, [this, reply]() {
        const QJsonArray netAlbums = reply->responseArray();
        QJsonArray albums;
        for (const QJsonValue &v: netAlbums) {
            const QJsonObject s = v.toObject();
            // Skip published albums: Cannot change album contents while in gallery
            if (s["in_gallery"].toBool()) continue;
            QJsonObject a;
            a.insert("name", s["title"]);
            a.insert("albumId", s["id"].toString());
            albums.append(a);
        }

        if (defaultAlbumId().isEmpty() && !albums.isEmpty()) {
            setDefaultAlbumId(albums.first().toObject().value("albumId").toString());
        }
        setAlbums(albums);
    });
    return reply;
}

ImgurReply *ImgurUploader::call(const QString &method,
                                const QVariantMap &params)
{
    QUrl url(baseUrl + method);
    return new ImgurReply(m_auth.get(url, params));
}

void ImgurUploader::setFailed(const QString &errorMessage)
{
    setErrorMessage(errorMessage);
    Q_EMIT done(false);
    setStatus(AbstractUploader::Ready);
}

void ImgurUploader::initialize()
{
    loadSettings();

    setStatus(AbstractUploader::RetrievingAccountInfo);

    SignalWaiter *waiter = new SignalWaiter(SIGNAL(finished()), this);
    QObject::connect(waiter, &SignalWaiter::finished,
                     this, [this, waiter]() {
        setUserId(m_profileInfo.value("id").toString());
        setUserName(m_profileInfo.value("url").toString());
        setAccountInfo(tr("Reputation: %1").
                       arg(m_profileInfo.value("reputation_name").toString()));
        Q_EMIT accountInfoChanged();

        setStatus(AbstractUploader::Ready);
        waiter->deleteLater();
    });

    waiter->add(getProfileInfo());
    waiter->add(getAlbums());
}

void ImgurUploader::onAuthenticationFinished()
{
    if (!m_auth.hasError()) {
        initialize();
    } else {
        qWarning() << "Authentication error:" << m_auth.errorMessage();
        setErrorMessage(m_auth.errorMessage());
        setStatus(AbstractUploader::NeedsAuthentication);
    }
}

void ImgurUploader::createAlbum(const QJsonObject &creationData)
{
    QString title = creationData["title"].toString();
    QVariantMap params {
        { "title", title },
        { "description", creationData["description"].toString() },
        { "privacy", creationData["privacy"].toString() },
    };
    QUrl endpoint(baseUrl + "album");
    ImgurReply *reply =
        new ImgurReply(m_auth.post(endpoint, params));
    QObject::connect(reply, &ImgurReply::finished,
                     this, [this, title, reply]() {
        if (Q_UNLIKELY(reply->isError())) {
            qWarning() << "Got error" << reply->errorMessage();
            setErrorMessage(reply->errorMessage());
            return;
        }
        const QJsonObject s = reply->response();
        QJsonArray albums = this->albums();
        QJsonObject a {
            { "name", title },
            { "albumId", s["id"] },
        };
        albums.append(a);

        setDefaultAlbumId(a.value("albumId").toString());
        setAlbums(albums);
    });
}

Photokinesis::AbstractUploader *
ImgurPlugin::createUploader(const UploaderProfile &profile,
                            QObject *parent)
{
    Q_INIT_RESOURCE(resources);
    return new ImgurUploader(profile, parent);
}

#include "uploader.moc"
