PROJECT_VERSION = 0.15

PARTS = $$split(PROJECT_VERSION, .)

APPLICATION_NAME = "it.mardy.Photokinesis"
APPLICATION_DISPLAY_NAME = "PhotoTeleport"

CONFIG += \
    c++11 \
    no_keywords
CONFIG -= debug_and_release_target

# Until we make a mobile version
CONFIG += desktop

INSTALL_PREFIX=/usr

!isEmpty(PREFIX) {
    INSTALL_PREFIX=$${PREFIX}
}

INSTALL_BIN_DIR = $${INSTALL_PREFIX}/bin
contains(INSTALL_PREFIX, "^/usr") {
    INSTALL_DATA_DIR = $${INSTALL_PREFIX}/share/Photokinesis
    INSTALL_ICON_DIR = $${INSTALL_PREFIX}/share/icons/hicolor/scalable/apps
    INSTALL_DESKTOP_DIR = $${INSTALL_PREFIX}/share/applications
} else:CONFIG(desktop) {
    INSTALL_DATA_DIR = $${INSTALL_PREFIX}/share
    INSTALL_ICON_DIR = $${INSTALL_DATA_DIR}
    INSTALL_DESKTOP_DIR = $${INSTALL_DATA_DIR}
} else {
    INSTALL_DATA_DIR = $${INSTALL_PREFIX}
}

PLUGIN_SUBDIR = lib/Photokinesis

macx {
    PLUGIN_DIR = ../plugins
} else:win32 {
    PLUGIN_DIR = Photokinesis
} else {
    PLUGIN_DIR = ../$${PLUGIN_SUBDIR}
}

DATA_DIR = data

include(coverage.pri)
